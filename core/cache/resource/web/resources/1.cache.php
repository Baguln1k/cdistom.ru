<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 1,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Главная',
    'longtitle' => '',
    'description' => '',
    'alias' => 'index',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 0,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 1,
    'menuindex' => 0,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1501047758,
    'editedby' => 1,
    'editedon' => 1503396630,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 0,
    'publishedby' => 0,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'index.html',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'logo' => 
    array (
      0 => 'logo',
      1 => 'assets/template/images/logocdi.png',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'addres' => 
    array (
      0 => 'addres',
      1 => 'Томск, ул. Говорова, д. 19 "В", ст. 1',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'vid' => 
    array (
      0 => 'vid',
      1 => 'assets/template/new3',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'titletop' => 
    array (
      0 => 'titletop',
      1 => 'Центр дентальной <span>имплантации</span>',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'ptit' => 
    array (
      0 => 'ptit',
      1 => 'Стоматологическая клиника Центр Дентальной Имплантации работает с 2010 года и успела заработать доверие десятков тысяч пациентов, жителей Томска, Северска и других регионов. <a href="http://cdistom.ru/about.html" style=" text-decoration: none;color: #fff; border-bottom: 1px solid #aaaaaa; padding-bottom: 2px; ">Подробнее</a>',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'zap' => 
    array (
      0 => 'zap',
      1 => '<a href="contacts.html">Записаться на прием</a>',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'usl' => 
    array (
      0 => 'usl',
      1 => '<a href="#" class="scrollmain">Услуги компании</a>',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'sliderusl' => 
    array (
      0 => 'sliderusl',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/images\\/sliderblock6.png","title":"\\u0418\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f","p":"\\u041d\\u0430\\u0447\\u043d\\u0438\\u0442\\u0435 \\u0441\\u0432\\u043e\\u0439 \\u0434\\u0435\\u043d\\u044c \\u0441 \\u0443\\u043b\\u044b\\u0431\\u043a\\u0438.<br>\\u0410 \\u0443\\u043b\\u044b\\u0431\\u043a\\u0443 \\u0441 \\u0426\\u0435\\u043d\\u0442\\u0440\\u0430 \\u0414\\u0435\\u043d\\u0442\\u0430\\u043b\\u044c\\u043d\\u043e\\u0439 \\u0418\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0446\\u0438\\u0438!","url":"implantation.html"},{"MIGX_id":"2","image":"assets\\/template\\/images\\/images\\/sliderblock2.png","title":"<span>\\u0414\\u0435\\u0442\\u0441\\u043a\\u0430\\u044f<\\/span> \\u0441\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u044f","p":"\\u0417\\u0434\\u043e\\u0440\\u043e\\u0432\\u044b\\u0435 \\u0437\\u0443\\u0431\\u043a\\u0438 \\u2013 \\u0437\\u0434\\u043e\\u0440\\u043e\\u0432\\u044b\\u0435 \\u0434\\u0435\\u0442\\u043a\\u0438!<br\\/> \\u041e\\u0441\\u043e\\u0431\\u044b\\u0439 \\u043f\\u043e\\u0434\\u0445\\u043e\\u0434 \\u043a \\u043c\\u0430\\u043b\\u0435\\u043d\\u044c\\u043a\\u043e\\u043c\\u0443 \\u043f\\u0430\\u0446\\u0438\\u0435\\u043d\\u0442\\u0443.","url":"services.html"},{"MIGX_id":"3","image":"assets\\/template\\/images\\/images\\/sliderblock3.png","title":"<span>\\u041f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f<\\/span> \\u0433\\u0438\\u0433\\u0438\\u0435\\u043d\\u0430","p":"\\u041a\\u0440\\u0430\\u0441\\u0438\\u0432\\u0430\\u044f \\u0435\\u0441\\u0442\\u0435\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u0430\\u044f \\u0443\\u043b\\u044b\\u0431\\u043a\\u0430 \\u2013 \\u044d\\u0442\\u043e \\u043b\\u0435\\u0433\\u043a\\u043e!","url":"hygiene.html"},{"MIGX_id":"4","image":"assets\\/template\\/images\\/images\\/sliderblock4.png","title":"\\u041f\\u0440\\u043e\\u0442\\u0435\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435","p":"\\u0412\\u043e\\u0441\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u0438\\u0442\\u044c \\u0443\\u043b\\u044b\\u0431\\u043a\\u0443 \\u2013 \\u043b\\u0435\\u0433\\u043a\\u043e!","url":"proteth.html"},{"MIGX_id":"5","image":"assets\\/template\\/images\\/images\\/sliderblock5.png","title":"\\u0422\\u0435\\u0440\\u0430\\u043f\\u0435\\u0432\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f <span>\\u0441\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u044f<\\/span>","p":"\\u0421\\u043e\\u0445\\u0440\\u0430\\u043d\\u0438\\u043c \\u0412\\u0430\\u0448\\u0438 \\u0437\\u0443\\u0431\\u044b \\u0437\\u0434\\u043e\\u0440\\u043e\\u0432\\u044b\\u043c\\u0438!","url":"terapia.html"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'title3block' => 
    array (
      0 => 'title3block',
      1 => 'Наши преимущества',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'preem' => 
    array (
      0 => 'preem',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/m3_b1.png","title":"\\u0411\\u0435\\u0441\\u043f\\u043b\\u0430\\u0442\\u043d\\u0430\\u044f \\u043a\\u043e\\u043d\\u0441\\u0443\\u043b\\u044c\\u0442\\u0430\\u0446\\u0438\\u044f \\u0432\\u0440\\u0430\\u0447\\u0430"},{"MIGX_id":"2","image":"assets\\/template\\/images\\/m3_b2.png","title":"\\u0423\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u043b\\u0435\\u043d\\u043e \\u0431\\u043e\\u043b\\u0435\\u0435 10 000 \\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0442\\u043e\\u0432"},{"MIGX_id":"3","image":"assets\\/template\\/images\\/m3_b3.png","title":"\\u0410\\u043d\\u0435\\u0441\\u0442\\u0435\\u0437\\u0438\\u044f \\u0434\\u043b\\u044f \\u0434\\u0435\\u0442\\u0435\\u0439 \\u0438 \\u0432\\u0437\\u0440\\u043e\\u0441\\u043b\\u044b\\u0445"},{"MIGX_id":"4","image":"assets\\/template\\/images\\/m3_b42.png","title":"10 \\u043b\\u0435\\u0442 \\u043d\\u0430 \\u0440\\u044b\\u043d\\u043a\\u0435 \\u0441\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0447\\u0435\\u0441\\u043a\\u0438\\u0445 \\u0443\\u0441\\u043b\\u0443\\u0433 \\u0422\\u043e\\u043c\\u0441\\u043a\\u0430"},{"MIGX_id":"5","image":"assets\\/template\\/images\\/m3_b5.png","title":"\\u041a\\u043e\\u0440\\u043f\\u043e\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0435\\u0434\\u043b\\u043e\\u0436\\u0435\\u043d\\u0438\\u0435, \\u0441\\u0435\\u043c\\u0435\\u0439\\u043d\\u044b\\u0435 \\u0441\\u043a\\u0438\\u0434\\u043a\\u0438"},{"MIGX_id":"6","image":"assets\\/template\\/images\\/m3_b6.png","title":"\\u0421\\u043e\\u0431\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u0430\\u044f \\u0437\\u0443\\u0431\\u043e\\u0442\\u0435\\u0445\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f \\u043b\\u0430\\u0431\\u043e\\u0440\\u0430\\u0442\\u043e\\u0440\\u0438\\u044f"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'tell3' => 
    array (
      0 => 'tell3',
      1 => '25-70-33',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'mailto' => 
    array (
      0 => 'mailto',
      1 => 'cdistom@mail.ru',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'pn-pt' => 
    array (
      0 => 'pn-pt',
      1 => '8:30-20:00',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'sub' => 
    array (
      0 => 'sub',
      1 => '8:30-14:00',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'vih' => 
    array (
      0 => 'vih',
      1 => 'выходной',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'uslugi' => 
    array (
      0 => 'uslugi',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/sm1.png","title":"\\u0418\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f","href":"implantation.html"},{"MIGX_id":"2","image":"assets\\/template\\/images\\/sm4.png","title":"\\u0422\\u0435\\u0440\\u0430\\u043f\\u0438\\u044f","href":"terapia.html"},{"MIGX_id":"3","image":"assets\\/template\\/images\\/sm5.png","title":"\\u041f\\u0440\\u043e\\u0442\\u0435\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435","href":"proteth.html"},{"MIGX_id":"4","image":"assets\\/template\\/images\\/sm6.png","title":"\\u0413\\u0438\\u0433\\u0438\\u0435\\u043d\\u0430 \\u0438 \\u043f\\u0440\\u043e\\u0444\\u0438\\u043b\\u0430\\u043a\\u0442\\u0438\\u043a\\u0430","href":"hygiene.html"},{"MIGX_id":"5","image":"assets\\/template\\/images\\/sm7.png","title":"\\u0414\\u0435\\u0442\\u0441\\u043a\\u0430\\u044f \\u0441\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u044f","href":"services.html"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'titl' => 
    array (
      0 => 'titl',
      1 => '[[*pagetitle]] - [[++site_name]]',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'keyw' => 
    array (
      0 => 'keyw',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'desc' => 
    array (
      0 => 'desc',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'tell' => 
    array (
      0 => 'tell',
      1 => '8 3822',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'tell2' => 
    array (
      0 => 'tell2',
      1 => '25-70-33',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>Главная - Центр дентальной имплантации</title>
        <base href="http://cdistom.ru/" />
        <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->

<script src="assets/template/js/main.js"></script>
</head>
<body>
    <div class="wrapper mainwrapper">
    <header class="header">
			<div class="blockin">
				<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
				<a href="tel:+8 382225-70-33" class="phone_top">
					<p class="address_top">Томск, ул. Говорова, д. 19 "В", ст. 1</p>
					8 3822 <span>25-70-33</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="http://cdistom.ru/" class="">
								<img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>
    <div id="fullpage">
	<div class="section main">
		<div class="fullscreen-bg__video">
			<video id="myVideo" loop muted controls="false" data-autoplay>
				<source src="assets/template/new3.webm" type="video/webm">
					<source src="assets/template/new3.ogg" type="video/ogg">
						<source src="assets/template/new3.mp4" type="video/mp4">
						</video>
					</div>
					<div class="wrapp layer">
						<div class="blockin">
							<div class="mainblockcontent">
								<div class="title">
									<div class="zub"> </div>
								Центр дентальной <span>имплантации</span>
								</div>
								<div class="text" style=" line-height: 26px; font-size: 15px; ">
                                Стоматологическая клиника Центр Дентальной Имплантации работает с 2010 года и успела заработать доверие десятков тысяч пациентов, жителей Томска, Северска и других регионов. <a href="http://cdistom.ru/about.html" style=" text-decoration: none;color: #fff; border-bottom: 1px solid #aaaaaa; padding-bottom: 2px; ">Подробнее</a>
								</div>
								<div class="linkblock">
									<a href="contacts.html">Записаться на прием</a>
									<a href="#" class="scrollmain">Услуги компании</a>
								</div>
							</div>
						</div>
					</div>
					<a href="#scrollTarget" class="scrollDown"><span></span></a>
				</div>
				<!--div class="section mainslider scrollTarget" style="background: #7D92A7;">
					<div class="coutbloc">
						<div class="arrowmain"></div>
						<div class="count"></div>
					</div>
					<div class="owl-carousel owlmain">
					    
					[[!getImageList? &tvname=`sliderusl` &tpl=`sliderusl`]]
						
					</div>
					<div class="scroll scrollmain"></div>
				</div-->
				<div class="section mainslider new-banner" style="background: #7D92A7;">
							<div class="coutbloc">
								<div class="arrowmain"></div>
								<div class="count"></div>
							</div>
							<div class="owl-carousel owlmain">

								<div class="item item1">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Имплантация</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Имплантация — самый прогрессивный метод восстановления улыбки. Имплантаты просты в уходе, 
												максимально естественны и долговечны. Кроме того, восстановить улыбку сегодня можно буквально за 1 день, 
												без разрезов и швов, абсолютно безболезненно и, если хотите, во сне! 
											</div>
											<ul class="new-banner-list">
												<li>Постоянный контроль качества</li>
												<li>Широкий ассортимент имплантантов</li>
												<li>Короткий срок заживления</li>
											</ul>
											<ul class="new-banner-list">
												<li>Удобство установки</li>
												<li>Удобство использования</li>
											</ul>
											<a href="/implantation.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-implantation-zub.png" alt="">
									</div>
								</div>
									<div class="item item2" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Протезирование</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Отсутствие даже одного зуба вызывает существенные неудобства в жизни человека. Это может привести
												к смещению зубов, нарушению прикуса и к ряду других побочных заболеваний. Чтобы избежать таких 
												неприятностей следует обратиться к специалисту по <b>протезированию зубов</b>
											</div>
											<ul class="new-banner-list">
												<h4>Несъемное протезирование</h4>
												<li>Коронка</li>
												<li>Мостовидное протезирование</li>
												<li>Имплантат</li>
											</ul>
											<ul class="new-banner-list">
												<h4>Эстетическая реставрация</h4>
												<li>Виниры</li>
												<li>Вкладки</li>
											</ul><br>
											<a href="/proteth.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-protez-zub.png" alt="">
									</div>
								</div>
								<div class="item item3" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Детская стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Нравится ли вашему ребёнку ходить к стоматологу? Если ваш ребёнок боится и переживает,
												приходите с ним в ЦДИ, наш девиз — "<b>Без слёз и Страхов!</b>". Чтобы дети остались 
												счастливыми, а родители довольными!
											</div>
											<ul class="new-banner-list">
												<h4>Направления детской стоматологии</h4>
												<li>Лечение кариеса молочных зубов</li>
												<li>Удаление молочных зубов</li>
												<li>Диагностика</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение пульпита молочных зубов</li>
												<li>Обезболивание</li>
												<li>Исправление прикуса</li>
											</ul><br>
											<a href="/services.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-detskaya-zub.png" alt="">
									</div>
								</div>
								<div class="item item4">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Терапевтическая стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Лечение зубов и дёсен, а также профилактический осмотр у стоматолога – это основа 
												красивой и здоровой улыбки. Если вы при мысли о стоматологе всё ещё слышите 
												звук бормашины и невольно вздрагиваете – скорее об этом забудьте! 
											</div>
											<ul class="new-banner-list">
												<h4>Услуги терапевтической стоматологии</h4>
												<li>Лечение кариеса неотложных форм</li>
												<li>Лечение каналов</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение осложненных форм кариеса (лечение каналов)</li>
												<li>Лечение дёсен</li>
											</ul>
											<a href="/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-terapevt-zub.png" alt="">
									</div>
									
								</div>
									<div class="item item5">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Профессиональная гигиена </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Профессиональная гигиена – это полный комплекс процедур, которые очищают, оздоравливают и совершенствуют 
												ваши зубы. Каким бы тщательным ни был домашний уход, только врач стоматолог сможет полностью 
												<b>убрать зубной камень и налёт</b>, а значит защитить зубы и дёсны от возможных заболеваний.
											</div>
											<ul class="new-banner-list">
												<h4>Услуги профессиональной гигиены</h4>
												<li>Удаление пигментированного налёта</li>
												<li>Минерализация зубов</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Полировка эмали</li>
											</ul>
											<a href="/hygiene.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-gigiena-zub.png" alt="">
									</div>
								</div>
							
								
									
							
								<!-- <div class="item item6">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Анестезиология </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Боитесь ходить к стоматологу из-за неприятных ощущений? Наш стоматологический центр избавит 
												вас от подобных страхов. Мы используем самые <b>современные и эффективные</b> методы обезболивания. 
												Благодаря им лечение не причиняет никаких неудобств.

											</div>
											<ul class="new-banner-list">
												<h4>Виды анестезии</h4>
												<li>Аппликационная анестезия</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Седация</li>
												<li>Седация ксеноном</li><br>
											</ul>
											<a href="uslugi/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-anastezia-zub.png" alt="">
									</div>
								</div>
								<div class="item item7">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Диагностика </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Если вы привыкли отдавать баснословные деньги не только за стоматологическое лечение, 
												но и за прохождение диагностики, то забудьте об этом. Стоматологический центр ЦДИ проводит 
												обследование клиентов<b> абсолютно бесплатно.</b>

											</div>
											<ul class="new-banner-list">
												<h4>Виды диагностики</h4>
												<li>Цифровой ОПГ</li>
												<li>Радиовизиография</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Алекслокатор</li><br>
											</ul><br>
											<a href="uslugi/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-diagnostika-zub.png" alt="">
									</div>
								</div> -->
							</div>
							<div class="scroll scrollmain"></div>
						</div>
				<div class="section premiummain">
					<div class="wrapp">
						<div class="blockin">
							<div class="b_title">Наши преимущества</div>
							<div class="body">
								<div class="blocks" style=" border: 0; ">
							    	[[!getImageList? &tvname=`preem` &tpl=`preemTpl`]]
								</div>
							</div>
						</div>
					</div>
					<div class="scroll scrollmain"></div>
				</div>
				<div class="section contactssection">
					<div class="map">
						<div id="map"></div>
					</div>
					<div class="contacts_page_block main_page_contacts">
						<div class="blockin">
							<div class="contacts_content">
								<div class="title"><span>Контакты</span></div>
								<div class="body clearafter">
									<div class="big_info">
										<div class="block">
											<div class="inlineblock">
												<p style=" font-size: 15px; "><strong>Телефон:</strong> 25-70-33</p>
												<p style=" font-size: 15px; "><strong>E-mail:</strong> <a href="mailto:cdistom@mail.ru">cdistom@mail.ru</a></p>
											</div>
										</div>
										<div class="block">
											<div class="inlineblock">
												<div class="m_title">Режим работы</div>
												<p style=" font-size: 15px; "><strong>Пн-Пт:</strong> 8:30-20:00</p>
												<p style=" font-size: 15px; "><strong>Сб:</strong> 8:30-14:00</p>
												<p style=" font-size: 15px; "><strong>Вc: </strong> выходной</p>
											</div>
										</div>
										<div class="block">
											<div class="inlineblock">
												<div class="copyright">
													<ul class="address social_footer">
														<li><a href="https://vk.com/cdistom"></a></li>
														<li><a href="https://www.facebook.com/tomskimplant/?modal=composer"></a></li>
														<li><a href="https://www.instagram.com/tomstom_implant/?hl=ru"></a></li>					
													</ul>

												</div>
											</div>
										</div>
									</div>
									<div class="addressblock">
									Томск, ул. Говорова, д. 19 "В", ст. 1
									</div>
								</div>
							</div>
						</div>
						<div class="rightblock">
							<div class="title">Записаться на прием</div>
							
							        [[!AjaxForm?
                                        &snippet=`FormIt`
                                        &form=`mainForm`
                                        &emailTpl=`mainFormReport`
                                        &emailFrom=`info@cdistom.ru`
                                        &hooks=`recaptchav2,email`
                                        &emailSubject=`Запись на прием`
                                        &emailTo=`cdi257033@yandex.ru,cdistom@mail.ru,konakova@fresco.bz,pboykov@fresco.bz`
                                        &validate=`name:required,phone:required,g-recaptcha-response:required`
                                        &validationErrorMessage=`В форме содержатся ошибки!`
                                        &successMessage=`<script>yaCounter45262389.reachGoal(\'main\');</script>`
                                    ]]
							
						
						</div>
							<script>
										jQuery(document).ready(function() {
											jQuery(function() {
												jQuery( "#calendar" ).datepicker({ dateFormat: \'dd.mm.yy\' });
												jQuery("#phone").inputmask("+7 (999) 999-99-99");
											});
										});
									</script>
					</div>
				</div>
			</div>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации"></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="contacts.html" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
			</body>
			</html>',
    '_isForward' => true,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[$head]]' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>Главная - Центр дентальной имплантации</title>
        <base href="http://cdistom.ru/" />
        <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
    '[[$header]]' => '<header class="header">
			<div class="blockin">
				<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
				<a href="tel:+8 382225-70-33" class="phone_top">
					<p class="address_top">Томск, ул. Говорова, д. 19 "В", ст. 1</p>
					8 3822 <span>25-70-33</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="http://cdistom.ru/" class="">
								<img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
    '[[$banner]]' => '<div class="section mainslider new-banner" style="background: #7D92A7;">
							<div class="coutbloc">
								<div class="arrowmain"></div>
								<div class="count"></div>
							</div>
							<div class="owl-carousel owlmain">

								<div class="item item1">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Имплантация</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Имплантация — самый прогрессивный метод восстановления улыбки. Имплантаты просты в уходе, 
												максимально естественны и долговечны. Кроме того, восстановить улыбку сегодня можно буквально за 1 день, 
												без разрезов и швов, абсолютно безболезненно и, если хотите, во сне! 
											</div>
											<ul class="new-banner-list">
												<li>Постоянный контроль качества</li>
												<li>Широкий ассортимент имплантантов</li>
												<li>Короткий срок заживления</li>
											</ul>
											<ul class="new-banner-list">
												<li>Удобство установки</li>
												<li>Удобство использования</li>
											</ul>
											<a href="/implantation.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-implantation-zub.png" alt="">
									</div>
								</div>
									<div class="item item2" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Протезирование</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Отсутствие даже одного зуба вызывает существенные неудобства в жизни человека. Это может привести
												к смещению зубов, нарушению прикуса и к ряду других побочных заболеваний. Чтобы избежать таких 
												неприятностей следует обратиться к специалисту по <b>протезированию зубов</b>
											</div>
											<ul class="new-banner-list">
												<h4>Несъемное протезирование</h4>
												<li>Коронка</li>
												<li>Мостовидное протезирование</li>
												<li>Имплантат</li>
											</ul>
											<ul class="new-banner-list">
												<h4>Эстетическая реставрация</h4>
												<li>Виниры</li>
												<li>Вкладки</li>
											</ul><br>
											<a href="/proteth.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-protez-zub.png" alt="">
									</div>
								</div>
								<div class="item item3" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Детская стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Нравится ли вашему ребёнку ходить к стоматологу? Если ваш ребёнок боится и переживает,
												приходите с ним в ЦДИ, наш девиз — "<b>Без слёз и Страхов!</b>". Чтобы дети остались 
												счастливыми, а родители довольными!
											</div>
											<ul class="new-banner-list">
												<h4>Направления детской стоматологии</h4>
												<li>Лечение кариеса молочных зубов</li>
												<li>Удаление молочных зубов</li>
												<li>Диагностика</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение пульпита молочных зубов</li>
												<li>Обезболивание</li>
												<li>Исправление прикуса</li>
											</ul><br>
											<a href="/services.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-detskaya-zub.png" alt="">
									</div>
								</div>
								<div class="item item4">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Терапевтическая стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Лечение зубов и дёсен, а также профилактический осмотр у стоматолога – это основа 
												красивой и здоровой улыбки. Если вы при мысли о стоматологе всё ещё слышите 
												звук бормашины и невольно вздрагиваете – скорее об этом забудьте! 
											</div>
											<ul class="new-banner-list">
												<h4>Услуги терапевтической стоматологии</h4>
												<li>Лечение кариеса неотложных форм</li>
												<li>Лечение каналов</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение осложненных форм кариеса (лечение каналов)</li>
												<li>Лечение дёсен</li>
											</ul>
											<a href="/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-terapevt-zub.png" alt="">
									</div>
									
								</div>
									<div class="item item5">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Профессиональная гигиена </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Профессиональная гигиена – это полный комплекс процедур, которые очищают, оздоравливают и совершенствуют 
												ваши зубы. Каким бы тщательным ни был домашний уход, только врач стоматолог сможет полностью 
												<b>убрать зубной камень и налёт</b>, а значит защитить зубы и дёсны от возможных заболеваний.
											</div>
											<ul class="new-banner-list">
												<h4>Услуги профессиональной гигиены</h4>
												<li>Удаление пигментированного налёта</li>
												<li>Минерализация зубов</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Полировка эмали</li>
											</ul>
											<a href="/hygiene.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-gigiena-zub.png" alt="">
									</div>
								</div>
							
								
									
							
								<!-- <div class="item item6">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Анестезиология </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Боитесь ходить к стоматологу из-за неприятных ощущений? Наш стоматологический центр избавит 
												вас от подобных страхов. Мы используем самые <b>современные и эффективные</b> методы обезболивания. 
												Благодаря им лечение не причиняет никаких неудобств.

											</div>
											<ul class="new-banner-list">
												<h4>Виды анестезии</h4>
												<li>Аппликационная анестезия</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Седация</li>
												<li>Седация ксеноном</li><br>
											</ul>
											<a href="uslugi/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-anastezia-zub.png" alt="">
									</div>
								</div>
								<div class="item item7">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Диагностика </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Если вы привыкли отдавать баснословные деньги не только за стоматологическое лечение, 
												но и за прохождение диагностики, то забудьте об этом. Стоматологический центр ЦДИ проводит 
												обследование клиентов<b> абсолютно бесплатно.</b>

											</div>
											<ul class="new-banner-list">
												<h4>Виды диагностики</h4>
												<li>Цифровой ОПГ</li>
												<li>Радиовизиография</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Алекслокатор</li><br>
											</ul><br>
											<a href="uslugi/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-diagnostika-zub.png" alt="">
									</div>
								</div> -->
							</div>
							<div class="scroll scrollmain"></div>
						</div>',
    '[[$footerindex]]' => '<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
			</body>
			</html>',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'head',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>[[*titl]]</title>
        <base href="[[++site_url]]" />
        <meta charset="[[++modx_charset]]" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>[[*titl]]</title>
        <base href="[[++site_url]]" />
        <meta charset="[[++modx_charset]]" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header' => 
      array (
        'fields' => 
        array (
          'id' => 15,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'header',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<header class="header">
			<div class="blockin">
				<a href="[[++site_url]]" class="logo"><img src="[[#1.tv.logo]]" alt=""></a>
				<a href="tel:+[[#1.tv.tell]][[#1.tv.tell2]]" class="phone_top">
					<p class="address_top">[[#1.tv.addres]]</p>
					[[#1.tv.tell]] <span>[[#1.tv.tell2]]</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="[[++site_url]]" class="">
								<img src="[[#1.tv.logo]]" alt="[[++site_name]]">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<header class="header">
			<div class="blockin">
				<a href="[[++site_url]]" class="logo"><img src="[[#1.tv.logo]]" alt=""></a>
				<a href="tel:+[[#1.tv.tell]][[#1.tv.tell2]]" class="phone_top">
					<p class="address_top">[[#1.tv.addres]]</p>
					[[#1.tv.tell]] <span>[[#1.tv.tell2]]</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="[[++site_url]]" class="">
								<img src="[[#1.tv.logo]]" alt="[[++site_name]]">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'banner' => 
      array (
        'fields' => 
        array (
          'id' => 60,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'banner',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '<div class="section mainslider new-banner" style="background: #7D92A7;">
							<div class="coutbloc">
								<div class="arrowmain"></div>
								<div class="count"></div>
							</div>
							<div class="owl-carousel owlmain">

								<div class="item item1">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Имплантация</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Имплантация — самый прогрессивный метод восстановления улыбки. Имплантаты просты в уходе, 
												максимально естественны и долговечны. Кроме того, восстановить улыбку сегодня можно буквально за 1 день, 
												без разрезов и швов, абсолютно безболезненно и, если хотите, во сне! 
											</div>
											<ul class="new-banner-list">
												<li>Постоянный контроль качества</li>
												<li>Широкий ассортимент имплантантов</li>
												<li>Короткий срок заживления</li>
											</ul>
											<ul class="new-banner-list">
												<li>Удобство установки</li>
												<li>Удобство использования</li>
											</ul>
											<a href="/implantation.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-implantation-zub.png" alt="">
									</div>
								</div>
									<div class="item item2" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Протезирование</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Отсутствие даже одного зуба вызывает существенные неудобства в жизни человека. Это может привести
												к смещению зубов, нарушению прикуса и к ряду других побочных заболеваний. Чтобы избежать таких 
												неприятностей следует обратиться к специалисту по <b>протезированию зубов</b>
											</div>
											<ul class="new-banner-list">
												<h4>Несъемное протезирование</h4>
												<li>Коронка</li>
												<li>Мостовидное протезирование</li>
												<li>Имплантат</li>
											</ul>
											<ul class="new-banner-list">
												<h4>Эстетическая реставрация</h4>
												<li>Виниры</li>
												<li>Вкладки</li>
											</ul><br>
											<a href="/proteth.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-protez-zub.png" alt="">
									</div>
								</div>
								<div class="item item3" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Детская стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Нравится ли вашему ребёнку ходить к стоматологу? Если ваш ребёнок боится и переживает,
												приходите с ним в ЦДИ, наш девиз — "<b>Без слёз и Страхов!</b>". Чтобы дети остались 
												счастливыми, а родители довольными!
											</div>
											<ul class="new-banner-list">
												<h4>Направления детской стоматологии</h4>
												<li>Лечение кариеса молочных зубов</li>
												<li>Удаление молочных зубов</li>
												<li>Диагностика</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение пульпита молочных зубов</li>
												<li>Обезболивание</li>
												<li>Исправление прикуса</li>
											</ul><br>
											<a href="/services.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-detskaya-zub.png" alt="">
									</div>
								</div>
								<div class="item item4">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Терапевтическая стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Лечение зубов и дёсен, а также профилактический осмотр у стоматолога – это основа 
												красивой и здоровой улыбки. Если вы при мысли о стоматологе всё ещё слышите 
												звук бормашины и невольно вздрагиваете – скорее об этом забудьте! 
											</div>
											<ul class="new-banner-list">
												<h4>Услуги терапевтической стоматологии</h4>
												<li>Лечение кариеса неотложных форм</li>
												<li>Лечение каналов</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение осложненных форм кариеса (лечение каналов)</li>
												<li>Лечение дёсен</li>
											</ul>
											<a href="/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-terapevt-zub.png" alt="">
									</div>
									
								</div>
									<div class="item item5">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Профессиональная гигиена </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Профессиональная гигиена – это полный комплекс процедур, которые очищают, оздоравливают и совершенствуют 
												ваши зубы. Каким бы тщательным ни был домашний уход, только врач стоматолог сможет полностью 
												<b>убрать зубной камень и налёт</b>, а значит защитить зубы и дёсны от возможных заболеваний.
											</div>
											<ul class="new-banner-list">
												<h4>Услуги профессиональной гигиены</h4>
												<li>Удаление пигментированного налёта</li>
												<li>Минерализация зубов</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Полировка эмали</li>
											</ul>
											<a href="/hygiene.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-gigiena-zub.png" alt="">
									</div>
								</div>
							
								
									
							
								<!-- <div class="item item6">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Анестезиология </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Боитесь ходить к стоматологу из-за неприятных ощущений? Наш стоматологический центр избавит 
												вас от подобных страхов. Мы используем самые <b>современные и эффективные</b> методы обезболивания. 
												Благодаря им лечение не причиняет никаких неудобств.

											</div>
											<ul class="new-banner-list">
												<h4>Виды анестезии</h4>
												<li>Аппликационная анестезия</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Седация</li>
												<li>Седация ксеноном</li><br>
											</ul>
											<a href="uslugi/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-anastezia-zub.png" alt="">
									</div>
								</div>
								<div class="item item7">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Диагностика </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Если вы привыкли отдавать баснословные деньги не только за стоматологическое лечение, 
												но и за прохождение диагностики, то забудьте об этом. Стоматологический центр ЦДИ проводит 
												обследование клиентов<b> абсолютно бесплатно.</b>

											</div>
											<ul class="new-banner-list">
												<h4>Виды диагностики</h4>
												<li>Цифровой ОПГ</li>
												<li>Радиовизиография</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Алекслокатор</li><br>
											</ul><br>
											<a href="uslugi/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-diagnostika-zub.png" alt="">
									</div>
								</div> -->
							</div>
							<div class="scroll scrollmain"></div>
						</div>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="section mainslider new-banner" style="background: #7D92A7;">
							<div class="coutbloc">
								<div class="arrowmain"></div>
								<div class="count"></div>
							</div>
							<div class="owl-carousel owlmain">

								<div class="item item1">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Имплантация</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Имплантация — самый прогрессивный метод восстановления улыбки. Имплантаты просты в уходе, 
												максимально естественны и долговечны. Кроме того, восстановить улыбку сегодня можно буквально за 1 день, 
												без разрезов и швов, абсолютно безболезненно и, если хотите, во сне! 
											</div>
											<ul class="new-banner-list">
												<li>Постоянный контроль качества</li>
												<li>Широкий ассортимент имплантантов</li>
												<li>Короткий срок заживления</li>
											</ul>
											<ul class="new-banner-list">
												<li>Удобство установки</li>
												<li>Удобство использования</li>
											</ul>
											<a href="/implantation.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-implantation-zub.png" alt="">
									</div>
								</div>
									<div class="item item2" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Протезирование</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Отсутствие даже одного зуба вызывает существенные неудобства в жизни человека. Это может привести
												к смещению зубов, нарушению прикуса и к ряду других побочных заболеваний. Чтобы избежать таких 
												неприятностей следует обратиться к специалисту по <b>протезированию зубов</b>
											</div>
											<ul class="new-banner-list">
												<h4>Несъемное протезирование</h4>
												<li>Коронка</li>
												<li>Мостовидное протезирование</li>
												<li>Имплантат</li>
											</ul>
											<ul class="new-banner-list">
												<h4>Эстетическая реставрация</h4>
												<li>Виниры</li>
												<li>Вкладки</li>
											</ul><br>
											<a href="/proteth.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-protez-zub.png" alt="">
									</div>
								</div>
								<div class="item item3" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Детская стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Нравится ли вашему ребёнку ходить к стоматологу? Если ваш ребёнок боится и переживает,
												приходите с ним в ЦДИ, наш девиз — "<b>Без слёз и Страхов!</b>". Чтобы дети остались 
												счастливыми, а родители довольными!
											</div>
											<ul class="new-banner-list">
												<h4>Направления детской стоматологии</h4>
												<li>Лечение кариеса молочных зубов</li>
												<li>Удаление молочных зубов</li>
												<li>Диагностика</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение пульпита молочных зубов</li>
												<li>Обезболивание</li>
												<li>Исправление прикуса</li>
											</ul><br>
											<a href="/services.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-detskaya-zub.png" alt="">
									</div>
								</div>
								<div class="item item4">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Терапевтическая стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Лечение зубов и дёсен, а также профилактический осмотр у стоматолога – это основа 
												красивой и здоровой улыбки. Если вы при мысли о стоматологе всё ещё слышите 
												звук бормашины и невольно вздрагиваете – скорее об этом забудьте! 
											</div>
											<ul class="new-banner-list">
												<h4>Услуги терапевтической стоматологии</h4>
												<li>Лечение кариеса неотложных форм</li>
												<li>Лечение каналов</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение осложненных форм кариеса (лечение каналов)</li>
												<li>Лечение дёсен</li>
											</ul>
											<a href="/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-terapevt-zub.png" alt="">
									</div>
									
								</div>
									<div class="item item5">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Профессиональная гигиена </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Профессиональная гигиена – это полный комплекс процедур, которые очищают, оздоравливают и совершенствуют 
												ваши зубы. Каким бы тщательным ни был домашний уход, только врач стоматолог сможет полностью 
												<b>убрать зубной камень и налёт</b>, а значит защитить зубы и дёсны от возможных заболеваний.
											</div>
											<ul class="new-banner-list">
												<h4>Услуги профессиональной гигиены</h4>
												<li>Удаление пигментированного налёта</li>
												<li>Минерализация зубов</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Полировка эмали</li>
											</ul>
											<a href="/hygiene.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-gigiena-zub.png" alt="">
									</div>
								</div>
							
								
									
							
								<!-- <div class="item item6">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Анестезиология </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Боитесь ходить к стоматологу из-за неприятных ощущений? Наш стоматологический центр избавит 
												вас от подобных страхов. Мы используем самые <b>современные и эффективные</b> методы обезболивания. 
												Благодаря им лечение не причиняет никаких неудобств.

											</div>
											<ul class="new-banner-list">
												<h4>Виды анестезии</h4>
												<li>Аппликационная анестезия</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Седация</li>
												<li>Седация ксеноном</li><br>
											</ul>
											<a href="uslugi/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-anastezia-zub.png" alt="">
									</div>
								</div>
								<div class="item item7">
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Диагностика </div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Если вы привыкли отдавать баснословные деньги не только за стоматологическое лечение, 
												но и за прохождение диагностики, то забудьте об этом. Стоматологический центр ЦДИ проводит 
												обследование клиентов<b> абсолютно бесплатно.</b>

											</div>
											<ul class="new-banner-list">
												<h4>Виды диагностики</h4>
												<li>Цифровой ОПГ</li>
												<li>Радиовизиография</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Алекслокатор</li><br>
											</ul><br>
											<a href="uslugi/terapia.html" class="all" style=" font-size: 14px; font-weight: 300; ">Подробнее</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-diagnostika-zub.png" alt="">
									</div>
								</div> -->
							</div>
							<div class="scroll scrollmain"></div>
						</div>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'footerindex' => 
      array (
        'fields' => 
        array (
          'id' => 16,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footerindex',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
			</body>
			</html>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
			</body>
			</html>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'recaptchav2_html' => 
      array (
        'fields' => 
        array (
          'id' => 41,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'recaptchav2_html',
          'description' => '',
          'editor_type' => 0,
          'category' => 37,
          'cache_type' => 0,
          'snippet' => '<div class="g-recaptcha" data-sitekey="[[+site_key]]"></div>
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=[[++cultureKey]]"></script>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<div class="g-recaptcha" data-sitekey="[[+site_key]]"></div>
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=[[++cultureKey]]"></script>',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'pdoMenu' => 
      array (
        'fields' => 
        array (
          'id' => 38,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoMenu',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */

// Convert parameters from Wayfinder if exists
if (isset($startId)) {
    $scriptProperties[\'parents\'] = $startId;
}
if (!empty($includeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $includeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',\' . $v;
        } else {
            $scriptProperties[\'resources\'] = $v;
        }
    }
}
if (!empty($excludeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $excludeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',-\' . $v;
        } else {
            $scriptProperties[\'resources\'] = \'-\' . $v;
        }
    }
}

if (!empty($previewUnpublished) && $modx->hasPermission(\'view_unpublished\')) {
    $scriptProperties[\'showUnpublished\'] = 1;
}

$scriptProperties[\'depth\'] = empty($level) ? 100 : abs($level) - 1;
if (!empty($contexts)) {
    $scriptProperties[\'context\'] = $contexts;
}
if (empty($scriptProperties[\'context\'])) {
    $scriptProperties[\'context\'] = $modx->resource->context_key;
}

// Save original parents specified by user
$specified_parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));

if ($scriptProperties[\'parents\'] === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
} elseif ($scriptProperties[\'parents\'] === 0 || $scriptProperties[\'parents\'] === \'0\') {
    if ($scriptProperties[\'depth\'] !== \'\' && $scriptProperties[\'depth\'] !== 100) {
        $contexts = array_map(\'trim\', explode(\',\', $scriptProperties[\'context\']));
        $parents = array();
        if (!empty($scriptProperties[\'showDeleted\'])) {
            $pdoFetch = $modx->getService(\'pdoFetch\');
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $pdoFetch->getChildIds(\'modResource\', 0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        } else {
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $modx->getChildIds(0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        }
        $scriptProperties[\'parents\'] = !empty($parents) ? implode(\',\', $parents) : \'+0\';
        $scriptProperties[\'depth\'] = 0;
    }
    $scriptProperties[\'includeParents\'] = 1;
    $scriptProperties[\'displayStart\'] = 0;
} else {
    $parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));
    $parents_in = $parents_out = array();
    foreach ($parents as $v) {
        if (!is_numeric($v)) {
            continue;
        }
        if ($v[0] == \'-\') {
            $parents_out[] = abs($v);
        } else {
            $parents_in[] = abs($v);
        }
    }

    if (empty($parents_in)) {
        $scriptProperties[\'includeParents\'] = 1;
        $scriptProperties[\'displayStart\'] = 0;
    }
}

if (!empty($displayStart)) {
    $scriptProperties[\'includeParents\'] = 1;
}
if (!empty($ph)) {
    $toPlaceholder = $ph;
}
if (!empty($sortOrder)) {
    $scriptProperties[\'sortdir\'] = $sortOrder;
}
if (!empty($sortBy)) {
    $scriptProperties[\'sortby\'] = $sortBy;
}
if (!empty($permissions)) {
    $scriptProperties[\'checkPermissions\'] = $permissions;
}
if (!empty($cacheResults)) {
    $scriptProperties[\'cache\'] = $cacheResults;
}
if (!empty($ignoreHidden)) {
    $scriptProperties[\'showHidden\'] = $ignoreHidden;
}

$wfTemplates = array(
    \'outerTpl\' => \'tplOuter\',
    \'rowTpl\' => \'tpl\',
    \'parentRowTpl\' => \'tplParentRow\',
    \'parentRowHereTpl\' => \'tplParentRowHere\',
    \'hereTpl\' => \'tplHere\',
    \'innerTpl\' => \'tplInner\',
    \'innerRowTpl\' => \'tplInnerRow\',
    \'innerHereTpl\' => \'tplInnerHere\',
    \'activeParentRowTpl\' => \'tplParentRowActive\',
    \'categoryFoldersTpl\' => \'tplCategoryFolder\',
    \'startItemTpl\' => \'tplStart\',
);
foreach ($wfTemplates as $k => $v) {
    if (isset(${$k})) {
        $scriptProperties[$v] = ${$k};
    }
}
//---

/** @var pdoMenu $pdoMenu */
$fqn = $modx->getOption(\'pdoMenu.class\', null, \'pdotools.pdomenu\', true);
$path = $modx->getOption(\'pdomenu_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoMenu = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoMenu->pdoTools->addTime(\'pdoTools loaded\');

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
if (empty($scriptProperties[\'cache_key\'])) {
    $scriptProperties[\'cache_key\'] = \'pdomenu/\' . sha1(serialize($scriptProperties));
}

$output = \'\';
$tree = array();
if ($cache) {
    $tree = $pdoMenu->pdoTools->getCache($scriptProperties);
}
if (empty($tree)) {
    $data = $pdoMenu->pdoTools->run();
    $data = $pdoMenu->pdoTools->buildTree($data, \'id\', \'parent\', $specified_parents);
    $tree = array();
    foreach ($data as $k => $v) {
        if (empty($v[\'id\'])) {
            if (!in_array($k, $specified_parents) && !$pdoMenu->checkResource($k)) {
                continue;
            } else {
                $tree = array_merge($tree, $v[\'children\']);
            }
        } else {
            $tree[$k] = $v;
        }
    }
    if ($cache) {
        $pdoMenu->pdoTools->setCache($tree, $scriptProperties);
    }
}
if (!empty($tree)) {
    $output = $pdoMenu->templateTree($tree);
}

if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $output .= \'<pre class="pdoMenuLog">\' . print_r($pdoMenu->pdoTools->getTime(), 1) . \'</pre>\';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
          'locked' => false,
          'properties' => 
          array (
            'showLog' => 
            array (
              'name' => 'showLog',
              'desc' => 'pdotools_prop_showLog',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать дополнительную информацию о работе сниппета. Только для авторизованных в контекте "mgr".',
              'area_trans' => '',
            ),
            'fastMode' => 
            array (
              'name' => 'fastMode',
              'desc' => 'pdotools_prop_fastMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Быстрый режим обработки чанков. Все необработанные теги (условия, сниппеты и т.п.) будут вырезаны.',
              'area_trans' => '',
            ),
            'level' => 
            array (
              'name' => 'level',
              'desc' => 'pdotools_prop_level',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Уровень генерируемого меню.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'pdotools_prop_parents',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список родителей, через запятую, для поиска результатов. По умолчанию выборка ограничена текущим родителем. Если поставить 0 - выборка не ограничивается. Если id родителя начинается с дефиса, он и его потомки исключается из выборки.',
              'area_trans' => '',
            ),
            'displayStart' => 
            array (
              'name' => 'displayStart',
              'desc' => 'pdotools_prop_displayStart',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить показ начальных узлов меню. Полезно при указании более одного "parents".',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'pdotools_prop_resources',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ресурсов, через запятую, для вывода в результатах. Если id ресурса начинается с дефиса, этот ресурс исключается из выборки.',
              'area_trans' => '',
            ),
            'templates' => 
            array (
              'name' => 'templates',
              'desc' => 'pdotools_prop_templates',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список шаблонов, через запятую, для фильтрации результатов. Если id шаблона начинается с дефиса, ресурсы с ним исключается из выборки.',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'pdotools_prop_context',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение выборки по контексту ресурсов.',
              'area_trans' => '',
            ),
            'cache' => 
            array (
              'name' => 'cache',
              'desc' => 'pdotools_prop_cache',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Кэширование результатов работы сниппета.',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'pdotools_prop_cacheTime',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 3600,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Время актуальности кэша в секундах.',
              'area_trans' => '',
            ),
            'cacheAnonymous' => 
            array (
              'name' => 'cacheAnonymous',
              'desc' => 'pdotools_prop_cacheAnonymous',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить кэширование только для неавторизованных посетителей.',
              'area_trans' => '',
            ),
            'plPrefix' => 
            array (
              'name' => 'plPrefix',
              'desc' => 'pdotools_prop_plPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'wf.',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для выставляемых плейсхолдеров, по умолчанию "wf.".',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'pdotools_prop_showHidden',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать ресурсы, скрытые в меню.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'pdotools_prop_showUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать неопубликованные ресурсы.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'pdotools_prop_showDeleted',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать удалённые ресурсы.',
              'area_trans' => '',
            ),
            'previewUnpublished' => 
            array (
              'name' => 'previewUnpublished',
              'desc' => 'pdotools_prop_previewUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить показ неопубликованных документов, если у пользователя есть на это разрешение.',
              'area_trans' => '',
            ),
            'hideSubMenus' => 
            array (
              'name' => 'hideSubMenus',
              'desc' => 'pdotools_prop_hideSubMenus',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Спрятать неактивные ветки меню.',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'pdotools_prop_useWeblinkUrl',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Генерировать ссылку с учетом класса ресурса.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'pdotools_prop_sortdir',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки: по убыванию или возрастанию.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'pdotools_prop_sortby',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'menuindex',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Любое поле ресурса для сортировки, включая ТВ параметр, если он указан в параметре "includeTVs". Можно указывать JSON строку с массивом нескольких полей. Для случайно сортировки укажите "RAND()"',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'rowIdPrefix' => 
            array (
              'name' => 'rowIdPrefix',
              'desc' => 'pdotools_prop_rowIdPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс id="" для выставления идентификатора в чанк.',
              'area_trans' => '',
            ),
            'firstClass' => 
            array (
              'name' => 'firstClass',
              'desc' => 'pdotools_prop_firstClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'first',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс для первого пункта меню.',
              'area_trans' => '',
            ),
            'lastClass' => 
            array (
              'name' => 'lastClass',
              'desc' => 'pdotools_prop_lastClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'last',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс последнего пункта меню.',
              'area_trans' => '',
            ),
            'hereClass' => 
            array (
              'name' => 'hereClass',
              'desc' => 'pdotools_prop_hereClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'active',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс для активного пункта меню.',
              'area_trans' => '',
            ),
            'parentClass' => 
            array (
              'name' => 'parentClass',
              'desc' => 'pdotools_prop_parentClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс категории меню.',
              'area_trans' => '',
            ),
            'rowClass' => 
            array (
              'name' => 'rowClass',
              'desc' => 'pdotools_prop_rowClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс одной строки меню.',
              'area_trans' => '',
            ),
            'outerClass' => 
            array (
              'name' => 'outerClass',
              'desc' => 'pdotools_prop_outerClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс обертки меню.',
              'area_trans' => '',
            ),
            'innerClass' => 
            array (
              'name' => 'innerClass',
              'desc' => 'pdotools_prop_innerClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс внутренних ссылок меню.',
              'area_trans' => '',
            ),
            'levelClass' => 
            array (
              'name' => 'levelClass',
              'desc' => 'pdotools_prop_levelClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс уровня меню. Например, если укажите "level", то будет "level1", "level2" и т.д.',
              'area_trans' => '',
            ),
            'selfClass' => 
            array (
              'name' => 'selfClass',
              'desc' => 'pdotools_prop_selfClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс текущего документа в меню.',
              'area_trans' => '',
            ),
            'webLinkClass' => 
            array (
              'name' => 'webLinkClass',
              'desc' => 'pdotools_prop_webLinkClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс документа-ссылки.',
              'area_trans' => '',
            ),
            'tplOuter' => 
            array (
              'name' => 'tplOuter',
              'desc' => 'pdotools_prop_tplOuter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <ul[[+classes]]>[[+wrapper]]</ul>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка всего блока меню.',
              'area_trans' => '',
            ),
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'pdotools_prop_tpl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li[[+classes]]><a href="[[+link]]" [[+attributes]]>[[+menutitle]]</a>[[+wrapper]]</li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для оформления ресурса. Если не указан, то содержимое полей ресурса будет распечатано на экран.',
              'area_trans' => '',
            ),
            'tplParentRow' => 
            array (
              'name' => 'tplParentRow',
              'desc' => 'pdotools_prop_tplParentRow',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplParentRowHere' => 
            array (
              'name' => 'tplParentRowHere',
              'desc' => 'pdotools_prop_tplParentRowHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления текущего контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplHere' => 
            array (
              'name' => 'tplHere',
              'desc' => 'pdotools_prop_tplHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк текущего документа',
              'area_trans' => '',
            ),
            'tplInner' => 
            array (
              'name' => 'tplInner',
              'desc' => 'pdotools_prop_tplInner',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка внутренних пунктов меню. Если пуст - будет использовать "tplInner".',
              'area_trans' => '',
            ),
            'tplInnerRow' => 
            array (
              'name' => 'tplInnerRow',
              'desc' => 'pdotools_prop_tplInnerRow',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка активного пункта меню.',
              'area_trans' => '',
            ),
            'tplInnerHere' => 
            array (
              'name' => 'tplInnerHere',
              'desc' => 'pdotools_prop_tplInnerHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка активного пункта меню.',
              'area_trans' => '',
            ),
            'tplParentRowActive' => 
            array (
              'name' => 'tplParentRowActive',
              'desc' => 'pdotools_prop_tplParentRowActive',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления активного контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplCategoryFolder' => 
            array (
              'name' => 'tplCategoryFolder',
              'desc' => 'pdotools_prop_tplCategoryFolder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Специальный чанк оформления категории. Категория - это документ с потомками и или нулевым шаблоном, или с атрибутом "rel=\\"category\\"".',
              'area_trans' => '',
            ),
            'tplStart' => 
            array (
              'name' => 'tplStart',
              'desc' => 'pdotools_prop_tplStart',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <h2[[+classes]]>[[+menutitle]]</h2>[[+wrapper]]',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления корневого пункта, при условии, что включен "displayStart".',
              'area_trans' => '',
            ),
            'checkPermissions' => 
            array (
              'name' => 'checkPermissions',
              'desc' => 'pdotools_prop_checkPermissions',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Укажите, какие разрешения нужно проверять у пользователя при выводе документов.',
              'area_trans' => '',
            ),
            'hereId' => 
            array (
              'name' => 'hereId',
              'desc' => 'pdotools_prop_hereId',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Id документа, текущего для генерируемого меню. Нужно указывать только если скрипт сам его неверно определяет, например при выводе меню из чанка другого сниппета.',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'pdotools_prop_where',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Массив дополнительных параметров выборки, закодированный в JSON.',
              'area_trans' => '',
            ),
            'select' => 
            array (
              'name' => 'select',
              'desc' => 'pdotools_prop_select',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список полей для выборки, через запятую. Можно указывать JSON строку с массивом, например {"modResource":"id,pagetitle,content"}.',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'pdotools_prop_scheme',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => '',
                  'text' => 'System default',
                  'name' => 'System default',
                ),
                1 => 
                array (
                  'value' => -1,
                  'text' => '-1 (relative to site_url)',
                  'name' => '-1 (relative to site_url)',
                ),
                2 => 
                array (
                  'value' => 'full',
                  'text' => 'full (absolute, prepended with site_url)',
                  'name' => 'full (absolute, prepended with site_url)',
                ),
                3 => 
                array (
                  'value' => 'abs',
                  'text' => 'abs (absolute, prepended with base_url)',
                  'name' => 'abs (absolute, prepended with base_url)',
                ),
                4 => 
                array (
                  'value' => 'http',
                  'text' => 'http (absolute, forced to http scheme)',
                  'name' => 'http (absolute, forced to http scheme)',
                ),
                5 => 
                array (
                  'value' => 'https',
                  'text' => 'https (absolute, forced to https scheme)',
                  'name' => 'https (absolute, forced to https scheme)',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема формирования ссылок: "uri" для подстановки поля uri документа (очень быстро) или параметр для modX::makeUrl().',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'countChildren' => 
            array (
              'name' => 'countChildren',
              'desc' => 'pdotools_prop_countChildren',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Вывести точное количество активных потомков документа в плейсхолдер [[+children]].',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdomenu.php',
          'content' => '/** @var array $scriptProperties */

// Convert parameters from Wayfinder if exists
if (isset($startId)) {
    $scriptProperties[\'parents\'] = $startId;
}
if (!empty($includeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $includeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',\' . $v;
        } else {
            $scriptProperties[\'resources\'] = $v;
        }
    }
}
if (!empty($excludeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $excludeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',-\' . $v;
        } else {
            $scriptProperties[\'resources\'] = \'-\' . $v;
        }
    }
}

if (!empty($previewUnpublished) && $modx->hasPermission(\'view_unpublished\')) {
    $scriptProperties[\'showUnpublished\'] = 1;
}

$scriptProperties[\'depth\'] = empty($level) ? 100 : abs($level) - 1;
if (!empty($contexts)) {
    $scriptProperties[\'context\'] = $contexts;
}
if (empty($scriptProperties[\'context\'])) {
    $scriptProperties[\'context\'] = $modx->resource->context_key;
}

// Save original parents specified by user
$specified_parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));

if ($scriptProperties[\'parents\'] === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
} elseif ($scriptProperties[\'parents\'] === 0 || $scriptProperties[\'parents\'] === \'0\') {
    if ($scriptProperties[\'depth\'] !== \'\' && $scriptProperties[\'depth\'] !== 100) {
        $contexts = array_map(\'trim\', explode(\',\', $scriptProperties[\'context\']));
        $parents = array();
        if (!empty($scriptProperties[\'showDeleted\'])) {
            $pdoFetch = $modx->getService(\'pdoFetch\');
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $pdoFetch->getChildIds(\'modResource\', 0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        } else {
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $modx->getChildIds(0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        }
        $scriptProperties[\'parents\'] = !empty($parents) ? implode(\',\', $parents) : \'+0\';
        $scriptProperties[\'depth\'] = 0;
    }
    $scriptProperties[\'includeParents\'] = 1;
    $scriptProperties[\'displayStart\'] = 0;
} else {
    $parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));
    $parents_in = $parents_out = array();
    foreach ($parents as $v) {
        if (!is_numeric($v)) {
            continue;
        }
        if ($v[0] == \'-\') {
            $parents_out[] = abs($v);
        } else {
            $parents_in[] = abs($v);
        }
    }

    if (empty($parents_in)) {
        $scriptProperties[\'includeParents\'] = 1;
        $scriptProperties[\'displayStart\'] = 0;
    }
}

if (!empty($displayStart)) {
    $scriptProperties[\'includeParents\'] = 1;
}
if (!empty($ph)) {
    $toPlaceholder = $ph;
}
if (!empty($sortOrder)) {
    $scriptProperties[\'sortdir\'] = $sortOrder;
}
if (!empty($sortBy)) {
    $scriptProperties[\'sortby\'] = $sortBy;
}
if (!empty($permissions)) {
    $scriptProperties[\'checkPermissions\'] = $permissions;
}
if (!empty($cacheResults)) {
    $scriptProperties[\'cache\'] = $cacheResults;
}
if (!empty($ignoreHidden)) {
    $scriptProperties[\'showHidden\'] = $ignoreHidden;
}

$wfTemplates = array(
    \'outerTpl\' => \'tplOuter\',
    \'rowTpl\' => \'tpl\',
    \'parentRowTpl\' => \'tplParentRow\',
    \'parentRowHereTpl\' => \'tplParentRowHere\',
    \'hereTpl\' => \'tplHere\',
    \'innerTpl\' => \'tplInner\',
    \'innerRowTpl\' => \'tplInnerRow\',
    \'innerHereTpl\' => \'tplInnerHere\',
    \'activeParentRowTpl\' => \'tplParentRowActive\',
    \'categoryFoldersTpl\' => \'tplCategoryFolder\',
    \'startItemTpl\' => \'tplStart\',
);
foreach ($wfTemplates as $k => $v) {
    if (isset(${$k})) {
        $scriptProperties[$v] = ${$k};
    }
}
//---

/** @var pdoMenu $pdoMenu */
$fqn = $modx->getOption(\'pdoMenu.class\', null, \'pdotools.pdomenu\', true);
$path = $modx->getOption(\'pdomenu_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoMenu = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoMenu->pdoTools->addTime(\'pdoTools loaded\');

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
if (empty($scriptProperties[\'cache_key\'])) {
    $scriptProperties[\'cache_key\'] = \'pdomenu/\' . sha1(serialize($scriptProperties));
}

$output = \'\';
$tree = array();
if ($cache) {
    $tree = $pdoMenu->pdoTools->getCache($scriptProperties);
}
if (empty($tree)) {
    $data = $pdoMenu->pdoTools->run();
    $data = $pdoMenu->pdoTools->buildTree($data, \'id\', \'parent\', $specified_parents);
    $tree = array();
    foreach ($data as $k => $v) {
        if (empty($v[\'id\'])) {
            if (!in_array($k, $specified_parents) && !$pdoMenu->checkResource($k)) {
                continue;
            } else {
                $tree = array_merge($tree, $v[\'children\']);
            }
        } else {
            $tree[$k] = $v;
        }
    }
    if ($cache) {
        $pdoMenu->pdoTools->setCache($tree, $scriptProperties);
    }
}
if (!empty($tree)) {
    $output = $pdoMenu->templateTree($tree);
}

if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $output .= \'<pre class="pdoMenuLog">\' . print_r($pdoMenu->pdoTools->getTime(), 1) . \'</pre>\';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'getImageList' => 
      array (
        'fields' => 
        array (
          'id' => 10,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'getImageList',
          'description' => '',
          'editor_type' => 0,
          'category' => 9,
          'cache_type' => 0,
          'snippet' => '/**
 * getImageList
 *
 * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>
 *
 * getImageList is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package migx
 */
/**
 * getImageList
 *
 * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution 
 *
 * @version 1.4
 * @author Bruno Perner <b.perner@gmx.de>
 * @copyright Copyright &copy; 2009-2014
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License
 * version 2 or (at your option) any later version.
 * @package migx
 */

/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/
/* get default properties */


$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');
$inherit_children_tvname = $modx->getOption(\'inherit_children_tvname\', $scriptProperties, \'\');
$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');
$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');
$emptyTpl = $modx->getOption(\'emptyTpl\', $scriptProperties, \'\'); 
$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');
$offset = $modx->getOption(\'offset\', $scriptProperties, 0);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);
$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images
$where = $modx->getOption(\'where\', $scriptProperties, \'\');
$where = !empty($where) ? $modx->fromJSON($where) : array();
$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');
$sort = !empty($sort) ? $modx->fromJSON($sort) : array();
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');
$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);
$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');
$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');
$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');
$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));
$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;
$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');
$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');
$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');
$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');
$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');
$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;
//split json into parts
$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));
$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from
$inheritFrom = !empty($inheritFrom) ? explode(\',\', $inheritFrom) : \'\';

$modx->setPlaceholder(\'docid\', $docid);

$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);
$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);

$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);
if (!($migx instanceof Migx))
    return \'\';
$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';

if (!empty($tvname)) {
    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {

        /*
        *   get inputProperties
        */


        $properties = $tv->get(\'input_properties\');
        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();

        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');
        if (!empty($migx->config[\'configs\'])) {
            $migx->loadConfigs();
            // get tabs from file or migx-config-table
            $formtabs = $migx->getTabs();
        }
        if (empty($formtabs) && isset($properties[\'formtabs\'])) {
            //try to get formtabs and its fields from properties
            $formtabs = $modx->fromJSON($properties[\'formtabs\']);
        }

        if (!empty($properties[\'basePath\'])) {
            if ($properties[\'autoResourceFolders\'] == \'true\') {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';
            } else {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];
            }
        }
        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {
            $jsonVarKey = $properties[\'jsonvarkey\'];
            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
        }

        if (empty($outputvalue)) {
            $outputvalue = $tv->renderOutput($docid);
            if (empty($outputvalue) && !empty($inheritFrom)) {
                foreach ($inheritFrom as $from) {
                    if ($from == \'parents\') {
                        if (!empty($inherit_children_tvname)){
                            //try to get items from optional MIGX-TV for children
                            if ($inh_tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inherit_children_tvname))) {
                                $outputvalue = $inh_tv->processInheritBinding(\'\', $docid);    
                            }
                        }
                        $outputvalue = empty($outputvalue) ? $tv->processInheritBinding(\'\', $docid) : $outputvalue;
                    } else {
                        $outputvalue = $tv->renderOutput($from);
                    }
                    if (!empty($outputvalue)) {
                        break;
                    }
                }
            }
        }


        /*
        *   get inputTvs 
        */
        $inputTvs = array();
        if (is_array($formtabs)) {

            //multiple different Forms
            // Note: use same field-names and inputTVs in all forms
            $inputTvs = $migx->extractInputTvs($formtabs);
        }
        if ($migx->source = $tv->getSource($migx->working_context, false)) {
            $migx->source->initialize();
        }

    }


}

if (empty($outputvalue)) {
    $modx->setPlaceholder($totalVar, 0);
    return \'\';
}

//echo $outputvalue.\'<br/><br/>\';

$items = $modx->fromJSON($outputvalue);

// where filter
if (is_array($where) && count($where) > 0) {
    $items = $migx->filterItems($where, $items);
}
$modx->setPlaceholder($totalVar, count($items));

if (!empty($reverse)) {
    $items = array_reverse($items);
}

// sort items
if (is_array($sort) && count($sort) > 0) {
    $items = $migx->sortDbResult($items, $sort);
}

$summaries = array();
$output = \'\';
$items = $offset > 0 ? array_slice($items, $offset) : $items;
$count = count($items);

if ($count > 0) {
    $limit = $limit == 0 || $limit > $count ? $count : $limit;
    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;
    //preselect important items
    $preitems = array();
    if ($randomize && $preselectLimit > 0) {
        for ($i = 0; $i < $preselectLimit; $i++) {
            $preitems[] = $items[$i];
            unset($items[$i]);
        }
        $limit = $limit - count($preitems);
    }

    //shuffle items
    if ($randomize) {
        shuffle($items);
    }

    //limit items
    $count = count($items);
    $tempitems = array();

    for ($i = 0; $i < $limit; $i++) {
        if ($i >= $count) {
            break;
        }
        $tempitems[] = $items[$i];
    }
    $items = $tempitems;

    //add preselected items and schuffle again
    if ($randomize && $preselectLimit > 0) {
        $items = array_merge($preitems, $items);
        shuffle($items);
    }

    $properties = array();
    foreach ($scriptProperties as $property => $value) {
        $properties[\'property.\' . $property] = $value;
    }

    $idx = 0;
    $output = array();
    $template = array();
    $count = count($items);

    foreach ($items as $key => $item) {
        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';
        $fields = array();
        foreach ($item as $field => $value) {
            if (is_array($value)) {
                if (is_array($value[0])) {
                    //nested array - convert to json
                    $value = $modx->toJson($value);
                } else {
                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)
                }
            }


            $inputTVkey = $formname . $field;

            if ($processTVs && isset($inputTvs[$inputTVkey])) {
                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {

                } else {
                    $tv = $modx->newObject(\'modTemplateVar\');
                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);
                }
                $inputTV = $inputTvs[$inputTVkey];

                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');
                //don\'t manipulate any urls here
                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');
                $tv->set(\'default_text\', $value);
                $value = $tv->renderOutput($docid);
                //set option back
                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);
                //now manipulate urls
                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {
                    $mTypes = explode(\',\', $mTypes);
                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {
                        //$value = $mediasource->prepareOutputUrl($value);
                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));
                    }
                }

            }
            $fields[$field] = $value;

        }

        if (!empty($addfields)) {
            foreach ($addfields as $addfield) {
                $addfield = explode(\':\', $addfield);
                $addname = $addfield[0];
                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';
                $fields[$addname] = $adddefault;
            }
        }

        if (!empty($sumFields)) {
            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);
            foreach ($sumFields as $sumField) {
                if (isset($fields[$sumField])) {
                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];
                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];
                }
            }
        }


        if ($toJsonPlaceholder) {
            $output[] = $fields;
        } else {
            $fields[\'_alt\'] = $idx % 2;
            $idx++;
            $fields[\'_first\'] = $idx == 1 ? true : \'\';
            $fields[\'_last\'] = $idx == $limit ? true : \'\';
            $fields[\'idx\'] = $idx;
            $rowtpl = \'\';
            //get changing tpls from field
            if (substr($tpl, 0, 7) == "@FIELD:") {
                $tplField = substr($tpl, 7);
                $rowtpl = $fields[$tplField];
            }

            if ($fields[\'_first\'] && !empty($tplFirst)) {
                $rowtpl = $tplFirst;
            }
            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {
                $rowtpl = $tplLast;
            }
            $tplidx = \'tpl_\' . $idx;
            if (empty($rowtpl) && !empty($$tplidx)) {
                $rowtpl = $$tplidx;
            }
            if ($idx > 1 && empty($rowtpl)) {
                $divisors = $migx->getDivisors($idx);
                if (!empty($divisors)) {
                    foreach ($divisors as $divisor) {
                        $tplnth = \'tpl_n\' . $divisor;
                        if (!empty($$tplnth)) {
                            $rowtpl = $$tplnth;
                            if (!empty($rowtpl)) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($count == 1 && isset($tpl_oneresult)) {
                $rowtpl = $tpl_oneresult;
            }

            $fields = array_merge($fields, $properties);

            if (!empty($rowtpl)) {
                $template = $migx->getTemplate($tpl, $template);
                $fields[\'_tpl\'] = $template[$tpl];
            } else {
                $rowtpl = $tpl;

            }
            $template = $migx->getTemplate($rowtpl, $template);


            if ($template[$rowtpl]) {
                $chunk = $modx->newObject(\'modChunk\');
                $chunk->setCacheable(false);
                $chunk->setContent($template[$rowtpl]);


                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {
                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);
                } else {
                    $output[] = $chunk->process($fields);
                }
            } else {
                if (!empty($placeholdersKeyField)) {
                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                } else {
                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                }
            }
        }


    }
}

if (count($summaries) > 0) {
    $modx->toPlaceholders($summaries);
}


if ($toJsonPlaceholder) {
    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));
    return \'\';
}

if (!empty($toSeparatePlaceholders)) {
    $modx->toPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}
/*
if (!empty($outerTpl))
$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));
else 
*/

if ($count > 0 && $splits > 0) {
    $size = ceil($count / $splits);
    $chunks = array_chunk($output, $size);
    $output = array();
    foreach ($chunks as $chunk) {
        $o = implode($outputSeparator, $chunk);
        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));
    }
    $outputSeparator = $splitSeparator;
}

if (is_array($output)) {
    $o = implode($outputSeparator, $output);
} else {
    $o = $output;
}

if (!empty($o) && !empty($wrapperTpl)) {
    $template = $migx->getTemplate($wrapperTpl);
    if ($template[$wrapperTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$wrapperTpl]);
        $properties[\'output\'] = $o;
        $o = $chunk->process($properties);
    }
}

if (empty($o) && !empty($emptyTpl)) {
    $template = $migx->getTemplate($emptyTpl);
    if ($template[$emptyTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$emptyTpl]);
        $o = $chunk->process($properties);
    }
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $o);
    return \'\';
}

return $o;',
          'locked' => false,
          'properties' => 
          array (
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * getImageList
 *
 * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>
 *
 * getImageList is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package migx
 */
/**
 * getImageList
 *
 * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution 
 *
 * @version 1.4
 * @author Bruno Perner <b.perner@gmx.de>
 * @copyright Copyright &copy; 2009-2014
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License
 * version 2 or (at your option) any later version.
 * @package migx
 */

/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/
/* get default properties */


$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');
$inherit_children_tvname = $modx->getOption(\'inherit_children_tvname\', $scriptProperties, \'\');
$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');
$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');
$emptyTpl = $modx->getOption(\'emptyTpl\', $scriptProperties, \'\'); 
$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');
$offset = $modx->getOption(\'offset\', $scriptProperties, 0);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);
$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images
$where = $modx->getOption(\'where\', $scriptProperties, \'\');
$where = !empty($where) ? $modx->fromJSON($where) : array();
$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');
$sort = !empty($sort) ? $modx->fromJSON($sort) : array();
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');
$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);
$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');
$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');
$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');
$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));
$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;
$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');
$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');
$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');
$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');
$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');
$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;
//split json into parts
$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));
$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from
$inheritFrom = !empty($inheritFrom) ? explode(\',\', $inheritFrom) : \'\';

$modx->setPlaceholder(\'docid\', $docid);

$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);
$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);

$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);
if (!($migx instanceof Migx))
    return \'\';
$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';

if (!empty($tvname)) {
    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {

        /*
        *   get inputProperties
        */


        $properties = $tv->get(\'input_properties\');
        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();

        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');
        if (!empty($migx->config[\'configs\'])) {
            $migx->loadConfigs();
            // get tabs from file or migx-config-table
            $formtabs = $migx->getTabs();
        }
        if (empty($formtabs) && isset($properties[\'formtabs\'])) {
            //try to get formtabs and its fields from properties
            $formtabs = $modx->fromJSON($properties[\'formtabs\']);
        }

        if (!empty($properties[\'basePath\'])) {
            if ($properties[\'autoResourceFolders\'] == \'true\') {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';
            } else {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];
            }
        }
        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {
            $jsonVarKey = $properties[\'jsonvarkey\'];
            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
        }

        if (empty($outputvalue)) {
            $outputvalue = $tv->renderOutput($docid);
            if (empty($outputvalue) && !empty($inheritFrom)) {
                foreach ($inheritFrom as $from) {
                    if ($from == \'parents\') {
                        if (!empty($inherit_children_tvname)){
                            //try to get items from optional MIGX-TV for children
                            if ($inh_tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inherit_children_tvname))) {
                                $outputvalue = $inh_tv->processInheritBinding(\'\', $docid);    
                            }
                        }
                        $outputvalue = empty($outputvalue) ? $tv->processInheritBinding(\'\', $docid) : $outputvalue;
                    } else {
                        $outputvalue = $tv->renderOutput($from);
                    }
                    if (!empty($outputvalue)) {
                        break;
                    }
                }
            }
        }


        /*
        *   get inputTvs 
        */
        $inputTvs = array();
        if (is_array($formtabs)) {

            //multiple different Forms
            // Note: use same field-names and inputTVs in all forms
            $inputTvs = $migx->extractInputTvs($formtabs);
        }
        if ($migx->source = $tv->getSource($migx->working_context, false)) {
            $migx->source->initialize();
        }

    }


}

if (empty($outputvalue)) {
    $modx->setPlaceholder($totalVar, 0);
    return \'\';
}

//echo $outputvalue.\'<br/><br/>\';

$items = $modx->fromJSON($outputvalue);

// where filter
if (is_array($where) && count($where) > 0) {
    $items = $migx->filterItems($where, $items);
}
$modx->setPlaceholder($totalVar, count($items));

if (!empty($reverse)) {
    $items = array_reverse($items);
}

// sort items
if (is_array($sort) && count($sort) > 0) {
    $items = $migx->sortDbResult($items, $sort);
}

$summaries = array();
$output = \'\';
$items = $offset > 0 ? array_slice($items, $offset) : $items;
$count = count($items);

if ($count > 0) {
    $limit = $limit == 0 || $limit > $count ? $count : $limit;
    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;
    //preselect important items
    $preitems = array();
    if ($randomize && $preselectLimit > 0) {
        for ($i = 0; $i < $preselectLimit; $i++) {
            $preitems[] = $items[$i];
            unset($items[$i]);
        }
        $limit = $limit - count($preitems);
    }

    //shuffle items
    if ($randomize) {
        shuffle($items);
    }

    //limit items
    $count = count($items);
    $tempitems = array();

    for ($i = 0; $i < $limit; $i++) {
        if ($i >= $count) {
            break;
        }
        $tempitems[] = $items[$i];
    }
    $items = $tempitems;

    //add preselected items and schuffle again
    if ($randomize && $preselectLimit > 0) {
        $items = array_merge($preitems, $items);
        shuffle($items);
    }

    $properties = array();
    foreach ($scriptProperties as $property => $value) {
        $properties[\'property.\' . $property] = $value;
    }

    $idx = 0;
    $output = array();
    $template = array();
    $count = count($items);

    foreach ($items as $key => $item) {
        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';
        $fields = array();
        foreach ($item as $field => $value) {
            if (is_array($value)) {
                if (is_array($value[0])) {
                    //nested array - convert to json
                    $value = $modx->toJson($value);
                } else {
                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)
                }
            }


            $inputTVkey = $formname . $field;

            if ($processTVs && isset($inputTvs[$inputTVkey])) {
                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {

                } else {
                    $tv = $modx->newObject(\'modTemplateVar\');
                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);
                }
                $inputTV = $inputTvs[$inputTVkey];

                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');
                //don\'t manipulate any urls here
                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');
                $tv->set(\'default_text\', $value);
                $value = $tv->renderOutput($docid);
                //set option back
                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);
                //now manipulate urls
                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {
                    $mTypes = explode(\',\', $mTypes);
                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {
                        //$value = $mediasource->prepareOutputUrl($value);
                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));
                    }
                }

            }
            $fields[$field] = $value;

        }

        if (!empty($addfields)) {
            foreach ($addfields as $addfield) {
                $addfield = explode(\':\', $addfield);
                $addname = $addfield[0];
                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';
                $fields[$addname] = $adddefault;
            }
        }

        if (!empty($sumFields)) {
            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);
            foreach ($sumFields as $sumField) {
                if (isset($fields[$sumField])) {
                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];
                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];
                }
            }
        }


        if ($toJsonPlaceholder) {
            $output[] = $fields;
        } else {
            $fields[\'_alt\'] = $idx % 2;
            $idx++;
            $fields[\'_first\'] = $idx == 1 ? true : \'\';
            $fields[\'_last\'] = $idx == $limit ? true : \'\';
            $fields[\'idx\'] = $idx;
            $rowtpl = \'\';
            //get changing tpls from field
            if (substr($tpl, 0, 7) == "@FIELD:") {
                $tplField = substr($tpl, 7);
                $rowtpl = $fields[$tplField];
            }

            if ($fields[\'_first\'] && !empty($tplFirst)) {
                $rowtpl = $tplFirst;
            }
            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {
                $rowtpl = $tplLast;
            }
            $tplidx = \'tpl_\' . $idx;
            if (empty($rowtpl) && !empty($$tplidx)) {
                $rowtpl = $$tplidx;
            }
            if ($idx > 1 && empty($rowtpl)) {
                $divisors = $migx->getDivisors($idx);
                if (!empty($divisors)) {
                    foreach ($divisors as $divisor) {
                        $tplnth = \'tpl_n\' . $divisor;
                        if (!empty($$tplnth)) {
                            $rowtpl = $$tplnth;
                            if (!empty($rowtpl)) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($count == 1 && isset($tpl_oneresult)) {
                $rowtpl = $tpl_oneresult;
            }

            $fields = array_merge($fields, $properties);

            if (!empty($rowtpl)) {
                $template = $migx->getTemplate($tpl, $template);
                $fields[\'_tpl\'] = $template[$tpl];
            } else {
                $rowtpl = $tpl;

            }
            $template = $migx->getTemplate($rowtpl, $template);


            if ($template[$rowtpl]) {
                $chunk = $modx->newObject(\'modChunk\');
                $chunk->setCacheable(false);
                $chunk->setContent($template[$rowtpl]);


                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {
                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);
                } else {
                    $output[] = $chunk->process($fields);
                }
            } else {
                if (!empty($placeholdersKeyField)) {
                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                } else {
                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                }
            }
        }


    }
}

if (count($summaries) > 0) {
    $modx->toPlaceholders($summaries);
}


if ($toJsonPlaceholder) {
    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));
    return \'\';
}

if (!empty($toSeparatePlaceholders)) {
    $modx->toPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}
/*
if (!empty($outerTpl))
$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));
else 
*/

if ($count > 0 && $splits > 0) {
    $size = ceil($count / $splits);
    $chunks = array_chunk($output, $size);
    $output = array();
    foreach ($chunks as $chunk) {
        $o = implode($outputSeparator, $chunk);
        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));
    }
    $outputSeparator = $splitSeparator;
}

if (is_array($output)) {
    $o = implode($outputSeparator, $output);
} else {
    $o = $output;
}

if (!empty($o) && !empty($wrapperTpl)) {
    $template = $migx->getTemplate($wrapperTpl);
    if ($template[$wrapperTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$wrapperTpl]);
        $properties[\'output\'] = $o;
        $o = $chunk->process($properties);
    }
}

if (empty($o) && !empty($emptyTpl)) {
    $template = $migx->getTemplate($emptyTpl);
    if ($template[$emptyTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$emptyTpl]);
        $o = $chunk->process($properties);
    }
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $o);
    return \'\';
}

return $o;',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'AjaxForm' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'AjaxForm',
          'description' => '',
          'editor_type' => 0,
          'category' => 2,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */
/** @var AjaxForm $AjaxForm */
if (!$modx->loadClass(\'ajaxform\', MODX_CORE_PATH . \'components/ajaxform/model/ajaxform/\', false, true)) {
    return false;
}
$AjaxForm = new AjaxForm($modx, $scriptProperties);

$snippet = $modx->getOption(\'snippet\', $scriptProperties, \'FormIt\', true);
$tpl = $modx->getOption(\'form\', $scriptProperties, \'tpl.AjaxForm.example\', true);
$formSelector = $modx->getOption(\'formSelector\', $scriptProperties, \'ajax_form\', true);
$objectName = $modx->getOption(\'objectName\', $scriptProperties, \'AjaxForm\', true);
$AjaxForm->loadJsCss($objectName);

/** @var pdoTools $pdo */
if (class_exists(\'pdoTools\') && $pdo = $modx->getService(\'pdoTools\')) {
    $content = $pdo->getChunk($tpl, $scriptProperties);
} else {
    $content = $modx->getChunk($tpl, $scriptProperties);
}
if (empty($content)) {
    return $modx->lexicon(\'af_err_chunk_nf\', array(\'name\' => $tpl));
}

// Add selector to tag form
if (preg_match(\'#<form.*?class=(?:"|\\\')(.*?)(?:"|\\\')#i\', $content, $matches)) {
    $classes = explode(\' \', $matches[1]);
    if (!in_array($formSelector, $classes)) {
        $classes[] = $formSelector;
        $classes = preg_replace(
            \'#class=(?:"|\\\')\' . $matches[1] . \'(?:"|\\\')#i\',
            \'class="\' . implode(\' \', $classes) . \'"\',
            $matches[0]
        );
        $content = str_ireplace($matches[0], $classes, $content);
    }
} else {
    $content = str_ireplace(\'<form\', \'<form class="\' . $formSelector . \'"\', $content);
}

// Add method = post
if (preg_match(\'#<form.*?method=(?:"|\\\')(.*?)(?:"|\\\')#i\', $content)) {
    $content = preg_replace(\'#<form(.*?)method=(?:"|\\\')(.*?)(?:"|\\\')#i\', \'<form\\\\1method="post"\', $content);
} else {
    $content = str_ireplace(\'<form\', \'<form method="post"\', $content);
}

// Add action for form processing
$hash = md5(http_build_query($scriptProperties));
$action = \'<input type="hidden" name="af_action" value="\' . $hash . \'" />\';
if ((stripos($content, \'</form>\') !== false)) {
    if (preg_match(\'#<input.*?name=(?:"|\\\')af_action(?:"|\\\').*?>#i\', $content, $matches)) {
        $content = str_ireplace($matches[0], \'\', $content);
    }
    $content = str_ireplace(\'</form>\', "\\n\\t$action\\n</form>", $content);
}

// Save settings to user`s session
$_SESSION[\'AjaxForm\'][$hash] = $scriptProperties;

// Call snippet for preparation of form
$action = !empty($_REQUEST[\'af_action\'])
    ? $_REQUEST[\'af_action\']
    : $hash;

$AjaxForm->process($action, $_REQUEST);

// Return chunk
return $content;',
          'locked' => false,
          'properties' => 
          array (
            'form' => 
            array (
              'name' => 'form',
              'desc' => 'ajaxform_prop_form',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'tpl.AjaxForm.example',
              'lexicon' => 'ajaxform:properties',
              'area' => '',
              'desc_trans' => 'Чанк с формой для отправки.',
              'area_trans' => '',
            ),
            'snippet' => 
            array (
              'name' => 'snippet',
              'desc' => 'ajaxform_prop_snippet',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'FormIt',
              'lexicon' => 'ajaxform:properties',
              'area' => '',
              'desc_trans' => 'Сниппет, который будет обрабатывать указанную форму.',
              'area_trans' => '',
            ),
            'frontend_css' => 
            array (
              'name' => 'frontend_css',
              'desc' => 'ajaxform_prop_frontend_css',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '[[+assetsUrl]]css/default.css',
              'lexicon' => 'ajaxform:properties',
              'area' => '',
              'desc_trans' => 'Файл с css стилями для подключения на фронтенде.',
              'area_trans' => '',
            ),
            'frontend_js' => 
            array (
              'name' => 'frontend_js',
              'desc' => 'ajaxform_prop_frontend_js',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '[[+assetsUrl]]js/default.js',
              'lexicon' => 'ajaxform:properties',
              'area' => '',
              'desc_trans' => 'Файл с javascript для подключения на фронтенде.',
              'area_trans' => '',
            ),
            'actionUrl' => 
            array (
              'name' => 'actionUrl',
              'desc' => 'ajaxform_prop_actionUrl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '[[+assetsUrl]]action.php',
              'lexicon' => 'ajaxform:properties',
              'area' => '',
              'desc_trans' => 'Коннектор для обработки ajax запросов.',
              'area_trans' => '',
            ),
            'formSelector' => 
            array (
              'name' => 'formSelector',
              'desc' => 'ajaxform_prop_formSelector',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'ajax_form',
              'lexicon' => 'ajaxform:properties',
              'area' => '',
              'desc_trans' => 'Имя CSS класса, который будеи использован как jQuery селектор для инициализации формы. По умолчанию "ajax_form".',
              'area_trans' => '',
            ),
            'objectName' => 
            array (
              'name' => 'objectName',
              'desc' => 'ajaxform_prop_objectName',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'AjaxForm',
              'lexicon' => 'ajaxform:properties',
              'area' => '',
              'desc_trans' => 'Имя объекта для инициализации в подключаемом javascript. По умолчанию "AjaxForm".',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/ajaxform/elements/snippets/snippet.ajaxform.php',
          'content' => '/** @var array $scriptProperties */
/** @var AjaxForm $AjaxForm */
if (!$modx->loadClass(\'ajaxform\', MODX_CORE_PATH . \'components/ajaxform/model/ajaxform/\', false, true)) {
    return false;
}
$AjaxForm = new AjaxForm($modx, $scriptProperties);

$snippet = $modx->getOption(\'snippet\', $scriptProperties, \'FormIt\', true);
$tpl = $modx->getOption(\'form\', $scriptProperties, \'tpl.AjaxForm.example\', true);
$formSelector = $modx->getOption(\'formSelector\', $scriptProperties, \'ajax_form\', true);
$objectName = $modx->getOption(\'objectName\', $scriptProperties, \'AjaxForm\', true);
$AjaxForm->loadJsCss($objectName);

/** @var pdoTools $pdo */
if (class_exists(\'pdoTools\') && $pdo = $modx->getService(\'pdoTools\')) {
    $content = $pdo->getChunk($tpl, $scriptProperties);
} else {
    $content = $modx->getChunk($tpl, $scriptProperties);
}
if (empty($content)) {
    return $modx->lexicon(\'af_err_chunk_nf\', array(\'name\' => $tpl));
}

// Add selector to tag form
if (preg_match(\'#<form.*?class=(?:"|\\\')(.*?)(?:"|\\\')#i\', $content, $matches)) {
    $classes = explode(\' \', $matches[1]);
    if (!in_array($formSelector, $classes)) {
        $classes[] = $formSelector;
        $classes = preg_replace(
            \'#class=(?:"|\\\')\' . $matches[1] . \'(?:"|\\\')#i\',
            \'class="\' . implode(\' \', $classes) . \'"\',
            $matches[0]
        );
        $content = str_ireplace($matches[0], $classes, $content);
    }
} else {
    $content = str_ireplace(\'<form\', \'<form class="\' . $formSelector . \'"\', $content);
}

// Add method = post
if (preg_match(\'#<form.*?method=(?:"|\\\')(.*?)(?:"|\\\')#i\', $content)) {
    $content = preg_replace(\'#<form(.*?)method=(?:"|\\\')(.*?)(?:"|\\\')#i\', \'<form\\\\1method="post"\', $content);
} else {
    $content = str_ireplace(\'<form\', \'<form method="post"\', $content);
}

// Add action for form processing
$hash = md5(http_build_query($scriptProperties));
$action = \'<input type="hidden" name="af_action" value="\' . $hash . \'" />\';
if ((stripos($content, \'</form>\') !== false)) {
    if (preg_match(\'#<input.*?name=(?:"|\\\')af_action(?:"|\\\').*?>#i\', $content, $matches)) {
        $content = str_ireplace($matches[0], \'\', $content);
    }
    $content = str_ireplace(\'</form>\', "\\n\\t$action\\n</form>", $content);
}

// Save settings to user`s session
$_SESSION[\'AjaxForm\'][$hash] = $scriptProperties;

// Call snippet for preparation of form
$action = !empty($_REQUEST[\'af_action\'])
    ? $_REQUEST[\'af_action\']
    : $hash;

$AjaxForm->process($action, $_REQUEST);

// Return chunk
return $content;',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'recaptchav2_render' => 
      array (
        'fields' => 
        array (
          'id' => 52,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'recaptchav2_render',
          'description' => '',
          'editor_type' => 0,
          'category' => 37,
          'cache_type' => 0,
          'snippet' => '/**
 * Renders ReCaptcha V2 form
 *
 * Based on https://github.com/google/ReCAPTCHA/tree/master/php
 *
 * @copyright Copyright (c) 2014, Google Inc.
 * @link      http://www.google.com/recaptcha
 *
 * Ported to MODX by YJ Tso @sepiariver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// Register API keys at https://www.google.com/recaptcha/admin
$site_key = $modx->getOption(\'recaptchav2.site_key\', null, \'\');
// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
$lang = $modx->getOption(\'cultureKey\', null, \'en\');

$recaptcha_html = $modx->getChunk(\'recaptchav2_html\', array(
    \'site_key\' => $site_key,
    \'lang\' => $lang,
    ));

if ($hook) { 
    $hook->setValue(\'recaptchav2_html\', $recaptcha_html); // This won\'t re-render on page reload there\'s validation errors
    return true;
} else { // This works at least
    return $recaptcha_html;
}',
          'locked' => false,
          'properties' => 
          array (
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * Renders ReCaptcha V2 form
 *
 * Based on https://github.com/google/ReCAPTCHA/tree/master/php
 *
 * @copyright Copyright (c) 2014, Google Inc.
 * @link      http://www.google.com/recaptcha
 *
 * Ported to MODX by YJ Tso @sepiariver
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// Register API keys at https://www.google.com/recaptcha/admin
$site_key = $modx->getOption(\'recaptchav2.site_key\', null, \'\');
// reCAPTCHA supported 40+ languages listed here: https://developers.google.com/recaptcha/docs/language
$lang = $modx->getOption(\'cultureKey\', null, \'en\');

$recaptcha_html = $modx->getChunk(\'recaptchav2_html\', array(
    \'site_key\' => $site_key,
    \'lang\' => $lang,
    ));

if ($hook) { 
    $hook->setValue(\'recaptchav2_html\', $recaptcha_html); // This won\'t re-render on page reload there\'s validation errors
    return true;
} else { // This works at least
    return $recaptcha_html;
}',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
        ),
      ),
      'pdoResources' => 
      array (
        'fields' => 
        array (
          'id' => 31,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoResources',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */
if (isset($parents) && $parents === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
}
if (!empty($returnIds)) {
    $scriptProperties[\'return\'] = \'ids\';
}

// Adding extra parameters into special place so we can put them in a results
/** @var modSnippet $snippet */
$additionalPlaceholders = $properties = array();
if (isset($this) && $this instanceof modSnippet) {
    $properties = $this->get(\'properties\');
}
elseif ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoResources\'))) {
    $properties = $snippet->get(\'properties\');
}
if (!empty($properties)) {
    foreach ($scriptProperties as $k => $v) {
        if (!isset($properties[$k])) {
            $additionalPlaceholders[$k] = $v;
        }
    }
}
$scriptProperties[\'additionalPlaceholders\'] = $additionalPlaceholders;

/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption(\'pdoFetch.class\', null, \'pdotools.pdofetch\', true);
$path = $modx->getOption(\'pdofetch_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoFetch = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoFetch->addTime(\'pdoTools loaded\');
$output = $pdoFetch->run();

$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="pdoResourcesLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

// Return output
if (!empty($returnIds)) {
    $modx->setPlaceholder(\'pdoResources.log\', $log);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
} elseif (!empty($toSeparatePlaceholders)) {
    $output[\'log\'] = $log;
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    $output .= $log;

    if (!empty($tplWrapper) && (!empty($wrapIfEmpty) || !empty($output))) {
        $output = $pdoFetch->getChunk($tplWrapper, array_merge($additionalPlaceholders, array(\'output\' => $output)),
            $pdoFetch->config[\'fastMode\']);
    }

    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
}',
          'locked' => false,
          'properties' => 
          array (
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'pdotools_prop_tpl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для оформления ресурса. Если не указан, то содержимое полей ресурса будет распечатано на экран.',
              'area_trans' => '',
            ),
            'returnIds' => 
            array (
              'name' => 'returnIds',
              'desc' => 'pdotools_prop_returnIds',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Возвращать строку со списком id ресурсов, вместо оформленных результатов.',
              'area_trans' => '',
            ),
            'showLog' => 
            array (
              'name' => 'showLog',
              'desc' => 'pdotools_prop_showLog',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать дополнительную информацию о работе сниппета. Только для авторизованных в контекте "mgr".',
              'area_trans' => '',
            ),
            'fastMode' => 
            array (
              'name' => 'fastMode',
              'desc' => 'pdotools_prop_fastMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Быстрый режим обработки чанков. Все необработанные теги (условия, сниппеты и т.п.) будут вырезаны.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'pdotools_prop_sortby',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'publishedon',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Любое поле ресурса для сортировки, включая ТВ параметр, если он указан в параметре "includeTVs". Можно указывать JSON строку с массивом нескольких полей. Для случайно сортировки укажите "RAND()"',
              'area_trans' => '',
            ),
            'sortbyTV' => 
            array (
              'name' => 'sortbyTV',
              'desc' => 'pdotools_prop_sortbyTV',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Сортировка по ТВ параметру. Если он не указан в &includeTVs, то будет подключен автоматически.',
              'area_trans' => '',
            ),
            'sortbyTVType' => 
            array (
              'name' => 'sortbyTVType',
              'desc' => 'pdotools_prop_sortbyTVType',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Тип сортировки по ТВ параметру. Возможные варианты: string, integer, decimal и datetime. Если пусто, то ТВ будет отсортирован в зависимости от его типа: как текст, число или дата.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'pdotools_prop_sortdir',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'DESC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки: по убыванию или возрастанию.',
              'area_trans' => '',
            ),
            'sortdirTV' => 
            array (
              'name' => 'sortdirTV',
              'desc' => 'pdotools_prop_sortdirTV',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки ТВ: по убыванию или возрастанию. Если не указан, то будет равен параметру &sortdir.',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'depth' => 
            array (
              'name' => 'depth',
              'desc' => 'pdotools_prop_depth',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Глубина поиска дочерних ресурсов от родителя.',
              'area_trans' => '',
            ),
            'outputSeparator' => 
            array (
              'name' => 'outputSeparator',
              'desc' => 'pdotools_prop_outputSeparator',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '
',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Необязательная строка для разделения результатов работы.',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'pdotools_prop_parents',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список родителей, через запятую, для поиска результатов. По умолчанию выборка ограничена текущим родителем. Если поставить 0 - выборка не ограничивается. Если id родителя начинается с дефиса, он и его потомки исключается из выборки.',
              'area_trans' => '',
            ),
            'includeContent' => 
            array (
              'name' => 'includeContent',
              'desc' => 'pdotools_prop_includeContent',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включаем поле "content" в выборку.',
              'area_trans' => '',
            ),
            'includeTVs' => 
            array (
              'name' => 'includeTVs',
              'desc' => 'pdotools_prop_includeTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров для выборки, через запятую. Например: "action,time" дадут плейсхолдеры [[+action]] и [[+time]].',
              'area_trans' => '',
            ),
            'prepareTVs' => 
            array (
              'name' => 'prepareTVs',
              'desc' => 'pdotools_prop_prepareTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '1',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров, которые нужно подготовить перед выводом. По умолчанию, установлено в "1", что означает подготовку всех ТВ, указанных в "&includeTVs=``"',
              'area_trans' => '',
            ),
            'processTVs' => 
            array (
              'name' => 'processTVs',
              'desc' => 'pdotools_prop_processTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров, которые нужно обработать перед выводом. Если установить в "1" - будут обработаны все ТВ, указанные в "&includeTVs=``". По умолчанию параметр пуст.',
              'area_trans' => '',
            ),
            'tvPrefix' => 
            array (
              'name' => 'tvPrefix',
              'desc' => 'pdotools_prop_tvPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'tv.',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для ТВ параметров.',
              'area_trans' => '',
            ),
            'tvFilters' => 
            array (
              'name' => 'tvFilters',
              'desc' => 'pdotools_prop_tvFilters',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список фильтров по ТВ, с разделителями AND и OR. Разделитель, указанный в параметре "&tvFiltersOrDelimiter" представляет логическое условие OR и по нему условия группируются в первую очередь.  Внутри каждой группы вы можете задать список значений, разделив их "&tvFiltersAndDelimiter". Поиск значений может проводиться в каком-то конкретном ТВ, если он указан ("myTV==value"), или в любом ("value"). Пример вызова: "&tvFilters=`filter2==one,filter1==bar%||filter1==foo`". <br />Обратите внимание: фильтрация использует оператор LIKE и знак "%" является метасимволом. <br />И еще: Поиск идёт по значениям, которые физически находятся в БД, то есть, сюда не подставляются значения по умолчанию из настроек ТВ.',
              'area_trans' => '',
            ),
            'tvFiltersAndDelimiter' => 
            array (
              'name' => 'tvFiltersAndDelimiter',
              'desc' => 'pdotools_prop_tvFiltersAndDelimiter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => ',',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Разделитель для условий AND в параметре "&tvFilters". По умолчанию: ",".',
              'area_trans' => '',
            ),
            'tvFiltersOrDelimiter' => 
            array (
              'name' => 'tvFiltersOrDelimiter',
              'desc' => 'pdotools_prop_tvFiltersOrDelimiter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '||',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Разделитель для условий OR в параметре "&tvFilters". По умолчанию: "||".',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'pdotools_prop_where',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Массив дополнительных параметров выборки, закодированный в JSON.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'pdotools_prop_showUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать неопубликованные ресурсы.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'pdotools_prop_showDeleted',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать удалённые ресурсы.',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'pdotools_prop_showHidden',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать ресурсы, скрытые в меню.',
              'area_trans' => '',
            ),
            'hideContainers' => 
            array (
              'name' => 'hideContainers',
              'desc' => 'pdotools_prop_hideContainers',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Отключает вывод контейнеров, то есть, ресурсов с isfolder = 1.',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'pdotools_prop_context',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение выборки по контексту ресурсов.',
              'area_trans' => '',
            ),
            'idx' => 
            array (
              'name' => 'idx',
              'desc' => 'pdotools_prop_idx',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Вы можете указать стартовый номер итерации вывода результатов.',
              'area_trans' => '',
            ),
            'first' => 
            array (
              'name' => 'first',
              'desc' => 'pdotools_prop_first',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер первой итерации вывода результатов.',
              'area_trans' => '',
            ),
            'last' => 
            array (
              'name' => 'last',
              'desc' => 'pdotools_prop_last',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер последней итерации вывода результатов. По умолчанию он рассчитается автоматически, по формуле (total + first - 1).',
              'area_trans' => '',
            ),
            'tplFirst' => 
            array (
              'name' => 'tplFirst',
              'desc' => 'pdotools_prop_tplFirst',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для первого ресурса в результатах.',
              'area_trans' => '',
            ),
            'tplLast' => 
            array (
              'name' => 'tplLast',
              'desc' => 'pdotools_prop_tplLast',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для последнего ресурса в результатах.',
              'area_trans' => '',
            ),
            'tplOdd' => 
            array (
              'name' => 'tplOdd',
              'desc' => 'pdotools_prop_tplOdd',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для каждого второго ресурса.',
              'area_trans' => '',
            ),
            'tplWrapper' => 
            array (
              'name' => 'tplWrapper',
              'desc' => 'pdotools_prop_tplWrapper',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка, для заворачивания всех результатов. Понимает один плейсхолдер: [[+output]]. Не работает вместе с параметром "toSeparatePlaceholders".',
              'area_trans' => '',
            ),
            'wrapIfEmpty' => 
            array (
              'name' => 'wrapIfEmpty',
              'desc' => 'pdotools_prop_wrapIfEmpty',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включает вывод чанка-обертки (tplWrapper) даже если результатов нет.',
              'area_trans' => '',
            ),
            'totalVar' => 
            array (
              'name' => 'totalVar',
              'desc' => 'pdotools_prop_totalVar',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'total',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя плейсхолдера для сохранения общего количества результатов.',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'pdotools_prop_resources',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ресурсов, через запятую, для вывода в результатах. Если id ресурса начинается с дефиса, этот ресурс исключается из выборки.',
              'area_trans' => '',
            ),
            'tplCondition' => 
            array (
              'name' => 'tplCondition',
              'desc' => 'pdotools_prop_tplCondition',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Поле ресурса, из которого будет получено значение для выбора чанка по условию в "conditionalTpls".',
              'area_trans' => '',
            ),
            'tplOperator' => 
            array (
              'name' => 'tplOperator',
              'desc' => 'pdotools_prop_tplOperator',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'is equal to',
                  'value' => '==',
                  'name' => 'is equal to',
                ),
                1 => 
                array (
                  'text' => 'is not equal to',
                  'value' => '!=',
                  'name' => 'is not equal to',
                ),
                2 => 
                array (
                  'text' => 'less than',
                  'value' => '<',
                  'name' => 'less than',
                ),
                3 => 
                array (
                  'text' => 'less than or equal to',
                  'value' => '<=',
                  'name' => 'less than or equal to',
                ),
                4 => 
                array (
                  'text' => 'greater than or equal to',
                  'value' => '>=',
                  'name' => 'greater than or equal to',
                ),
                5 => 
                array (
                  'text' => 'is empty',
                  'value' => 'empty',
                  'name' => 'is empty',
                ),
                6 => 
                array (
                  'text' => 'is not empty',
                  'value' => '!empty',
                  'name' => 'is not empty',
                ),
                7 => 
                array (
                  'text' => 'is null',
                  'value' => 'null',
                  'name' => 'is null',
                ),
                8 => 
                array (
                  'text' => 'is in array',
                  'value' => 'inarray',
                  'name' => 'is in array',
                ),
                9 => 
                array (
                  'text' => 'is between',
                  'value' => 'between',
                  'name' => 'is between',
                ),
              ),
              'value' => '==',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Необязательный оператор для проведения сравнения поля ресурса в "tplCondition" с массивом значений и чанков в "conditionalTpls".',
              'area_trans' => '',
            ),
            'conditionalTpls' => 
            array (
              'name' => 'conditionalTpls',
              'desc' => 'pdotools_prop_conditionalTpls',
              'type' => 'textarea',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'JSON строка с массивом, у которого в ключах указано то, с чем будет сравниваться "tplCondition", а в значениях - чанки, которые будут использованы для вывода, если сравнение будет успешно. Оператор сравнения указывается в "tplOperator". Для операторов типа "isempty" можно использовать массив без ключей.',
              'area_trans' => '',
            ),
            'select' => 
            array (
              'name' => 'select',
              'desc' => 'pdotools_prop_select',
              'type' => 'textarea',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список полей для выборки, через запятую. Можно указывать JSON строку с массивом, например {"modResource":"id,pagetitle,content"}.',
              'area_trans' => '',
            ),
            'toSeparatePlaceholders' => 
            array (
              'name' => 'toSeparatePlaceholders',
              'desc' => 'pdotools_prop_toSeparatePlaceholders',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если вы укажете слово в этом параметре, то ВСЕ результаты будут выставлены в разные плейсхолдеры, начинающиеся с этого слова и заканчивающиеся порядковым номером строки, от нуля. Например, указав в параметре "myPl", вы получите плейсхолдеры [[+myPl0]], [[+myPl1]] и т.д.',
              'area_trans' => '',
            ),
            'loadModels' => 
            array (
              'name' => 'loadModels',
              'desc' => 'pdotools_prop_loadModels',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список компонентов, через запятую, чьи модели нужно загрузить для построения запроса. Например: "&loadModels=`ms2gallery,msearch2`".',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'pdotools_prop_scheme',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => '',
                  'text' => 'System default',
                  'name' => 'System default',
                ),
                1 => 
                array (
                  'value' => -1,
                  'text' => '-1 (relative to site_url)',
                  'name' => '-1 (relative to site_url)',
                ),
                2 => 
                array (
                  'value' => 'full',
                  'text' => 'full (absolute, prepended with site_url)',
                  'name' => 'full (absolute, prepended with site_url)',
                ),
                3 => 
                array (
                  'value' => 'abs',
                  'text' => 'abs (absolute, prepended with base_url)',
                  'name' => 'abs (absolute, prepended with base_url)',
                ),
                4 => 
                array (
                  'value' => 'http',
                  'text' => 'http (absolute, forced to http scheme)',
                  'name' => 'http (absolute, forced to http scheme)',
                ),
                5 => 
                array (
                  'value' => 'https',
                  'text' => 'https (absolute, forced to https scheme)',
                  'name' => 'https (absolute, forced to https scheme)',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема формирования ссылок: "uri" для подстановки поля uri документа (очень быстро) или параметр для modX::makeUrl().',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'pdotools_prop_useWeblinkUrl',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Генерировать ссылку с учетом класса ресурса.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdoresources.php',
          'content' => '/** @var array $scriptProperties */
if (isset($parents) && $parents === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
}
if (!empty($returnIds)) {
    $scriptProperties[\'return\'] = \'ids\';
}

// Adding extra parameters into special place so we can put them in a results
/** @var modSnippet $snippet */
$additionalPlaceholders = $properties = array();
if (isset($this) && $this instanceof modSnippet) {
    $properties = $this->get(\'properties\');
}
elseif ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoResources\'))) {
    $properties = $snippet->get(\'properties\');
}
if (!empty($properties)) {
    foreach ($scriptProperties as $k => $v) {
        if (!isset($properties[$k])) {
            $additionalPlaceholders[$k] = $v;
        }
    }
}
$scriptProperties[\'additionalPlaceholders\'] = $additionalPlaceholders;

/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption(\'pdoFetch.class\', null, \'pdotools.pdofetch\', true);
$path = $modx->getOption(\'pdofetch_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoFetch = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoFetch->addTime(\'pdoTools loaded\');
$output = $pdoFetch->run();

$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="pdoResourcesLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

// Return output
if (!empty($returnIds)) {
    $modx->setPlaceholder(\'pdoResources.log\', $log);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
} elseif (!empty($toSeparatePlaceholders)) {
    $output[\'log\'] = $log;
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    $output .= $log;

    if (!empty($tplWrapper) && (!empty($wrapIfEmpty) || !empty($output))) {
        $output = $pdoFetch->getChunk($tplWrapper, array_merge($additionalPlaceholders, array(\'output\' => $output)),
            $pdoFetch->config[\'fastMode\']);
    }

    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
}',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'titl' => 
      array (
        'fields' => 
        array (
          'id' => 22,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl',
          'caption' => 'Мета тайтл',
          'description' => '',
          'editor_type' => 0,
          'category' => 20,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '[[*pagetitle]] - [[++site_name]]',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[*pagetitle]] - [[++site_name]]',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'logo' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'logo',
          'caption' => 'Логотип',
          'description' => '',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell' => 
      array (
        'fields' => 
        array (
          'id' => 4,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell',
          'caption' => 'Начало номера',
          'description' => 'Первые 5 цифр номера',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 3,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell2' => 
      array (
        'fields' => 
        array (
          'id' => 5,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell2',
          'caption' => 'Остальные цыфры',
          'description' => 'остальные цифры',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 4,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'addres' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'addres',
          'caption' => 'Адрес',
          'description' => '',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'vid' => 
      array (
        'fields' => 
        array (
          'id' => 6,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'vid',
          'caption' => 'Видео ролик с главной страницы',
          'description' => 'При смене видео добавлять без расширения (mp4/org и.т.д)',
          'editor_type' => 0,
          'category' => 14,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'titletop' => 
      array (
        'fields' => 
        array (
          'id' => 7,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titletop',
          'caption' => 'Заголовок ',
          'description' => '',
          'editor_type' => 0,
          'category' => 14,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'ptit' => 
      array (
        'fields' => 
        array (
          'id' => 8,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'ptit',
          'caption' => 'Текст под заголовком',
          'description' => '',
          'editor_type' => 0,
          'category' => 14,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'zap' => 
      array (
        'fields' => 
        array (
          'id' => 9,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'zap',
          'caption' => 'Кнопка "Записаться на прием"',
          'description' => '',
          'editor_type' => 0,
          'category' => 14,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'usl' => 
      array (
        'fields' => 
        array (
          'id' => 10,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'usl',
          'caption' => 'Кнопка "Услуги  компании"',
          'description' => '',
          'editor_type' => 0,
          'category' => 14,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'title3block' => 
      array (
        'fields' => 
        array (
          'id' => 12,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'title3block',
          'caption' => 'Заголовок с 3 Блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 16,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell3' => 
      array (
        'fields' => 
        array (
          'id' => 14,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell3',
          'caption' => 'Телефон с 4 Блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'mailto' => 
      array (
        'fields' => 
        array (
          'id' => 15,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'mailto',
          'caption' => 'E-mail с 4 Блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'pn-pt' => 
      array (
        'fields' => 
        array (
          'id' => 16,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'pn-pt',
          'caption' => 'Режим работы Пн-Пт',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'sub' => 
      array (
        'fields' => 
        array (
          'id' => 17,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'sub',
          'caption' => 'Режим работы Суб',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'vih' => 
      array (
        'fields' => 
        array (
          'id' => 18,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'vih',
          'caption' => 'Выходной',
          'description' => '',
          'editor_type' => 0,
          'category' => 17,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);