<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 6,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Позитивный взгляд на детскую стоматологию',
    'longtitle' => '',
    'description' => '',
    'alias' => 'pozitivnyij-vzglyad-na-detskuyu-stomatologiyu',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 5,
    'isfolder' => 0,
    'introtext' => '	Когда-то поход к стоматологу был непростым испытанием, но современным детям повезло. Им можно лечить зубы без боли! Правда, еще нужно избавиться от страха перед визитом к доктору. Об этом, а также о современных технологиях, идеальной анестезии, грамотной профилактике и гигиене.',
    'content' => '<p><img style="margin: 0px 20px 25px 0px;" src="assets/template/images/deti1-1.jpg" align="left" /></p>
<p>Когда-то поход к стоматологу был непростым испытанием, но современным детям повезло. Им можно лечить зубы без боли! Правда, еще нужно избавиться от страха перед визитом к доктору. Об этом, а также о современных технологиях, идеальной анестезии, грамотной профилактике и гигиене вам расскажут специалисты Центра дентальной имплантации.</p>
<p>&nbsp;</p>
<h2 style="font-family: \'Roboto\'; text-transform: uppercase; font-weight: 300; margin-bottom: 0px;">Руслан Добрецов</h2>
<p><em>врач-анестезиолог-реаниматолог</em></p>
<hr style="border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);" />
<p><br /> <img style="width: 350px; margin: 0px 25px 20px 0px;" src="assets/template/images/art-1-1.jpg" align="left" /></p>
<p>В Центре дентальной имплантации мы предлагаем родителям и малышам воспользоваться современной медицинской услугой. Речь идет об обезболивании при помощи кислородно-ксеноновой ингаляции, при котором происходит погружение ребенка в состояние расслабления и покоя на период лечения. Эта методика &ndash; современная альтернатива наркозу. Она позволяет маленькому пациенту находиться в полном сознании, дышать, глотать и контактировать с врачом, позволяет вылечить зубки без страха. Рекомендую её детям от трех лет и старше. Ксенон абсолютно безопасен: в небольшой дозе он присутствует в составе воздуха, а из организма полностью выводится через легкие. Кстати, в медицине ксенон используется еще и для снятия усталости и психологических проблем. Использование ксенона в нашей стране одобрено и рекомендовано Минздравом России (Приказ Министерства здравоохранения Российской Федерации №363 от 8 октября 1999 г.). Предварительная подготовка включает в себя: отказ от приема пищи не менее чем за два часа, отказ от приема жидкостей &ndash; за час. В нашей клинике прием с применением ксеноновой седации ведется под постоянным контролем врача-анестезиолога, а также оборудования для анестезиологического мониторинга состояния пациента. Ребенок не почувствует никаких неприятных ощущений.</p>
<p>&nbsp;</p>
<h2 style="font-family: \'Roboto\'; text-transform: uppercase; font-weight: 300; margin-bottom: 0px;">Елена Якушева</h2>
<p><em>врач-терапевт, детский стоматолог</em></p>
<hr style="border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);" />
<p><br /> <img style="width: 350px; margin: 0px 25px 20px 0px;" src="assets/template/images/art-2.jpg" align="left" /></p>
<p>Маленький ребенок просто не способен долго и спокойно сидеть с открытым ртом. В клинике &laquo;Центр дентальной имплантации&raquo; детей лечат бережно заботливо и внимательно. Мы стараемся завоевать доверие ребенка и стать ему, прежде всего, другом, а потом уже врачом. Методы насильственного удержания сопротивляющегося ребенка в кресле стоматолога давно устарели и не имеют никакого отношения к качественному лечению, а главное &ndash; вызывают нежелание следить за своим здоровьем.</p>
<p>Наш центр &ndash; одна из немногих стоматологических клиник, предлагающих лечение кариеса молочных зубов с помощью светоотражаемых пломб. Часто в стоматологиях родителям говорят, что детям подобные пломбы не ставят заменяя их обычными, цементными пломбами. Большой опыт в прогрессивные технологии позволяют нам ставить светоотражаемые пломбы, которые продержатся до естественной смены молочных зубов на постоянные.</p>
<p>Многочисленные клиники г.Томска проводят лечение пульпита у детей. Но, как правило, эта процедура занимает не менее трех визитов к стоматологу. А это лишняя психологическая нагрузка и для ребенка и для родителей. Плюс увеличение стоимости лечения. В Центре дентальной имплантации лечение пульпита молочного зуба проводится всего за одно посещение! Эта методика отлично зарекомендовала себя в лучших зарубежных клиниках. Живая пульпа молочного зуба покрывается специальным биосовместимым материалом. Он затвердевает и становится надежной защитой от внешних болезнетворных бактерий. Такое лечение пульпита признано наиболее эффективными и минимально травмирующим зуб.</p>
<p>Мы рекомендуем начинать знакомство малыша со стоматологами не с лечения, а, например, с чистки зубов &ndash; после этого ребенку будет не страшно идти на следующую процедуру. Чистка зубов происходит особыми щеточками, а также зубной нитью. При необходимости проводится аппаратная гигиена зубов. После очищения зубы полируются и покрываются специальными минеральными гелями для укрепления эмали. Также нужно не забывать о приходе на профилактический прием к детскому стоматологу и чистку раз в 3-6 месяцев. При такой периодичности мы без труда сможем увидеть даже малейшие изменения эмали и предотвратить кариозное поражение зуба. Важно, чтобы родители просто помогали ребенку с домашней гигиеной полости рта.</p>
<p>Наши специалисты проводят профилактические осмотры после лечения (санации) бесплатно и после приема каждый ребенок получает награду в виде замечательной игрушки или средства ухода за полостью рта. Ждем вашего ребенка к нам в гости каждые 6 месяцев. Найдите время для профосмотра! А наши администраторы обязательно напомнят о том, что пришло время приехать к нам на прием.</p>
<p>&nbsp;</p>
<h2 style="font-family: \'Roboto\'; text-transform: uppercase; font-weight: 300; margin-bottom: 0px;">Кристина Мацук</h2>
<p><em>врач-терапевт, детский стоматолог</em></p>
<hr style="border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);" />
<p><br /> <img style="width: 350px; margin: 0px 25px 20px 0px;" src="assets/template/images/art-1-2.jpg" align="left" /></p>
<p>В развитии кариеса большую роль играют воспитание и гигиена зубов. Основной причиной кариеса являются бактерии, содержащиеся в зубном налете. Он легко образуется в результате потребления &ldquo;мягкой&rdquo;, термически обработанной углеводной пищи, которая составляет основу ежедневного рациона большинства людей. Каждый родитель должен помнить, что его ребенку необходима профилактика детского кариеса. Мы покажем малышу как правильно чистить зубы, и расскажем родителям, как выбирать средства гигиены для детских зубов. После прорезывания постоянных зубов в целях профилактики кариеса рекомендую проводить герметизацию фиссур. Фиссура - это углубление, бороздки внутри зуба, в которых скапливаются остатки пищи. Толщина эмали в этой области зуба минимальная, поэтому развитие кариеса высоковероятно. Фиссуры заполняют специальными акриловыми смолами и поверхность зуба становится ровной, а зубной налет при чистке удаляется полностью.</p>
<p>Такую не эстетическую методику укрепления зубной эмали, как серебрение молочных зубов, стараемся не использовать. Предпочитаем более современную, действенную - фторирование зубов. Родителям важно знать, что фторирование не защищает уже пораженные кариесом зубы. Покрытый фторирующей пленкой очаг заражения продолжит разрастаться вглубь, захватывая ткани зуба, расположенные под поврежденной эмалью. Единственное исключение из этого правила &ndash; самая начальная стадия кариеса (так называемая стадия пятна), когда процесс еще не проник вглубь эмали.</p>
<p>Стоит заметить, фторирование уменьшает риск возникновения кариеса на 60% и повышает прочность эмали в 10 раз, продлевая срок службы зубных пломб.</p>
<p><strong style="color: #032a4b; font-weight: 500; font-size: 15px;">Как это происходит?</strong></p>
<p>Вновь прорезавшиеся зубы у малышей еще недостаточно крепки, эмаль недостаточно минерализована, а потому может быть легко повреждена кислотами, выделяемыми кариозными бактериями. Минерализация зуба продолжается около 2-х лет. В это время зубам ребенка требуется дополнительная защита, здесь и поможет фторирование. У детей постарше меняется гормональный фон, повышается вязкость слюны, которая не может столь же эффективно орошать полость рта и очищать эмаль. К тому же к этому возрасту контроль родителей за личной гигиеной полости рта ребенка ослабевает. Наши специалисты рассчитывают на помощь и сотрудничество родителей. От этого зависит, в каком настроении ребенок придет в клинику, как он будет контактировать с доктором, успешно ли пройдет лечение.</p>
<p><strong style="color: #032a4b; font-weight: 500; font-size: 15px;">Для этого можем дать вам несколько советов:</strong></p>
<ol>
<li>Расскажите ребенку о визите к детскому стоматологу, как об интересном но рядовом событии, например, как о знакомстве с новыми друзьями.</li>
<li>С ребенком может поговорить член семьи, который сам не испытывает страха перед зубными врачами. Ребенок может почувствовать ваши растерянности и страх, и сам начнет бояться.</li>
<li>Не говорите, чтобы он &ldquo;не боялся&rdquo; или, что &ldquo;не будет больно&rdquo; или &ldquo;надо будет потерпеть&rdquo;. Мысль что посещение детского стоматолога требует смелости или может причинить боль, могла не приходить ребенку в голову.</li>
<li>Не рассказывайте ребенку о том, что ему будут делать, особенно, если это касается анестезии, удаления или пломбирования зубов. Детский стоматолог индивидуально определит, что необходимо знать вашему ребенку перед процедурой.</li>
<li>Выберите утреннее время для первого визита, чтобы ребенок не думал о предстоящем целый день. Маленькие дети как правило, ведут себя лучше в утреннее время.</li>
<li>Рекомендуем сопровождать ребенка только одному родителю. Возьмите с собой любимую книгу или игрушку ребенка. Перед тем как вас пригласят в кабинет, сядьте вместе и почитайте вслух, посмотрите картинки, поиграйте.</li>
<li>Отнеситесь к первому визиту к детскому стоматологу со всей серьезностью. Хорошее впечатление от первого посещения врача может сформировать отношение ребенка и позитивно настроить его на будущие помещения стоматолога.</li>
</ol>
<p>Доверившись специалистам Центра дентальной имплантации, вы можете быть уверены, что ваш ребенок получит только положительный опыт стоматологического лечения, что может обрести и сохранить доверие к стоматологу. А это значит прогнозируемый эффект лечения, здоровую и красивую улыбку на долгие годы.</p>
<p>&nbsp;</p>',
    'richtext' => 1,
    'template' => 4,
    'menuindex' => 0,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1501761225,
    'editedby' => 1,
    'editedon' => 1501830799,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1501761180,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html',
    'uri_override' => 0,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'titl' => 
    array (
      0 => 'titl',
      1 => '[[*pagetitle]] - [[++site_name]]',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'keyw' => 
    array (
      0 => 'keyw',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'desc' => 
    array (
      0 => 'desc',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'image' => 
    array (
      0 => 'image',
      1 => 'assets/template/images/deti1-1-small.jpg',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>Позитивный взгляд на детскую стоматологию - Центр дентальной имплантации</title>
        <base href="http://cdistom.ru/" />
        <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->

</head>
<body class="fix_scroll">
    <div class="wrapper">
    <header class="header">
			<div class="blockin">
				<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
				<a href="tel:+8 382225-70-33" class="phone_top">
					<p class="address_top">Томск, ул. Говорова, д. 19 "В", ст. 1</p>
					8 3822 <span>25-70-33</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="http://cdistom.ru/" class="">
								<img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>
     <ul class="B_crumbBox path"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Советы" href="faq/"><span itemprop="title">Советы</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Позитивный взгляд на детскую стоматологию" href="faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html"><span itemprop="title">Позитивный взгляд на детскую стоматологию</span></a></li>
</ul>
		<div class="contetn" style=" padding: 70px 0 20px 0; ">
			<div class="site_title wow fadeInUp" >
				<span>Позитивный взгляд на детскую стоматологию</span>
			</div>

		</div>
		<div class="blockin bodyfaq wow fadeInUp" style="width: 60%;font-size: 15px;line-height: 26px;text-align: justify;">
			<!--<img src="assets/template/images/deti1-1-small.jpg" align="left" style="margin: 0px 20px 25px 0px;">-->
			<p><img style="margin: 0px 20px 25px 0px;" src="assets/template/images/deti1-1.jpg" align="left" /></p>
<p>Когда-то поход к стоматологу был непростым испытанием, но современным детям повезло. Им можно лечить зубы без боли! Правда, еще нужно избавиться от страха перед визитом к доктору. Об этом, а также о современных технологиях, идеальной анестезии, грамотной профилактике и гигиене вам расскажут специалисты Центра дентальной имплантации.</p>
<p>&nbsp;</p>
<h2 style="font-family: \'Roboto\'; text-transform: uppercase; font-weight: 300; margin-bottom: 0px;">Руслан Добрецов</h2>
<p><em>врач-анестезиолог-реаниматолог</em></p>
<hr style="border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);" />
<p><br /> <img style="width: 350px; margin: 0px 25px 20px 0px;" src="assets/template/images/art-1-1.jpg" align="left" /></p>
<p>В Центре дентальной имплантации мы предлагаем родителям и малышам воспользоваться современной медицинской услугой. Речь идет об обезболивании при помощи кислородно-ксеноновой ингаляции, при котором происходит погружение ребенка в состояние расслабления и покоя на период лечения. Эта методика &ndash; современная альтернатива наркозу. Она позволяет маленькому пациенту находиться в полном сознании, дышать, глотать и контактировать с врачом, позволяет вылечить зубки без страха. Рекомендую её детям от трех лет и старше. Ксенон абсолютно безопасен: в небольшой дозе он присутствует в составе воздуха, а из организма полностью выводится через легкие. Кстати, в медицине ксенон используется еще и для снятия усталости и психологических проблем. Использование ксенона в нашей стране одобрено и рекомендовано Минздравом России (Приказ Министерства здравоохранения Российской Федерации №363 от 8 октября 1999 г.). Предварительная подготовка включает в себя: отказ от приема пищи не менее чем за два часа, отказ от приема жидкостей &ndash; за час. В нашей клинике прием с применением ксеноновой седации ведется под постоянным контролем врача-анестезиолога, а также оборудования для анестезиологического мониторинга состояния пациента. Ребенок не почувствует никаких неприятных ощущений.</p>
<p>&nbsp;</p>
<h2 style="font-family: \'Roboto\'; text-transform: uppercase; font-weight: 300; margin-bottom: 0px;">Елена Якушева</h2>
<p><em>врач-терапевт, детский стоматолог</em></p>
<hr style="border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);" />
<p><br /> <img style="width: 350px; margin: 0px 25px 20px 0px;" src="assets/template/images/art-2.jpg" align="left" /></p>
<p>Маленький ребенок просто не способен долго и спокойно сидеть с открытым ртом. В клинике &laquo;Центр дентальной имплантации&raquo; детей лечат бережно заботливо и внимательно. Мы стараемся завоевать доверие ребенка и стать ему, прежде всего, другом, а потом уже врачом. Методы насильственного удержания сопротивляющегося ребенка в кресле стоматолога давно устарели и не имеют никакого отношения к качественному лечению, а главное &ndash; вызывают нежелание следить за своим здоровьем.</p>
<p>Наш центр &ndash; одна из немногих стоматологических клиник, предлагающих лечение кариеса молочных зубов с помощью светоотражаемых пломб. Часто в стоматологиях родителям говорят, что детям подобные пломбы не ставят заменяя их обычными, цементными пломбами. Большой опыт в прогрессивные технологии позволяют нам ставить светоотражаемые пломбы, которые продержатся до естественной смены молочных зубов на постоянные.</p>
<p>Многочисленные клиники г.Томска проводят лечение пульпита у детей. Но, как правило, эта процедура занимает не менее трех визитов к стоматологу. А это лишняя психологическая нагрузка и для ребенка и для родителей. Плюс увеличение стоимости лечения. В Центре дентальной имплантации лечение пульпита молочного зуба проводится всего за одно посещение! Эта методика отлично зарекомендовала себя в лучших зарубежных клиниках. Живая пульпа молочного зуба покрывается специальным биосовместимым материалом. Он затвердевает и становится надежной защитой от внешних болезнетворных бактерий. Такое лечение пульпита признано наиболее эффективными и минимально травмирующим зуб.</p>
<p>Мы рекомендуем начинать знакомство малыша со стоматологами не с лечения, а, например, с чистки зубов &ndash; после этого ребенку будет не страшно идти на следующую процедуру. Чистка зубов происходит особыми щеточками, а также зубной нитью. При необходимости проводится аппаратная гигиена зубов. После очищения зубы полируются и покрываются специальными минеральными гелями для укрепления эмали. Также нужно не забывать о приходе на профилактический прием к детскому стоматологу и чистку раз в 3-6 месяцев. При такой периодичности мы без труда сможем увидеть даже малейшие изменения эмали и предотвратить кариозное поражение зуба. Важно, чтобы родители просто помогали ребенку с домашней гигиеной полости рта.</p>
<p>Наши специалисты проводят профилактические осмотры после лечения (санации) бесплатно и после приема каждый ребенок получает награду в виде замечательной игрушки или средства ухода за полостью рта. Ждем вашего ребенка к нам в гости каждые 6 месяцев. Найдите время для профосмотра! А наши администраторы обязательно напомнят о том, что пришло время приехать к нам на прием.</p>
<p>&nbsp;</p>
<h2 style="font-family: \'Roboto\'; text-transform: uppercase; font-weight: 300; margin-bottom: 0px;">Кристина Мацук</h2>
<p><em>врач-терапевт, детский стоматолог</em></p>
<hr style="border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);" />
<p><br /> <img style="width: 350px; margin: 0px 25px 20px 0px;" src="assets/template/images/art-1-2.jpg" align="left" /></p>
<p>В развитии кариеса большую роль играют воспитание и гигиена зубов. Основной причиной кариеса являются бактерии, содержащиеся в зубном налете. Он легко образуется в результате потребления &ldquo;мягкой&rdquo;, термически обработанной углеводной пищи, которая составляет основу ежедневного рациона большинства людей. Каждый родитель должен помнить, что его ребенку необходима профилактика детского кариеса. Мы покажем малышу как правильно чистить зубы, и расскажем родителям, как выбирать средства гигиены для детских зубов. После прорезывания постоянных зубов в целях профилактики кариеса рекомендую проводить герметизацию фиссур. Фиссура - это углубление, бороздки внутри зуба, в которых скапливаются остатки пищи. Толщина эмали в этой области зуба минимальная, поэтому развитие кариеса высоковероятно. Фиссуры заполняют специальными акриловыми смолами и поверхность зуба становится ровной, а зубной налет при чистке удаляется полностью.</p>
<p>Такую не эстетическую методику укрепления зубной эмали, как серебрение молочных зубов, стараемся не использовать. Предпочитаем более современную, действенную - фторирование зубов. Родителям важно знать, что фторирование не защищает уже пораженные кариесом зубы. Покрытый фторирующей пленкой очаг заражения продолжит разрастаться вглубь, захватывая ткани зуба, расположенные под поврежденной эмалью. Единственное исключение из этого правила &ndash; самая начальная стадия кариеса (так называемая стадия пятна), когда процесс еще не проник вглубь эмали.</p>
<p>Стоит заметить, фторирование уменьшает риск возникновения кариеса на 60% и повышает прочность эмали в 10 раз, продлевая срок службы зубных пломб.</p>
<p><strong style="color: #032a4b; font-weight: 500; font-size: 15px;">Как это происходит?</strong></p>
<p>Вновь прорезавшиеся зубы у малышей еще недостаточно крепки, эмаль недостаточно минерализована, а потому может быть легко повреждена кислотами, выделяемыми кариозными бактериями. Минерализация зуба продолжается около 2-х лет. В это время зубам ребенка требуется дополнительная защита, здесь и поможет фторирование. У детей постарше меняется гормональный фон, повышается вязкость слюны, которая не может столь же эффективно орошать полость рта и очищать эмаль. К тому же к этому возрасту контроль родителей за личной гигиеной полости рта ребенка ослабевает. Наши специалисты рассчитывают на помощь и сотрудничество родителей. От этого зависит, в каком настроении ребенок придет в клинику, как он будет контактировать с доктором, успешно ли пройдет лечение.</p>
<p><strong style="color: #032a4b; font-weight: 500; font-size: 15px;">Для этого можем дать вам несколько советов:</strong></p>
<ol>
<li>Расскажите ребенку о визите к детскому стоматологу, как об интересном но рядовом событии, например, как о знакомстве с новыми друзьями.</li>
<li>С ребенком может поговорить член семьи, который сам не испытывает страха перед зубными врачами. Ребенок может почувствовать ваши растерянности и страх, и сам начнет бояться.</li>
<li>Не говорите, чтобы он &ldquo;не боялся&rdquo; или, что &ldquo;не будет больно&rdquo; или &ldquo;надо будет потерпеть&rdquo;. Мысль что посещение детского стоматолога требует смелости или может причинить боль, могла не приходить ребенку в голову.</li>
<li>Не рассказывайте ребенку о том, что ему будут делать, особенно, если это касается анестезии, удаления или пломбирования зубов. Детский стоматолог индивидуально определит, что необходимо знать вашему ребенку перед процедурой.</li>
<li>Выберите утреннее время для первого визита, чтобы ребенок не думал о предстоящем целый день. Маленькие дети как правило, ведут себя лучше в утреннее время.</li>
<li>Рекомендуем сопровождать ребенка только одному родителю. Возьмите с собой любимую книгу или игрушку ребенка. Перед тем как вас пригласят в кабинет, сядьте вместе и почитайте вслух, посмотрите картинки, поиграйте.</li>
<li>Отнеситесь к первому визиту к детскому стоматологу со всей серьезностью. Хорошее впечатление от первого посещения врача может сформировать отношение ребенка и позитивно настроить его на будущие помещения стоматолога.</li>
</ol>
<p>Доверившись специалистам Центра дентальной имплантации, вы можете быть уверены, что ваш ребенок получит только положительный опыт стоматологического лечения, что может обрести и сохранить доверие к стоматологу. А это значит прогнозируемый эффект лечения, здоровую и красивую улыбку на долгие годы.</p>
<p>&nbsp;</p>
		</div>

		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="implantation.html">Имплантация</a></li>
						<li><a href="services.html">Детская стоматология</a></li>
						<li><a href="terapia.html">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="hygiene.html">Гигиена и профилактика</a></li>
						<li><a href="proteth.html">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> 8:30-20:00</li>
						<li><strong>Сб: </strong> 8:30-14:00</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> 25-70-33</li>
						<li><strong>E-mail: </strong> <a href="mailto:cdistom@mail.ru"> cdistom@mail.ru</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br>  ул. Говорова, д. 19 "В", ст. 1</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="https://vk.com/cdistom"></a></li>
							<li><a href="https://www.facebook.com/tomskimplant/?modal=composer"></a></li>
							<li><a href="https://www.instagram.com/tomstom_implant/?hl=ru"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="contacts.html" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[$head]]' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>Позитивный взгляд на детскую стоматологию - Центр дентальной имплантации</title>
        <base href="http://cdistom.ru/" />
        <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
    '[[$header]]' => '<header class="header">
			<div class="blockin">
				<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
				<a href="tel:+8 382225-70-33" class="phone_top">
					<p class="address_top">Томск, ул. Говорова, д. 19 "В", ст. 1</p>
					8 3822 <span>25-70-33</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="http://cdistom.ru/" class="">
								<img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
    '[[$?resource=`6`&description=`Позитивный взгляд на детскую стоматологию`&text=`Позитивный взгляд на детскую стоматологию`]]' => '<a class="B_currentCrumb" itemprop="url" rel="Позитивный взгляд на детскую стоматологию" href="faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html"><span itemprop="title">Позитивный взгляд на детскую стоматологию</span></a>',
    '[[$?resource=`5`&description=`Советы`&text=`Советы`]]' => '<a class="B_crumb" itemprop="url" rel="Советы" href="faq/"><span itemprop="title">Советы</span></a>',
    '[[$?description=`Главная`&text=`Главная`]]' => '<a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a>',
    '[[$?text=`<a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a>`]]' => '<li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>',
    '[[$?text=`<a class="B_crumb" itemprop="url" rel="Советы" href="faq/"><span itemprop="title">Советы</span></a>`]]' => '<li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Советы" href="faq/"><span itemprop="title">Советы</span></a></li>',
    '[[$?text=`<a class="B_currentCrumb" itemprop="url" rel="Позитивный взгляд на детскую стоматологию" href="faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html"><span itemprop="title">Позитивный взгляд на детскую стоматологию</span></a>`]]' => '<li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Позитивный взгляд на детскую стоматологию" href="faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html"><span itemprop="title">Позитивный взгляд на детскую стоматологию</span></a></li>',
    '[[$?text=`<li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Советы" href="faq/"><span itemprop="title">Советы</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Позитивный взгляд на детскую стоматологию" href="faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html"><span itemprop="title">Позитивный взгляд на детскую стоматологию</span></a></li>
`]]' => '<ul class="B_crumbBox path"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Советы" href="faq/"><span itemprop="title">Советы</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Позитивный взгляд на детскую стоматологию" href="faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html"><span itemprop="title">Позитивный взгляд на детскую стоматологию</span></a></li>
</ul>',
    '[[Breadcrumbs]]' => '<ul class="B_crumbBox path"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Советы" href="faq/"><span itemprop="title">Советы</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Позитивный взгляд на детскую стоматологию" href="faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html"><span itemprop="title">Позитивный взгляд на детскую стоматологию</span></a></li>
</ul>',
    '[[$Breadcrumbs]]' => '<ul class="B_crumbBox path"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Советы" href="faq/"><span itemprop="title">Советы</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Позитивный взгляд на детскую стоматологию" href="faq/pozitivnyij-vzglyad-na-detskuyu-stomatologiyu.html"><span itemprop="title">Позитивный взгляд на детскую стоматологию</span></a></li>
</ul>',
    '[[$footer]]' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="implantation.html">Имплантация</a></li>
						<li><a href="services.html">Детская стоматология</a></li>
						<li><a href="terapia.html">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="hygiene.html">Гигиена и профилактика</a></li>
						<li><a href="proteth.html">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> 8:30-20:00</li>
						<li><strong>Сб: </strong> 8:30-14:00</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> 25-70-33</li>
						<li><strong>E-mail: </strong> <a href="mailto:cdistom@mail.ru"> cdistom@mail.ru</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br>  ул. Говорова, д. 19 "В", ст. 1</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="https://vk.com/cdistom"></a></li>
							<li><a href="https://www.facebook.com/tomskimplant/?modal=composer"></a></li>
							<li><a href="https://www.instagram.com/tomstom_implant/?hl=ru"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="contacts.html" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'head',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>[[*titl]]</title>
        <base href="[[++site_url]]" />
        <meta charset="[[++modx_charset]]" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>[[*titl]]</title>
        <base href="[[++site_url]]" />
        <meta charset="[[++modx_charset]]" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header' => 
      array (
        'fields' => 
        array (
          'id' => 15,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'header',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<header class="header">
			<div class="blockin">
				<a href="[[++site_url]]" class="logo"><img src="[[#1.tv.logo]]" alt=""></a>
				<a href="tel:+[[#1.tv.tell]][[#1.tv.tell2]]" class="phone_top">
					<p class="address_top">[[#1.tv.addres]]</p>
					[[#1.tv.tell]] <span>[[#1.tv.tell2]]</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="[[++site_url]]" class="">
								<img src="[[#1.tv.logo]]" alt="[[++site_name]]">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<header class="header">
			<div class="blockin">
				<a href="[[++site_url]]" class="logo"><img src="[[#1.tv.logo]]" alt=""></a>
				<a href="tel:+[[#1.tv.tell]][[#1.tv.tell2]]" class="phone_top">
					<p class="address_top">[[#1.tv.addres]]</p>
					[[#1.tv.tell]] <span>[[#1.tv.tell2]]</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="[[++site_url]]" class="">
								<img src="[[#1.tv.logo]]" alt="[[++site_name]]">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'Breadcrumbs' => 
      array (
        'fields' => 
        array (
          'id' => 38,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'Breadcrumbs',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '[[Breadcrumbs]]',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[Breadcrumbs]]',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'footer' => 
      array (
        'fields' => 
        array (
          'id' => 27,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="[[~11]]">Имплантация</a></li>
						<li><a href="[[~15]]">Детская стоматология</a></li>
						<li><a href="[[~12]]">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="[[~14]]">Гигиена и профилактика</a></li>
						<li><a href="[[~13]]">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> [[++ot]]</li>
						<li><strong>Сб: </strong> [[++sub]]</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> [[++num]]</li>
						<li><strong>E-mail: </strong> <a href="mailto:[[++emailt]]"> [[++emailt]]</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br> [[++uli]]</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="[[++vk]]"></a></li>
							<li><a href="[[++fb]]"></a></li>
							<li><a href="[[++ig]]"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="[[~17]]" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="[[~11]]">Имплантация</a></li>
						<li><a href="[[~15]]">Детская стоматология</a></li>
						<li><a href="[[~12]]">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="[[~14]]">Гигиена и профилактика</a></li>
						<li><a href="[[~13]]">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> [[++ot]]</li>
						<li><strong>Сб: </strong> [[++sub]]</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> [[++num]]</li>
						<li><strong>E-mail: </strong> <a href="mailto:[[++emailt]]"> [[++emailt]]</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br> [[++uli]]</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="[[++vk]]"></a></li>
							<li><a href="[[++fb]]"></a></li>
							<li><a href="[[++ig]]"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="[[~17]]" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'Breadcrumbs' => 
      array (
        'fields' => 
        array (
          'id' => 50,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Breadcrumbs',
          'description' => '',
          'editor_type' => 0,
          'category' => 34,
          'cache_type' => 0,
          'snippet' => '/**
 * BreadCrumbs
 *
 * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>
 *
 * BreadCrumbs is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package breadcrumbs
 */
/**
 * @name BreadCrumbs
 * @version 0.9f
 * @created 2006-06-12
 * @since 2009-05-11
 * @author Jared <jaredc@honeydewdesign.com>
 * @editor Bill Wilson
 * @editor wendy@djamoer.net
 * @editor grad
 * @editor Shaun McCormick <shaun@collabpad.com>
 * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>
 * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>
 * @tester Bob Ray
 * @package breadcrumbs
 *
 * This snippet was designed to show the path through the various levels of site
 * structure back to the root. It is NOT necessarily the path the user took to
 * arrive at a given page.
 *
 * @see breadcrumbs.class.php for config settings
 *
 * Included classes
 * .B_crumbBox Span that surrounds all crumb output
 * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown
 * .B_currentCrumb Span or A tag surrounding the current crumb
 * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not
 * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .
 * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or
 * hide)
 * .B_homeCrumb Class given to the home crumb
 */
require_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';
$bc = new BreadCrumbs($modx,$scriptProperties);
return $bc->run();',
          'locked' => false,
          'properties' => NULL,
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * BreadCrumbs
 *
 * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>
 *
 * BreadCrumbs is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package breadcrumbs
 */
/**
 * @name BreadCrumbs
 * @version 0.9f
 * @created 2006-06-12
 * @since 2009-05-11
 * @author Jared <jaredc@honeydewdesign.com>
 * @editor Bill Wilson
 * @editor wendy@djamoer.net
 * @editor grad
 * @editor Shaun McCormick <shaun@collabpad.com>
 * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>
 * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>
 * @tester Bob Ray
 * @package breadcrumbs
 *
 * This snippet was designed to show the path through the various levels of site
 * structure back to the root. It is NOT necessarily the path the user took to
 * arrive at a given page.
 *
 * @see breadcrumbs.class.php for config settings
 *
 * Included classes
 * .B_crumbBox Span that surrounds all crumb output
 * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown
 * .B_currentCrumb Span or A tag surrounding the current crumb
 * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not
 * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .
 * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or
 * hide)
 * .B_homeCrumb Class given to the home crumb
 */
require_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';
$bc = new BreadCrumbs($modx,$scriptProperties);
return $bc->run();',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'pdoMenu' => 
      array (
        'fields' => 
        array (
          'id' => 38,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoMenu',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */

// Convert parameters from Wayfinder if exists
if (isset($startId)) {
    $scriptProperties[\'parents\'] = $startId;
}
if (!empty($includeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $includeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',\' . $v;
        } else {
            $scriptProperties[\'resources\'] = $v;
        }
    }
}
if (!empty($excludeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $excludeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',-\' . $v;
        } else {
            $scriptProperties[\'resources\'] = \'-\' . $v;
        }
    }
}

if (!empty($previewUnpublished) && $modx->hasPermission(\'view_unpublished\')) {
    $scriptProperties[\'showUnpublished\'] = 1;
}

$scriptProperties[\'depth\'] = empty($level) ? 100 : abs($level) - 1;
if (!empty($contexts)) {
    $scriptProperties[\'context\'] = $contexts;
}
if (empty($scriptProperties[\'context\'])) {
    $scriptProperties[\'context\'] = $modx->resource->context_key;
}

// Save original parents specified by user
$specified_parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));

if ($scriptProperties[\'parents\'] === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
} elseif ($scriptProperties[\'parents\'] === 0 || $scriptProperties[\'parents\'] === \'0\') {
    if ($scriptProperties[\'depth\'] !== \'\' && $scriptProperties[\'depth\'] !== 100) {
        $contexts = array_map(\'trim\', explode(\',\', $scriptProperties[\'context\']));
        $parents = array();
        if (!empty($scriptProperties[\'showDeleted\'])) {
            $pdoFetch = $modx->getService(\'pdoFetch\');
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $pdoFetch->getChildIds(\'modResource\', 0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        } else {
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $modx->getChildIds(0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        }
        $scriptProperties[\'parents\'] = !empty($parents) ? implode(\',\', $parents) : \'+0\';
        $scriptProperties[\'depth\'] = 0;
    }
    $scriptProperties[\'includeParents\'] = 1;
    $scriptProperties[\'displayStart\'] = 0;
} else {
    $parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));
    $parents_in = $parents_out = array();
    foreach ($parents as $v) {
        if (!is_numeric($v)) {
            continue;
        }
        if ($v[0] == \'-\') {
            $parents_out[] = abs($v);
        } else {
            $parents_in[] = abs($v);
        }
    }

    if (empty($parents_in)) {
        $scriptProperties[\'includeParents\'] = 1;
        $scriptProperties[\'displayStart\'] = 0;
    }
}

if (!empty($displayStart)) {
    $scriptProperties[\'includeParents\'] = 1;
}
if (!empty($ph)) {
    $toPlaceholder = $ph;
}
if (!empty($sortOrder)) {
    $scriptProperties[\'sortdir\'] = $sortOrder;
}
if (!empty($sortBy)) {
    $scriptProperties[\'sortby\'] = $sortBy;
}
if (!empty($permissions)) {
    $scriptProperties[\'checkPermissions\'] = $permissions;
}
if (!empty($cacheResults)) {
    $scriptProperties[\'cache\'] = $cacheResults;
}
if (!empty($ignoreHidden)) {
    $scriptProperties[\'showHidden\'] = $ignoreHidden;
}

$wfTemplates = array(
    \'outerTpl\' => \'tplOuter\',
    \'rowTpl\' => \'tpl\',
    \'parentRowTpl\' => \'tplParentRow\',
    \'parentRowHereTpl\' => \'tplParentRowHere\',
    \'hereTpl\' => \'tplHere\',
    \'innerTpl\' => \'tplInner\',
    \'innerRowTpl\' => \'tplInnerRow\',
    \'innerHereTpl\' => \'tplInnerHere\',
    \'activeParentRowTpl\' => \'tplParentRowActive\',
    \'categoryFoldersTpl\' => \'tplCategoryFolder\',
    \'startItemTpl\' => \'tplStart\',
);
foreach ($wfTemplates as $k => $v) {
    if (isset(${$k})) {
        $scriptProperties[$v] = ${$k};
    }
}
//---

/** @var pdoMenu $pdoMenu */
$fqn = $modx->getOption(\'pdoMenu.class\', null, \'pdotools.pdomenu\', true);
$path = $modx->getOption(\'pdomenu_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoMenu = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoMenu->pdoTools->addTime(\'pdoTools loaded\');

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
if (empty($scriptProperties[\'cache_key\'])) {
    $scriptProperties[\'cache_key\'] = \'pdomenu/\' . sha1(serialize($scriptProperties));
}

$output = \'\';
$tree = array();
if ($cache) {
    $tree = $pdoMenu->pdoTools->getCache($scriptProperties);
}
if (empty($tree)) {
    $data = $pdoMenu->pdoTools->run();
    $data = $pdoMenu->pdoTools->buildTree($data, \'id\', \'parent\', $specified_parents);
    $tree = array();
    foreach ($data as $k => $v) {
        if (empty($v[\'id\'])) {
            if (!in_array($k, $specified_parents) && !$pdoMenu->checkResource($k)) {
                continue;
            } else {
                $tree = array_merge($tree, $v[\'children\']);
            }
        } else {
            $tree[$k] = $v;
        }
    }
    if ($cache) {
        $pdoMenu->pdoTools->setCache($tree, $scriptProperties);
    }
}
if (!empty($tree)) {
    $output = $pdoMenu->templateTree($tree);
}

if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $output .= \'<pre class="pdoMenuLog">\' . print_r($pdoMenu->pdoTools->getTime(), 1) . \'</pre>\';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
          'locked' => false,
          'properties' => 
          array (
            'showLog' => 
            array (
              'name' => 'showLog',
              'desc' => 'pdotools_prop_showLog',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать дополнительную информацию о работе сниппета. Только для авторизованных в контекте "mgr".',
              'area_trans' => '',
            ),
            'fastMode' => 
            array (
              'name' => 'fastMode',
              'desc' => 'pdotools_prop_fastMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Быстрый режим обработки чанков. Все необработанные теги (условия, сниппеты и т.п.) будут вырезаны.',
              'area_trans' => '',
            ),
            'level' => 
            array (
              'name' => 'level',
              'desc' => 'pdotools_prop_level',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Уровень генерируемого меню.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'pdotools_prop_parents',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список родителей, через запятую, для поиска результатов. По умолчанию выборка ограничена текущим родителем. Если поставить 0 - выборка не ограничивается. Если id родителя начинается с дефиса, он и его потомки исключается из выборки.',
              'area_trans' => '',
            ),
            'displayStart' => 
            array (
              'name' => 'displayStart',
              'desc' => 'pdotools_prop_displayStart',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить показ начальных узлов меню. Полезно при указании более одного "parents".',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'pdotools_prop_resources',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ресурсов, через запятую, для вывода в результатах. Если id ресурса начинается с дефиса, этот ресурс исключается из выборки.',
              'area_trans' => '',
            ),
            'templates' => 
            array (
              'name' => 'templates',
              'desc' => 'pdotools_prop_templates',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список шаблонов, через запятую, для фильтрации результатов. Если id шаблона начинается с дефиса, ресурсы с ним исключается из выборки.',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'pdotools_prop_context',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение выборки по контексту ресурсов.',
              'area_trans' => '',
            ),
            'cache' => 
            array (
              'name' => 'cache',
              'desc' => 'pdotools_prop_cache',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Кэширование результатов работы сниппета.',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'pdotools_prop_cacheTime',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 3600,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Время актуальности кэша в секундах.',
              'area_trans' => '',
            ),
            'cacheAnonymous' => 
            array (
              'name' => 'cacheAnonymous',
              'desc' => 'pdotools_prop_cacheAnonymous',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить кэширование только для неавторизованных посетителей.',
              'area_trans' => '',
            ),
            'plPrefix' => 
            array (
              'name' => 'plPrefix',
              'desc' => 'pdotools_prop_plPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'wf.',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для выставляемых плейсхолдеров, по умолчанию "wf.".',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'pdotools_prop_showHidden',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать ресурсы, скрытые в меню.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'pdotools_prop_showUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать неопубликованные ресурсы.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'pdotools_prop_showDeleted',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать удалённые ресурсы.',
              'area_trans' => '',
            ),
            'previewUnpublished' => 
            array (
              'name' => 'previewUnpublished',
              'desc' => 'pdotools_prop_previewUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить показ неопубликованных документов, если у пользователя есть на это разрешение.',
              'area_trans' => '',
            ),
            'hideSubMenus' => 
            array (
              'name' => 'hideSubMenus',
              'desc' => 'pdotools_prop_hideSubMenus',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Спрятать неактивные ветки меню.',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'pdotools_prop_useWeblinkUrl',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Генерировать ссылку с учетом класса ресурса.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'pdotools_prop_sortdir',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки: по убыванию или возрастанию.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'pdotools_prop_sortby',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'menuindex',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Любое поле ресурса для сортировки, включая ТВ параметр, если он указан в параметре "includeTVs". Можно указывать JSON строку с массивом нескольких полей. Для случайно сортировки укажите "RAND()"',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'rowIdPrefix' => 
            array (
              'name' => 'rowIdPrefix',
              'desc' => 'pdotools_prop_rowIdPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс id="" для выставления идентификатора в чанк.',
              'area_trans' => '',
            ),
            'firstClass' => 
            array (
              'name' => 'firstClass',
              'desc' => 'pdotools_prop_firstClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'first',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс для первого пункта меню.',
              'area_trans' => '',
            ),
            'lastClass' => 
            array (
              'name' => 'lastClass',
              'desc' => 'pdotools_prop_lastClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'last',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс последнего пункта меню.',
              'area_trans' => '',
            ),
            'hereClass' => 
            array (
              'name' => 'hereClass',
              'desc' => 'pdotools_prop_hereClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'active',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс для активного пункта меню.',
              'area_trans' => '',
            ),
            'parentClass' => 
            array (
              'name' => 'parentClass',
              'desc' => 'pdotools_prop_parentClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс категории меню.',
              'area_trans' => '',
            ),
            'rowClass' => 
            array (
              'name' => 'rowClass',
              'desc' => 'pdotools_prop_rowClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс одной строки меню.',
              'area_trans' => '',
            ),
            'outerClass' => 
            array (
              'name' => 'outerClass',
              'desc' => 'pdotools_prop_outerClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс обертки меню.',
              'area_trans' => '',
            ),
            'innerClass' => 
            array (
              'name' => 'innerClass',
              'desc' => 'pdotools_prop_innerClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс внутренних ссылок меню.',
              'area_trans' => '',
            ),
            'levelClass' => 
            array (
              'name' => 'levelClass',
              'desc' => 'pdotools_prop_levelClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс уровня меню. Например, если укажите "level", то будет "level1", "level2" и т.д.',
              'area_trans' => '',
            ),
            'selfClass' => 
            array (
              'name' => 'selfClass',
              'desc' => 'pdotools_prop_selfClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс текущего документа в меню.',
              'area_trans' => '',
            ),
            'webLinkClass' => 
            array (
              'name' => 'webLinkClass',
              'desc' => 'pdotools_prop_webLinkClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс документа-ссылки.',
              'area_trans' => '',
            ),
            'tplOuter' => 
            array (
              'name' => 'tplOuter',
              'desc' => 'pdotools_prop_tplOuter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <ul[[+classes]]>[[+wrapper]]</ul>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка всего блока меню.',
              'area_trans' => '',
            ),
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'pdotools_prop_tpl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li[[+classes]]><a href="[[+link]]" [[+attributes]]>[[+menutitle]]</a>[[+wrapper]]</li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для оформления ресурса. Если не указан, то содержимое полей ресурса будет распечатано на экран.',
              'area_trans' => '',
            ),
            'tplParentRow' => 
            array (
              'name' => 'tplParentRow',
              'desc' => 'pdotools_prop_tplParentRow',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplParentRowHere' => 
            array (
              'name' => 'tplParentRowHere',
              'desc' => 'pdotools_prop_tplParentRowHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления текущего контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplHere' => 
            array (
              'name' => 'tplHere',
              'desc' => 'pdotools_prop_tplHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк текущего документа',
              'area_trans' => '',
            ),
            'tplInner' => 
            array (
              'name' => 'tplInner',
              'desc' => 'pdotools_prop_tplInner',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка внутренних пунктов меню. Если пуст - будет использовать "tplInner".',
              'area_trans' => '',
            ),
            'tplInnerRow' => 
            array (
              'name' => 'tplInnerRow',
              'desc' => 'pdotools_prop_tplInnerRow',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка активного пункта меню.',
              'area_trans' => '',
            ),
            'tplInnerHere' => 
            array (
              'name' => 'tplInnerHere',
              'desc' => 'pdotools_prop_tplInnerHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка активного пункта меню.',
              'area_trans' => '',
            ),
            'tplParentRowActive' => 
            array (
              'name' => 'tplParentRowActive',
              'desc' => 'pdotools_prop_tplParentRowActive',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления активного контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplCategoryFolder' => 
            array (
              'name' => 'tplCategoryFolder',
              'desc' => 'pdotools_prop_tplCategoryFolder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Специальный чанк оформления категории. Категория - это документ с потомками и или нулевым шаблоном, или с атрибутом "rel=\\"category\\"".',
              'area_trans' => '',
            ),
            'tplStart' => 
            array (
              'name' => 'tplStart',
              'desc' => 'pdotools_prop_tplStart',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <h2[[+classes]]>[[+menutitle]]</h2>[[+wrapper]]',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления корневого пункта, при условии, что включен "displayStart".',
              'area_trans' => '',
            ),
            'checkPermissions' => 
            array (
              'name' => 'checkPermissions',
              'desc' => 'pdotools_prop_checkPermissions',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Укажите, какие разрешения нужно проверять у пользователя при выводе документов.',
              'area_trans' => '',
            ),
            'hereId' => 
            array (
              'name' => 'hereId',
              'desc' => 'pdotools_prop_hereId',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Id документа, текущего для генерируемого меню. Нужно указывать только если скрипт сам его неверно определяет, например при выводе меню из чанка другого сниппета.',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'pdotools_prop_where',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Массив дополнительных параметров выборки, закодированный в JSON.',
              'area_trans' => '',
            ),
            'select' => 
            array (
              'name' => 'select',
              'desc' => 'pdotools_prop_select',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список полей для выборки, через запятую. Можно указывать JSON строку с массивом, например {"modResource":"id,pagetitle,content"}.',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'pdotools_prop_scheme',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => '',
                  'text' => 'System default',
                  'name' => 'System default',
                ),
                1 => 
                array (
                  'value' => -1,
                  'text' => '-1 (relative to site_url)',
                  'name' => '-1 (relative to site_url)',
                ),
                2 => 
                array (
                  'value' => 'full',
                  'text' => 'full (absolute, prepended with site_url)',
                  'name' => 'full (absolute, prepended with site_url)',
                ),
                3 => 
                array (
                  'value' => 'abs',
                  'text' => 'abs (absolute, prepended with base_url)',
                  'name' => 'abs (absolute, prepended with base_url)',
                ),
                4 => 
                array (
                  'value' => 'http',
                  'text' => 'http (absolute, forced to http scheme)',
                  'name' => 'http (absolute, forced to http scheme)',
                ),
                5 => 
                array (
                  'value' => 'https',
                  'text' => 'https (absolute, forced to https scheme)',
                  'name' => 'https (absolute, forced to https scheme)',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема формирования ссылок: "uri" для подстановки поля uri документа (очень быстро) или параметр для modX::makeUrl().',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'countChildren' => 
            array (
              'name' => 'countChildren',
              'desc' => 'pdotools_prop_countChildren',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Вывести точное количество активных потомков документа в плейсхолдер [[+children]].',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdomenu.php',
          'content' => '/** @var array $scriptProperties */

// Convert parameters from Wayfinder if exists
if (isset($startId)) {
    $scriptProperties[\'parents\'] = $startId;
}
if (!empty($includeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $includeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',\' . $v;
        } else {
            $scriptProperties[\'resources\'] = $v;
        }
    }
}
if (!empty($excludeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $excludeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',-\' . $v;
        } else {
            $scriptProperties[\'resources\'] = \'-\' . $v;
        }
    }
}

if (!empty($previewUnpublished) && $modx->hasPermission(\'view_unpublished\')) {
    $scriptProperties[\'showUnpublished\'] = 1;
}

$scriptProperties[\'depth\'] = empty($level) ? 100 : abs($level) - 1;
if (!empty($contexts)) {
    $scriptProperties[\'context\'] = $contexts;
}
if (empty($scriptProperties[\'context\'])) {
    $scriptProperties[\'context\'] = $modx->resource->context_key;
}

// Save original parents specified by user
$specified_parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));

if ($scriptProperties[\'parents\'] === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
} elseif ($scriptProperties[\'parents\'] === 0 || $scriptProperties[\'parents\'] === \'0\') {
    if ($scriptProperties[\'depth\'] !== \'\' && $scriptProperties[\'depth\'] !== 100) {
        $contexts = array_map(\'trim\', explode(\',\', $scriptProperties[\'context\']));
        $parents = array();
        if (!empty($scriptProperties[\'showDeleted\'])) {
            $pdoFetch = $modx->getService(\'pdoFetch\');
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $pdoFetch->getChildIds(\'modResource\', 0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        } else {
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $modx->getChildIds(0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        }
        $scriptProperties[\'parents\'] = !empty($parents) ? implode(\',\', $parents) : \'+0\';
        $scriptProperties[\'depth\'] = 0;
    }
    $scriptProperties[\'includeParents\'] = 1;
    $scriptProperties[\'displayStart\'] = 0;
} else {
    $parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));
    $parents_in = $parents_out = array();
    foreach ($parents as $v) {
        if (!is_numeric($v)) {
            continue;
        }
        if ($v[0] == \'-\') {
            $parents_out[] = abs($v);
        } else {
            $parents_in[] = abs($v);
        }
    }

    if (empty($parents_in)) {
        $scriptProperties[\'includeParents\'] = 1;
        $scriptProperties[\'displayStart\'] = 0;
    }
}

if (!empty($displayStart)) {
    $scriptProperties[\'includeParents\'] = 1;
}
if (!empty($ph)) {
    $toPlaceholder = $ph;
}
if (!empty($sortOrder)) {
    $scriptProperties[\'sortdir\'] = $sortOrder;
}
if (!empty($sortBy)) {
    $scriptProperties[\'sortby\'] = $sortBy;
}
if (!empty($permissions)) {
    $scriptProperties[\'checkPermissions\'] = $permissions;
}
if (!empty($cacheResults)) {
    $scriptProperties[\'cache\'] = $cacheResults;
}
if (!empty($ignoreHidden)) {
    $scriptProperties[\'showHidden\'] = $ignoreHidden;
}

$wfTemplates = array(
    \'outerTpl\' => \'tplOuter\',
    \'rowTpl\' => \'tpl\',
    \'parentRowTpl\' => \'tplParentRow\',
    \'parentRowHereTpl\' => \'tplParentRowHere\',
    \'hereTpl\' => \'tplHere\',
    \'innerTpl\' => \'tplInner\',
    \'innerRowTpl\' => \'tplInnerRow\',
    \'innerHereTpl\' => \'tplInnerHere\',
    \'activeParentRowTpl\' => \'tplParentRowActive\',
    \'categoryFoldersTpl\' => \'tplCategoryFolder\',
    \'startItemTpl\' => \'tplStart\',
);
foreach ($wfTemplates as $k => $v) {
    if (isset(${$k})) {
        $scriptProperties[$v] = ${$k};
    }
}
//---

/** @var pdoMenu $pdoMenu */
$fqn = $modx->getOption(\'pdoMenu.class\', null, \'pdotools.pdomenu\', true);
$path = $modx->getOption(\'pdomenu_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoMenu = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoMenu->pdoTools->addTime(\'pdoTools loaded\');

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
if (empty($scriptProperties[\'cache_key\'])) {
    $scriptProperties[\'cache_key\'] = \'pdomenu/\' . sha1(serialize($scriptProperties));
}

$output = \'\';
$tree = array();
if ($cache) {
    $tree = $pdoMenu->pdoTools->getCache($scriptProperties);
}
if (empty($tree)) {
    $data = $pdoMenu->pdoTools->run();
    $data = $pdoMenu->pdoTools->buildTree($data, \'id\', \'parent\', $specified_parents);
    $tree = array();
    foreach ($data as $k => $v) {
        if (empty($v[\'id\'])) {
            if (!in_array($k, $specified_parents) && !$pdoMenu->checkResource($k)) {
                continue;
            } else {
                $tree = array_merge($tree, $v[\'children\']);
            }
        } else {
            $tree[$k] = $v;
        }
    }
    if ($cache) {
        $pdoMenu->pdoTools->setCache($tree, $scriptProperties);
    }
}
if (!empty($tree)) {
    $output = $pdoMenu->templateTree($tree);
}

if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $output .= \'<pre class="pdoMenuLog">\' . print_r($pdoMenu->pdoTools->getTime(), 1) . \'</pre>\';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'pdoResources' => 
      array (
        'fields' => 
        array (
          'id' => 31,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoResources',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */
if (isset($parents) && $parents === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
}
if (!empty($returnIds)) {
    $scriptProperties[\'return\'] = \'ids\';
}

// Adding extra parameters into special place so we can put them in a results
/** @var modSnippet $snippet */
$additionalPlaceholders = $properties = array();
if (isset($this) && $this instanceof modSnippet) {
    $properties = $this->get(\'properties\');
}
elseif ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoResources\'))) {
    $properties = $snippet->get(\'properties\');
}
if (!empty($properties)) {
    foreach ($scriptProperties as $k => $v) {
        if (!isset($properties[$k])) {
            $additionalPlaceholders[$k] = $v;
        }
    }
}
$scriptProperties[\'additionalPlaceholders\'] = $additionalPlaceholders;

/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption(\'pdoFetch.class\', null, \'pdotools.pdofetch\', true);
$path = $modx->getOption(\'pdofetch_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoFetch = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoFetch->addTime(\'pdoTools loaded\');
$output = $pdoFetch->run();

$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="pdoResourcesLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

// Return output
if (!empty($returnIds)) {
    $modx->setPlaceholder(\'pdoResources.log\', $log);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
} elseif (!empty($toSeparatePlaceholders)) {
    $output[\'log\'] = $log;
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    $output .= $log;

    if (!empty($tplWrapper) && (!empty($wrapIfEmpty) || !empty($output))) {
        $output = $pdoFetch->getChunk($tplWrapper, array_merge($additionalPlaceholders, array(\'output\' => $output)),
            $pdoFetch->config[\'fastMode\']);
    }

    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
}',
          'locked' => false,
          'properties' => 
          array (
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'pdotools_prop_tpl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для оформления ресурса. Если не указан, то содержимое полей ресурса будет распечатано на экран.',
              'area_trans' => '',
            ),
            'returnIds' => 
            array (
              'name' => 'returnIds',
              'desc' => 'pdotools_prop_returnIds',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Возвращать строку со списком id ресурсов, вместо оформленных результатов.',
              'area_trans' => '',
            ),
            'showLog' => 
            array (
              'name' => 'showLog',
              'desc' => 'pdotools_prop_showLog',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать дополнительную информацию о работе сниппета. Только для авторизованных в контекте "mgr".',
              'area_trans' => '',
            ),
            'fastMode' => 
            array (
              'name' => 'fastMode',
              'desc' => 'pdotools_prop_fastMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Быстрый режим обработки чанков. Все необработанные теги (условия, сниппеты и т.п.) будут вырезаны.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'pdotools_prop_sortby',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'publishedon',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Любое поле ресурса для сортировки, включая ТВ параметр, если он указан в параметре "includeTVs". Можно указывать JSON строку с массивом нескольких полей. Для случайно сортировки укажите "RAND()"',
              'area_trans' => '',
            ),
            'sortbyTV' => 
            array (
              'name' => 'sortbyTV',
              'desc' => 'pdotools_prop_sortbyTV',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Сортировка по ТВ параметру. Если он не указан в &includeTVs, то будет подключен автоматически.',
              'area_trans' => '',
            ),
            'sortbyTVType' => 
            array (
              'name' => 'sortbyTVType',
              'desc' => 'pdotools_prop_sortbyTVType',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Тип сортировки по ТВ параметру. Возможные варианты: string, integer, decimal и datetime. Если пусто, то ТВ будет отсортирован в зависимости от его типа: как текст, число или дата.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'pdotools_prop_sortdir',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'DESC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки: по убыванию или возрастанию.',
              'area_trans' => '',
            ),
            'sortdirTV' => 
            array (
              'name' => 'sortdirTV',
              'desc' => 'pdotools_prop_sortdirTV',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки ТВ: по убыванию или возрастанию. Если не указан, то будет равен параметру &sortdir.',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'depth' => 
            array (
              'name' => 'depth',
              'desc' => 'pdotools_prop_depth',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Глубина поиска дочерних ресурсов от родителя.',
              'area_trans' => '',
            ),
            'outputSeparator' => 
            array (
              'name' => 'outputSeparator',
              'desc' => 'pdotools_prop_outputSeparator',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '
',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Необязательная строка для разделения результатов работы.',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'pdotools_prop_parents',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список родителей, через запятую, для поиска результатов. По умолчанию выборка ограничена текущим родителем. Если поставить 0 - выборка не ограничивается. Если id родителя начинается с дефиса, он и его потомки исключается из выборки.',
              'area_trans' => '',
            ),
            'includeContent' => 
            array (
              'name' => 'includeContent',
              'desc' => 'pdotools_prop_includeContent',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включаем поле "content" в выборку.',
              'area_trans' => '',
            ),
            'includeTVs' => 
            array (
              'name' => 'includeTVs',
              'desc' => 'pdotools_prop_includeTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров для выборки, через запятую. Например: "action,time" дадут плейсхолдеры [[+action]] и [[+time]].',
              'area_trans' => '',
            ),
            'prepareTVs' => 
            array (
              'name' => 'prepareTVs',
              'desc' => 'pdotools_prop_prepareTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '1',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров, которые нужно подготовить перед выводом. По умолчанию, установлено в "1", что означает подготовку всех ТВ, указанных в "&includeTVs=``"',
              'area_trans' => '',
            ),
            'processTVs' => 
            array (
              'name' => 'processTVs',
              'desc' => 'pdotools_prop_processTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров, которые нужно обработать перед выводом. Если установить в "1" - будут обработаны все ТВ, указанные в "&includeTVs=``". По умолчанию параметр пуст.',
              'area_trans' => '',
            ),
            'tvPrefix' => 
            array (
              'name' => 'tvPrefix',
              'desc' => 'pdotools_prop_tvPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'tv.',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для ТВ параметров.',
              'area_trans' => '',
            ),
            'tvFilters' => 
            array (
              'name' => 'tvFilters',
              'desc' => 'pdotools_prop_tvFilters',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список фильтров по ТВ, с разделителями AND и OR. Разделитель, указанный в параметре "&tvFiltersOrDelimiter" представляет логическое условие OR и по нему условия группируются в первую очередь.  Внутри каждой группы вы можете задать список значений, разделив их "&tvFiltersAndDelimiter". Поиск значений может проводиться в каком-то конкретном ТВ, если он указан ("myTV==value"), или в любом ("value"). Пример вызова: "&tvFilters=`filter2==one,filter1==bar%||filter1==foo`". <br />Обратите внимание: фильтрация использует оператор LIKE и знак "%" является метасимволом. <br />И еще: Поиск идёт по значениям, которые физически находятся в БД, то есть, сюда не подставляются значения по умолчанию из настроек ТВ.',
              'area_trans' => '',
            ),
            'tvFiltersAndDelimiter' => 
            array (
              'name' => 'tvFiltersAndDelimiter',
              'desc' => 'pdotools_prop_tvFiltersAndDelimiter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => ',',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Разделитель для условий AND в параметре "&tvFilters". По умолчанию: ",".',
              'area_trans' => '',
            ),
            'tvFiltersOrDelimiter' => 
            array (
              'name' => 'tvFiltersOrDelimiter',
              'desc' => 'pdotools_prop_tvFiltersOrDelimiter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '||',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Разделитель для условий OR в параметре "&tvFilters". По умолчанию: "||".',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'pdotools_prop_where',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Массив дополнительных параметров выборки, закодированный в JSON.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'pdotools_prop_showUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать неопубликованные ресурсы.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'pdotools_prop_showDeleted',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать удалённые ресурсы.',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'pdotools_prop_showHidden',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать ресурсы, скрытые в меню.',
              'area_trans' => '',
            ),
            'hideContainers' => 
            array (
              'name' => 'hideContainers',
              'desc' => 'pdotools_prop_hideContainers',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Отключает вывод контейнеров, то есть, ресурсов с isfolder = 1.',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'pdotools_prop_context',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение выборки по контексту ресурсов.',
              'area_trans' => '',
            ),
            'idx' => 
            array (
              'name' => 'idx',
              'desc' => 'pdotools_prop_idx',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Вы можете указать стартовый номер итерации вывода результатов.',
              'area_trans' => '',
            ),
            'first' => 
            array (
              'name' => 'first',
              'desc' => 'pdotools_prop_first',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер первой итерации вывода результатов.',
              'area_trans' => '',
            ),
            'last' => 
            array (
              'name' => 'last',
              'desc' => 'pdotools_prop_last',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер последней итерации вывода результатов. По умолчанию он рассчитается автоматически, по формуле (total + first - 1).',
              'area_trans' => '',
            ),
            'tplFirst' => 
            array (
              'name' => 'tplFirst',
              'desc' => 'pdotools_prop_tplFirst',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для первого ресурса в результатах.',
              'area_trans' => '',
            ),
            'tplLast' => 
            array (
              'name' => 'tplLast',
              'desc' => 'pdotools_prop_tplLast',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для последнего ресурса в результатах.',
              'area_trans' => '',
            ),
            'tplOdd' => 
            array (
              'name' => 'tplOdd',
              'desc' => 'pdotools_prop_tplOdd',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для каждого второго ресурса.',
              'area_trans' => '',
            ),
            'tplWrapper' => 
            array (
              'name' => 'tplWrapper',
              'desc' => 'pdotools_prop_tplWrapper',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка, для заворачивания всех результатов. Понимает один плейсхолдер: [[+output]]. Не работает вместе с параметром "toSeparatePlaceholders".',
              'area_trans' => '',
            ),
            'wrapIfEmpty' => 
            array (
              'name' => 'wrapIfEmpty',
              'desc' => 'pdotools_prop_wrapIfEmpty',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включает вывод чанка-обертки (tplWrapper) даже если результатов нет.',
              'area_trans' => '',
            ),
            'totalVar' => 
            array (
              'name' => 'totalVar',
              'desc' => 'pdotools_prop_totalVar',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'total',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя плейсхолдера для сохранения общего количества результатов.',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'pdotools_prop_resources',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ресурсов, через запятую, для вывода в результатах. Если id ресурса начинается с дефиса, этот ресурс исключается из выборки.',
              'area_trans' => '',
            ),
            'tplCondition' => 
            array (
              'name' => 'tplCondition',
              'desc' => 'pdotools_prop_tplCondition',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Поле ресурса, из которого будет получено значение для выбора чанка по условию в "conditionalTpls".',
              'area_trans' => '',
            ),
            'tplOperator' => 
            array (
              'name' => 'tplOperator',
              'desc' => 'pdotools_prop_tplOperator',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'is equal to',
                  'value' => '==',
                  'name' => 'is equal to',
                ),
                1 => 
                array (
                  'text' => 'is not equal to',
                  'value' => '!=',
                  'name' => 'is not equal to',
                ),
                2 => 
                array (
                  'text' => 'less than',
                  'value' => '<',
                  'name' => 'less than',
                ),
                3 => 
                array (
                  'text' => 'less than or equal to',
                  'value' => '<=',
                  'name' => 'less than or equal to',
                ),
                4 => 
                array (
                  'text' => 'greater than or equal to',
                  'value' => '>=',
                  'name' => 'greater than or equal to',
                ),
                5 => 
                array (
                  'text' => 'is empty',
                  'value' => 'empty',
                  'name' => 'is empty',
                ),
                6 => 
                array (
                  'text' => 'is not empty',
                  'value' => '!empty',
                  'name' => 'is not empty',
                ),
                7 => 
                array (
                  'text' => 'is null',
                  'value' => 'null',
                  'name' => 'is null',
                ),
                8 => 
                array (
                  'text' => 'is in array',
                  'value' => 'inarray',
                  'name' => 'is in array',
                ),
                9 => 
                array (
                  'text' => 'is between',
                  'value' => 'between',
                  'name' => 'is between',
                ),
              ),
              'value' => '==',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Необязательный оператор для проведения сравнения поля ресурса в "tplCondition" с массивом значений и чанков в "conditionalTpls".',
              'area_trans' => '',
            ),
            'conditionalTpls' => 
            array (
              'name' => 'conditionalTpls',
              'desc' => 'pdotools_prop_conditionalTpls',
              'type' => 'textarea',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'JSON строка с массивом, у которого в ключах указано то, с чем будет сравниваться "tplCondition", а в значениях - чанки, которые будут использованы для вывода, если сравнение будет успешно. Оператор сравнения указывается в "tplOperator". Для операторов типа "isempty" можно использовать массив без ключей.',
              'area_trans' => '',
            ),
            'select' => 
            array (
              'name' => 'select',
              'desc' => 'pdotools_prop_select',
              'type' => 'textarea',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список полей для выборки, через запятую. Можно указывать JSON строку с массивом, например {"modResource":"id,pagetitle,content"}.',
              'area_trans' => '',
            ),
            'toSeparatePlaceholders' => 
            array (
              'name' => 'toSeparatePlaceholders',
              'desc' => 'pdotools_prop_toSeparatePlaceholders',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если вы укажете слово в этом параметре, то ВСЕ результаты будут выставлены в разные плейсхолдеры, начинающиеся с этого слова и заканчивающиеся порядковым номером строки, от нуля. Например, указав в параметре "myPl", вы получите плейсхолдеры [[+myPl0]], [[+myPl1]] и т.д.',
              'area_trans' => '',
            ),
            'loadModels' => 
            array (
              'name' => 'loadModels',
              'desc' => 'pdotools_prop_loadModels',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список компонентов, через запятую, чьи модели нужно загрузить для построения запроса. Например: "&loadModels=`ms2gallery,msearch2`".',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'pdotools_prop_scheme',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => '',
                  'text' => 'System default',
                  'name' => 'System default',
                ),
                1 => 
                array (
                  'value' => -1,
                  'text' => '-1 (relative to site_url)',
                  'name' => '-1 (relative to site_url)',
                ),
                2 => 
                array (
                  'value' => 'full',
                  'text' => 'full (absolute, prepended with site_url)',
                  'name' => 'full (absolute, prepended with site_url)',
                ),
                3 => 
                array (
                  'value' => 'abs',
                  'text' => 'abs (absolute, prepended with base_url)',
                  'name' => 'abs (absolute, prepended with base_url)',
                ),
                4 => 
                array (
                  'value' => 'http',
                  'text' => 'http (absolute, forced to http scheme)',
                  'name' => 'http (absolute, forced to http scheme)',
                ),
                5 => 
                array (
                  'value' => 'https',
                  'text' => 'https (absolute, forced to https scheme)',
                  'name' => 'https (absolute, forced to https scheme)',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема формирования ссылок: "uri" для подстановки поля uri документа (очень быстро) или параметр для modX::makeUrl().',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'pdotools_prop_useWeblinkUrl',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Генерировать ссылку с учетом класса ресурса.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdoresources.php',
          'content' => '/** @var array $scriptProperties */
if (isset($parents) && $parents === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
}
if (!empty($returnIds)) {
    $scriptProperties[\'return\'] = \'ids\';
}

// Adding extra parameters into special place so we can put them in a results
/** @var modSnippet $snippet */
$additionalPlaceholders = $properties = array();
if (isset($this) && $this instanceof modSnippet) {
    $properties = $this->get(\'properties\');
}
elseif ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoResources\'))) {
    $properties = $snippet->get(\'properties\');
}
if (!empty($properties)) {
    foreach ($scriptProperties as $k => $v) {
        if (!isset($properties[$k])) {
            $additionalPlaceholders[$k] = $v;
        }
    }
}
$scriptProperties[\'additionalPlaceholders\'] = $additionalPlaceholders;

/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption(\'pdoFetch.class\', null, \'pdotools.pdofetch\', true);
$path = $modx->getOption(\'pdofetch_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoFetch = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoFetch->addTime(\'pdoTools loaded\');
$output = $pdoFetch->run();

$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="pdoResourcesLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

// Return output
if (!empty($returnIds)) {
    $modx->setPlaceholder(\'pdoResources.log\', $log);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
} elseif (!empty($toSeparatePlaceholders)) {
    $output[\'log\'] = $log;
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    $output .= $log;

    if (!empty($tplWrapper) && (!empty($wrapIfEmpty) || !empty($output))) {
        $output = $pdoFetch->getChunk($tplWrapper, array_merge($additionalPlaceholders, array(\'output\' => $output)),
            $pdoFetch->config[\'fastMode\']);
    }

    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
}',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'titl' => 
      array (
        'fields' => 
        array (
          'id' => 22,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl',
          'caption' => 'Мета тайтл',
          'description' => '',
          'editor_type' => 0,
          'category' => 20,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '[[*pagetitle]] - [[++site_name]]',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[*pagetitle]] - [[++site_name]]',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'logo' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'logo',
          'caption' => 'Логотип',
          'description' => '',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell' => 
      array (
        'fields' => 
        array (
          'id' => 4,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell',
          'caption' => 'Начало номера',
          'description' => 'Первые 5 цифр номера',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 3,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell2' => 
      array (
        'fields' => 
        array (
          'id' => 5,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell2',
          'caption' => 'Остальные цыфры',
          'description' => 'остальные цифры',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 4,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'addres' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'addres',
          'caption' => 'Адрес',
          'description' => '',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'image' => 
      array (
        'fields' => 
        array (
          'id' => 25,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'image',
          'caption' => 'Изображение',
          'description' => '',
          'editor_type' => 0,
          'category' => 22,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);