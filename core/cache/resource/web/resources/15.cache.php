<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 15,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Детская стоматология',
    'longtitle' => '',
    'description' => '',
    'alias' => 'services',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 3,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '',
    'richtext' => 1,
    'template' => 10,
    'menuindex' => 0,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1501845614,
    'editedby' => 1,
    'editedon' => 1503561090,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1501845480,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'services.html',
    'uri_override' => 1,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'titl' => 
    array (
      0 => 'titl',
      1 => '[[*pagetitle]] - [[++site_name]]',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'keyw' => 
    array (
      0 => 'keyw',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'desc' => 
    array (
      0 => 'desc',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'image' => 
    array (
      0 => 'image',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'icon_image' => 
    array (
      0 => 'icon_image',
      1 => '/assets/template/images/sm7.png',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'vrach' => 
    array (
      0 => 'vrach',
      1 => '8||10',
      2 => 'delim',
      3 => NULL,
      4 => 'listbox-multiple',
    ),
    'image-1' => 
    array (
      0 => 'image-1',
      1 => 'assets/template/images/images/sliderblock2.png',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'titl1' => 
    array (
      0 => 'titl1',
      1 => 'Детская <span>стоматология',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'image-2' => 
    array (
      0 => 'image-2',
      1 => 'assets/template/images/finish1.png',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'image-3' => 
    array (
      0 => 'image-3',
      1 => 'assets/template/images/finish2.png',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'titl2' => 
    array (
      0 => 'titl2',
      1 => 'Здоровые зубки – <span>здоровые детки!</span>',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'p111' => 
    array (
      0 => 'p111',
      1 => 'Для детей любого возраста',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'p2' => 
    array (
      0 => 'p2',
      1 => '<p>Нравится ли вашему ребёнку ходить к стоматологу? Общаться с добрым доктором, считать свои зубы и слушать увлекательные истории о том, как за ними ухаживать? 					</p> 					
<p>Если ваш ребёнок боится и переживает, приходите с ним в ЦДИ, наш девиз<br>– "Без слёз и страхов!". Чтобы дети остались счастливыми, а родители довольными!</p>',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'titldet1' => 
    array (
      0 => 'titldet1',
      1 => 'Ваши семейные врачи',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'p22' => 
    array (
      0 => 'p22',
      1 => 'Без страха и боли',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'pdet1' => 
    array (
      0 => 'pdet1',
      1 => 'Наши специалисты давно уже стали «семейными» врачами и когда ребенок узнает, что он идет к тому же доктору, что и его мама или папа, то охотнее идет на прием.',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'titl3' => 
    array (
      0 => 'titl3',
      1 => 'Особый подход <span>к маленькому пациенту!</span>',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'p3' => 
    array (
      0 => 'p3',
      1 => '<p style="padding-top: 10px;font-size: 14px;line-height: 22px;">Ребёнок боится стоматолога потому, что все эти странные инструменты и серьёзные доктора ему незнакомы и оттого неприятны. Но если рассказать: что мы будем делать, как нам в этом поможет оборудование и почему малышу будет комфортно, он перестанет пугаться!</p> <p>&nbsp;</p>',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'slep1' => 
    array (
      0 => 'slep1',
      1 => 'Лечение зубов "во сне"',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'chank1' => 
    array (
      0 => 'chank1',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/smin1.png","title":"\\u0421\\u043a\\u0430\\u0437\\u043a\\u0430 \\u043f\\u0440\\u043e \\u0437\\u0443\\u0431\\u043d\\u044b\\u0435 \\u0449\\u0451\\u0442\\u043a\\u0438","text":"\\u0412 \\u0438\\u0433\\u0440\\u043e\\u0432\\u043e\\u0439 \\u0444\\u043e\\u0440\\u043c\\u0435 \\u0434\\u043e\\u043a\\u0442\\u043e\\u0440 \\u0437\\u043d\\u0430\\u043a\\u043e\\u043c\\u0438\\u0442\\u0441\\u044f \\u0441 \\u0440\\u0435\\u0431\\u0451\\u043d\\u043a\\u043e\\u043c, \\u043e\\u043d\\u0438 \\u0432\\u043c\\u0435\\u0441\\u0442\\u0435 \\u0441\\u0447\\u0438\\u0442\\u0430\\u044e\\u0442 \\u0437\\u0443\\u0431\\u043a\\u0438. \\u0410 \\u0437\\u0430\\u0445\\u0432\\u0430\\u0442\\u044b\\u0432\\u0430\\u044e\\u0449\\u0430\\u044f \\u0438\\u0441\\u0442\\u043e\\u0440\\u0438\\u044f \\u043f\\u0440\\u043e \\u0437\\u0443\\u0431\\u043d\\u044b\\u0435 \\u0449\\u0451\\u0442\\u043a\\u0438 \\u043f\\u043e\\u043c\\u043e\\u0436\\u0435\\u0442 \\u043c\\u0430\\u043b\\u0435\\u043d\\u044c\\u043a\\u043e\\u043c\\u0443 \\u043f\\u0430\\u0446\\u0438\\u0435\\u043d\\u0442\\u0443 \\u043f\\u043e\\u043d\\u044f\\u0442\\u044c \\u0432\\u0430\\u0436\\u043d\\u043e\\u0441\\u0442\\u044c \\u0443\\u0445\\u043e\\u0434\\u0430 \\u0437\\u0430 \\u0437\\u0443\\u0431\\u0430\\u043c\\u0438."},{"MIGX_id":"2","image":"assets\\/template\\/images\\/smin2.png","title":"\\u041b\\u0435\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0431\\u0435\\u0437 \\u0431\\u043e\\u043b\\u0438","text":"\\u0412 \\u0437\\u0430\\u0432\\u0438\\u0441\\u0438\\u043c\\u043e\\u0441\\u0442\\u0438 \\u043e\\u0442 \\u0432\\u043e\\u0437\\u0440\\u0430\\u0441\\u0442\\u0430 \\u0440\\u0435\\u0431\\u0451\\u043d\\u043a\\u0430, \\u0441\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433 \\u043f\\u043e\\u0434\\u0431\\u0435\\u0440\\u0451\\u0442 \\u0434\\u043b\\u044f \\u043d\\u0435\\u0433\\u043e \\u0432\\u0430\\u0440\\u0438\\u0430\\u043d\\u0442 \\u043e\\u0431\\u0435\\u0437\\u0431\\u043e\\u043b\\u0438\\u0432\\u0430\\u044e\\u0449\\u0435\\u0433\\u043e, \\u0447\\u0442\\u043e\\u0431\\u044b \\u0432 \\u043f\\u0440\\u043e\\u0446\\u0435\\u0441\\u0441\\u0435 \\u043b\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f \\u043c\\u0430\\u043b\\u044b\\u0448 \\u043d\\u0435 \\u0447\\u0443\\u0432\\u0441\\u0442\\u0432\\u043e\\u0432\\u0430\\u043b \\u043d\\u0438\\u043a\\u0430\\u043a\\u0438\\u0445 \\u043d\\u0435\\u0443\\u0434\\u043e\\u0431\\u0441\\u0442\\u0432! \\u0412 \\u043d\\u0430\\u0448\\u0435\\u043c \\u0448\\u0442\\u0430\\u0442\\u0435 \\u0435\\u0441\\u0442\\u044c \\u043f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u0438\\u043e\\u043d\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0430\\u043d\\u0435\\u0441\\u0442\\u0435\\u0437\\u0438\\u043e\\u043b\\u043e\\u0433, \\u0430 \\u0432\\u0438\\u0434\\u044b \\u043e\\u0431\\u0435\\u0437\\u0431\\u043e\\u043b\\u0438\\u0432\\u0430\\u043d\\u0438\\u044f \\u0430\\u0431\\u0441\\u043e\\u043b\\u044e\\u0442\\u043d\\u043e \\u0431\\u0435\\u0437\\u0432\\u0440\\u0435\\u0434\\u043d\\u044b."}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'slepp1' => 
    array (
      0 => 'slepp1',
      1 => '<p> 	Этот вариант приведёт ребёнка в восторг! Если ваш ребёнок беспокоится и не даёт никому посмотреть свои зубы, мы можем вылечить их «во сне». А точнее с помощью седации ксеноном – самым безопасным видом анестезии.</p> <p>Малышу понравится весь процесс: нужно надеть интересную маску, словно он выполняет важную миссию. А буквально через несколько минут ребёнок чувствует расслабление, и процедура становится комфортной и приятной. Чтобы ребенок вернулся в исходное состояние из расслабления, необходимо лишь увеличить подачу кислорода. Ксенон выводится из организма уже через несколько минут, не вызывает привыкания и рекомендован к применению самым маленьким пациентам. 					</p>',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'titl4' => 
    array (
      0 => 'titl4',
      1 => 'Почему нужно лечить <span>молочные зубы?</span>',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'p4' => 
    array (
      0 => 'p4',
      1 => 'Существует большое заблуждение, что молочные зубы лечить необязательно – ведь это сменный, тренировочный комплект',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'slep2' => 
    array (
      0 => 'slep2',
      1 => 'Современный подход',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'chank11' => 
    array (
      0 => 'chank11',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/sb1.png","title":"\\t\\u0418\\u043d\\u0434\\u0438\\u0432\\u0438\\u0434\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043f\\u043b\\u0430\\u043d \\u043b\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f"},{"MIGX_id":"2","image":"assets\\/template\\/images\\/sb2.png","title":"\\t\\u041b\\u0435\\u0447\\u0435\\u043d\\u0438\\u0435 \\u0432\\u043e \\u0441\\u043d\\u0435"},{"MIGX_id":"3","image":"assets\\/template\\/images\\/sb3.png","title":"\\t\\u041e\\u0441\\u043e\\u0431\\u044b\\u0439 \\u043f\\u043e\\u0434\\u0445\\u043e\\u0434 \\u043a \\u043c\\u0430\\u043b\\u0435\\u043d\\u044c\\u043a\\u0438\\u043c \\u043f\\u0430\\u0446\\u0438\\u0435\\u043d\\u0442\\u0430\\u043c"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'chank16' => 
    array (
      0 => 'chank16',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/p_i_1.jpg","href":"\\u0418\\u043c\\u0435\\u043d\\u043d\\u043e \\u043c\\u043e\\u043b\\u043e\\u0447\\u043d\\u044b\\u0435 \\u0437\\u0443\\u0431\\u044b \\u043e\\u0442\\u0432\\u0435\\u0447\\u0430\\u044e\\u0442 \\u0437\\u0430 \\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0447\\u0435\\u043b\\u044e\\u0441\\u0442\\u0438. \\u0415\\u0441\\u043b\\u0438 \\u0438\\u0437-\\u0437\\u0430 \\u043a\\u0430\\u0440\\u0438\\u0435\\u0441\\u0430 \\u0440\\u0435\\u0431\\u0451\\u043d\\u043e\\u043a \\u0440\\u0430\\u043d\\u043e \\u043f\\u043e\\u0442\\u0435\\u0440\\u044f\\u0435\\u0442 \\u0437\\u0443\\u0431, \\u043c\\u043e\\u0436\\u0435\\u0442 \\u0441\\u0444\\u043e\\u0440\\u043c\\u0438\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c\\u0441\\u044f \\u043d\\u0435\\u043f\\u0440\\u0430\\u0432\\u0438\\u043b\\u044c\\u043d\\u044b\\u0439 \\u043f\\u0440\\u0438\\u043a\\u0443\\u0441. \\u0410 \\u044d\\u0442\\u043e \\u0432\\u043f\\u043e\\u0441\\u043b\\u0435\\u0434\\u0441\\u0442\\u0432\\u0438\\u0438 \\u0432\\u043b\\u0438\\u044f\\u0435\\u0442 \\u043d\\u0430 \\u0441\\u043e\\u0441\\u0442\\u043e\\u044f\\u043d\\u0438\\u0435 \\u0436\\u0435\\u043b\\u0443\\u0434\\u043e\\u0447\\u043d\\u043e-\\u043a\\u0438\\u0448\\u0435\\u0447\\u043d\\u043e\\u0433\\u043e \\u0442\\u0440\\u0430\\u043a\\u0442\\u0430!"},{"MIGX_id":"3","image":"assets\\/template\\/images\\/p_i_2.jpg","href":"\\u041f\\u043e\\u0441\\u0442\\u043e\\u044f\\u043d\\u043d\\u044b\\u0435 \\u0437\\u0443\\u0431\\u043a\\u0438 \\u00ab\\u0441\\u043f\\u044f\\u0442\\u00bb \\u043f\\u0440\\u044f\\u043c\\u043e \\u043f\\u043e\\u0434 \\u043c\\u043e\\u043b\\u043e\\u0447\\u043d\\u044b\\u043c\\u0438 \\u0437\\u0443\\u0431\\u0430\\u043c\\u0438! \\u0418 \\u0438\\u043d\\u0444\\u0435\\u043a\\u0446\\u0438\\u044f \\u043e\\u0442 \\u043c\\u043e\\u043b\\u043e\\u0447\\u043d\\u043e\\u0433\\u043e \\u0437\\u0443\\u0431\\u0430 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u0437\\u0430\\u043f\\u0440\\u043e\\u0441\\u0442\\u043e \\u043f\\u0435\\u0440\\u0435\\u0439\\u0442\\u0438 \\u043a \\u0441\\u043e\\u0441\\u0435\\u0434\\u0443 \\u0441\\u043d\\u0438\\u0437\\u0443. \\u0422\\u043e\\u0433\\u0434\\u0430 \\u043f\\u043e\\u0441\\u0442\\u043e\\u044f\\u043d\\u043d\\u044b\\u0435 \\u0437\\u0443\\u0431\\u044b \\u043c\\u043e\\u0433\\u0443\\u0442 \\u0432\\u044b\\u0440\\u0430\\u0441\\u0442\\u0438 \\u0443\\u0436\\u0435 \\u043d\\u0435\\u0437\\u0434\\u043e\\u0440\\u043e\\u0432\\u044b\\u043c\\u0438."},{"MIGX_id":"2","image":"assets\\/template\\/images\\/p_i_33.jpg","href":"\\u041d\\u0435\\u0432\\u044b\\u043b\\u0435\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u043a\\u0430\\u0440\\u0438\\u0435\\u0441 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u043f\\u0440\\u0435\\u043e\\u0431\\u0440\\u0430\\u0437\\u043e\\u0432\\u0430\\u0442\\u044c\\u0441\\u044f \\u0432 \\u043f\\u0443\\u043b\\u044c\\u043f\\u0438\\u0442 \\u0438 \\u043f\\u0440\\u0438\\u0447\\u0438\\u043d\\u0438\\u0442\\u044c \\u0441\\u0438\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0431\\u0435\\u0441\\u043f\\u043e\\u043a\\u043e\\u0439\\u0441\\u0442\\u0432\\u0430 \\u0440\\u0435\\u0431\\u0451\\u043d\\u043a\\u0443. \\u0427\\u0442\\u043e\\u0431\\u044b \\u043d\\u0435 \\u0437\\u0430\\u043f\\u0443\\u0441\\u043a\\u0430\\u0442\\u044c \\u044d\\u0442\\u043e\\u0442 \\u043f\\u0440\\u043e\\u0446\\u0435\\u0441\\u0441, \\u043d\\u0435\\u043e\\u0431\\u0445\\u043e\\u0434\\u0438\\u043c\\u043e \\u0432 \\u0446\\u0435\\u043b\\u044f\\u0445 \\u043f\\u0440\\u043e\\u0444\\u0438\\u043b\\u0430\\u043a\\u0442\\u0438\\u043a\\u0438 \\u0440\\u0430\\u0437 \\u0432 6 \\u043c\\u0435\\u0441\\u044f\\u0446\\u0435\\u0432 \\u043f\\u043e\\u0441\\u0435\\u0449\\u0430\\u0442\\u044c \\u0441\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0430."}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'chank10' => 
    array (
      0 => 'chank10',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/block_tn_23.jpg","href":"\\u0417\\u0430\\u043f\\u0435\\u0447\\u0430\\u0442\\u0430\\u043d\\u043d\\u044b\\u0439 \\u043d\\u0430\\u0431\\u043e\\u0440 \\u0438\\u043d\\u0441\\u0442\\u0440\\u0443\\u043c\\u0435\\u043d\\u0442\\u043e\\u0432, \\u0438\\u043d\\u0434\\u0438\\u0432\\u0438\\u0434\\u0443\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0434\\u043b\\u044f \\u043a\\u0430\\u0436\\u0434\\u043e\\u0433\\u043e \\u043f\\u0430\\u0446\\u0438\\u0435\\u043d\\u0442\\u0430, \\u043e\\u0442\\u043a\\u0440\\u044b\\u0432\\u0430\\u0435\\u0442\\u0441\\u044f \\u0442\\u043e\\u043b\\u044c\\u043a\\u043e \\u0432 \\u0432\\u0430\\u0448\\u0435\\u043c \\u043f\\u0440\\u0438\\u0441\\u0443\\u0442\\u0441\\u0442\\u0432\\u0438\\u0438 \\u0438 \\u0430\\u0431\\u0441\\u043e\\u043b\\u044e\\u0442\\u043d\\u043e \\u0441\\u0442\\u0435\\u0440\\u0438\\u043b\\u0435\\u043d."},{"MIGX_id":"2","image":"assets\\/template\\/images\\/block_tn_02.jpg","href":"\\u041c\\u0430\\u043a\\u0441\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u043e\\u0435 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043e\\u0434\\u043d\\u043e\\u0440\\u0430\\u0437\\u043e\\u0432\\u044b\\u0445 \\u043c\\u0430\\u0442\\u0435\\u0440\\u0438\\u0430\\u043b\\u043e\\u0432 \\u0438 \\u0438\\u043d\\u0441\\u0442\\u0440\\u0443\\u043c\\u0435\\u043d\\u0442\\u043e\\u0432, 100% \\u0441\\u0442\\u0435\\u0440\\u0438\\u043b\\u044c\\u043d\\u043e\\u0435 \\u0441\\u043e\\u0441\\u0442\\u043e\\u044f\\u043d\\u0438\\u0435 \\u043e\\u0431\\u043e\\u0440\\u0443\\u0434\\u043e\\u0432\\u0430\\u043d\\u0438\\u044f."},{"MIGX_id":"3","image":"assets\\/template\\/images\\/block_tn_25.jpg","href":"\\t\\u041a\\u043e\\u043c\\u043f\\u043b\\u0435\\u043a\\u0441\\u043d\\u044b\\u0439 \\u043f\\u043e\\u0434\\u0445\\u043e\\u0434 \\u043a \\u043b\\u044e\\u0431\\u043e\\u0439 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0435 \\u043f\\u043e\\u0437\\u0432\\u043e\\u043b\\u044f\\u0435\\u0442 \\u043f\\u043e\\u0434\\u043a\\u043b\\u044e\\u0447\\u0430\\u0442\\u044c \\u0432\\u0440\\u0430\\u0447\\u0435\\u0439 \\u0440\\u0430\\u0437\\u043d\\u044b\\u0445 \\u0441\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u0438\\u0437\\u0430\\u0446\\u0438\\u0439."}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'slep3' => 
    array (
      0 => 'slep3',
      1 => '<span>ЦДИ</span> – клиника европейского формата',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'chank8' => 
    array (
      0 => 'chank8',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/open1.png","title":"\\u041b\\u0435\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043a\\u0430\\u0440\\u0438\\u0435\\u0441\\u0430 \\u043c\\u043e\\u043b\\u043e\\u0447\\u043d\\u044b\\u0445 \\u0437\\u0443\\u0431\\u043e\\u0432","text":"\\u041d\\u0430\\u0448\\u0438 \\u0441\\u0442\\u043e\\u043c\\u0430\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438 \\u044d\\u0444\\u0444\\u0435\\u043a\\u0442\\u0438\\u0432\\u043d\\u043e \\u0438 \\u0431\\u0435\\u0437 \\u0431\\u043e\\u043b\\u0438 \\u0432\\u044b\\u043b\\u0435\\u0447\\u0430\\u0442 \\u043a\\u0430\\u043a \\u043f\\u043e\\u0432\\u0435\\u0440\\u0445\\u043d\\u043e\\u0441\\u0442\\u043d\\u044b\\u0439, \\u0442\\u0430\\u043a \\u0438 \\u0433\\u043b\\u0443\\u0431\\u043e\\u043a\\u0438\\u0439 \\u043a\\u0430\\u0440\\u0438\\u0435\\u0441."},{"MIGX_id":"2","image":"assets\\/template\\/images\\/open2.png","title":"\\u041b\\u0435\\u0447\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0443\\u043b\\u044c\\u043f\\u0438\\u0442\\u0430 \\u043c\\u043e\\u043b\\u043e\\u0447\\u043d\\u044b\\u0445 \\u0437\\u0443\\u0431\\u043e\\u0432","text":"\\u041d\\u0430\\u0448\\u0430 \\u0437\\u0430\\u0434\\u0430\\u0447\\u0430 \\u2013 \\u043c\\u0430\\u043a\\u0441\\u0438\\u043c\\u0430\\u043b\\u044c\\u043d\\u043e \\u0441\\u043e\\u0445\\u0440\\u0430\\u043d\\u0438\\u0442\\u044c \\u0437\\u0430\\u0431\\u043e\\u043b\\u0435\\u0432\\u0448\\u0438\\u0439 \\u0437\\u0443\\u0431, \\u043f\\u0440\\u0438 \\u044d\\u0442\\u043e\\u043c \\u0440\\u0435\\u0431\\u0451\\u043d\\u043e\\u043a \\u043c\\u043e\\u0436\\u0435\\u0442 \\u043f\\u043e\\u0447\\u0443\\u0432\\u0441\\u0442\\u0432\\u043e\\u0432\\u0430\\u0442\\u044c \\u0441\\u0435\\u0431\\u044f \\u043a\\u043e\\u0441\\u043c\\u043e\\u043d\\u0430\\u0432\\u0442\\u043e\\u043c \\u0438 \\u00ab\\u043f\\u043e\\u043b\\u0435\\u0442\\u0430\\u0442\\u044c \\u0432\\u043e \\u0441\\u043d\\u0435\\u00bb \\u0432\\u043e \\u0432\\u0440\\u0435\\u043c\\u044f \\u043b\\u0435\\u0447\\u0435\\u043d\\u0438\\u044f."},{"MIGX_id":"3","image":"assets\\/template\\/images\\/cleaning3.png","title":"\\u0423\\u0434\\u0430\\u043b\\u0435\\u043d\\u0438\\u0435 \\u043c\\u043e\\u043b\\u043e\\u0447\\u043d\\u044b\\u0445 \\u0437\\u0443\\u0431\\u043e\\u0432","text":"\\u041f\\u0440\\u0438 \\u043d\\u0435\\u043e\\u0431\\u0445\\u043e\\u0434\\u0438\\u043c\\u043e\\u0441\\u0442\\u0438 \\u0443\\u0434\\u0430\\u043b\\u0438\\u0442\\u044c \\u0437\\u0443\\u0431, \\u043d\\u0430\\u0448 \\u0434\\u043e\\u043a\\u0442\\u043e\\u0440 \\u0434\\u0435\\u043b\\u0430\\u0435\\u0442 \\u044d\\u0442\\u043e \\u0430\\u043a\\u043a\\u0443\\u0440\\u0430\\u0442\\u043d\\u043e \\u0438 \\u0431\\u0435\\u0437\\u0431\\u043e\\u043b\\u0435\\u0437\\u043d\\u0435\\u043d\\u043d\\u043e. \\u0410 \\u0435\\u0441\\u043b\\u0438 \\u043f\\u043e\\u0441\\u0442\\u043e\\u044f\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0443\\u0431 \\u043e\\u0436\\u0438\\u0434\\u0430\\u0435\\u0442\\u0441\\u044f \\u0435\\u0449\\u0451 \\u043d\\u0435 \\u0441\\u043a\\u043e\\u0440\\u043e, \\u043c\\u044b \\u0443\\u0441\\u0442\\u0430\\u043d\\u043e\\u0432\\u0438\\u043c \\u0441\\u043f\\u0435\\u0446\\u0438\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u00ab\\u0434\\u0435\\u0440\\u0436\\u0430\\u0442\\u0435\\u043b\\u044c \\u043c\\u0435\\u0441\\u0442\\u0430\\u00bb, \\u0447\\u0442\\u043e\\u0431\\u044b \\u043d\\u0435 \\u043d\\u0430\\u0440\\u0443\\u0448\\u0438\\u0442\\u044c \\u0440\\u0430\\u0437\\u0432\\u0438\\u0442\\u0438\\u0435 \\u043f\\u0440\\u0438\\u043a\\u0443\\u0441\\u0430."},{"MIGX_id":"4","image":"assets\\/template\\/images\\/ss1.png","title":"\\u041e\\u0431\\u0435\\u0437\\u0431\\u043e\\u043b\\u0438\\u0432\\u0430\\u043d\\u0438\\u0435","text":"\\u041d\\u0430\\u0448\\u0435 \\u043e\\u0433\\u0440\\u043e\\u043c\\u043d\\u043e\\u0435 \\u043f\\u0440\\u0435\\u0438\\u043c\\u0443\\u0449\\u0435\\u0441\\u0442\\u0432\\u043e \\u2013 \\u043d\\u0430\\u043b\\u0438\\u0447\\u0438\\u0435 \\u0432 \\u0448\\u0442\\u0430\\u0442\\u0435 \\u0441\\u043e\\u0431\\u0441\\u0442\\u0432\\u0435\\u043d\\u043d\\u043e\\u0433\\u043e \\u0430\\u043d\\u0435\\u0441\\u0442\\u0435\\u0437\\u0438\\u043e\\u043b\\u043e\\u0433\\u0430. \\u042d\\u0442\\u043e \\u0432\\u0440\\u0430\\u0447, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0439 \\u0437\\u043d\\u0430\\u0435\\u0442 \\u0432\\u0441\\u0451 \\u043e \\u043a\\u043e\\u043c\\u0444\\u043e\\u0440\\u0442\\u043d\\u044b\\u0445 \\u043f\\u0440\\u043e\\u0446\\u0435\\u0434\\u0443\\u0440\\u0430\\u0445. \\u041c\\u044b \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u0443\\u0435\\u043c \\u0441\\u0430\\u043c\\u044b\\u0435 \\u0431\\u0435\\u0437\\u043e\\u043f\\u0430\\u0441\\u043d\\u044b\\u0435 \\u043c\\u0435\\u0442\\u043e\\u0434\\u044b \\u0430\\u043d\\u0435\\u0441\\u0442\\u0435\\u0437\\u0438\\u0438, \\u043a\\u043e\\u0442\\u043e\\u0440\\u044b\\u0435 \\u0431\\u044b\\u0441\\u0442\\u0440\\u043e \\u0432\\u044b\\u0432\\u043e\\u0434\\u044f\\u0442\\u0441\\u044f \\u0438 \\u043d\\u0435 \\u043e\\u0441\\u0442\\u0430\\u0432\\u043b\\u044f\\u044e\\u0442 \\u0441\\u043b\\u0435\\u0434\\u0430 \\u0432 \\u043e\\u0440\\u0433\\u0430\\u043d\\u0438\\u0437\\u043c\\u0435."},{"MIGX_id":"5","image":"assets\\/template\\/images\\/ss2.png","title":"\\u0414\\u0438\\u0430\\u0433\\u043d\\u043e\\u0441\\u0442\\u0438\\u043a\\u0430","text":"\\u0421\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u043e\\u0435 \\u0435\\u0432\\u0440\\u043e\\u043f\\u0435\\u0439\\u0441\\u043a\\u043e\\u0435 \\u043e\\u0431\\u043e\\u0440\\u0443\\u0434\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0437\\u0432\\u043e\\u043b\\u044f\\u0435\\u0442 \\u043d\\u0430 \\u0441\\u0430\\u043c\\u044b\\u0445 \\u0440\\u0430\\u043d\\u043d\\u0438\\u0445 \\u044d\\u0442\\u0430\\u043f\\u0430\\u0445 \\u0432\\u044b\\u044f\\u0432\\u0438\\u0442\\u044c \\u0443\\u044f\\u0437\\u0432\\u0438\\u043c\\u043e\\u0441\\u0442\\u044c \\u044d\\u043c\\u0430\\u043b\\u0438, \\u0441\\u0434\\u0435\\u043b\\u0430\\u0442\\u044c \\u0442\\u043e\\u0447\\u043d\\u044b\\u0439 \\u0441\\u043d\\u0438\\u043c\\u043e\\u043a \\u0447\\u0435\\u043b\\u044e\\u0441\\u0442\\u0438 \\u0438 \\u043f\\u0440\\u0435\\u0434\\u043e\\u0442\\u0432\\u0440\\u0430\\u0442\\u0438\\u0442\\u044c \\u043d\\u0430\\u0440\\u0443\\u0448\\u0435\\u043d\\u0438\\u044f. \\u041d\\u0430\\u0448\\u0438 \\u0432\\u0440\\u0430\\u0447\\u0438 \\u0438\\u043c\\u0435\\u044e\\u0442 \\u043e\\u0433\\u0440\\u043e\\u043c\\u043d\\u044b\\u0439 \\u043e\\u043f\\u044b\\u0442 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b \\u0441 \\u043c\\u0430\\u043b\\u0435\\u043d\\u044c\\u043a\\u0438\\u043c\\u0438 \\u043f\\u0430\\u0446\\u0438\\u0435\\u043d\\u0442\\u0430\\u043c\\u0438. \\u0414\\u043e\\u0432\\u0435\\u0440\\u044f\\u0439\\u0442\\u0435 \\u043f\\u0440\\u043e\\u0444\\u0435\\u0441\\u0441\\u0438\\u043e\\u043d\\u0430\\u043b\\u0430\\u043c!"},{"MIGX_id":"6","image":"assets\\/template\\/images\\/ss2.png","title":"\\u0418\\u0441\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u0435 \\u043f\\u0440\\u0438\\u043a\\u0443\\u0441\\u0430","text":"\\u0414\\u043b\\u044f \\u0441\\u0430\\u043c\\u044b\\u0445 \\u043c\\u0430\\u043b\\u0435\\u043d\\u044c\\u043a\\u0438\\u0445 \\u043f\\u0430\\u0446\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u0438\\u043d\\u0434\\u0438\\u0432\\u0438\\u0434\\u0443\\u0430\\u043b\\u044c\\u043d\\u043e \\u0438\\u0437\\u0433\\u043e\\u0442\\u0430\\u0432\\u043b\\u0438\\u0432\\u0430\\u044e\\u0442\\u0441\\u044f \\u0442\\u0440\\u0435\\u0439\\u043d\\u0435\\u0440\\u044b \\u0438\\u043b\\u0438 \\u043f\\u043b\\u0430\\u0441\\u0442\\u0438\\u043d\\u043a\\u0438. \\u0410 \\u0434\\u043b\\u044f \\u043f\\u0430\\u0446\\u0438\\u0435\\u043d\\u0442\\u043e\\u0432 \\u043f\\u043e\\u0441\\u0442\\u0430\\u0440\\u0448\\u0435 \\u2013 \\u043d\\u0435\\u0441\\u043a\\u043e\\u043b\\u044c\\u043a\\u043e \\u0432\\u0438\\u0434\\u043e\\u0432 \\u0431\\u0440\\u0435\\u043a\\u0435\\u0442-\\u0441\\u0438\\u0441\\u0442\\u0435\\u043c. \\u0421\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u043e\\u0435 \\u043e\\u0431\\u043e\\u0440\\u0443\\u0434\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u043f\\u043e\\u0437\\u0432\\u043e\\u043b\\u044f\\u0435\\u0442 \\u0441 \\u0442\\u043e\\u0447\\u043d\\u043e\\u0441\\u0442\\u044c\\u044e \\u043e\\u043f\\u0440\\u0435\\u0434\\u0435\\u043b\\u0438\\u0442\\u044c \\u043d\\u0430\\u0440\\u0443\\u0448\\u0435\\u043d\\u0438\\u0435."}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'slepp3' => 
    array (
      0 => 'slepp3',
      1 => 'Комфорт ребенка и его родителей – самое главное для эффективного лечения. Поэтому мы делаем всё для вашего удобства и безопасности.',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'titl12' => 
    array (
      0 => 'titl12',
      1 => 'УЛЫБКА РЕБЁНКА БЕСЦЕННА!',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'p12' => 
    array (
      0 => 'p12',
      1 => 'И сейчас от вашего выбора и ваших действий зависит его здоровье. Станьте для<br> него примером и с вниманием отнеситесь к его зубам. А мы вам в этом поможем!',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>Детская стоматология - Центр дентальной имплантации</title>
        <base href="http://cdistom.ru/" />
        <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->

<script>
		$(document).ready(function(){
			$(".vrachi").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:2,
					}
				}
			});

			$(document).ready(function(){
				owlcount();
			});

			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});

			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});

		});
	</script>
</head>
<body class="fix_scroll">
<div class="wrapper mainwrapper">
<header class="header">
			<div class="blockin">
				<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
				<a href="tel:+8 382225-70-33" class="phone_top">
					<p class="address_top">Томск, ул. Говорова, д. 19 "В", ст. 1</p>
					8 3822 <span>25-70-33</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="http://cdistom.ru/" class="">
								<img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>

		<div class="section mainslider sliderblock  new-banner child-banner" style="background-image: url(assets/template/images/images/sliderblock2.png);">
			<div class="coutbloc wow fadeInUp">
				<div class="arrowmain">
					<a href="proteth.html"  class="owl-prev"></a>
					<a href="terapia.html" class="owl-next"></a>
				</div>
				<div class="count"><span>3</span> / 5</div>
			</div>
			<div class="owlmain finishblock wow fadeInUp">
					<div class="item item3" >
									<div class="wraslider">
										<div class="blockin">
											<div class="s_title" style=" padding: 25px 0 0 0; ">Детская стоматология</div>
											<div class="s_text" style=" padding-bottom: 20px; ">
												Нравится ли вашему ребёнку ходить к стоматологу? Если ваш ребёнок боится и переживает,
												приходите с ним в ЦДИ, наш девиз — "<b>Без слёз и Страхов!</b>". Чтобы дети остались 
												счастливыми, а родители довольными!
											</div>
											<ul class="new-banner-list">
												<h4>Направления детской стоматологии</h4>
												<li>Лечение кариеса молочных зубов</li>
												<li>Удаление молочных зубов</li>
												<li>Диагностика</li>
											</ul>
											<ul class="new-banner-list">
											
												<li>Лечение пульпита молочных зубов</li>
												<li>Обезболивание</li>
												<li>Исправление прикуса</li>
											</ul><br>
											<a href="contacts.html" class="all" style=" font-size: 14px; font-weight: 300; ">Записаться на прием</a>
										</div>
									</div>
									<div class="new-banner-img">
										<img src="assets/template/images/banner/banner-detskaya-zub.png" alt="">
									</div>
								</div>	
			</div>
		</div>
		<div class="chistka info_hygiene servicestop wow fadeInUp" data-wow-offset="100">
			<div class="textblock">
				<div class="title" style="padding-bottom: 10px">Здоровые зубки – <span>здоровые детки!</span></div>
				<div class="text" style=" font-size: 15px; line-height: 22px; ">
						<p>Нравится ли вашему ребёнку ходить к стоматологу? Общаться с добрым доктором, считать свои зубы и слушать увлекательные истории о том, как за ними ухаживать? 					</p> 					
<p>Если ваш ребёнок боится и переживает, приходите с ним в ЦДИ, наш девиз<br>– "Без слёз и страхов!". Чтобы дети остались счастливыми, а родители довольными!</p>
				</div>
			</div>
		</div>
		<div class="docte_info_block_page yak1">
			<div class="blockin">
						<div class="doct_2_wr  wq [[!If?
   &subject=`2`
   &operator=`gte`
   &operand=`3`
   &then=`vrachi owl-carousel`
   &else=`
]] 2"><div class="left wow fadeInUp item left_2">
	
			<img src="assets/template/images/masuk_new.png" alt="">
			<div class="min_text_info">
				<ul class="doctor__text">
					<li>Ваш доктор</li>
					<li class="full__name">Мацук Кристина Александровна</li>
					<li>
						<span>Специализация:</span> Врач-стоматолог-терапевт, детский стоматолог, гигиенист стоматологический
					</li>
					<li class="none icon1">
						<span>Образование:</span> Сибирский государственный медицинский университет
					</li>
				[[!getImageList? &tvname=`staj_diplomi` &tpl=`vrachmigxTpl` &docid=`8`]]
				</ul>
				<div class="site_button_wrap">
					<a href="doctors/doc_matzuck.html" class="site_but">О докторе</a>
				</div>
			</div>
		</div>
<div class="left wow fadeInUp item left_2">
	
			<img src="assets/template/images/yakusheva_new.png" alt="">
			<div class="min_text_info">
				<ul class="doctor__text">
					<li>Ваш доктор</li>
					<li class="full__name">Якушева Елена Александровна</li>
					<li>
						<span>Специализация:</span> Врач-стоматолог-терапевт, детский стоматолог, гигиенист стоматологический
					</li>
					<li class="none icon1">
						<span>Образование:</span> Кемеровская государственная медицинская академия
					</li>
				[[!getImageList? &tvname=`staj_diplomi` &tpl=`vrachmigxTpl` &docid=`10`]]
				</ul>
				<div class="site_button_wrap">
					<a href="doctors/doc_yakusheva.html" class="site_but">О докторе</a>
				</div>
			</div>
		</div></div>
			</div>
		</div>
		<div class="docte_info_block_page">
			<div class="blockin">
				<div class="right wow fadeInUp">
					<div class="titleblock">
							Особый подход <span>к маленькому пациенту!</span>
					</div>
					<div class="text_block_info">
						 <p style="padding-top: 10px;font-size: 14px;line-height: 22px;">Ребёнок боится стоматолога потому, что все эти странные инструменты и серьёзные доктора ему незнакомы и оттого неприятны. Но если рассказать: что мы будем делать, как нам в этом поможет оборудование и почему малышу будет комфортно, он перестанет пугаться!</p> <p>&nbsp;</p>
						
					</div>
					<ul>
					    [[!getImageList? &tvname=`chank1` &tpl=`chank1`]]
					</ul>
				</div>
			</div>
		</div>

		<div class="chistka info_hygiene serviceyellow wow fadeInUp">
			<div class="textblock">
				<div class="title"><span>Ваши семейные врачи</span></div>
				<div class="text" style=" font-size: 15px; line-height: 22px; ">
					
					<p>
						Наши специалисты давно уже стали «семейными» врачами и когда ребенок узнает, что он идет к тому же доктору, что и его мама или папа, то охотнее идет на прием.
					</p>
				</div>
			</div>
		</div>

		<div class="js_pic_block serviceblock">
			<div class="left_info wow fadeInUp" data-wow-delay="0.2">
				<div class="title" style="line-height: 43px;">Почему нужно лечить <span>молочные зубы?</span></div>
				<div class="text" style=" font-size: 14px; ">
						Существует большое заблуждение, что молочные зубы лечить необязательно – ведь это сменный, тренировочный комплект
				</div>
				<div class="m_title" style=" font-size: 22px; margin-bottom: 15px; line-height: 27px; ">Что может произойти если не вылечить зуб?</div>
				<div class="blocks">
				 [[!getImageList? &tvname=`chank16` &tpl=`chank16`]]
				</div>
			</div>
			<div class="rightimg wow fadeInUp">
				<img src="assets/template/images/child.png" alt="">
			</div>
		</div>	

		<div class="chistka grey wow fadeInUp">
			<div class="site_title" style="padding-bottom: 15px;">Направления детской стоматологии</div>
			<div class="body_block">
			     [[!getImageList? &tvname=`chank8` &tpl=`chank8`]]
			</div>
		</div>

		<div class="consultblock">
			<div class="textblock wow fadeInUp">
				<div class="b_title" >
					<span style=" color: #032a4b; ">Лечение зубов "во сне"</span>
				</div>
				<div class="text" style=" font-size: 14px; line-height: 22px; padding-top: 10px; ">
				    <p> 	Этот вариант приведёт ребёнка в восторг! Если ваш ребёнок беспокоится и не даёт никому посмотреть свои зубы, мы можем вылечить их «во сне». А точнее с помощью седации ксеноном – самым безопасным видом анестезии.</p> <p>Малышу понравится весь процесс: нужно надеть интересную маску, словно он выполняет важную миссию. А буквально через несколько минут ребёнок чувствует расслабление, и процедура становится комфортной и приятной. Чтобы ребенок вернулся в исходное состояние из расслабления, необходимо лишь увеличить подачу кислорода. Ксенон выводится из организма уже через несколько минут, не вызывает привыкания и рекомендован к применению самым маленьким пациентам. 					</p>
				</div>
			</div>
			<img src="assets/template/images/servicetextblock.jpg" alt="" class="wow fadeInUp">
		</div>

		<div class="bottomblock_premium servicesinfo">
			<div class="site_title wow fadeIn">
				<span>
					Современный подход
					<span>[[*slepp2]]</span>
				</span>
			</div>
			<div class="body">
			    [[!getImageList? &tvname=`chank11` &tpl=`chank11`]]
			</div>
		</div>

		<div class="js_pic_block greybg">
			<div class="left_info wow fadeInLeft" style="padding: 50px 40px 50px 80px;">
				<div class="title" style="color: #28415c; line-height: 43px;"><span>ЦДИ</span> – клиника европейского формата</div>
				<div class="text" style="line-height: 22px;font-size: 14px;padding-bottom: 20px;">
				Комфорт ребенка и его родителей – самое главное для эффективного лечения. Поэтому мы делаем всё для вашего удобства и безопасности.	
				</div>
				<div class="blocks">
				    [[!getImageList? &tvname=`chank10` &tpl=`chank10`]]
				</div>
			</div>
			<div class="rightimg wow fadeInRight">
				<img src="assets/template/images/protectimgbot.jpg" alt="">
			</div>
		</div>	

		<div class="siteinfoblock doc_page_infobot wow fadeInUp">
			<div class="title">УЛЫБКА РЕБЁНКА БЕСЦЕННА!</div>
			<div class="text" style="font-size: 15px;line-height: 22px;">
				И сейчас от вашего выбора и ваших действий зависит его здоровье. Станьте для<br> него примером и с вниманием отнеситесь к его зубам. А мы вам в этом поможем!
			</div>
			<a href="contacts.html">Записаться на прием</a>
		</div>
		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="implantation.html">Имплантация</a></li>
						<li><a href="services.html">Детская стоматология</a></li>
						<li><a href="terapia.html">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="hygiene.html">Гигиена и профилактика</a></li>
						<li><a href="proteth.html">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> 8:30-20:00</li>
						<li><strong>Сб: </strong> 8:30-14:00</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> 25-70-33</li>
						<li><strong>E-mail: </strong> <a href="mailto:cdistom@mail.ru"> cdistom@mail.ru</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br>  ул. Говорова, д. 19 "В", ст. 1</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="https://vk.com/cdistom"></a></li>
							<li><a href="https://www.facebook.com/tomskimplant/?modal=composer"></a></li>
							<li><a href="https://www.instagram.com/tomstom_implant/?hl=ru"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="contacts.html" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[$head]]' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>Детская стоматология - Центр дентальной имплантации</title>
        <base href="http://cdistom.ru/" />
        <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
    '[[$header]]' => '<header class="header">
			<div class="blockin">
				<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
				<a href="tel:+8 382225-70-33" class="phone_top">
					<p class="address_top">Томск, ул. Говорова, д. 19 "В", ст. 1</p>
					8 3822 <span>25-70-33</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="http://cdistom.ru/" class="">
								<img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
    '[[pdoResources?
                    &parents=`4`
                    &idx=`1`
                    &tpl=`vrachiTpl`
                    &resources=`8,10`
                    &tplWrapper=`@CODE: <div class="doct_2_wr  wq [[!If?
   &subject=`[[+count]]`
   &operator=`gte`
   &operand=`3`
   &then=`vrachi owl-carousel`
   &else=``
]] [[+count]]">[[+output]]</div>`
                    &includeTVs=`image,staj_diplomi,obrazovanie,special,image_original`
                    &context=`web`
                    &totalVar=`count`
                  ]]' => '<div class="doct_2_wr  wq [[!If?
   &subject=`2`
   &operator=`gte`
   &operand=`3`
   &then=`vrachi owl-carousel`
   &else=`
]] 2"><div class="left wow fadeInUp item left_2">
	
			<img src="assets/template/images/masuk_new.png" alt="">
			<div class="min_text_info">
				<ul class="doctor__text">
					<li>Ваш доктор</li>
					<li class="full__name">Мацук Кристина Александровна</li>
					<li>
						<span>Специализация:</span> Врач-стоматолог-терапевт, детский стоматолог, гигиенист стоматологический
					</li>
					<li class="none icon1">
						<span>Образование:</span> Сибирский государственный медицинский университет
					</li>
				[[!getImageList? &tvname=`staj_diplomi` &tpl=`vrachmigxTpl` &docid=`8`]]
				</ul>
				<div class="site_button_wrap">
					<a href="doctors/doc_matzuck.html" class="site_but">О докторе</a>
				</div>
			</div>
		</div>
<div class="left wow fadeInUp item left_2">
	
			<img src="assets/template/images/yakusheva_new.png" alt="">
			<div class="min_text_info">
				<ul class="doctor__text">
					<li>Ваш доктор</li>
					<li class="full__name">Якушева Елена Александровна</li>
					<li>
						<span>Специализация:</span> Врач-стоматолог-терапевт, детский стоматолог, гигиенист стоматологический
					</li>
					<li class="none icon1">
						<span>Образование:</span> Кемеровская государственная медицинская академия
					</li>
				[[!getImageList? &tvname=`staj_diplomi` &tpl=`vrachmigxTpl` &docid=`10`]]
				</ul>
				<div class="site_button_wrap">
					<a href="doctors/doc_yakusheva.html" class="site_but">О докторе</a>
				</div>
			</div>
		</div></div>',
    '[[$footer]]' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="implantation.html">Имплантация</a></li>
						<li><a href="services.html">Детская стоматология</a></li>
						<li><a href="terapia.html">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="hygiene.html">Гигиена и профилактика</a></li>
						<li><a href="proteth.html">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> 8:30-20:00</li>
						<li><strong>Сб: </strong> 8:30-14:00</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> 25-70-33</li>
						<li><strong>E-mail: </strong> <a href="mailto:cdistom@mail.ru"> cdistom@mail.ru</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br>  ул. Говорова, д. 19 "В", ст. 1</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="https://vk.com/cdistom"></a></li>
							<li><a href="https://www.facebook.com/tomskimplant/?modal=composer"></a></li>
							<li><a href="https://www.instagram.com/tomstom_implant/?hl=ru"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="contacts.html" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'head',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>[[*titl]]</title>
        <base href="[[++site_url]]" />
        <meta charset="[[++modx_charset]]" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>[[*titl]]</title>
        <base href="[[++site_url]]" />
        <meta charset="[[++modx_charset]]" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header' => 
      array (
        'fields' => 
        array (
          'id' => 15,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'header',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<header class="header">
			<div class="blockin">
				<a href="[[++site_url]]" class="logo"><img src="[[#1.tv.logo]]" alt=""></a>
				<a href="tel:+[[#1.tv.tell]][[#1.tv.tell2]]" class="phone_top">
					<p class="address_top">[[#1.tv.addres]]</p>
					[[#1.tv.tell]] <span>[[#1.tv.tell2]]</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="[[++site_url]]" class="">
								<img src="[[#1.tv.logo]]" alt="[[++site_name]]">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<header class="header">
			<div class="blockin">
				<a href="[[++site_url]]" class="logo"><img src="[[#1.tv.logo]]" alt=""></a>
				<a href="tel:+[[#1.tv.tell]][[#1.tv.tell2]]" class="phone_top">
					<p class="address_top">[[#1.tv.addres]]</p>
					[[#1.tv.tell]] <span>[[#1.tv.tell2]]</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="[[++site_url]]" class="">
								<img src="[[#1.tv.logo]]" alt="[[++site_name]]">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'footer' => 
      array (
        'fields' => 
        array (
          'id' => 27,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="[[~11]]">Имплантация</a></li>
						<li><a href="[[~15]]">Детская стоматология</a></li>
						<li><a href="[[~12]]">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="[[~14]]">Гигиена и профилактика</a></li>
						<li><a href="[[~13]]">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> [[++ot]]</li>
						<li><strong>Сб: </strong> [[++sub]]</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> [[++num]]</li>
						<li><strong>E-mail: </strong> <a href="mailto:[[++emailt]]"> [[++emailt]]</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br> [[++uli]]</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="[[++vk]]"></a></li>
							<li><a href="[[++fb]]"></a></li>
							<li><a href="[[++ig]]"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="[[~17]]" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="[[~11]]">Имплантация</a></li>
						<li><a href="[[~15]]">Детская стоматология</a></li>
						<li><a href="[[~12]]">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="[[~14]]">Гигиена и профилактика</a></li>
						<li><a href="[[~13]]">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> [[++ot]]</li>
						<li><strong>Сб: </strong> [[++sub]]</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> [[++num]]</li>
						<li><strong>E-mail: </strong> <a href="mailto:[[++emailt]]"> [[++emailt]]</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br> [[++uli]]</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="[[++vk]]"></a></li>
							<li><a href="[[++fb]]"></a></li>
							<li><a href="[[++ig]]"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="[[~17]]" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'pdoResources' => 
      array (
        'fields' => 
        array (
          'id' => 31,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoResources',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */
if (isset($parents) && $parents === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
}
if (!empty($returnIds)) {
    $scriptProperties[\'return\'] = \'ids\';
}

// Adding extra parameters into special place so we can put them in a results
/** @var modSnippet $snippet */
$additionalPlaceholders = $properties = array();
if (isset($this) && $this instanceof modSnippet) {
    $properties = $this->get(\'properties\');
}
elseif ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoResources\'))) {
    $properties = $snippet->get(\'properties\');
}
if (!empty($properties)) {
    foreach ($scriptProperties as $k => $v) {
        if (!isset($properties[$k])) {
            $additionalPlaceholders[$k] = $v;
        }
    }
}
$scriptProperties[\'additionalPlaceholders\'] = $additionalPlaceholders;

/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption(\'pdoFetch.class\', null, \'pdotools.pdofetch\', true);
$path = $modx->getOption(\'pdofetch_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoFetch = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoFetch->addTime(\'pdoTools loaded\');
$output = $pdoFetch->run();

$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="pdoResourcesLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

// Return output
if (!empty($returnIds)) {
    $modx->setPlaceholder(\'pdoResources.log\', $log);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
} elseif (!empty($toSeparatePlaceholders)) {
    $output[\'log\'] = $log;
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    $output .= $log;

    if (!empty($tplWrapper) && (!empty($wrapIfEmpty) || !empty($output))) {
        $output = $pdoFetch->getChunk($tplWrapper, array_merge($additionalPlaceholders, array(\'output\' => $output)),
            $pdoFetch->config[\'fastMode\']);
    }

    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
}',
          'locked' => false,
          'properties' => 
          array (
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'pdotools_prop_tpl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для оформления ресурса. Если не указан, то содержимое полей ресурса будет распечатано на экран.',
              'area_trans' => '',
            ),
            'returnIds' => 
            array (
              'name' => 'returnIds',
              'desc' => 'pdotools_prop_returnIds',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Возвращать строку со списком id ресурсов, вместо оформленных результатов.',
              'area_trans' => '',
            ),
            'showLog' => 
            array (
              'name' => 'showLog',
              'desc' => 'pdotools_prop_showLog',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать дополнительную информацию о работе сниппета. Только для авторизованных в контекте "mgr".',
              'area_trans' => '',
            ),
            'fastMode' => 
            array (
              'name' => 'fastMode',
              'desc' => 'pdotools_prop_fastMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Быстрый режим обработки чанков. Все необработанные теги (условия, сниппеты и т.п.) будут вырезаны.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'pdotools_prop_sortby',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'publishedon',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Любое поле ресурса для сортировки, включая ТВ параметр, если он указан в параметре "includeTVs". Можно указывать JSON строку с массивом нескольких полей. Для случайно сортировки укажите "RAND()"',
              'area_trans' => '',
            ),
            'sortbyTV' => 
            array (
              'name' => 'sortbyTV',
              'desc' => 'pdotools_prop_sortbyTV',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Сортировка по ТВ параметру. Если он не указан в &includeTVs, то будет подключен автоматически.',
              'area_trans' => '',
            ),
            'sortbyTVType' => 
            array (
              'name' => 'sortbyTVType',
              'desc' => 'pdotools_prop_sortbyTVType',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Тип сортировки по ТВ параметру. Возможные варианты: string, integer, decimal и datetime. Если пусто, то ТВ будет отсортирован в зависимости от его типа: как текст, число или дата.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'pdotools_prop_sortdir',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'DESC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки: по убыванию или возрастанию.',
              'area_trans' => '',
            ),
            'sortdirTV' => 
            array (
              'name' => 'sortdirTV',
              'desc' => 'pdotools_prop_sortdirTV',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки ТВ: по убыванию или возрастанию. Если не указан, то будет равен параметру &sortdir.',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'depth' => 
            array (
              'name' => 'depth',
              'desc' => 'pdotools_prop_depth',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Глубина поиска дочерних ресурсов от родителя.',
              'area_trans' => '',
            ),
            'outputSeparator' => 
            array (
              'name' => 'outputSeparator',
              'desc' => 'pdotools_prop_outputSeparator',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '
',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Необязательная строка для разделения результатов работы.',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'pdotools_prop_parents',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список родителей, через запятую, для поиска результатов. По умолчанию выборка ограничена текущим родителем. Если поставить 0 - выборка не ограничивается. Если id родителя начинается с дефиса, он и его потомки исключается из выборки.',
              'area_trans' => '',
            ),
            'includeContent' => 
            array (
              'name' => 'includeContent',
              'desc' => 'pdotools_prop_includeContent',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включаем поле "content" в выборку.',
              'area_trans' => '',
            ),
            'includeTVs' => 
            array (
              'name' => 'includeTVs',
              'desc' => 'pdotools_prop_includeTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров для выборки, через запятую. Например: "action,time" дадут плейсхолдеры [[+action]] и [[+time]].',
              'area_trans' => '',
            ),
            'prepareTVs' => 
            array (
              'name' => 'prepareTVs',
              'desc' => 'pdotools_prop_prepareTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '1',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров, которые нужно подготовить перед выводом. По умолчанию, установлено в "1", что означает подготовку всех ТВ, указанных в "&includeTVs=``"',
              'area_trans' => '',
            ),
            'processTVs' => 
            array (
              'name' => 'processTVs',
              'desc' => 'pdotools_prop_processTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров, которые нужно обработать перед выводом. Если установить в "1" - будут обработаны все ТВ, указанные в "&includeTVs=``". По умолчанию параметр пуст.',
              'area_trans' => '',
            ),
            'tvPrefix' => 
            array (
              'name' => 'tvPrefix',
              'desc' => 'pdotools_prop_tvPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'tv.',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для ТВ параметров.',
              'area_trans' => '',
            ),
            'tvFilters' => 
            array (
              'name' => 'tvFilters',
              'desc' => 'pdotools_prop_tvFilters',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список фильтров по ТВ, с разделителями AND и OR. Разделитель, указанный в параметре "&tvFiltersOrDelimiter" представляет логическое условие OR и по нему условия группируются в первую очередь.  Внутри каждой группы вы можете задать список значений, разделив их "&tvFiltersAndDelimiter". Поиск значений может проводиться в каком-то конкретном ТВ, если он указан ("myTV==value"), или в любом ("value"). Пример вызова: "&tvFilters=`filter2==one,filter1==bar%||filter1==foo`". <br />Обратите внимание: фильтрация использует оператор LIKE и знак "%" является метасимволом. <br />И еще: Поиск идёт по значениям, которые физически находятся в БД, то есть, сюда не подставляются значения по умолчанию из настроек ТВ.',
              'area_trans' => '',
            ),
            'tvFiltersAndDelimiter' => 
            array (
              'name' => 'tvFiltersAndDelimiter',
              'desc' => 'pdotools_prop_tvFiltersAndDelimiter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => ',',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Разделитель для условий AND в параметре "&tvFilters". По умолчанию: ",".',
              'area_trans' => '',
            ),
            'tvFiltersOrDelimiter' => 
            array (
              'name' => 'tvFiltersOrDelimiter',
              'desc' => 'pdotools_prop_tvFiltersOrDelimiter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '||',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Разделитель для условий OR в параметре "&tvFilters". По умолчанию: "||".',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'pdotools_prop_where',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Массив дополнительных параметров выборки, закодированный в JSON.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'pdotools_prop_showUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать неопубликованные ресурсы.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'pdotools_prop_showDeleted',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать удалённые ресурсы.',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'pdotools_prop_showHidden',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать ресурсы, скрытые в меню.',
              'area_trans' => '',
            ),
            'hideContainers' => 
            array (
              'name' => 'hideContainers',
              'desc' => 'pdotools_prop_hideContainers',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Отключает вывод контейнеров, то есть, ресурсов с isfolder = 1.',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'pdotools_prop_context',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение выборки по контексту ресурсов.',
              'area_trans' => '',
            ),
            'idx' => 
            array (
              'name' => 'idx',
              'desc' => 'pdotools_prop_idx',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Вы можете указать стартовый номер итерации вывода результатов.',
              'area_trans' => '',
            ),
            'first' => 
            array (
              'name' => 'first',
              'desc' => 'pdotools_prop_first',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер первой итерации вывода результатов.',
              'area_trans' => '',
            ),
            'last' => 
            array (
              'name' => 'last',
              'desc' => 'pdotools_prop_last',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер последней итерации вывода результатов. По умолчанию он рассчитается автоматически, по формуле (total + first - 1).',
              'area_trans' => '',
            ),
            'tplFirst' => 
            array (
              'name' => 'tplFirst',
              'desc' => 'pdotools_prop_tplFirst',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для первого ресурса в результатах.',
              'area_trans' => '',
            ),
            'tplLast' => 
            array (
              'name' => 'tplLast',
              'desc' => 'pdotools_prop_tplLast',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для последнего ресурса в результатах.',
              'area_trans' => '',
            ),
            'tplOdd' => 
            array (
              'name' => 'tplOdd',
              'desc' => 'pdotools_prop_tplOdd',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для каждого второго ресурса.',
              'area_trans' => '',
            ),
            'tplWrapper' => 
            array (
              'name' => 'tplWrapper',
              'desc' => 'pdotools_prop_tplWrapper',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка, для заворачивания всех результатов. Понимает один плейсхолдер: [[+output]]. Не работает вместе с параметром "toSeparatePlaceholders".',
              'area_trans' => '',
            ),
            'wrapIfEmpty' => 
            array (
              'name' => 'wrapIfEmpty',
              'desc' => 'pdotools_prop_wrapIfEmpty',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включает вывод чанка-обертки (tplWrapper) даже если результатов нет.',
              'area_trans' => '',
            ),
            'totalVar' => 
            array (
              'name' => 'totalVar',
              'desc' => 'pdotools_prop_totalVar',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'total',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя плейсхолдера для сохранения общего количества результатов.',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'pdotools_prop_resources',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ресурсов, через запятую, для вывода в результатах. Если id ресурса начинается с дефиса, этот ресурс исключается из выборки.',
              'area_trans' => '',
            ),
            'tplCondition' => 
            array (
              'name' => 'tplCondition',
              'desc' => 'pdotools_prop_tplCondition',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Поле ресурса, из которого будет получено значение для выбора чанка по условию в "conditionalTpls".',
              'area_trans' => '',
            ),
            'tplOperator' => 
            array (
              'name' => 'tplOperator',
              'desc' => 'pdotools_prop_tplOperator',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'is equal to',
                  'value' => '==',
                  'name' => 'is equal to',
                ),
                1 => 
                array (
                  'text' => 'is not equal to',
                  'value' => '!=',
                  'name' => 'is not equal to',
                ),
                2 => 
                array (
                  'text' => 'less than',
                  'value' => '<',
                  'name' => 'less than',
                ),
                3 => 
                array (
                  'text' => 'less than or equal to',
                  'value' => '<=',
                  'name' => 'less than or equal to',
                ),
                4 => 
                array (
                  'text' => 'greater than or equal to',
                  'value' => '>=',
                  'name' => 'greater than or equal to',
                ),
                5 => 
                array (
                  'text' => 'is empty',
                  'value' => 'empty',
                  'name' => 'is empty',
                ),
                6 => 
                array (
                  'text' => 'is not empty',
                  'value' => '!empty',
                  'name' => 'is not empty',
                ),
                7 => 
                array (
                  'text' => 'is null',
                  'value' => 'null',
                  'name' => 'is null',
                ),
                8 => 
                array (
                  'text' => 'is in array',
                  'value' => 'inarray',
                  'name' => 'is in array',
                ),
                9 => 
                array (
                  'text' => 'is between',
                  'value' => 'between',
                  'name' => 'is between',
                ),
              ),
              'value' => '==',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Необязательный оператор для проведения сравнения поля ресурса в "tplCondition" с массивом значений и чанков в "conditionalTpls".',
              'area_trans' => '',
            ),
            'conditionalTpls' => 
            array (
              'name' => 'conditionalTpls',
              'desc' => 'pdotools_prop_conditionalTpls',
              'type' => 'textarea',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'JSON строка с массивом, у которого в ключах указано то, с чем будет сравниваться "tplCondition", а в значениях - чанки, которые будут использованы для вывода, если сравнение будет успешно. Оператор сравнения указывается в "tplOperator". Для операторов типа "isempty" можно использовать массив без ключей.',
              'area_trans' => '',
            ),
            'select' => 
            array (
              'name' => 'select',
              'desc' => 'pdotools_prop_select',
              'type' => 'textarea',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список полей для выборки, через запятую. Можно указывать JSON строку с массивом, например {"modResource":"id,pagetitle,content"}.',
              'area_trans' => '',
            ),
            'toSeparatePlaceholders' => 
            array (
              'name' => 'toSeparatePlaceholders',
              'desc' => 'pdotools_prop_toSeparatePlaceholders',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если вы укажете слово в этом параметре, то ВСЕ результаты будут выставлены в разные плейсхолдеры, начинающиеся с этого слова и заканчивающиеся порядковым номером строки, от нуля. Например, указав в параметре "myPl", вы получите плейсхолдеры [[+myPl0]], [[+myPl1]] и т.д.',
              'area_trans' => '',
            ),
            'loadModels' => 
            array (
              'name' => 'loadModels',
              'desc' => 'pdotools_prop_loadModels',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список компонентов, через запятую, чьи модели нужно загрузить для построения запроса. Например: "&loadModels=`ms2gallery,msearch2`".',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'pdotools_prop_scheme',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => '',
                  'text' => 'System default',
                  'name' => 'System default',
                ),
                1 => 
                array (
                  'value' => -1,
                  'text' => '-1 (relative to site_url)',
                  'name' => '-1 (relative to site_url)',
                ),
                2 => 
                array (
                  'value' => 'full',
                  'text' => 'full (absolute, prepended with site_url)',
                  'name' => 'full (absolute, prepended with site_url)',
                ),
                3 => 
                array (
                  'value' => 'abs',
                  'text' => 'abs (absolute, prepended with base_url)',
                  'name' => 'abs (absolute, prepended with base_url)',
                ),
                4 => 
                array (
                  'value' => 'http',
                  'text' => 'http (absolute, forced to http scheme)',
                  'name' => 'http (absolute, forced to http scheme)',
                ),
                5 => 
                array (
                  'value' => 'https',
                  'text' => 'https (absolute, forced to https scheme)',
                  'name' => 'https (absolute, forced to https scheme)',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема формирования ссылок: "uri" для подстановки поля uri документа (очень быстро) или параметр для modX::makeUrl().',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'pdotools_prop_useWeblinkUrl',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Генерировать ссылку с учетом класса ресурса.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdoresources.php',
          'content' => '/** @var array $scriptProperties */
if (isset($parents) && $parents === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
}
if (!empty($returnIds)) {
    $scriptProperties[\'return\'] = \'ids\';
}

// Adding extra parameters into special place so we can put them in a results
/** @var modSnippet $snippet */
$additionalPlaceholders = $properties = array();
if (isset($this) && $this instanceof modSnippet) {
    $properties = $this->get(\'properties\');
}
elseif ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoResources\'))) {
    $properties = $snippet->get(\'properties\');
}
if (!empty($properties)) {
    foreach ($scriptProperties as $k => $v) {
        if (!isset($properties[$k])) {
            $additionalPlaceholders[$k] = $v;
        }
    }
}
$scriptProperties[\'additionalPlaceholders\'] = $additionalPlaceholders;

/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption(\'pdoFetch.class\', null, \'pdotools.pdofetch\', true);
$path = $modx->getOption(\'pdofetch_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoFetch = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoFetch->addTime(\'pdoTools loaded\');
$output = $pdoFetch->run();

$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="pdoResourcesLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

// Return output
if (!empty($returnIds)) {
    $modx->setPlaceholder(\'pdoResources.log\', $log);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
} elseif (!empty($toSeparatePlaceholders)) {
    $output[\'log\'] = $log;
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    $output .= $log;

    if (!empty($tplWrapper) && (!empty($wrapIfEmpty) || !empty($output))) {
        $output = $pdoFetch->getChunk($tplWrapper, array_merge($additionalPlaceholders, array(\'output\' => $output)),
            $pdoFetch->config[\'fastMode\']);
    }

    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
}',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'pdoMenu' => 
      array (
        'fields' => 
        array (
          'id' => 38,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoMenu',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */

// Convert parameters from Wayfinder if exists
if (isset($startId)) {
    $scriptProperties[\'parents\'] = $startId;
}
if (!empty($includeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $includeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',\' . $v;
        } else {
            $scriptProperties[\'resources\'] = $v;
        }
    }
}
if (!empty($excludeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $excludeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',-\' . $v;
        } else {
            $scriptProperties[\'resources\'] = \'-\' . $v;
        }
    }
}

if (!empty($previewUnpublished) && $modx->hasPermission(\'view_unpublished\')) {
    $scriptProperties[\'showUnpublished\'] = 1;
}

$scriptProperties[\'depth\'] = empty($level) ? 100 : abs($level) - 1;
if (!empty($contexts)) {
    $scriptProperties[\'context\'] = $contexts;
}
if (empty($scriptProperties[\'context\'])) {
    $scriptProperties[\'context\'] = $modx->resource->context_key;
}

// Save original parents specified by user
$specified_parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));

if ($scriptProperties[\'parents\'] === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
} elseif ($scriptProperties[\'parents\'] === 0 || $scriptProperties[\'parents\'] === \'0\') {
    if ($scriptProperties[\'depth\'] !== \'\' && $scriptProperties[\'depth\'] !== 100) {
        $contexts = array_map(\'trim\', explode(\',\', $scriptProperties[\'context\']));
        $parents = array();
        if (!empty($scriptProperties[\'showDeleted\'])) {
            $pdoFetch = $modx->getService(\'pdoFetch\');
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $pdoFetch->getChildIds(\'modResource\', 0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        } else {
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $modx->getChildIds(0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        }
        $scriptProperties[\'parents\'] = !empty($parents) ? implode(\',\', $parents) : \'+0\';
        $scriptProperties[\'depth\'] = 0;
    }
    $scriptProperties[\'includeParents\'] = 1;
    $scriptProperties[\'displayStart\'] = 0;
} else {
    $parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));
    $parents_in = $parents_out = array();
    foreach ($parents as $v) {
        if (!is_numeric($v)) {
            continue;
        }
        if ($v[0] == \'-\') {
            $parents_out[] = abs($v);
        } else {
            $parents_in[] = abs($v);
        }
    }

    if (empty($parents_in)) {
        $scriptProperties[\'includeParents\'] = 1;
        $scriptProperties[\'displayStart\'] = 0;
    }
}

if (!empty($displayStart)) {
    $scriptProperties[\'includeParents\'] = 1;
}
if (!empty($ph)) {
    $toPlaceholder = $ph;
}
if (!empty($sortOrder)) {
    $scriptProperties[\'sortdir\'] = $sortOrder;
}
if (!empty($sortBy)) {
    $scriptProperties[\'sortby\'] = $sortBy;
}
if (!empty($permissions)) {
    $scriptProperties[\'checkPermissions\'] = $permissions;
}
if (!empty($cacheResults)) {
    $scriptProperties[\'cache\'] = $cacheResults;
}
if (!empty($ignoreHidden)) {
    $scriptProperties[\'showHidden\'] = $ignoreHidden;
}

$wfTemplates = array(
    \'outerTpl\' => \'tplOuter\',
    \'rowTpl\' => \'tpl\',
    \'parentRowTpl\' => \'tplParentRow\',
    \'parentRowHereTpl\' => \'tplParentRowHere\',
    \'hereTpl\' => \'tplHere\',
    \'innerTpl\' => \'tplInner\',
    \'innerRowTpl\' => \'tplInnerRow\',
    \'innerHereTpl\' => \'tplInnerHere\',
    \'activeParentRowTpl\' => \'tplParentRowActive\',
    \'categoryFoldersTpl\' => \'tplCategoryFolder\',
    \'startItemTpl\' => \'tplStart\',
);
foreach ($wfTemplates as $k => $v) {
    if (isset(${$k})) {
        $scriptProperties[$v] = ${$k};
    }
}
//---

/** @var pdoMenu $pdoMenu */
$fqn = $modx->getOption(\'pdoMenu.class\', null, \'pdotools.pdomenu\', true);
$path = $modx->getOption(\'pdomenu_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoMenu = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoMenu->pdoTools->addTime(\'pdoTools loaded\');

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
if (empty($scriptProperties[\'cache_key\'])) {
    $scriptProperties[\'cache_key\'] = \'pdomenu/\' . sha1(serialize($scriptProperties));
}

$output = \'\';
$tree = array();
if ($cache) {
    $tree = $pdoMenu->pdoTools->getCache($scriptProperties);
}
if (empty($tree)) {
    $data = $pdoMenu->pdoTools->run();
    $data = $pdoMenu->pdoTools->buildTree($data, \'id\', \'parent\', $specified_parents);
    $tree = array();
    foreach ($data as $k => $v) {
        if (empty($v[\'id\'])) {
            if (!in_array($k, $specified_parents) && !$pdoMenu->checkResource($k)) {
                continue;
            } else {
                $tree = array_merge($tree, $v[\'children\']);
            }
        } else {
            $tree[$k] = $v;
        }
    }
    if ($cache) {
        $pdoMenu->pdoTools->setCache($tree, $scriptProperties);
    }
}
if (!empty($tree)) {
    $output = $pdoMenu->templateTree($tree);
}

if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $output .= \'<pre class="pdoMenuLog">\' . print_r($pdoMenu->pdoTools->getTime(), 1) . \'</pre>\';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
          'locked' => false,
          'properties' => 
          array (
            'showLog' => 
            array (
              'name' => 'showLog',
              'desc' => 'pdotools_prop_showLog',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать дополнительную информацию о работе сниппета. Только для авторизованных в контекте "mgr".',
              'area_trans' => '',
            ),
            'fastMode' => 
            array (
              'name' => 'fastMode',
              'desc' => 'pdotools_prop_fastMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Быстрый режим обработки чанков. Все необработанные теги (условия, сниппеты и т.п.) будут вырезаны.',
              'area_trans' => '',
            ),
            'level' => 
            array (
              'name' => 'level',
              'desc' => 'pdotools_prop_level',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Уровень генерируемого меню.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'pdotools_prop_parents',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список родителей, через запятую, для поиска результатов. По умолчанию выборка ограничена текущим родителем. Если поставить 0 - выборка не ограничивается. Если id родителя начинается с дефиса, он и его потомки исключается из выборки.',
              'area_trans' => '',
            ),
            'displayStart' => 
            array (
              'name' => 'displayStart',
              'desc' => 'pdotools_prop_displayStart',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить показ начальных узлов меню. Полезно при указании более одного "parents".',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'pdotools_prop_resources',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ресурсов, через запятую, для вывода в результатах. Если id ресурса начинается с дефиса, этот ресурс исключается из выборки.',
              'area_trans' => '',
            ),
            'templates' => 
            array (
              'name' => 'templates',
              'desc' => 'pdotools_prop_templates',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список шаблонов, через запятую, для фильтрации результатов. Если id шаблона начинается с дефиса, ресурсы с ним исключается из выборки.',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'pdotools_prop_context',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение выборки по контексту ресурсов.',
              'area_trans' => '',
            ),
            'cache' => 
            array (
              'name' => 'cache',
              'desc' => 'pdotools_prop_cache',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Кэширование результатов работы сниппета.',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'pdotools_prop_cacheTime',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 3600,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Время актуальности кэша в секундах.',
              'area_trans' => '',
            ),
            'cacheAnonymous' => 
            array (
              'name' => 'cacheAnonymous',
              'desc' => 'pdotools_prop_cacheAnonymous',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить кэширование только для неавторизованных посетителей.',
              'area_trans' => '',
            ),
            'plPrefix' => 
            array (
              'name' => 'plPrefix',
              'desc' => 'pdotools_prop_plPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'wf.',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для выставляемых плейсхолдеров, по умолчанию "wf.".',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'pdotools_prop_showHidden',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать ресурсы, скрытые в меню.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'pdotools_prop_showUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать неопубликованные ресурсы.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'pdotools_prop_showDeleted',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать удалённые ресурсы.',
              'area_trans' => '',
            ),
            'previewUnpublished' => 
            array (
              'name' => 'previewUnpublished',
              'desc' => 'pdotools_prop_previewUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить показ неопубликованных документов, если у пользователя есть на это разрешение.',
              'area_trans' => '',
            ),
            'hideSubMenus' => 
            array (
              'name' => 'hideSubMenus',
              'desc' => 'pdotools_prop_hideSubMenus',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Спрятать неактивные ветки меню.',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'pdotools_prop_useWeblinkUrl',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Генерировать ссылку с учетом класса ресурса.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'pdotools_prop_sortdir',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки: по убыванию или возрастанию.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'pdotools_prop_sortby',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'menuindex',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Любое поле ресурса для сортировки, включая ТВ параметр, если он указан в параметре "includeTVs". Можно указывать JSON строку с массивом нескольких полей. Для случайно сортировки укажите "RAND()"',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'rowIdPrefix' => 
            array (
              'name' => 'rowIdPrefix',
              'desc' => 'pdotools_prop_rowIdPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс id="" для выставления идентификатора в чанк.',
              'area_trans' => '',
            ),
            'firstClass' => 
            array (
              'name' => 'firstClass',
              'desc' => 'pdotools_prop_firstClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'first',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс для первого пункта меню.',
              'area_trans' => '',
            ),
            'lastClass' => 
            array (
              'name' => 'lastClass',
              'desc' => 'pdotools_prop_lastClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'last',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс последнего пункта меню.',
              'area_trans' => '',
            ),
            'hereClass' => 
            array (
              'name' => 'hereClass',
              'desc' => 'pdotools_prop_hereClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'active',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс для активного пункта меню.',
              'area_trans' => '',
            ),
            'parentClass' => 
            array (
              'name' => 'parentClass',
              'desc' => 'pdotools_prop_parentClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс категории меню.',
              'area_trans' => '',
            ),
            'rowClass' => 
            array (
              'name' => 'rowClass',
              'desc' => 'pdotools_prop_rowClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс одной строки меню.',
              'area_trans' => '',
            ),
            'outerClass' => 
            array (
              'name' => 'outerClass',
              'desc' => 'pdotools_prop_outerClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс обертки меню.',
              'area_trans' => '',
            ),
            'innerClass' => 
            array (
              'name' => 'innerClass',
              'desc' => 'pdotools_prop_innerClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс внутренних ссылок меню.',
              'area_trans' => '',
            ),
            'levelClass' => 
            array (
              'name' => 'levelClass',
              'desc' => 'pdotools_prop_levelClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс уровня меню. Например, если укажите "level", то будет "level1", "level2" и т.д.',
              'area_trans' => '',
            ),
            'selfClass' => 
            array (
              'name' => 'selfClass',
              'desc' => 'pdotools_prop_selfClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс текущего документа в меню.',
              'area_trans' => '',
            ),
            'webLinkClass' => 
            array (
              'name' => 'webLinkClass',
              'desc' => 'pdotools_prop_webLinkClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс документа-ссылки.',
              'area_trans' => '',
            ),
            'tplOuter' => 
            array (
              'name' => 'tplOuter',
              'desc' => 'pdotools_prop_tplOuter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <ul[[+classes]]>[[+wrapper]]</ul>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка всего блока меню.',
              'area_trans' => '',
            ),
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'pdotools_prop_tpl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li[[+classes]]><a href="[[+link]]" [[+attributes]]>[[+menutitle]]</a>[[+wrapper]]</li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для оформления ресурса. Если не указан, то содержимое полей ресурса будет распечатано на экран.',
              'area_trans' => '',
            ),
            'tplParentRow' => 
            array (
              'name' => 'tplParentRow',
              'desc' => 'pdotools_prop_tplParentRow',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplParentRowHere' => 
            array (
              'name' => 'tplParentRowHere',
              'desc' => 'pdotools_prop_tplParentRowHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления текущего контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplHere' => 
            array (
              'name' => 'tplHere',
              'desc' => 'pdotools_prop_tplHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк текущего документа',
              'area_trans' => '',
            ),
            'tplInner' => 
            array (
              'name' => 'tplInner',
              'desc' => 'pdotools_prop_tplInner',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка внутренних пунктов меню. Если пуст - будет использовать "tplInner".',
              'area_trans' => '',
            ),
            'tplInnerRow' => 
            array (
              'name' => 'tplInnerRow',
              'desc' => 'pdotools_prop_tplInnerRow',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка активного пункта меню.',
              'area_trans' => '',
            ),
            'tplInnerHere' => 
            array (
              'name' => 'tplInnerHere',
              'desc' => 'pdotools_prop_tplInnerHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка активного пункта меню.',
              'area_trans' => '',
            ),
            'tplParentRowActive' => 
            array (
              'name' => 'tplParentRowActive',
              'desc' => 'pdotools_prop_tplParentRowActive',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления активного контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplCategoryFolder' => 
            array (
              'name' => 'tplCategoryFolder',
              'desc' => 'pdotools_prop_tplCategoryFolder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Специальный чанк оформления категории. Категория - это документ с потомками и или нулевым шаблоном, или с атрибутом "rel=\\"category\\"".',
              'area_trans' => '',
            ),
            'tplStart' => 
            array (
              'name' => 'tplStart',
              'desc' => 'pdotools_prop_tplStart',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <h2[[+classes]]>[[+menutitle]]</h2>[[+wrapper]]',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления корневого пункта, при условии, что включен "displayStart".',
              'area_trans' => '',
            ),
            'checkPermissions' => 
            array (
              'name' => 'checkPermissions',
              'desc' => 'pdotools_prop_checkPermissions',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Укажите, какие разрешения нужно проверять у пользователя при выводе документов.',
              'area_trans' => '',
            ),
            'hereId' => 
            array (
              'name' => 'hereId',
              'desc' => 'pdotools_prop_hereId',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Id документа, текущего для генерируемого меню. Нужно указывать только если скрипт сам его неверно определяет, например при выводе меню из чанка другого сниппета.',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'pdotools_prop_where',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Массив дополнительных параметров выборки, закодированный в JSON.',
              'area_trans' => '',
            ),
            'select' => 
            array (
              'name' => 'select',
              'desc' => 'pdotools_prop_select',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список полей для выборки, через запятую. Можно указывать JSON строку с массивом, например {"modResource":"id,pagetitle,content"}.',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'pdotools_prop_scheme',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => '',
                  'text' => 'System default',
                  'name' => 'System default',
                ),
                1 => 
                array (
                  'value' => -1,
                  'text' => '-1 (relative to site_url)',
                  'name' => '-1 (relative to site_url)',
                ),
                2 => 
                array (
                  'value' => 'full',
                  'text' => 'full (absolute, prepended with site_url)',
                  'name' => 'full (absolute, prepended with site_url)',
                ),
                3 => 
                array (
                  'value' => 'abs',
                  'text' => 'abs (absolute, prepended with base_url)',
                  'name' => 'abs (absolute, prepended with base_url)',
                ),
                4 => 
                array (
                  'value' => 'http',
                  'text' => 'http (absolute, forced to http scheme)',
                  'name' => 'http (absolute, forced to http scheme)',
                ),
                5 => 
                array (
                  'value' => 'https',
                  'text' => 'https (absolute, forced to https scheme)',
                  'name' => 'https (absolute, forced to https scheme)',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема формирования ссылок: "uri" для подстановки поля uri документа (очень быстро) или параметр для modX::makeUrl().',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'countChildren' => 
            array (
              'name' => 'countChildren',
              'desc' => 'pdotools_prop_countChildren',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Вывести точное количество активных потомков документа в плейсхолдер [[+children]].',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdomenu.php',
          'content' => '/** @var array $scriptProperties */

// Convert parameters from Wayfinder if exists
if (isset($startId)) {
    $scriptProperties[\'parents\'] = $startId;
}
if (!empty($includeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $includeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',\' . $v;
        } else {
            $scriptProperties[\'resources\'] = $v;
        }
    }
}
if (!empty($excludeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $excludeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',-\' . $v;
        } else {
            $scriptProperties[\'resources\'] = \'-\' . $v;
        }
    }
}

if (!empty($previewUnpublished) && $modx->hasPermission(\'view_unpublished\')) {
    $scriptProperties[\'showUnpublished\'] = 1;
}

$scriptProperties[\'depth\'] = empty($level) ? 100 : abs($level) - 1;
if (!empty($contexts)) {
    $scriptProperties[\'context\'] = $contexts;
}
if (empty($scriptProperties[\'context\'])) {
    $scriptProperties[\'context\'] = $modx->resource->context_key;
}

// Save original parents specified by user
$specified_parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));

if ($scriptProperties[\'parents\'] === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
} elseif ($scriptProperties[\'parents\'] === 0 || $scriptProperties[\'parents\'] === \'0\') {
    if ($scriptProperties[\'depth\'] !== \'\' && $scriptProperties[\'depth\'] !== 100) {
        $contexts = array_map(\'trim\', explode(\',\', $scriptProperties[\'context\']));
        $parents = array();
        if (!empty($scriptProperties[\'showDeleted\'])) {
            $pdoFetch = $modx->getService(\'pdoFetch\');
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $pdoFetch->getChildIds(\'modResource\', 0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        } else {
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $modx->getChildIds(0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        }
        $scriptProperties[\'parents\'] = !empty($parents) ? implode(\',\', $parents) : \'+0\';
        $scriptProperties[\'depth\'] = 0;
    }
    $scriptProperties[\'includeParents\'] = 1;
    $scriptProperties[\'displayStart\'] = 0;
} else {
    $parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));
    $parents_in = $parents_out = array();
    foreach ($parents as $v) {
        if (!is_numeric($v)) {
            continue;
        }
        if ($v[0] == \'-\') {
            $parents_out[] = abs($v);
        } else {
            $parents_in[] = abs($v);
        }
    }

    if (empty($parents_in)) {
        $scriptProperties[\'includeParents\'] = 1;
        $scriptProperties[\'displayStart\'] = 0;
    }
}

if (!empty($displayStart)) {
    $scriptProperties[\'includeParents\'] = 1;
}
if (!empty($ph)) {
    $toPlaceholder = $ph;
}
if (!empty($sortOrder)) {
    $scriptProperties[\'sortdir\'] = $sortOrder;
}
if (!empty($sortBy)) {
    $scriptProperties[\'sortby\'] = $sortBy;
}
if (!empty($permissions)) {
    $scriptProperties[\'checkPermissions\'] = $permissions;
}
if (!empty($cacheResults)) {
    $scriptProperties[\'cache\'] = $cacheResults;
}
if (!empty($ignoreHidden)) {
    $scriptProperties[\'showHidden\'] = $ignoreHidden;
}

$wfTemplates = array(
    \'outerTpl\' => \'tplOuter\',
    \'rowTpl\' => \'tpl\',
    \'parentRowTpl\' => \'tplParentRow\',
    \'parentRowHereTpl\' => \'tplParentRowHere\',
    \'hereTpl\' => \'tplHere\',
    \'innerTpl\' => \'tplInner\',
    \'innerRowTpl\' => \'tplInnerRow\',
    \'innerHereTpl\' => \'tplInnerHere\',
    \'activeParentRowTpl\' => \'tplParentRowActive\',
    \'categoryFoldersTpl\' => \'tplCategoryFolder\',
    \'startItemTpl\' => \'tplStart\',
);
foreach ($wfTemplates as $k => $v) {
    if (isset(${$k})) {
        $scriptProperties[$v] = ${$k};
    }
}
//---

/** @var pdoMenu $pdoMenu */
$fqn = $modx->getOption(\'pdoMenu.class\', null, \'pdotools.pdomenu\', true);
$path = $modx->getOption(\'pdomenu_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoMenu = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoMenu->pdoTools->addTime(\'pdoTools loaded\');

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
if (empty($scriptProperties[\'cache_key\'])) {
    $scriptProperties[\'cache_key\'] = \'pdomenu/\' . sha1(serialize($scriptProperties));
}

$output = \'\';
$tree = array();
if ($cache) {
    $tree = $pdoMenu->pdoTools->getCache($scriptProperties);
}
if (empty($tree)) {
    $data = $pdoMenu->pdoTools->run();
    $data = $pdoMenu->pdoTools->buildTree($data, \'id\', \'parent\', $specified_parents);
    $tree = array();
    foreach ($data as $k => $v) {
        if (empty($v[\'id\'])) {
            if (!in_array($k, $specified_parents) && !$pdoMenu->checkResource($k)) {
                continue;
            } else {
                $tree = array_merge($tree, $v[\'children\']);
            }
        } else {
            $tree[$k] = $v;
        }
    }
    if ($cache) {
        $pdoMenu->pdoTools->setCache($tree, $scriptProperties);
    }
}
if (!empty($tree)) {
    $output = $pdoMenu->templateTree($tree);
}

if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $output .= \'<pre class="pdoMenuLog">\' . print_r($pdoMenu->pdoTools->getTime(), 1) . \'</pre>\';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'If' => 
      array (
        'fields' => 
        array (
          'id' => 30,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'If',
          'description' => 'Simple if (conditional) snippet',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * If
 *
 * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick
 * <shaun@modx.com>
 *
 * If is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * If is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package if
 */
/**
 * Simple if (conditional) snippet
 *
 * @package if
 */
if (!empty($debug)) {
    print_r($scriptProperties);
    if (!empty($die)) die();
}
$modx->parser->processElementTags(\'\',$subject,true,true);

$output = \'\';
$operator = !empty($operator) ? $operator : \'\';
$operand = !isset($operand) ? \'\' : $operand;
if (isset($subject)) {
    if (!empty($operator)) {
        $operator = strtolower($operator);
        switch ($operator) {
            case \'!=\':
            case \'neq\':
            case \'not\':
            case \'isnot\':
            case \'isnt\':
            case \'unequal\':
            case \'notequal\':
                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<\':
            case \'lt\':
            case \'less\':
            case \'lessthan\':
                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>\':
            case \'gt\':
            case \'greater\':
            case \'greaterthan\':
                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<=\':
            case \'lte\':
            case \'lessthanequals\':
            case \'lessthanorequalto\':
                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>=\':
            case \'gte\':
            case \'greaterthanequals\':
            case \'greaterthanequalto\':
                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'isempty\':
            case \'empty\':
                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'!empty\':
            case \'notempty\':
            case \'isnotempty\':
                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'isnull\':
            case \'null\':
                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'inarray\':
            case \'in_array\':
            case \'ia\':
                $operand = explode(\',\',$operand);
                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'==\':
            case \'=\':
            case \'eq\':
            case \'is\':
            case \'equal\':
            case \'equals\':
            case \'equalto\':
            default:
                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
        }
    }
}
if (!empty($debug)) { var_dump($output); }
unset($subject);
return $output;',
          'locked' => false,
          'properties' => 
          array (
            'subject' => 
            array (
              'name' => 'subject',
              'desc' => 'The data being affected.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The data being affected.',
              'area_trans' => '',
            ),
            'operator' => 
            array (
              'name' => 'operator',
              'desc' => 'The type of conditional.',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => 'EQ',
                  'text' => 'EQ',
                  'name' => 'EQ',
                ),
                1 => 
                array (
                  'value' => 'NEQ',
                  'text' => 'NEQ',
                  'name' => 'NEQ',
                ),
                2 => 
                array (
                  'value' => 'LT',
                  'text' => 'LT',
                  'name' => 'LT',
                ),
                3 => 
                array (
                  'value' => 'GT',
                  'text' => 'GT',
                  'name' => 'GT',
                ),
                4 => 
                array (
                  'value' => 'LTE',
                  'text' => 'LTE',
                  'name' => 'LTE',
                ),
                5 => 
                array (
                  'value' => 'GT',
                  'text' => 'GTE',
                  'name' => 'GTE',
                ),
                6 => 
                array (
                  'value' => 'EMPTY',
                  'text' => 'EMPTY',
                  'name' => 'EMPTY',
                ),
                7 => 
                array (
                  'value' => 'NOTEMPTY',
                  'text' => 'NOTEMPTY',
                  'name' => 'NOTEMPTY',
                ),
                8 => 
                array (
                  'value' => 'ISNULL',
                  'text' => 'ISNULL',
                  'name' => 'ISNULL',
                ),
                9 => 
                array (
                  'value' => 'inarray',
                  'text' => 'INARRAY',
                  'name' => 'INARRAY',
                ),
              ),
              'value' => 'EQ',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'The type of conditional.',
              'area_trans' => '',
            ),
            'operand' => 
            array (
              'name' => 'operand',
              'desc' => 'When comparing to the subject, this is the data to compare to.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'When comparing to the subject, this is the data to compare to.',
              'area_trans' => '',
            ),
            'then' => 
            array (
              'name' => 'then',
              'desc' => 'If conditional was successful, output this.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If conditional was successful, output this.',
              'area_trans' => '',
            ),
            'else' => 
            array (
              'name' => 'else',
              'desc' => 'If conditional was unsuccessful, output this.',
              'type' => 'textfield',
              'options' => '',
              'value' => '',
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'If conditional was unsuccessful, output this.',
              'area_trans' => '',
            ),
            'debug' => 
            array (
              'name' => 'debug',
              'desc' => 'Will output the parameters passed in, as well as the end output. Leave off when not testing.',
              'type' => 'combo-boolean',
              'options' => '',
              'value' => false,
              'lexicon' => NULL,
              'area' => '',
              'desc_trans' => 'Will output the parameters passed in, as well as the end output. Leave off when not testing.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * If
 *
 * Copyright 2009-2010 by Jason Coward <jason@modx.com> and Shaun McCormick
 * <shaun@modx.com>
 *
 * If is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 *
 * If is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * If; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package if
 */
/**
 * Simple if (conditional) snippet
 *
 * @package if
 */
if (!empty($debug)) {
    print_r($scriptProperties);
    if (!empty($die)) die();
}
$modx->parser->processElementTags(\'\',$subject,true,true);

$output = \'\';
$operator = !empty($operator) ? $operator : \'\';
$operand = !isset($operand) ? \'\' : $operand;
if (isset($subject)) {
    if (!empty($operator)) {
        $operator = strtolower($operator);
        switch ($operator) {
            case \'!=\':
            case \'neq\':
            case \'not\':
            case \'isnot\':
            case \'isnt\':
            case \'unequal\':
            case \'notequal\':
                $output = (($subject != $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<\':
            case \'lt\':
            case \'less\':
            case \'lessthan\':
                $output = (($subject < $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>\':
            case \'gt\':
            case \'greater\':
            case \'greaterthan\':
                $output = (($subject > $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'<=\':
            case \'lte\':
            case \'lessthanequals\':
            case \'lessthanorequalto\':
                $output = (($subject <= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'>=\':
            case \'gte\':
            case \'greaterthanequals\':
            case \'greaterthanequalto\':
                $output = (($subject >= $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
            case \'isempty\':
            case \'empty\':
                $output = empty($subject) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'!empty\':
            case \'notempty\':
            case \'isnotempty\':
                $output = !empty($subject) && $subject != \'\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'isnull\':
            case \'null\':
                $output = $subject == null || strtolower($subject) == \'null\' ? $then : (isset($else) ? $else : \'\');
                break;
            case \'inarray\':
            case \'in_array\':
            case \'ia\':
                $operand = explode(\',\',$operand);
                $output = in_array($subject,$operand) ? $then : (isset($else) ? $else : \'\');
                break;
            case \'==\':
            case \'=\':
            case \'eq\':
            case \'is\':
            case \'equal\':
            case \'equals\':
            case \'equalto\':
            default:
                $output = (($subject == $operand) ? $then : (isset($else) ? $else : \'\'));
                break;
        }
    }
}
if (!empty($debug)) { var_dump($output); }
unset($subject);
return $output;',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'getImageList' => 
      array (
        'fields' => 
        array (
          'id' => 10,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'getImageList',
          'description' => '',
          'editor_type' => 0,
          'category' => 9,
          'cache_type' => 0,
          'snippet' => '/**
 * getImageList
 *
 * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>
 *
 * getImageList is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package migx
 */
/**
 * getImageList
 *
 * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution 
 *
 * @version 1.4
 * @author Bruno Perner <b.perner@gmx.de>
 * @copyright Copyright &copy; 2009-2014
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License
 * version 2 or (at your option) any later version.
 * @package migx
 */

/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/
/* get default properties */


$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');
$inherit_children_tvname = $modx->getOption(\'inherit_children_tvname\', $scriptProperties, \'\');
$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');
$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');
$emptyTpl = $modx->getOption(\'emptyTpl\', $scriptProperties, \'\'); 
$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');
$offset = $modx->getOption(\'offset\', $scriptProperties, 0);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);
$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images
$where = $modx->getOption(\'where\', $scriptProperties, \'\');
$where = !empty($where) ? $modx->fromJSON($where) : array();
$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');
$sort = !empty($sort) ? $modx->fromJSON($sort) : array();
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');
$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);
$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');
$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');
$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');
$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));
$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;
$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');
$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');
$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');
$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');
$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');
$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;
//split json into parts
$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));
$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from
$inheritFrom = !empty($inheritFrom) ? explode(\',\', $inheritFrom) : \'\';

$modx->setPlaceholder(\'docid\', $docid);

$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);
$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);

$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);
if (!($migx instanceof Migx))
    return \'\';
$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';

if (!empty($tvname)) {
    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {

        /*
        *   get inputProperties
        */


        $properties = $tv->get(\'input_properties\');
        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();

        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');
        if (!empty($migx->config[\'configs\'])) {
            $migx->loadConfigs();
            // get tabs from file or migx-config-table
            $formtabs = $migx->getTabs();
        }
        if (empty($formtabs) && isset($properties[\'formtabs\'])) {
            //try to get formtabs and its fields from properties
            $formtabs = $modx->fromJSON($properties[\'formtabs\']);
        }

        if (!empty($properties[\'basePath\'])) {
            if ($properties[\'autoResourceFolders\'] == \'true\') {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';
            } else {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];
            }
        }
        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {
            $jsonVarKey = $properties[\'jsonvarkey\'];
            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
        }

        if (empty($outputvalue)) {
            $outputvalue = $tv->renderOutput($docid);
            if (empty($outputvalue) && !empty($inheritFrom)) {
                foreach ($inheritFrom as $from) {
                    if ($from == \'parents\') {
                        if (!empty($inherit_children_tvname)){
                            //try to get items from optional MIGX-TV for children
                            if ($inh_tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inherit_children_tvname))) {
                                $outputvalue = $inh_tv->processInheritBinding(\'\', $docid);    
                            }
                        }
                        $outputvalue = empty($outputvalue) ? $tv->processInheritBinding(\'\', $docid) : $outputvalue;
                    } else {
                        $outputvalue = $tv->renderOutput($from);
                    }
                    if (!empty($outputvalue)) {
                        break;
                    }
                }
            }
        }


        /*
        *   get inputTvs 
        */
        $inputTvs = array();
        if (is_array($formtabs)) {

            //multiple different Forms
            // Note: use same field-names and inputTVs in all forms
            $inputTvs = $migx->extractInputTvs($formtabs);
        }
        if ($migx->source = $tv->getSource($migx->working_context, false)) {
            $migx->source->initialize();
        }

    }


}

if (empty($outputvalue)) {
    $modx->setPlaceholder($totalVar, 0);
    return \'\';
}

//echo $outputvalue.\'<br/><br/>\';

$items = $modx->fromJSON($outputvalue);

// where filter
if (is_array($where) && count($where) > 0) {
    $items = $migx->filterItems($where, $items);
}
$modx->setPlaceholder($totalVar, count($items));

if (!empty($reverse)) {
    $items = array_reverse($items);
}

// sort items
if (is_array($sort) && count($sort) > 0) {
    $items = $migx->sortDbResult($items, $sort);
}

$summaries = array();
$output = \'\';
$items = $offset > 0 ? array_slice($items, $offset) : $items;
$count = count($items);

if ($count > 0) {
    $limit = $limit == 0 || $limit > $count ? $count : $limit;
    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;
    //preselect important items
    $preitems = array();
    if ($randomize && $preselectLimit > 0) {
        for ($i = 0; $i < $preselectLimit; $i++) {
            $preitems[] = $items[$i];
            unset($items[$i]);
        }
        $limit = $limit - count($preitems);
    }

    //shuffle items
    if ($randomize) {
        shuffle($items);
    }

    //limit items
    $count = count($items);
    $tempitems = array();

    for ($i = 0; $i < $limit; $i++) {
        if ($i >= $count) {
            break;
        }
        $tempitems[] = $items[$i];
    }
    $items = $tempitems;

    //add preselected items and schuffle again
    if ($randomize && $preselectLimit > 0) {
        $items = array_merge($preitems, $items);
        shuffle($items);
    }

    $properties = array();
    foreach ($scriptProperties as $property => $value) {
        $properties[\'property.\' . $property] = $value;
    }

    $idx = 0;
    $output = array();
    $template = array();
    $count = count($items);

    foreach ($items as $key => $item) {
        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';
        $fields = array();
        foreach ($item as $field => $value) {
            if (is_array($value)) {
                if (is_array($value[0])) {
                    //nested array - convert to json
                    $value = $modx->toJson($value);
                } else {
                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)
                }
            }


            $inputTVkey = $formname . $field;

            if ($processTVs && isset($inputTvs[$inputTVkey])) {
                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {

                } else {
                    $tv = $modx->newObject(\'modTemplateVar\');
                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);
                }
                $inputTV = $inputTvs[$inputTVkey];

                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');
                //don\'t manipulate any urls here
                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');
                $tv->set(\'default_text\', $value);
                $value = $tv->renderOutput($docid);
                //set option back
                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);
                //now manipulate urls
                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {
                    $mTypes = explode(\',\', $mTypes);
                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {
                        //$value = $mediasource->prepareOutputUrl($value);
                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));
                    }
                }

            }
            $fields[$field] = $value;

        }

        if (!empty($addfields)) {
            foreach ($addfields as $addfield) {
                $addfield = explode(\':\', $addfield);
                $addname = $addfield[0];
                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';
                $fields[$addname] = $adddefault;
            }
        }

        if (!empty($sumFields)) {
            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);
            foreach ($sumFields as $sumField) {
                if (isset($fields[$sumField])) {
                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];
                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];
                }
            }
        }


        if ($toJsonPlaceholder) {
            $output[] = $fields;
        } else {
            $fields[\'_alt\'] = $idx % 2;
            $idx++;
            $fields[\'_first\'] = $idx == 1 ? true : \'\';
            $fields[\'_last\'] = $idx == $limit ? true : \'\';
            $fields[\'idx\'] = $idx;
            $rowtpl = \'\';
            //get changing tpls from field
            if (substr($tpl, 0, 7) == "@FIELD:") {
                $tplField = substr($tpl, 7);
                $rowtpl = $fields[$tplField];
            }

            if ($fields[\'_first\'] && !empty($tplFirst)) {
                $rowtpl = $tplFirst;
            }
            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {
                $rowtpl = $tplLast;
            }
            $tplidx = \'tpl_\' . $idx;
            if (empty($rowtpl) && !empty($$tplidx)) {
                $rowtpl = $$tplidx;
            }
            if ($idx > 1 && empty($rowtpl)) {
                $divisors = $migx->getDivisors($idx);
                if (!empty($divisors)) {
                    foreach ($divisors as $divisor) {
                        $tplnth = \'tpl_n\' . $divisor;
                        if (!empty($$tplnth)) {
                            $rowtpl = $$tplnth;
                            if (!empty($rowtpl)) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($count == 1 && isset($tpl_oneresult)) {
                $rowtpl = $tpl_oneresult;
            }

            $fields = array_merge($fields, $properties);

            if (!empty($rowtpl)) {
                $template = $migx->getTemplate($tpl, $template);
                $fields[\'_tpl\'] = $template[$tpl];
            } else {
                $rowtpl = $tpl;

            }
            $template = $migx->getTemplate($rowtpl, $template);


            if ($template[$rowtpl]) {
                $chunk = $modx->newObject(\'modChunk\');
                $chunk->setCacheable(false);
                $chunk->setContent($template[$rowtpl]);


                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {
                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);
                } else {
                    $output[] = $chunk->process($fields);
                }
            } else {
                if (!empty($placeholdersKeyField)) {
                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                } else {
                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                }
            }
        }


    }
}

if (count($summaries) > 0) {
    $modx->toPlaceholders($summaries);
}


if ($toJsonPlaceholder) {
    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));
    return \'\';
}

if (!empty($toSeparatePlaceholders)) {
    $modx->toPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}
/*
if (!empty($outerTpl))
$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));
else 
*/

if ($count > 0 && $splits > 0) {
    $size = ceil($count / $splits);
    $chunks = array_chunk($output, $size);
    $output = array();
    foreach ($chunks as $chunk) {
        $o = implode($outputSeparator, $chunk);
        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));
    }
    $outputSeparator = $splitSeparator;
}

if (is_array($output)) {
    $o = implode($outputSeparator, $output);
} else {
    $o = $output;
}

if (!empty($o) && !empty($wrapperTpl)) {
    $template = $migx->getTemplate($wrapperTpl);
    if ($template[$wrapperTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$wrapperTpl]);
        $properties[\'output\'] = $o;
        $o = $chunk->process($properties);
    }
}

if (empty($o) && !empty($emptyTpl)) {
    $template = $migx->getTemplate($emptyTpl);
    if ($template[$emptyTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$emptyTpl]);
        $o = $chunk->process($properties);
    }
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $o);
    return \'\';
}

return $o;',
          'locked' => false,
          'properties' => 
          array (
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * getImageList
 *
 * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>
 *
 * getImageList is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package migx
 */
/**
 * getImageList
 *
 * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution 
 *
 * @version 1.4
 * @author Bruno Perner <b.perner@gmx.de>
 * @copyright Copyright &copy; 2009-2014
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License
 * version 2 or (at your option) any later version.
 * @package migx
 */

/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/
/* get default properties */


$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');
$inherit_children_tvname = $modx->getOption(\'inherit_children_tvname\', $scriptProperties, \'\');
$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');
$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');
$emptyTpl = $modx->getOption(\'emptyTpl\', $scriptProperties, \'\'); 
$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');
$offset = $modx->getOption(\'offset\', $scriptProperties, 0);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);
$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images
$where = $modx->getOption(\'where\', $scriptProperties, \'\');
$where = !empty($where) ? $modx->fromJSON($where) : array();
$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');
$sort = !empty($sort) ? $modx->fromJSON($sort) : array();
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');
$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);
$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');
$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');
$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');
$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));
$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;
$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');
$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');
$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');
$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');
$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');
$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;
//split json into parts
$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));
$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from
$inheritFrom = !empty($inheritFrom) ? explode(\',\', $inheritFrom) : \'\';

$modx->setPlaceholder(\'docid\', $docid);

$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);
$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);

$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);
if (!($migx instanceof Migx))
    return \'\';
$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';

if (!empty($tvname)) {
    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {

        /*
        *   get inputProperties
        */


        $properties = $tv->get(\'input_properties\');
        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();

        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');
        if (!empty($migx->config[\'configs\'])) {
            $migx->loadConfigs();
            // get tabs from file or migx-config-table
            $formtabs = $migx->getTabs();
        }
        if (empty($formtabs) && isset($properties[\'formtabs\'])) {
            //try to get formtabs and its fields from properties
            $formtabs = $modx->fromJSON($properties[\'formtabs\']);
        }

        if (!empty($properties[\'basePath\'])) {
            if ($properties[\'autoResourceFolders\'] == \'true\') {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';
            } else {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];
            }
        }
        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {
            $jsonVarKey = $properties[\'jsonvarkey\'];
            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
        }

        if (empty($outputvalue)) {
            $outputvalue = $tv->renderOutput($docid);
            if (empty($outputvalue) && !empty($inheritFrom)) {
                foreach ($inheritFrom as $from) {
                    if ($from == \'parents\') {
                        if (!empty($inherit_children_tvname)){
                            //try to get items from optional MIGX-TV for children
                            if ($inh_tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inherit_children_tvname))) {
                                $outputvalue = $inh_tv->processInheritBinding(\'\', $docid);    
                            }
                        }
                        $outputvalue = empty($outputvalue) ? $tv->processInheritBinding(\'\', $docid) : $outputvalue;
                    } else {
                        $outputvalue = $tv->renderOutput($from);
                    }
                    if (!empty($outputvalue)) {
                        break;
                    }
                }
            }
        }


        /*
        *   get inputTvs 
        */
        $inputTvs = array();
        if (is_array($formtabs)) {

            //multiple different Forms
            // Note: use same field-names and inputTVs in all forms
            $inputTvs = $migx->extractInputTvs($formtabs);
        }
        if ($migx->source = $tv->getSource($migx->working_context, false)) {
            $migx->source->initialize();
        }

    }


}

if (empty($outputvalue)) {
    $modx->setPlaceholder($totalVar, 0);
    return \'\';
}

//echo $outputvalue.\'<br/><br/>\';

$items = $modx->fromJSON($outputvalue);

// where filter
if (is_array($where) && count($where) > 0) {
    $items = $migx->filterItems($where, $items);
}
$modx->setPlaceholder($totalVar, count($items));

if (!empty($reverse)) {
    $items = array_reverse($items);
}

// sort items
if (is_array($sort) && count($sort) > 0) {
    $items = $migx->sortDbResult($items, $sort);
}

$summaries = array();
$output = \'\';
$items = $offset > 0 ? array_slice($items, $offset) : $items;
$count = count($items);

if ($count > 0) {
    $limit = $limit == 0 || $limit > $count ? $count : $limit;
    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;
    //preselect important items
    $preitems = array();
    if ($randomize && $preselectLimit > 0) {
        for ($i = 0; $i < $preselectLimit; $i++) {
            $preitems[] = $items[$i];
            unset($items[$i]);
        }
        $limit = $limit - count($preitems);
    }

    //shuffle items
    if ($randomize) {
        shuffle($items);
    }

    //limit items
    $count = count($items);
    $tempitems = array();

    for ($i = 0; $i < $limit; $i++) {
        if ($i >= $count) {
            break;
        }
        $tempitems[] = $items[$i];
    }
    $items = $tempitems;

    //add preselected items and schuffle again
    if ($randomize && $preselectLimit > 0) {
        $items = array_merge($preitems, $items);
        shuffle($items);
    }

    $properties = array();
    foreach ($scriptProperties as $property => $value) {
        $properties[\'property.\' . $property] = $value;
    }

    $idx = 0;
    $output = array();
    $template = array();
    $count = count($items);

    foreach ($items as $key => $item) {
        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';
        $fields = array();
        foreach ($item as $field => $value) {
            if (is_array($value)) {
                if (is_array($value[0])) {
                    //nested array - convert to json
                    $value = $modx->toJson($value);
                } else {
                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)
                }
            }


            $inputTVkey = $formname . $field;

            if ($processTVs && isset($inputTvs[$inputTVkey])) {
                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {

                } else {
                    $tv = $modx->newObject(\'modTemplateVar\');
                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);
                }
                $inputTV = $inputTvs[$inputTVkey];

                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');
                //don\'t manipulate any urls here
                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');
                $tv->set(\'default_text\', $value);
                $value = $tv->renderOutput($docid);
                //set option back
                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);
                //now manipulate urls
                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {
                    $mTypes = explode(\',\', $mTypes);
                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {
                        //$value = $mediasource->prepareOutputUrl($value);
                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));
                    }
                }

            }
            $fields[$field] = $value;

        }

        if (!empty($addfields)) {
            foreach ($addfields as $addfield) {
                $addfield = explode(\':\', $addfield);
                $addname = $addfield[0];
                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';
                $fields[$addname] = $adddefault;
            }
        }

        if (!empty($sumFields)) {
            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);
            foreach ($sumFields as $sumField) {
                if (isset($fields[$sumField])) {
                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];
                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];
                }
            }
        }


        if ($toJsonPlaceholder) {
            $output[] = $fields;
        } else {
            $fields[\'_alt\'] = $idx % 2;
            $idx++;
            $fields[\'_first\'] = $idx == 1 ? true : \'\';
            $fields[\'_last\'] = $idx == $limit ? true : \'\';
            $fields[\'idx\'] = $idx;
            $rowtpl = \'\';
            //get changing tpls from field
            if (substr($tpl, 0, 7) == "@FIELD:") {
                $tplField = substr($tpl, 7);
                $rowtpl = $fields[$tplField];
            }

            if ($fields[\'_first\'] && !empty($tplFirst)) {
                $rowtpl = $tplFirst;
            }
            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {
                $rowtpl = $tplLast;
            }
            $tplidx = \'tpl_\' . $idx;
            if (empty($rowtpl) && !empty($$tplidx)) {
                $rowtpl = $$tplidx;
            }
            if ($idx > 1 && empty($rowtpl)) {
                $divisors = $migx->getDivisors($idx);
                if (!empty($divisors)) {
                    foreach ($divisors as $divisor) {
                        $tplnth = \'tpl_n\' . $divisor;
                        if (!empty($$tplnth)) {
                            $rowtpl = $$tplnth;
                            if (!empty($rowtpl)) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($count == 1 && isset($tpl_oneresult)) {
                $rowtpl = $tpl_oneresult;
            }

            $fields = array_merge($fields, $properties);

            if (!empty($rowtpl)) {
                $template = $migx->getTemplate($tpl, $template);
                $fields[\'_tpl\'] = $template[$tpl];
            } else {
                $rowtpl = $tpl;

            }
            $template = $migx->getTemplate($rowtpl, $template);


            if ($template[$rowtpl]) {
                $chunk = $modx->newObject(\'modChunk\');
                $chunk->setCacheable(false);
                $chunk->setContent($template[$rowtpl]);


                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {
                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);
                } else {
                    $output[] = $chunk->process($fields);
                }
            } else {
                if (!empty($placeholdersKeyField)) {
                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                } else {
                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                }
            }
        }


    }
}

if (count($summaries) > 0) {
    $modx->toPlaceholders($summaries);
}


if ($toJsonPlaceholder) {
    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));
    return \'\';
}

if (!empty($toSeparatePlaceholders)) {
    $modx->toPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}
/*
if (!empty($outerTpl))
$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));
else 
*/

if ($count > 0 && $splits > 0) {
    $size = ceil($count / $splits);
    $chunks = array_chunk($output, $size);
    $output = array();
    foreach ($chunks as $chunk) {
        $o = implode($outputSeparator, $chunk);
        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));
    }
    $outputSeparator = $splitSeparator;
}

if (is_array($output)) {
    $o = implode($outputSeparator, $output);
} else {
    $o = $output;
}

if (!empty($o) && !empty($wrapperTpl)) {
    $template = $migx->getTemplate($wrapperTpl);
    if ($template[$wrapperTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$wrapperTpl]);
        $properties[\'output\'] = $o;
        $o = $chunk->process($properties);
    }
}

if (empty($o) && !empty($emptyTpl)) {
    $template = $migx->getTemplate($emptyTpl);
    if ($template[$emptyTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$emptyTpl]);
        $o = $chunk->process($properties);
    }
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $o);
    return \'\';
}

return $o;',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'titl' => 
      array (
        'fields' => 
        array (
          'id' => 22,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl',
          'caption' => 'Мета тайтл',
          'description' => '',
          'editor_type' => 0,
          'category' => 20,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '[[*pagetitle]] - [[++site_name]]',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[*pagetitle]] - [[++site_name]]',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'logo' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'logo',
          'caption' => 'Логотип',
          'description' => '',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell' => 
      array (
        'fields' => 
        array (
          'id' => 4,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell',
          'caption' => 'Начало номера',
          'description' => 'Первые 5 цифр номера',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 3,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell2' => 
      array (
        'fields' => 
        array (
          'id' => 5,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell2',
          'caption' => 'Остальные цыфры',
          'description' => 'остальные цифры',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 4,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'addres' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'addres',
          'caption' => 'Адрес',
          'description' => '',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'image-1' => 
      array (
        'fields' => 
        array (
          'id' => 55,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'image-1',
          'caption' => 'Изображение слайда',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'titl2' => 
      array (
        'fields' => 
        array (
          'id' => 59,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl2',
          'caption' => 'Заголовок со второго блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 4,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'p2' => 
      array (
        'fields' => 
        array (
          'id' => 58,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'textarea',
          'name' => 'p2',
          'caption' => 'Текста под заголовком',
          'description' => 'Текста под заголовком на 2 блоке',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 5,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'vrach' => 
      array (
        'fields' => 
        array (
          'id' => 51,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'listbox-multiple',
          'name' => 'vrach',
          'caption' => 'Врачи',
          'description' => '',
          'editor_type' => 0,
          'category' => 36,
          'locked' => false,
          'elements' => '@SELECT pagetitle, id FROM modx_site_content WHERE parent=4',
          'rank' => 0,
          'display' => 'delim',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'listWidth' => '',
            'title' => '',
            'typeAhead' => 'false',
            'typeAheadDelay' => '250',
            'listEmptyText' => '',
            'stackItems' => 'false',
          ),
          'output_properties' => 
          array (
            'delimiter' => ',',
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'titl3' => 
      array (
        'fields' => 
        array (
          'id' => 60,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl3',
          'caption' => 'Заголовок с 3 блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 6,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'p3' => 
      array (
        'fields' => 
        array (
          'id' => 61,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'textarea',
          'name' => 'p3',
          'caption' => 'Текст под заголовком 3 блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 7,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'titldet1' => 
      array (
        'fields' => 
        array (
          'id' => 99,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titldet1',
          'caption' => 'Заголовок 4',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 5,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'pdet1' => 
      array (
        'fields' => 
        array (
          'id' => 100,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'pdet1',
          'caption' => 'Текст на втором банере',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 6,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'titl4' => 
      array (
        'fields' => 
        array (
          'id' => 63,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl4',
          'caption' => 'Заголовок с 4 блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 11,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'p4' => 
      array (
        'fields' => 
        array (
          'id' => 64,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'textarea',
          'name' => 'p4',
          'caption' => 'Текст под заголовком 4 блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 12,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'slep1' => 
      array (
        'fields' => 
        array (
          'id' => 101,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'slep1',
          'caption' => 'Заголовок стоматологии  во сне',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 8,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'slepp1' => 
      array (
        'fields' => 
        array (
          'id' => 102,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'textarea',
          'name' => 'slepp1',
          'caption' => 'Текст под заголовком стоматологии  во сне',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 10,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'slep2' => 
      array (
        'fields' => 
        array (
          'id' => 103,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'slep2',
          'caption' => 'заголовок блока "Современный подход"',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 12,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'slep3' => 
      array (
        'fields' => 
        array (
          'id' => 104,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'slep3',
          'caption' => 'Заголовок от блока- ЗУБОТЕХНИЧЕСКАЯ ЛАБОРАТОРИЯ',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 13,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'slepp3' => 
      array (
        'fields' => 
        array (
          'id' => 105,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'textarea',
          'name' => 'slepp3',
          'caption' => 'Текст под заголовком ',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 14,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'titl12' => 
      array (
        'fields' => 
        array (
          'id' => 80,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl12',
          'caption' => 'Заголовок с Последнего блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 25,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'p12' => 
      array (
        'fields' => 
        array (
          'id' => 81,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'textarea',
          'name' => 'p12',
          'caption' => 'Текст под заголовком последнего блока',
          'description' => '',
          'editor_type' => 0,
          'category' => 38,
          'locked' => false,
          'elements' => '',
          'rank' => 26,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);