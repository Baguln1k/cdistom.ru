<?php  return array (
  'resourceClass' => 'modDocument',
  'resource' => 
  array (
    'id' => 20,
    'type' => 'document',
    'contentType' => 'text/html',
    'pagetitle' => 'Синдяк Игорь Николаевич',
    'longtitle' => '',
    'description' => '',
    'alias' => 'doc-sidnyakshh',
    'link_attributes' => '',
    'published' => 1,
    'pub_date' => 0,
    'unpub_date' => 0,
    'parent' => 4,
    'isfolder' => 0,
    'introtext' => '',
    'content' => '<p>Врач высшей категории. Восстанавливает отсутствующие зубы с использованием зарубежных имплантатов высокого качества. В Австрии принял участие в семинаре "Управление мягкой тканью в импланталогии и челюстно-лицевой хирургии". Серьезный подход, профессионализм, высокое качество имплантата &ndash; гарантируют успешных исход операции!</p>',
    'richtext' => 1,
    'template' => 6,
    'menuindex' => 1,
    'searchable' => 1,
    'cacheable' => 1,
    'createdby' => 1,
    'createdon' => 1502348371,
    'editedby' => 1,
    'editedon' => 1508216408,
    'deleted' => 0,
    'deletedon' => 0,
    'deletedby' => 0,
    'publishedon' => 1502348340,
    'publishedby' => 1,
    'menutitle' => '',
    'donthit' => 0,
    'privateweb' => 0,
    'privatemgr' => 0,
    'content_dispo' => 0,
    'hidemenu' => 0,
    'class_key' => 'modDocument',
    'context_key' => 'web',
    'content_type' => 1,
    'uri' => 'doctors/doc_sidnyakshh.html',
    'uri_override' => 1,
    'hide_children_in_tree' => 0,
    'show_in_tree' => 1,
    'properties' => NULL,
    'titl' => 
    array (
      0 => 'titl',
      1 => '[[*pagetitle]] - [[++site_name]]',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'keyw' => 
    array (
      0 => 'keyw',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'desc' => 
    array (
      0 => 'desc',
      1 => '',
      2 => 'default',
      3 => NULL,
      4 => 'textarea',
    ),
    'image' => 
    array (
      0 => 'image',
      1 => 'assets/template/images/doc-sidnyak-3.png',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'activnost' => 
    array (
      0 => 'activnost',
      1 => '[{"MIGX_id":"1","year":"2011 ","desc":"\\u0423\\u0447\\u0430\\u0441\\u0442\\u043d\\u0438\\u043a \\u043c\\u0430\\u0441\\u0442\\u0435\\u0440-\\u043a\\u043b\\u0430\\u0441\\u0441\\u0430 \\u00ab\\u0418\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435 \\u0434\\u0438\\u043e\\u0434\\u043d\\u043e\\u0433\\u043e \\u043b\\u0430\\u0437\\u0435\\u0440\\u0430 \\u043d\\u0430 \\u043c\\u044f\\u0433\\u043a\\u0438\\u0445 \\u0442\\u043a\\u0430\\u043d\\u044f\\u0445 \\u043f\\u043e\\u043b\\u043e\\u0441\\u0442\\u0438 \\u0440\\u0442\\u0430\\u00bb, \\u0434-\\u0440 \\u041d.\\u0420\\u043e\\u043c\\u0430\\u043d\\u0435\\u043d\\u043a\\u043e.\\n","city":"\\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, ","data":"2011 \\u0433."},{"MIGX_id":"2","year":"2012","desc":"\\u041f\\u0440\\u043e\\u0441\\u043b\\u0443\\u0448\\u0430\\u043b \\u043a\\u0443\\u0440\\u0441 \\u00ab\\u0423\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u0438\\u0435 \\u043c\\u044f\\u0433\\u043a\\u043e\\u0439 \\u0442\\u043a\\u0430\\u043d\\u044c\\u044e \\u0432 \\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438 \\u0438 \\u0447\\u0435\\u043b\\u044e\\u0441\\u0442\\u043d\\u043e-\\u043b\\u0438\\u0446\\u0435\\u0432\\u043e\\u0439 \\u0445\\u0438\\u0440\\u0443\\u0440\\u0433\\u0438\\u0438\\u00bb, \\u0434-\\u0440\\u0430 \\u0425\\u0430\\u043d\\u0441 \\u0422\\u0440\\u0430\\u043a\\u0441\\u043b\\u0435\\u0440, \\u041d\\u043e\\u0440\\u0431\\u0435\\u0440\\u0442 \\u0424\\u043e\\u043a, \\u0411\\u0435\\u0440\\u043d\\u0430\\u0440\\u0434 \\u0413\\u0435\\u0439\\u0437\\u0435\\u043d\\u0445\\u0430\\u0433\\u0435\\u043d.","city":"\\u0433.\\u0412\\u0435\\u043d\\u0430 (\\u0410\\u0432\\u0441\\u0442\\u0440\\u0438\\u044f),","data":" 2012\\u0433."},{"MIGX_id":"3","year":"2012","desc":"\\t\\t\\t\\t\\t\\t\\t\\u041f\\u0440\\u043e\\u0448\\u0451\\u043b \\u043a\\u0443\\u0440\\u0441 \\u043f\\u043e \\u041a\\u043b\\u0438\\u043d\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0439 \\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0446\\u0438\\u0438, \\u043f\\u0440\\u043e\\u0446\\u0435\\u0434\\u0443\\u0440\\u0430 \\u0441\\u0438\\u043d\\u0443\\u0441-\\u043b\\u0438\\u0444\\u0442\\u0438\\u043d\\u0433, \\u0434-\\u0440 \\u0410.\\u041f\\u0430\\u0440\\u0438\\u0446\\u043a\\u0438 (\\u0418\\u0437\\u0440\\u0430\\u0438\\u043b\\u044c).\\n","city":"\\u0433. \\u041c\\u043e\\u0441\\u043a\\u0432\\u0430 ","data":"2012 \\u0433."},{"MIGX_id":"4","year":"2012\\u0433","desc":"\\u0423\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435 \\u0432 \\u0442\\u0435\\u043e\\u0440\\u0435\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c \\u043a\\u0443\\u0440\\u0441\\u0435 \\u0438 \\u043c\\u0430\\u0441\\u0442\\u0435\\u0440-\\u043a\\u043b\\u0430\\u0441\\u0441\\u0435 \\u00ab\\u041c\\u0435\\u0442\\u043e\\u0434\\u0438\\u043a\\u0430 \\u0442\\u0440\\u0451\\u0445\\u043c\\u0435\\u0440\\u043d\\u043e\\u0439 \\u0430\\u0443\\u0433\\u043c\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u0438 \\u043a\\u043e\\u043b\\u044c\\u0446\\u0435\\u0432\\u0438\\u0434\\u043d\\u044b\\u043c\\u0438 \\u0442\\u0440\\u0430\\u043d\\u0441\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0442\\u0430\\u043c\\u0438. \\u041f\\u043b\\u0430\\u0441\\u0442\\u0438\\u043a\\u0430 \\u043c\\u044f\\u0433\\u043a\\u0438\\u0445 \\u0442\\u043a\\u0430\\u043d\\u0435\\u0439\\u00bb, \\u0434-\\u0440 \\u0411\\u0435\\u0440\\u043d\\u0430\\u0440\\u0434 \\u0413\\u0438\\u0437\\u0435\\u043d\\u0445\\u0430\\u0433\\u0435\\u043d (\\u0413\\u0435\\u0440\\u043c\\u0430\\u043d\\u0438\\u044f).\\n","city":"\\u0433.\\u041d\\u043e\\u0432\\u043e\\u0441\\u0438\\u0431\\u0438\\u0440\\u0441\\u043a, ","data":"2012\\u0433."},{"MIGX_id":"5","year":"2013 ","desc":"\\t\\t\\t\\t\\t\\t\\t\\u041f\\u0440\\u0438\\u043d\\u044f\\u043b \\u0443\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435 \\u0432 \\u0442\\u0440\\u0435\\u043d\\u0438\\u043d\\u0433\\u0435 \\u00ab\\u0421\\u043e\\u0432\\u0440\\u0435\\u043c\\u0435\\u043d\\u043d\\u044b\\u0435 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438 \\u0432 \\u043f\\u043e\\u043c\\u043e\\u0449\\u044c \\u0445\\u0438\\u0440\\u0443\\u0440\\u0433\\u0443-\\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u043e\\u043b\\u043e\\u0433\\u0443\\u00bb, \\u0434-\\u0440 \\u0424\\u0440\\u0435\\u0434 \\u0411\\u0435\\u0440\\u0433\\u043c\\u0430\\u043d (\\u0413\\u0435\\u0440\\u043c\\u0430\\u043d\\u0438\\u044f).\\n","city":"\\u0433. \\u041d\\u043e\\u0432\\u043e\\u0441\\u0438\\u0431\\u0438\\u0440\\u0441\\u043a, ","data":"2013 \\u0433."},{"MIGX_id":"6","year":"2014","desc":"\\u041f\\u0440\\u0438\\u043d\\u044f\\u043b \\u0443\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435 \\u0432 \\u0442\\u0440\\u0435\\u043d\\u0438\\u043d\\u0433\\u0435 \\u00ab\\u041f\\u0440\\u043e\\u0442\\u043e\\u043a\\u043e\\u043b\\u044b \\u0424\\u0440\\u0430\\u043d\\u043a\\u0444\\u0443\\u0440\\u0442\\u0441\\u043a\\u043e\\u0433\\u043e \\u0443\\u043d\\u0438\\u0432\\u0435\\u0440\\u0441\\u0438\\u0442\\u0435\\u0442\\u0430 \\u043f\\u043e \\u043f\\u0440\\u043e\\u0442\\u0435\\u0437\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0438\\u044e \\u043d\\u0430 \\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0442\\u0430\\u0445: \\u043e\\u0431\\u044a\\u0435\\u0434\\u0438\\u043d\\u044f\\u044f \\u043d\\u043e\\u0432\\u0435\\u0439\\u0448\\u0438\\u0435 \\u0442\\u0435\\u0445\\u043d\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438 \\u0438 \\u043c\\u043d\\u043e\\u0433\\u043e\\u043b\\u0435\\u0442\\u043d\\u0438\\u0439 \\u043e\\u043f\\u044b\\u0442 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b\\u00bb, \\u0434-\\u0440 \\u041f\\u0430\\u0443\\u043b\\u044c \\u0412\\u0430\\u0439\\u0433\\u0435\\u043b\\u044c (\\u0413\\u0435\\u0440\\u043c\\u0430\\u043d\\u0438\\u044f).\\n","city":"\\u0433.\\u041d\\u043e\\u0432\\u043e\\u0441\\u0438\\u0431\\u0438\\u0440\\u0441\\u043a, ","data":"2014\\u0433."},{"MIGX_id":"7","year":"2014","desc":"\\u0423\\u0447\\u0430\\u0441\\u0442\\u043d\\u0438\\u043a \\u0432 \\u0441\\u0435\\u043c\\u0438\\u043d\\u0430\\u0440\\u0430 \\u00ab\\u0422\\u0440\\u0435\\u0445\\u043c\\u0435\\u0440\\u043d\\u0430\\u044f \\u0440\\u0430\\u0434\\u0438\\u043e\\u0434\\u0438\\u0430\\u0433\\u043d\\u043e\\u0441\\u0442\\u0438\\u043a\\u0430 \\u0447\\u0435\\u043b\\u044e\\u0441\\u0442\\u043d\\u043e-\\u043b\\u0438\\u0446\\u0435\\u0432\\u043e\\u0439 \\u043e\\u0431\\u043b\\u0430\\u0441\\u0442\\u0438. \\u041a\\u043e\\u043d\\u0443\\u0441\\u043d\\u043e-\\u043b\\u0443\\u0447\\u0435\\u0432\\u0430\\u044f \\u0442\\u043e\\u043c\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044f. \\u041e\\u0441\\u043d\\u043e\\u0432\\u044b \\u0432\\u0438\\u0437\\u0443\\u0430\\u043b\\u0438\\u0437\\u0430\\u0446\\u0438\\u0438. \\u0423\\u0433\\u043b\\u0443\\u0431\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439 \\u043a\\u0443\\u0440\\u0441\\u00bb, \\u0432\\u0440\\u0430\\u0447-\\u0440\\u0435\\u043d\\u0442\\u0433\\u0435\\u043d\\u043e\\u043b\\u043e\\u0433 \\u0420\\u043e\\u0433\\u0430\\u0446\\u043a\\u0438\\u043d \\u0414.\\u0412.\\n","city":"\\u0433.\\u0422\\u043e\\u043c\\u0441\\u043a, ","data":"2014\\u0433."},{"MIGX_id":"8","year":"2014 ","desc":"\\u041f\\u0440\\u0438\\u043d\\u044f\\u043b \\u0443\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435 \\u0432 \\u0441\\u0435\\u043c\\u0438\\u043d\\u0430\\u0440\\u0435 \\u00ab\\u041d\\u0430\\u043f\\u0440\\u0430\\u0432\\u043b\\u0435\\u043d\\u043d\\u0430\\u044f \\u0440\\u0435\\u0433\\u0435\\u043d\\u0435\\u0440\\u0430\\u0446\\u0438\\u044f \\u043a\\u043e\\u0441\\u0442\\u043d\\u043e\\u0439 \\u0442\\u043a\\u0430\\u043d\\u0438, \\u043d\\u0435\\u043c\\u0435\\u0434\\u043b\\u0435\\u043d\\u043d\\u0430\\u044f \\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f \\u0438 \\u043d\\u0435\\u043c\\u0435\\u0434\\u043b\\u0435\\u043d\\u043d\\u0430\\u044f \\u043d\\u0430\\u0433\\u0440\\u0443\\u0437\\u043a\\u0430\\u00bb, \\u0434-\\u0440 \\u0428\\u043b\\u044e\\u043c\\u043e \\u0411\\u0438\\u0440\\u0448\\u0430\\u043d (\\u0413\\u0435\\u0440\\u043c\\u0430\\u043d\\u0438\\u044f).\\n","city":"\\u0433. \\u0421\\u0430\\u043d\\u043a\\u0442-\\u041f\\u0435\\u0442\\u0435\\u0440\\u0431\\u0443\\u0440\\u0433, ","data":"2014 \\u0433."},{"MIGX_id":"9","year":"2015","desc":"\\t\\t\\t\\t\\t\\t\\t\\u041f\\u0440\\u0438\\u043d\\u044f\\u043b \\u0443\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435 \\u0432 \\u043c\\u0430\\u0441\\u0442\\u0435\\u0440-\\u043a\\u043b\\u0430\\u0441\\u0441\\u0435 \\u00ab\\u041c\\u0435\\u043d\\u0435\\u0434\\u0436\\u043c\\u0435\\u043d\\u0442 \\u043c\\u044f\\u0433\\u043a\\u0438\\u0445 \\u0442\\u043a\\u0430\\u043d\\u0435\\u0439 \\u043f\\u0440\\u0438 \\u043b\\u0435\\u0447\\u0435\\u043d\\u0438\\u0438 \\u0441 \\u0438\\u0441\\u043f\\u043e\\u043b\\u044c\\u0437\\u043e\\u0432\\u0430\\u043d\\u0438\\u0435\\u043c \\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0442\\u043e\\u0432\\u00bb,  \\u0422\\u043e\\u043c\\u0430\\u0441 \\u0425\\u0430\\u043d\\u0441\\u0435\\u0440 (\\u0413\\u0435\\u0440\\u043c\\u0430\\u043d\\u0438\\u044f).\\n","city":"\\u0433. \\u041d\\u043e\\u0432\\u043e\\u0441\\u0438\\u0431\\u0438\\u0440\\u0441\\u043a, ","data":"2015\\u0433."},{"MIGX_id":"10","year":"2015","desc":"\\u041f\\u0440\\u0438\\u043d\\u044f\\u043b \\u0443\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435 \\u0432 II \\u041c\\u0435\\u0436\\u0434\\u0443\\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u043c \\u0421\\u0438\\u0431\\u0438\\u0440\\u0441\\u043a\\u043e\\u043c \\u0421\\u0438\\u043c\\u043f\\u043e\\u0437\\u0438\\u0443\\u043c\\u0435 \\u043f\\u043e \\u0418\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438 ,\\u041c\\u0430\\u0440\\u043a\\u043e \\u0414\\u0438\\u0434\\u0436\\u0438\\u0434\\u0438 (\\u0418\\u0442\\u0430\\u043b\\u0438\\u044f), \\u0422\\u043e\\u043c\\u0430\\u0441 \\u0425\\u0430\\u043d\\u0441\\u0435\\u0440 (\\u0413\\u0435\\u0440\\u043c\\u0430\\u043d\\u0438\\u044f), \\u0415\\u0432\\u0433\\u0435\\u043d\\u0438\\u0439 \\u041a\\u0440\\u0435\\u043d\\u0446 (\\u0413\\u0435\\u0440\\u043c\\u0430\\u043d\\u0438\\u044f), \\u0410\\u043d\\u0434\\u0440\\u0435\\u0439 \\u042f\\u0440\\u0435\\u043c\\u0435\\u043d\\u043a\\u043e (\\u0420\\u043e\\u0441\\u0441\\u0438\\u044f), \\u0412\\u043b\\u0430\\u0434\\u0438\\u0441\\u043b\\u0430\\u0432 \\u0411\\u0430\\u0431\\u0438\\u043d\\u0446\\u0435\\u0432 (\\u0420\\u043e\\u0441\\u0441\\u0438\\u044f).","city":"\\u0433.\\u041d\\u043e\\u0432\\u043e\\u0441\\u0438\\u0431\\u0438\\u0440\\u0441\\u043a, ","data":"2015\\u0433."},{"MIGX_id":"11","year":"2016","desc":"\\t\\u041f\\u0440\\u0438\\u043d\\u044f\\u043b \\u0443\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435 \\u0432 X \\u041c\\u0435\\u0436\\u0434\\u0443\\u043d\\u0430\\u0440\\u043e\\u0434\\u043d\\u043e\\u043c \\u0418\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c \\u041a\\u043e\\u043d\\u0433\\u0440\\u0435\\u0441\\u0441\\u0435 \\u00ab\\u0417\\u043e\\u043b\\u043e\\u0442\\u044b\\u0435 \\u0441\\u0442\\u0430\\u043d\\u0434\\u0430\\u0440\\u0442\\u044b \\u0438 \\u0438\\u043d\\u043d\\u043e\\u0432\\u0430\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0435 \\u043a\\u043e\\u043d\\u0446\\u0435\\u043f\\u0446\\u0438\\u0438 \\u0432 \\u043f\\u043e\\u0432\\u0441\\u0435\\u0434\\u043d\\u0435\\u0432\\u043d\\u043e\\u0439 \\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0439 \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u043a\\u0435\\u00bb \\u0438 \\u043f\\u043e\\u0441\\u0435\\u0442\\u0438\\u043b \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u0443\\u044e \\u0441\\u0435\\u0441\\u0441\\u0438\\u044e \\u00ab\\u0418\\u043d\\u0442\\u0435\\u043d\\u0441\\u0438\\u0432\\u043d\\u044b\\u0439 \\u043a\\u0443\\u0440\\u0441 \\u043f\\u043e \\u043c\\u0435\\u0442\\u043e\\u0434\\u0438\\u043a\\u0430\\u043c \\u0430\\u0443\\u0433\\u043c\\u0435\\u043d\\u0442\\u0430\\u0446\\u0438\\u0438 \\u0442\\u0432\\u0435\\u0440\\u0434\\u044b\\u0445 \\u0438 \\u043c\\u044f\\u0433\\u043a\\u0438\\u0445 \\u0442\\u043a\\u0430\\u043d\\u0435\\u0439\\u00bb,\\u043f\\u0440\\u043e\\u0444. \\u041c\\u0430\\u0440\\u0438\\u043e \\u0411\\u0435\\u0440\\u0435\\u0442\\u0442\\u0430 (\\u0418\\u0442\\u0430\\u043b\\u0438\\u044f).\\n","city":"\\u0433.\\u041c\\u043e\\u0441\\u043a\\u0432\\u0430, ","data":"2016\\u0433."},{"MIGX_id":"12","year":"2016","desc":"\\u041f\\u0440\\u043e\\u0441\\u043b\\u0443\\u0448\\u0430\\u043b \\u043b\\u0435\\u043a\\u0446\\u0438\\u043e\\u043d\\u043d\\u044b\\u0439 \\u043a\\u0443\\u0440\\u0441 \\u00ab\\u041f\\u0440\\u043e\\u0434\\u0432\\u0438\\u043d\\u0443\\u0442\\u044b\\u0435 \\u043c\\u0435\\u0442\\u043e\\u0434\\u0438\\u043a\\u0438 \\u0432 \\u0438\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u043e\\u043b\\u043e\\u0433\\u0438\\u0438\\u00bb, \\u0434-\\u0440 \\u041c\\u0435\\u0438\\u0440 \\u041c\\u0430\\u043c\\u0440\\u0430\\u0435\\u0432.\\n","city":"\\u0433.\\u041d\\u043e\\u0432\\u043e\\u0441\\u0438\\u0431\\u0438\\u0440\\u0441\\u043a, ","data":"2016\\u0433."},{"MIGX_id":"13","year":"2016\\u0433","desc":"\\u041f\\u0440\\u0438\\u043d\\u044f\\u043b \\u0443\\u0447\\u0430\\u0441\\u0442\\u0438\\u0435 \\u0432 \\u0441\\u0435\\u043c\\u0438\\u043d\\u0430\\u0440\\u0435 \\u0421.\\u0410.\\u041f\\u043e\\u043f\\u043e\\u0432\\u0430 \\u00ab\\u0410\\u0431\\u0441\\u043e\\u043b\\u044e\\u0442\\u043d\\u044b\\u0439 \\u0430\\u043d\\u043a\\u043e\\u0440\\u0430\\u0436 \\u0432 \\u043e\\u0440\\u0442\\u043e\\u0434\\u043e\\u043d\\u0442\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u0439 \\u043f\\u0440\\u0430\\u043a\\u0442\\u0438\\u043a\\u0435\\u00bb.\\n","city":"\\u0433.\\u041d\\u043e\\u0432\\u043e\\u0441\\u0438\\u0431\\u0438\\u0440\\u0441\\u043a, ","data":"2016\\u0433."}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'okaz_uslugi' => 
    array (
      0 => 'okaz_uslugi',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/sm1.png","title":"\\u0418\\u043c\\u043f\\u043b\\u0430\\u043d\\u0442\\u0430\\u0446\\u0438\\u044f","url":"[[~11]]"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'staj_diplomi' => 
    array (
      0 => 'staj_diplomi',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/doci1.png","title":"26 \\u043b\\u0435\\u0442  \\u0441\\u0442\\u0430\\u0436 \\u0440\\u0430\\u0431\\u043e\\u0442\\u044b"},{"MIGX_id":"2","image":"assets\\/template\\/images\\/doci2.png","title":"1 \\u043f\\u0430\\u0442\\u0435\\u043d\\u0442 \\u0420\\u0424  \\u043d\\u0430 \\u0438\\u0437\\u043e\\u0431\\u0440\\u0435\\u0442\\u0435\\u043d\\u0438\\u0435"},{"MIGX_id":"3","image":"assets\\/template\\/images\\/doci3.png","title":"14 \\u043d\\u0430\\u0443\\u0447\\u043d\\u044b\\u0445 \\u043f\\u0443\\u0431\\u043b\\u0438\\u043a\\u0430\\u0446\\u0438\\u0439"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'obrazovanie' => 
    array (
      0 => 'obrazovanie',
      1 => 'Красноярский государственный медицинский институт',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'special' => 
    array (
      0 => 'special',
      1 => 'Врач-стоматолог-хирург, имплантолог, врач высшей категории',
      2 => 'default',
      3 => NULL,
      4 => 'text',
    ),
    'preview_image' => 
    array (
      0 => 'preview_image',
      1 => 'assets/template/images/doc-sidnyak.jpg',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    'reviews_more' => 
    array (
      0 => 'reviews_more',
      1 => '23||33||34||38||43',
      2 => 'delim',
      3 => NULL,
      4 => 'listbox-multiple',
    ),
    'otzivi' => 
    array (
      0 => 'otzivi',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/otzivy\\/otz-1.jpg"},{"MIGX_id":"2","image":"assets\\/template\\/images\\/otzivy\\/otz-9.jpg"},{"MIGX_id":"3","image":"assets\\/template\\/images\\/otzivy\\/otz-10.jpg"},{"MIGX_id":"4","image":"assets\\/template\\/images\\/otzivy\\/otz-14.jpg"},{"MIGX_id":"5","image":"assets\\/template\\/images\\/otzivy\\/otz-19.jpg"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'dimlom_sertivifikat' => 
    array (
      0 => 'dimlom_sertivifikat',
      1 => '[{"MIGX_id":"1","image":"assets\\/template\\/images\\/diplomy\\/sidnyak\\/dip-1.jpg"},{"MIGX_id":"2","image":"assets\\/template\\/images\\/diplomy\\/sidnyak\\/dip-2.jpg"},{"MIGX_id":"3","image":"assets\\/template\\/images\\/diplomy\\/sidnyak\\/dip-3.jpg"}]',
      2 => 'default',
      3 => NULL,
      4 => 'migx',
    ),
    'image_original' => 
    array (
      0 => 'image_original',
      1 => 'assets/template/images/sindyak_new.png',
      2 => 'default',
      3 => NULL,
      4 => 'image',
    ),
    '_content' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>Синдяк Игорь Николаевич - Центр дентальной имплантации</title>
        <base href="http://cdistom.ru/" />
        <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->

<script>
    
		$(document).ready(function(){
			$(".thanks_slider").owlCarousel({
				items:1,
				margin:40,
				nav:true,
				loop:true,
			});
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});

			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}

			$(document).ready(function(){
				owlcount();
			});

			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});

			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});

		});
	
</script>
</head>
<body class="fix_scroll">
    <div class="wrapper">
    <header class="header">
			<div class="blockin">
				<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
				<a href="tel:+8 382225-70-33" class="phone_top">
					<p class="address_top">Томск, ул. Говорова, д. 19 "В", ст. 1</p>
					8 3822 <span>25-70-33</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="http://cdistom.ru/" class="">
								<img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>
       <ul class="B_crumbBox path"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Врачи" href="doctors/"><span itemprop="title">Врачи</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Синдяк Игорь Николаевич" href="doctors/doc_sidnyakshh.html"><span itemprop="title">Синдяк Игорь Николаевич</span></a></li>
</ul>
		<div class="doctor_page_top">
			<div class="left wow fadeInUp" style="padding: 30px 15px;">
				<img src="assets/template/images/doc-sidnyak-3.png" alt="" style=" width: 75%; ">
				<div class="siteubutton">
					<a href="contacts.html" class="site_but">Задать вопрос врачу</a>
				</div>
			</div>
			<div class="right">
				<div class="top_block wow fadeInUp" data-wow-delay="0.2s" style="padding-left: 50px;">
					<div class="b_title" style="color: #333;font-size: 35px;font-weight: 300;padding-top: 10px;padding-bottom: 15px;">Синдяк Игорь Николаевич</div>
					<div class="info" style=" font-size: 14px; line-height: 27px; ">
						Врач-стоматолог-хирург, имплантолог, врач высшей категории</div>
						<div class="anonce" style=" font-size: 14px; color: #032A4B; ">
							<span>Образование:</span> Красноярский государственный медицинский институт
						</div>
					</div>
					<div class="doc_info wow fadeInUp" data-wow-delay="0.4s" style="padding-left: 50px;">
					    [[!getImageList? &tvname=`staj_diplomi` &tpl=`staj_diplomiTpl`]]
					</div>
				
					<div class="text_block wow fadeInUp" data-wow-delay="0.6s" style="color: #666666;font-size: 14px;padding-left: 50px;line-height: 24px;padding: 30px 50px;padding-top: 25px;visibility: visible;animation-delay: 0.6s;animation-name: fadeInUp;">
						<p>Врач высшей категории. Восстанавливает отсутствующие зубы с использованием зарубежных имплантатов высокого качества. В Австрии принял участие в семинаре "Управление мягкой тканью в импланталогии и челюстно-лицевой хирургии". Серьезный подход, профессионализм, высокое качество имплантата &ndash; гарантируют успешных исход операции!</p>
					</div>
						<div class="bottom_block wow fadeInUp" data-wow-delay="0.8s" style="padding-left: 50px; visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">
						<div class="b_title" style=" font-size: 22px; color: #333333; font-weight: 500; text-transform: uppercase; margin: 10px 0; padding-bottom: 10px; ">Оказываемые услуги</div>
						<ul>
							[[!getImageList? &tvname=`okaz_uslugi` &tpl=`okaz_uslugiTpl`]]
						</ul>
					</div>
					
				</div>
			</div>
				<div class="data_slider_wrap wow fadeInUp">
				<div class="site_title">Профессиональная активность</div>
				<div class="blockin">
					<div class="data_slider owl-carousel">
							[[!getImageList? &tvname=`activnost` &tpl=`activnostTpl`]]
					</div>
				</div>
			</div>
		
			<div class="thanks_block doctor_thanks wow fadeInUp sertificates-slider">
				<div class="site_title"><span>Дипломы и сертификаты</span></div>
				<div class="body">
					<div class="owl-carousel thanks_slider">
					    	[[!getImageList? &tvname=`dimlom_sertivifikat` &tpl=`diplom_sertifikatTpl`]]
					</div>
				</div>
			</div>
		

	   
              	<div class="reviews-block doctor_thanks wow fadeInUp">
				<div class="site_title"><span>Отзывы</span></div>
				<div class="body">
				    
				    <div id="pdopage">
                        
                        <div class="rows">
                            [[!pdoPage?
                                &parents=`22`
                                &ajaxMode=`button`
                                &tpl=`otz`
                                &includeTVs=`image`
                                &includeContent=`1`
                                 &leftJoin=`{
                        		"Parent":{
                        			"class": "modResource",
                        			"alias": "Parent",
                        			"on":"Parent.id = modResource.parent"
                        		}
                            	}`
                            	&sortby=`{"Parent.menuindex":"desc", "menuindex":"asc"}`
                            	&limit=`4`
                                &resources=`23,33,34,38,43`
                                ]]
                                
                      
                        </div>
                        
    				    <div class="show-all">
        				       [[!+page.nav]]
    				    </div>
                    </div>
				    
				    
				    
				   
				</div>
			</div>
	 
			
		<!-- POP UP -->	
			<div class="pop-up-review">
			    <div class="pop-up-content">
			        <div class="head-pop-up">
			            <h3 class="pop-name"></h3>
			            <div class="close-review"></div>
			        </div>
			        <div class="text-pop-up">
			        </div>
	                <div class="date-pop-up">
                        <time class="date-review"></time>
                    </div>
			    </div>
			</div>
		<!-- POP UP END -->
		
<link href="assets/template/css/jquery.fancybox.min.css" rel="stylesheet">
<script type="text/javascript" src="assets/template/js/jquery.fancybox.min.js"></script>
<style>
    .reviews-block {
        padding: 60px 0;
    }
    .reviews-block .body {
        padding: 0 65px;
    }
    .reviews-block .site_title {
        margin-bottom: 20px;
    }
    .body-review img {
        float: left;
        margin: -1px;
        margin-right: 15px;
        border: 1px solid #cecece;
        max-width: 180px;
        height: 247px;
    }
    
    .body-review:after {
        content: "";
        clear: both;
        display: block;
    }
    .review-block {
        background: #fff;
        border: 1px solid #cecece;
        box-shadow: 0px 1px 2px 0px rgba(206, 206, 206, 0.64);
        width: 47%;
        display: inline-block;
        vertical-align: top;
        margin: 15px;
    }
    .haed-review {
        padding: 15px;
    }
    .haed-review h3 {
        font-size: 16px;
        margin: 0;
        display: inline-block;
    }
    time.date-review {
        color: #666666;
        font-weight: bold;
        float: right;
        padding-top: 4px;
        padding-left: 23px;
        background: url(assets/template/images/calgrey.png) left center no-repeat;
        font-size: 12px;
        margin-top: -3px;
    }
    .text-review {
        line-height: 1.5;
        margin-bottom: 15px;
        padding-right: 15px;
        height: 145px;
        overflow: hidden;
    }
    a.read-more {
        color: #21beb2;
        text-transform: uppercase;
        display: block;
        cursor: pointer;
    }
    .show-all {
        text-align: center;
        margin: 15px;
    }
    a.all {
    	display: inline-block;
    	line-height: 55px;
    	text-align: center;
    	width: 100%;
    	text-decoration: none;
    	color: #fff;
    	border: 1px solid transparent;
    	border-radius: 40px;
    	max-width: 235px;
    	background: #21beb2;
    	font-size: 13px;
    }
    
    a.all:hover {
    	border-color: #21beb2;
    	background: none;
    	color: #21beb2;
    }
    .pop-up-review {
        position: fixed;
        top: 0px;
        background: #fff;
        width: 600px;
        z-index: -99;
        margin: 14% 28%;
        border-radius: 4px;
        box-shadow: 0px 2px 2px 1px rgba(2, 2, 2, 0.5);
        overflow: hidden;
        visibility: hidden;
        opacity: 0;
        -webkit-transform: scale(0);
        -moz-transform: scale(0);
        -ms-transform: scale(0);
        -o-transform: scale(0);
        transform: scale(0);
        -webkit-transition: 0.4s;
        -moz-transition: 0.4s;
        -ms-transition: 0.4s;
        -o-transition: 0.4s;
        transition: 0.4s;
    }
    .pop-up-review.active {
        z-index: 99;
        visibility: visible;
        opacity: 1;
        -webkit-transform: scale(1);
        -moz-transform: scale(1);
        -ms-transform: scale(1);
        -o-transform: scale(1);
        transform: scale(1)
    }
    
    .head-pop-up {
        background: #21beb2;
        padding: 15px;
    }
    
    .head-pop-up h3 {
        margin: 0;
        color: #fff;
        text-shadow: 0px 1px 1px rgba(0, 0, 0, 0.4);
        display: inline-block;
        font-size: 18px;
    }
    
    .close-review {
        width: 22px;
        height: 22px;
        float: right;
        background: url(assets/template/images/close-review.png) center no-repeat;
        cursor: pointer;
       -webkit-transition: 0.3s;
        -moz-transition: 0.3s;
        -ms-transition: 0.3s;
        -o-transition: 0.3s;
        transition: 0.3s;
    }
    .close-review:hover {
        -webkit-transform: scale(0.9);
        -moz-transform: scale(0.9);
        -ms-transform: scale(0.9);
        -o-transform: scale(0.9);
        transform: scale(0.9);
    }
    .text-pop-up {
        padding: 15px;
    }
    
    .date-pop-up time.date-review {
        float: none;
        display: block;
        color: #21beb2;
        background: url(assets/template/images/calblue.png) left center no-repeat;
    }
    .date-pop-up {
        padding: 15px;
        padding-top: 0;
    }
    .overlay {
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(0, 0, 0, 0.3);
        z-index: -1;
        display: none;
    }
    .overlay.active {
        z-index: 99;
        display: block;
    }
    
    
    @media(max-width: 1300px) {
        .reviews-block .body {
            padding: 0 10px;
        }
        .pop-up-review {
            margin: 14% 27%;
        }
    }
    @media(max-width: 1200px) {   
        .review-block {
            margin: 10px;
        }
        .pop-up-review {
            margin: 14% 24%;
        }
    }
     @media(max-width: 1100px) {  
    
        time.date-review {
            float: none;
        }
        .pop-up-review {
            margin: 18% 21%;
        }
        .haed-review h3 {
            display: block;
        }
        .haed-review {
            padding: 10px;
        }
        .body-review img {
            margin-right: 10px;
        }
    }
    @media(max-width: 994px) {   
        .review-block {
            width: 100%;
            margin: 0;
            margin-bottom: 15px;
        }
        .pop-up-review {
            margin: 22% 18%;
        }
    }
    @media(max-width: 768px) {  
        .pop-up-review {
            width: 90%;
            margin: 25% 5%;
        }
    }
   @media(max-width: 450px) { 
        .body-review img {
            max-width: 130px;
            height: 180px;
        }
        .text-review {
            line-height: 1;
            height: 80px;
            margin-bottom: 5px;
        }
        .haed-review h3 {
            font-size: 14px;
            line-height: 1;
        }
        a.read-more {
            font-size: 12px;
        }
   }
        
</style>
	
<script>
    $(\'a.read-more, .text-review\').click(function(){
    	var name = $(this).parent().find(\'.name-review\').text();
    	var dater =  $(this).parent().find(\'.date-review\').text();
    	var textr = $(this).parent().find(\'.text-review\').html();
    
        $(\'.head-pop-up .pop-name\').html(name);
        $(\'.text-pop-up\').html(textr);
        $(\'.date-pop-up .date-review\').html(dater);
        
        $(\'.pop-up-review\').addClass(\'active\');
        $(\'.overlay\').addClass(\'active\');
    });
    
    $(\'.close-review\').click(function(){
        $(\'.pop-up-review\').removeClass(\'active\');
        $(\'.overlay\').removeClass(\'active\');
    });
</script>
		
		<div class="siteinfoblock doc_page_infobot wow fadeInUp animated" style="visibility: visible;">
			<div class="title">Запись на прием</div>
			<div class="text">
				<p>Запишитесь на прием к этому врачу</p>
			</div>
			<a href="contacts.html">Записаться на прием</a>
		</div>

			<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="implantation.html">Имплантация</a></li>
						<li><a href="services.html">Детская стоматология</a></li>
						<li><a href="terapia.html">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="hygiene.html">Гигиена и профилактика</a></li>
						<li><a href="proteth.html">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> 8:30-20:00</li>
						<li><strong>Сб: </strong> 8:30-14:00</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> 25-70-33</li>
						<li><strong>E-mail: </strong> <a href="mailto:cdistom@mail.ru"> cdistom@mail.ru</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br>  ул. Говорова, д. 19 "В", ст. 1</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="https://vk.com/cdistom"></a></li>
							<li><a href="https://www.facebook.com/tomskimplant/?modal=composer"></a></li>
							<li><a href="https://www.instagram.com/tomstom_implant/?hl=ru"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="contacts.html" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>
	</body>
	</html>',
    '_isForward' => false,
  ),
  'contentType' => 
  array (
    'id' => 1,
    'name' => 'HTML',
    'description' => 'HTML content',
    'mime_type' => 'text/html',
    'file_extensions' => '.html',
    'headers' => NULL,
    'binary' => 0,
  ),
  'policyCache' => 
  array (
  ),
  'elementCache' => 
  array (
    '[[$head]]' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>Синдяк Игорь Николаевич - Центр дентальной имплантации</title>
        <base href="http://cdistom.ru/" />
        <meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
    '[[$header]]' => '<header class="header">
			<div class="blockin">
				<a href="http://cdistom.ru/" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
				<a href="tel:+8 382225-70-33" class="phone_top">
					<p class="address_top">Томск, ул. Говорова, д. 19 "В", ст. 1</p>
					8 3822 <span>25-70-33</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="http://cdistom.ru/" class="">
								<img src="assets/template/images/logocdi.png" alt="Центр дентальной имплантации">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
    '[[$?resource=`20`&description=`Синдяк Игорь Николаевич`&text=`Синдяк Игорь Николаевич`]]' => '<a class="B_currentCrumb" itemprop="url" rel="Синдяк Игорь Николаевич" href="doctors/doc_sidnyakshh.html"><span itemprop="title">Синдяк Игорь Николаевич</span></a>',
    '[[$?resource=`4`&description=`Врачи`&text=`Врачи`]]' => '<a class="B_crumb" itemprop="url" rel="Врачи" href="doctors/"><span itemprop="title">Врачи</span></a>',
    '[[$?description=`Главная`&text=`Главная`]]' => '<a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a>',
    '[[$?text=`<a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a>`]]' => '<li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>',
    '[[$?text=`<a class="B_crumb" itemprop="url" rel="Врачи" href="doctors/"><span itemprop="title">Врачи</span></a>`]]' => '<li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Врачи" href="doctors/"><span itemprop="title">Врачи</span></a></li>',
    '[[$?text=`<a class="B_currentCrumb" itemprop="url" rel="Синдяк Игорь Николаевич" href="doctors/doc_sidnyakshh.html"><span itemprop="title">Синдяк Игорь Николаевич</span></a>`]]' => '<li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Синдяк Игорь Николаевич" href="doctors/doc_sidnyakshh.html"><span itemprop="title">Синдяк Игорь Николаевич</span></a></li>',
    '[[$?text=`<li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Врачи" href="doctors/"><span itemprop="title">Врачи</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Синдяк Игорь Николаевич" href="doctors/doc_sidnyakshh.html"><span itemprop="title">Синдяк Игорь Николаевич</span></a></li>
`]]' => '<ul class="B_crumbBox path"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Врачи" href="doctors/"><span itemprop="title">Врачи</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Синдяк Игорь Николаевич" href="doctors/doc_sidnyakshh.html"><span itemprop="title">Синдяк Игорь Николаевич</span></a></li>
</ul>',
    '[[Breadcrumbs]]' => '<ul class="B_crumbBox path"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Врачи" href="doctors/"><span itemprop="title">Врачи</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Синдяк Игорь Николаевич" href="doctors/doc_sidnyakshh.html"><span itemprop="title">Синдяк Игорь Николаевич</span></a></li>
</ul>',
    '[[$Breadcrumbs]]' => '<ul class="B_crumbBox path"><li class="B_firstCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_homeCrumb" itemprop="url" rel="Главная" href="http://cdistom.ru/"><span itemprop="title">Главная</span></a></li>
  <li itemscope="itemscope" class="B_crumb" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_crumb" itemprop="url" rel="Врачи" href="doctors/"><span itemprop="title">Врачи</span></a></li>
  <li class="B_lastCrumb" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb"><a class="B_currentCrumb" itemprop="url" rel="Синдяк Игорь Николаевич" href="doctors/doc_sidnyakshh.html"><span itemprop="title">Синдяк Игорь Николаевич</span></a></li>
</ul>',
    '[[*staj_diplomi:!empty=`<div class="doc_info wow fadeInUp" data-wow-delay="0.4s" style="padding-left: 50px;">
					    [[!getImageList? &tvname=`staj_diplomi` &tpl=`staj_diplomiTpl`]]
					</div>`]]' => '<div class="doc_info wow fadeInUp" data-wow-delay="0.4s" style="padding-left: 50px;">
					    [[!getImageList? &tvname=`staj_diplomi` &tpl=`staj_diplomiTpl`]]
					</div>',
    '[[*okaz_uslugi:!empty=`<div class="bottom_block wow fadeInUp" data-wow-delay="0.8s" style="padding-left: 50px; visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">
						<div class="b_title" style=" font-size: 22px; color: #333333; font-weight: 500; text-transform: uppercase; margin: 10px 0; padding-bottom: 10px; ">Оказываемые услуги</div>
						<ul>
							[[!getImageList? &tvname=`okaz_uslugi` &tpl=`okaz_uslugiTpl`]]
						</ul>
					</div>`]]' => '<div class="bottom_block wow fadeInUp" data-wow-delay="0.8s" style="padding-left: 50px; visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">
						<div class="b_title" style=" font-size: 22px; color: #333333; font-weight: 500; text-transform: uppercase; margin: 10px 0; padding-bottom: 10px; ">Оказываемые услуги</div>
						<ul>
							[[!getImageList? &tvname=`okaz_uslugi` &tpl=`okaz_uslugiTpl`]]
						</ul>
					</div>',
    '[[*activnost:!empty=`<div class="data_slider_wrap wow fadeInUp">
				<div class="site_title">Профессиональная активность</div>
				<div class="blockin">
					<div class="data_slider owl-carousel">
							[[!getImageList? &tvname=`activnost` &tpl=`activnostTpl`]]
					</div>
				</div>
			</div>`]]' => '<div class="data_slider_wrap wow fadeInUp">
				<div class="site_title">Профессиональная активность</div>
				<div class="blockin">
					<div class="data_slider owl-carousel">
							[[!getImageList? &tvname=`activnost` &tpl=`activnostTpl`]]
					</div>
				</div>
			</div>',
    '[[*dimlom_sertivifikat:!empty=`<div class="thanks_block doctor_thanks wow fadeInUp sertificates-slider">
				<div class="site_title"><span>Дипломы и сертификаты</span></div>
				<div class="body">
					<div class="owl-carousel thanks_slider">
					    	[[!getImageList? &tvname=`dimlom_sertivifikat` &tpl=`diplom_sertifikatTpl`]]
					</div>
				</div>
			</div>`]]' => '<div class="thanks_block doctor_thanks wow fadeInUp sertificates-slider">
				<div class="site_title"><span>Дипломы и сертификаты</span></div>
				<div class="body">
					<div class="owl-carousel thanks_slider">
					    	[[!getImageList? &tvname=`dimlom_sertivifikat` &tpl=`diplom_sertifikatTpl`]]
					</div>
				</div>
			</div>',
    '[[*reviews_more:!empty=`
              	<div class="reviews-block doctor_thanks wow fadeInUp">
				<div class="site_title"><span>Отзывы</span></div>
				<div class="body">
				    
				    <div id="pdopage">
                        
                        <div class="rows">
                            [[!pdoPage?
                                &parents=`22`
                                &ajaxMode=`button`
                                &tpl=`otz`
                                &includeTVs=`image`
                                &includeContent=`1`
                                 &leftJoin=`{
                        		"Parent":{
                        			"class": "modResource",
                        			"alias": "Parent",
                        			"on":"Parent.id = modResource.parent"
                        		}
                            	}`
                            	&sortby=`{"Parent.menuindex":"desc", "menuindex":"asc"}`
                            	&limit=`4`
                                &resources=`23,33,34,38,43`
                                ]]
                                
                      
                        </div>
                        
    				    <div class="show-all">
        				       [[!+page.nav]]
    				    </div>
                    </div>
				    
				    
				    
				   
				</div>
			</div>`]]' => '
              	<div class="reviews-block doctor_thanks wow fadeInUp">
				<div class="site_title"><span>Отзывы</span></div>
				<div class="body">
				    
				    <div id="pdopage">
                        
                        <div class="rows">
                            [[!pdoPage?
                                &parents=`22`
                                &ajaxMode=`button`
                                &tpl=`otz`
                                &includeTVs=`image`
                                &includeContent=`1`
                                 &leftJoin=`{
                        		"Parent":{
                        			"class": "modResource",
                        			"alias": "Parent",
                        			"on":"Parent.id = modResource.parent"
                        		}
                            	}`
                            	&sortby=`{"Parent.menuindex":"desc", "menuindex":"asc"}`
                            	&limit=`4`
                                &resources=`23,33,34,38,43`
                                ]]
                                
                      
                        </div>
                        
    				    <div class="show-all">
        				       [[!+page.nav]]
    				    </div>
                    </div>
				    
				    
				    
				   
				</div>
			</div>',
    '[[$footer]]' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="implantation.html">Имплантация</a></li>
						<li><a href="services.html">Детская стоматология</a></li>
						<li><a href="terapia.html">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="hygiene.html">Гигиена и профилактика</a></li>
						<li><a href="proteth.html">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> 8:30-20:00</li>
						<li><strong>Сб: </strong> 8:30-14:00</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> 25-70-33</li>
						<li><strong>E-mail: </strong> <a href="mailto:cdistom@mail.ru"> cdistom@mail.ru</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br>  ул. Говорова, д. 19 "В", ст. 1</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="https://vk.com/cdistom"></a></li>
							<li><a href="https://www.facebook.com/tomskimplant/?modal=composer"></a></li>
							<li><a href="https://www.instagram.com/tomstom_implant/?hl=ru"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="contacts.html" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
    '[[pdoResources?tpl=`otz`&returnIds=``&showLog=``&fastMode=``&sortby=`{"Parent.menuindex":"desc", "menuindex":"asc"}`&sortbyTV=``&sortbyTVType=``&sortdir=`DESC`&sortdirTV=`ASC`&limit=`4`&offset=`0`&depth=`10`&outputSeparator=`
`&toPlaceholder=``&parents=`22`&includeContent=`1`&includeTVs=`image`&prepareTVs=`1`&processTVs=``&tvPrefix=`tv.`&tvFilters=``&tvFiltersAndDelimiter=`,`&tvFiltersOrDelimiter=`||`&where=``&showUnpublished=``&showDeleted=``&showHidden=`1`&hideContainers=``&context=``&idx=``&first=``&last=``&tplFirst=``&tplLast=``&tplOdd=``&tplWrapper=``&wrapIfEmpty=``&totalVar=`page.total`&resources=`23,33,34,38,43`&tplCondition=``&tplOperator=`==`&conditionalTpls=``&select=``&toSeparatePlaceholders=``&loadModels=``&scheme=``&useWeblinkUrl=``&plPrefix=``&maxLimit=`100`&page=`1`&pageVarKey=`page`&pageLimit=`5`&element=`pdoResources`&pageNavVar=`page.nav`&pageCountVar=`pageCount`&pageLinkScheme=``&tplPage=`@INLINE <li><a href="[[+href]]">[[+pageNo]]</a></li>`&tplPageWrapper=`@INLINE <div class="pagination"><ul class="pagination">[[+first]][[+prev]][[+pages]][[+next]][[+last]]</ul></div>`&tplPageActive=`@INLINE <li class="active"><a href="[[+href]]">[[+pageNo]]</a></li>`&tplPageFirst=`@INLINE <li class="control"><a href="[[+href]]">[[%pdopage_first]]</a></li>`&tplPageLast=`@INLINE <li class="control"><a href="[[+href]]">[[%pdopage_last]]</a></li>`&tplPagePrev=`@INLINE <li class="control"><a href="[[+href]]">&laquo;</a></li>`&tplPageNext=`@INLINE <li class="control"><a href="[[+href]]">&raquo;</a></li>`&tplPageSkip=`@INLINE <li class="disabled"><span>...</span></li>`&tplPageFirstEmpty=`@INLINE <li class="disabled"><span>[[%pdopage_first]]</span></li>`&tplPageLastEmpty=`@INLINE <li class="disabled"><span>[[%pdopage_last]]</span></li>`&tplPagePrevEmpty=`@INLINE <li class="disabled"><span>&laquo;</span></li>`&tplPageNextEmpty=`@INLINE <li class="disabled"><span>&raquo;</span></li>`&cache=``&cacheTime=`3600`&cacheAnonymous=``&ajax=`1`&ajaxMode=`button`&ajaxElemWrapper=`#pdopage`&ajaxElemRows=`#pdopage .rows`&ajaxElemPagination=`#pdopage .pagination`&ajaxElemLink=`#pdopage .pagination a`&ajaxElemMore=`#pdopage .btn-more`&ajaxTplMore=`@INLINE <button class="btn btn-default btn-more">[[%pdopage_more]]</button>`&ajaxHistory=``&setMeta=`1`&strictMode=`1`&leftJoin=`{
                        		"Parent":{
                        			"class": "modResource",
                        			"alias": "Parent",
                        			"on":"Parent.id = modResource.parent"
                        		}
                            	}`&request=`d2b402bac21d7ecd0d38254075b1c1e5`]]' => '<div class="review-block">
				        <div class="body-review">
				            <a href="assets/template/images/otzivy/otz-1.jpg" data-fancybox="reviews" data-caption="Л.Ф. Дирзина">
				                <img src="assets/template/images/otzivy/otz-1.jpg" alt="">
				            </a>
			                <div class="haed-review">
			                    <h3 class="name-review">Л.Ф. Дирзина</h3>
			                    <time class="date-review">17 октября 2017</time>
			                </div>
			                <div class="text-review">
			                  <p><strong>Что&nbsp;Вас беспокоило больше всего перед обращением к стоматологу?</strong></p>
<p>Требовалось лечение, протезирование.</p>
<p><strong>Что больше Всего вам понравилось в нашей клинике?</strong></p>
<p>Большое желание и возможность помочь пациенту, учитывая все особенности, используя передовые технологии.</p>
<p><strong>Какое впечатление осталось у Вас о нашей клинике? </strong></p>
<p>Высокий профессионализм всех докторов. Польностю доверилась опыту В.В. Ворошилова, И.Н. Синдяка, И.Е. Якушевой и не пожалела. Всем знакомым советую эту клинику.</p>
<p><strong>Что бы Вы хотели пожелать нашей клинике? </strong></p>
<p>Всего самого доброго! Адектватных пациентов!&nbsp;</p>
			                </div>
			                 <a class="read-more">Читать полностью</a>
			            </div>
				    </div>
				    
				    <script>
    $(\'a.read-more, .text-review\').click(function(){
    	var name = $(this).parent().find(\'.name-review\').text();
    	var dater =  $(this).parent().find(\'.date-review\').text();
    	var textr = $(this).parent().find(\'.text-review\').html();
    
        $(\'.head-pop-up .pop-name\').html(name);
        $(\'.text-pop-up\').html(textr);
        $(\'.date-pop-up .date-review\').html(dater);
        
        $(\'.pop-up-review\').addClass(\'active\');
        $(\'.overlay\').addClass(\'active\');
    });
    
    $(\'.close-review\').click(function(){
        $(\'.pop-up-review\').removeClass(\'active\');
        $(\'.overlay\').removeClass(\'active\');
    });
</script>
<div class="review-block">
				        <div class="body-review">
				            <a href="assets/template/images/otzivy/otz-9.jpg" data-fancybox="reviews" data-caption="А.В.Лесникова">
				                <img src="assets/template/images/otzivy/otz-9.jpg" alt="">
				            </a>
			                <div class="haed-review">
			                    <h3 class="name-review">А.В.Лесникова</h3>
			                    <time class="date-review">17 октября 2017</time>
			                </div>
			                <div class="text-review">
			                  <p><span style="font-weight: 400;"><strong>Что бы Вы хотели пожелать нашей клинике?</strong></span></p>
<p><span style="font-weight: 400;">Эстетический вид, срочно требовалось протезирование и имплантация зубов. </span></p>
<p><span style="font-weight: 400;"><strong>Что больше Всего вам понравилось в нашей клинике?</strong></span></p>
<p><span style="font-weight: 400;">Замечательный персонал (абсолютно все). Виталий Викторович и Игорь Николаевич замечательные врачи. </span></p>
<p><span style="font-weight: 400;"><strong>Какое впечатление осталось у Вас о нашей клинике?</strong></span></p>
<p><span style="font-weight: 400;">Отличное.</span></p>
<p><span style="font-weight: 400;"><strong>Что бы Вы хотели пожелать нашей клинике?</strong></span></p>
<p><span style="font-weight: 400;">Процветания!!! Удачи!!! И всего самого наилучшего! </span></p>
			                </div>
			                 <a class="read-more">Читать полностью</a>
			            </div>
				    </div>
				    
				    <script>
    $(\'a.read-more, .text-review\').click(function(){
    	var name = $(this).parent().find(\'.name-review\').text();
    	var dater =  $(this).parent().find(\'.date-review\').text();
    	var textr = $(this).parent().find(\'.text-review\').html();
    
        $(\'.head-pop-up .pop-name\').html(name);
        $(\'.text-pop-up\').html(textr);
        $(\'.date-pop-up .date-review\').html(dater);
        
        $(\'.pop-up-review\').addClass(\'active\');
        $(\'.overlay\').addClass(\'active\');
    });
    
    $(\'.close-review\').click(function(){
        $(\'.pop-up-review\').removeClass(\'active\');
        $(\'.overlay\').removeClass(\'active\');
    });
</script>
<div class="review-block">
				        <div class="body-review">
				            <a href="assets/template/images/otzivy/otz-10.jpg" data-fancybox="reviews" data-caption="Л.Л. Кетенева">
				                <img src="assets/template/images/otzivy/otz-10.jpg" alt="">
				            </a>
			                <div class="haed-review">
			                    <h3 class="name-review">Л.Л. Кетенева</h3>
			                    <time class="date-review">17 октября 2017</time>
			                </div>
			                <div class="text-review">
			                  <p><span style="font-weight: 400;"><strong>Что&nbsp;Вас беспокоило больше всего перед обращением к стоматологу?</strong></span></p>
<p><span style="font-weight: 400;">Эстетический вид зубов верхнего и нижнего ряда. </span></p>
<p><span style="font-weight: 400;"><strong>Что больше Всего вам понравилось в нашей клинике?</strong></span></p>
<p><span style="font-weight: 400;">Чуткое, доброжелательное отношение, высокий профессиональный уровень. Виталий Викторович и Игорь Николаевич - врачи от Бога!&nbsp;&nbsp;</span></p>
<p><span style="font-weight: 400;"><strong>Какое впечатление осталось у Вас о нашей клинике?</strong></span></p>
<p><span style="font-weight: 400;">Отличное.&nbsp;Работа выполняется качественно, в сжатые сроки. Ответственное отношение к выполняемой работе.</span></p>
<p><span style="font-weight: 400;"><strong>Что бы Вы хотели пожелать нашей клинике?</strong></span></p>
<p><span style="font-weight: 400;">Больше пациентов. Успехов и процветания ЦДИ. Спасибо всему персоналу клиники. &nbsp;</span></p>
			                </div>
			                 <a class="read-more">Читать полностью</a>
			            </div>
				    </div>
				    
				    <script>
    $(\'a.read-more, .text-review\').click(function(){
    	var name = $(this).parent().find(\'.name-review\').text();
    	var dater =  $(this).parent().find(\'.date-review\').text();
    	var textr = $(this).parent().find(\'.text-review\').html();
    
        $(\'.head-pop-up .pop-name\').html(name);
        $(\'.text-pop-up\').html(textr);
        $(\'.date-pop-up .date-review\').html(dater);
        
        $(\'.pop-up-review\').addClass(\'active\');
        $(\'.overlay\').addClass(\'active\');
    });
    
    $(\'.close-review\').click(function(){
        $(\'.pop-up-review\').removeClass(\'active\');
        $(\'.overlay\').removeClass(\'active\');
    });
</script>
<div class="review-block">
				        <div class="body-review">
				            <a href="assets/template/images/otzivy/otz-14.jpg" data-fancybox="reviews" data-caption="Отзыв">
				                <img src="assets/template/images/otzivy/otz-14.jpg" alt="">
				            </a>
			                <div class="haed-review">
			                    <h3 class="name-review">Отзыв</h3>
			                    <time class="date-review">17 октября 2017</time>
			                </div>
			                <div class="text-review">
			                  <p><span style="font-weight: 400;">Импланты стоят уже почти 6 лет. Сегодня лечила кариес. В полном восторге от Елены Александровны. Ну и моя любовь - Виталий Викторович и Игорь Николаевич! Уезжал из города, думаю, что такое великолепное отношения и компентность вряд ли встречу! Потрясающие администратор Жанна и Ирина! Огромное всем спасибо! Люблю вас всех! &nbsp;</span></p>
			                </div>
			                 <a class="read-more">Читать полностью</a>
			            </div>
				    </div>
				    
				    <script>
    $(\'a.read-more, .text-review\').click(function(){
    	var name = $(this).parent().find(\'.name-review\').text();
    	var dater =  $(this).parent().find(\'.date-review\').text();
    	var textr = $(this).parent().find(\'.text-review\').html();
    
        $(\'.head-pop-up .pop-name\').html(name);
        $(\'.text-pop-up\').html(textr);
        $(\'.date-pop-up .date-review\').html(dater);
        
        $(\'.pop-up-review\').addClass(\'active\');
        $(\'.overlay\').addClass(\'active\');
    });
    
    $(\'.close-review\').click(function(){
        $(\'.pop-up-review\').removeClass(\'active\');
        $(\'.overlay\').removeClass(\'active\');
    });
</script>',
  ),
  'sourceCache' => 
  array (
    'modChunk' => 
    array (
      'head' => 
      array (
        'fields' => 
        array (
          'id' => 1,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'head',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>[[*titl]]</title>
        <base href="[[++site_url]]" />
        <meta charset="[[++modx_charset]]" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<!DOCTYPE html>
<html lang="ru">
<head>
	
	 <title>[[*titl]]</title>
        <base href="[[++site_url]]" />
        <meta charset="[[++modx_charset]]" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" href="assets/template/favicon.ico" type="image/x-icon" />

	<link rel="stylesheet" href="assets/template/css/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/template/css/styles7222.css">
	<link rel="stylesheet" href="assets/template/css/owl.carousel.css">
	<link rel="stylesheet" href="assets/template/css/banner.css">
	<!--link rel="stylesheet" href="assets/template/css/animate.min.css"-->
	<script src="assets/template/js/jquery-3.2.1.min.js"></script>
	
	<style>
	    #jGrowl {visibility:hidden;}
	</style>

	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script> -->

	<script src="assets/template/js/wow.min.js"></script>
	<script src="assets/template/js/jquery-ui.min.js"></script>
	<script src="assets/template/js/owl.carousel.min.js"></script>
	<script src="assets/template/js/fullpage.js"></script>
	<script src="assets/template/js/wow.min.js"></script>
	<script src="https://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
	<script src="assets/template/js/scripts.js"></script>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement(\'img\')).src = \'https://vk.com/rtrg?p=VK-RTRG-151000-gv0xx\';</script> 
	<!-- Begin LeadBack code {literal} -->
<script>
    var _emv = _emv || [];
    _emv[\'campaign\'] = \'000e82a96e908e9fc14d1030\';
    
    (function() {
        var em = document.createElement(\'script\'); em.type = \'text/javascript\'; em.async = true;
        em.src = (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + \'leadback.ru/js/leadback.js\';
        var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(em, s);
    })();
</script>
<!-- End LeadBack code {/literal} -->
	<script src="assets/template/js/jquery.inputmask.js"></script>
	<script>
		$(document).ready(function(){
	
			$(".data_slider").owlCarousel({
				items:3,
				margin:50,
				nav:true,
				loop:true,
				responsive:{
					0:{
						items:1,
						autoHeight:true
					},
					767:{
						items:2,
						autoHeight:true
					},
					1024:{
						items:3,
					}
				}
			});
			owlcount = function(){
				$(\'.data_slider .owl-item .item\').removeClass(\'style1\');
				$(\'.data_slider .owl-item .item\').removeClass(\'style3\');
				$(\'.data_slider .owl-item.active\').first().find(\'.item\').addClass(\'style1\');
				$(\'.data_slider .owl-item.active\').last().find(\'.item\').addClass(\'style3\');
			}
			$(document).ready(function(){
				owlcount();
			});
			$(window).on(\'resize\' , function(){
				setTimeout(function(){
					owlcount();
				},500);
			});
			$(\'.data_slider\').on(\'changed.owl.carousel\', function(e) {
				setTimeout(function(){
					owlcount();
				});
			});
		});
	</script>
	<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script> -->
	
	

	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMDwSywCPHHL3ysbGiJ8AWPSfpkJaz1OM"></script>
	
		<script type="text/javascript">
		google.maps.event.addDomListener(window, \'load\', init);
		function init() {
			var mapOptions = {
				zoom: 11,
				center: new google.maps.LatLng(56.509553, 84.984578), 
				styles: [{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#fcfcfc"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#eeeeee"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#dddddd"}]}]
			};
			var mapElement = document.getElementById(\'map\');
			var map = new google.maps.Map(mapElement, mapOptions);
		}
	</script>

	<style>
		.section.premiummain .blocks a:hover {
			border: 3px solid #fff !important;
		}
	</style>
	<!--script src=\'https://www.google.com/recaptcha/api.js\'></script-->
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
					(function (d, w, c) {
						(w[c] = w[c] || []).push(function() {
							try {
								w.yaCounter45262389 = new Ya.Metrika({
									id:45262389,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									webvisor:true
								});
							} catch(e) { }
						});

						var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
						s.type = "text/javascript";
						s.async = true;
						s.src = "https://mc.yandex.ru/metrika/watch.js";

						if (w.opera == "[object Opera]") {
							d.addEventListener("DOMContentLoaded", f, false);
						} else { f(); }
					})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/45262389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'header' => 
      array (
        'fields' => 
        array (
          'id' => 15,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'header',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '<header class="header">
			<div class="blockin">
				<a href="[[++site_url]]" class="logo"><img src="[[#1.tv.logo]]" alt=""></a>
				<a href="tel:+[[#1.tv.tell]][[#1.tv.tell2]]" class="phone_top">
					<p class="address_top">[[#1.tv.addres]]</p>
					[[#1.tv.tell]] <span>[[#1.tv.tell2]]</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="[[++site_url]]" class="">
								<img src="[[#1.tv.logo]]" alt="[[++site_name]]">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '<header class="header">
			<div class="blockin">
				<a href="[[++site_url]]" class="logo"><img src="[[#1.tv.logo]]" alt=""></a>
				<a href="tel:+[[#1.tv.tell]][[#1.tv.tell2]]" class="phone_top">
					<p class="address_top">[[#1.tv.addres]]</p>
					[[#1.tv.tell]] <span>[[#1.tv.tell2]]</span>
				</a>
				<div class="burger">
					<span></span>
					<span></span>
					<span></span>
					<span></span>
				</div>
				<div class="top_menu">
					<ul>
						<li>
							<a href="[[++site_url]]" class="">
								<img src="[[#1.tv.logo]]" alt="[[++site_name]]">
							</a>
						</li>
			
						[[!pdoMenu?
						&parents=`0`
						&tplOuter=`ulTpl`
						&tpl=`liTpl`
						&level=`1`]]
					</ul>
				</div>
			</div>
		</header>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'Breadcrumbs' => 
      array (
        'fields' => 
        array (
          'id' => 38,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'Breadcrumbs',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '[[Breadcrumbs]]',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[Breadcrumbs]]',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'footer' => 
      array (
        'fields' => 
        array (
          'id' => 27,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'footer',
          'description' => '',
          'editor_type' => 0,
          'category' => 1,
          'cache_type' => 0,
          'snippet' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="[[~11]]">Имплантация</a></li>
						<li><a href="[[~15]]">Детская стоматология</a></li>
						<li><a href="[[~12]]">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="[[~14]]">Гигиена и профилактика</a></li>
						<li><a href="[[~13]]">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> [[++ot]]</li>
						<li><strong>Сб: </strong> [[++sub]]</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> [[++num]]</li>
						<li><strong>E-mail: </strong> <a href="mailto:[[++emailt]]"> [[++emailt]]</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br> [[++uli]]</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="[[++vk]]"></a></li>
							<li><a href="[[++fb]]"></a></li>
							<li><a href="[[++ig]]"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="[[~17]]" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
          'locked' => false,
          'properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '		<footer class="footer wow fadeInUp">
			
			<div class="blockin">
				<div class="footer_wrap">
					<div class="logo_bot">
						<a href="#"><img src="assets/template/images/logobot.png" alt=""></a>
					</div>
					<ul>
						<li><a href="[[~11]]">Имплантация</a></li>
						<li><a href="[[~15]]">Детская стоматология</a></li>
						<li><a href="[[~12]]">Терапия</a></li>
					</ul>
					<ul>
						<li><a href="[[~14]]">Гигиена и профилактика</a></li>
						<li><a href="[[~13]]">Протезирование</a></li>
						<li><a href="#">Диагностика</a></li>
					</ul>
					<ul class="address">
						<li><strong>Пн-Пт: </strong> [[++ot]]</li>
						<li><strong>Сб: </strong> [[++sub]]</li>
						<li><strong>Вс: </strong>  выходной</li>
					</ul>
					<ul class="address">
						<li><strong>Телефон: </strong> [[++num]]</li>
						<li><strong>E-mail: </strong> <a href="mailto:[[++emailt]]"> [[++emailt]]</a></li>
						<li><strong>Адрес: </strong> 634057, Томск,<br> [[++uli]]</li>
					</ul>
					<div class="copyright">
						<a href="assets/template/policy.pdf" target="__blank">Политика конфиденциальности</a>
						<ul class="address social_footer">
							<li><a href="[[++vk]]"></a></li>
							<li><a href="[[++fb]]"></a></li>
							<li><a href="[[++ig]]"></a></li>					
						</ul>
						<a href="#" class="fresco">FRESCO —  поддержка и разработка</a>

					</div>
				</div>
			</div>

		</footer>
			<div class="services_popup_block">
				<div class="blockwrap">
					<header class="header">
						<div class="blockin">
							<a href="#" class="logo"><img src="assets/template/images/logocdi.png" alt=""></a>
							<div class="closeservice"></div>
						</div>
					</header>
					<div class="blockin">
						<div class="services_wrap_popup">
							<div class="leftblock">
								<div class="title">
									услуги
									<br>центра
								</div>
							</div>
							<div class="rightblock" style=" max-width: 590px; ">
								<ul>
								    	[[!pdoResources? 
								    	    &parents=`3`
								    	    &tpl=`uslugiTpl`
								    	    &includeTVs=`icon_image`
								    	]]
								
								</ul>
								<a href="[[~17]]" class="site_but">Записаться на консультацию</a>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
	
	<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, \'script\', \'cloud.roistat.com\', \'1ef794cdf8cf93c3e49c86cee69299a8\');
</script>
 <script>
         $("#mixparagraph").mixItUp({
                selectors: {
                    target: \'.price-paragraph\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
                
       "animation": {
         "duration": 1,
        "nudge": false,
        "reverseOut": false,
        "effects": "",
           "enable":false
    }
 
            });


 $("#mixcontainer").mixItUp({
                selectors: {
                    target: \'.mix\',
                    filter: \'.filter\'
                },
                load: {
                    filter: \'.ortodontiya\'
                },
   "animation": {
           "duration": 250,
        "nudge": false,
        "reverseOut": false,
        "effects": "fade translateY(20%)"
    }
            });
</script>',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modSnippet' => 
    array (
      'Breadcrumbs' => 
      array (
        'fields' => 
        array (
          'id' => 50,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'Breadcrumbs',
          'description' => '',
          'editor_type' => 0,
          'category' => 34,
          'cache_type' => 0,
          'snippet' => '/**
 * BreadCrumbs
 *
 * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>
 *
 * BreadCrumbs is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package breadcrumbs
 */
/**
 * @name BreadCrumbs
 * @version 0.9f
 * @created 2006-06-12
 * @since 2009-05-11
 * @author Jared <jaredc@honeydewdesign.com>
 * @editor Bill Wilson
 * @editor wendy@djamoer.net
 * @editor grad
 * @editor Shaun McCormick <shaun@collabpad.com>
 * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>
 * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>
 * @tester Bob Ray
 * @package breadcrumbs
 *
 * This snippet was designed to show the path through the various levels of site
 * structure back to the root. It is NOT necessarily the path the user took to
 * arrive at a given page.
 *
 * @see breadcrumbs.class.php for config settings
 *
 * Included classes
 * .B_crumbBox Span that surrounds all crumb output
 * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown
 * .B_currentCrumb Span or A tag surrounding the current crumb
 * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not
 * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .
 * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or
 * hide)
 * .B_homeCrumb Class given to the home crumb
 */
require_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';
$bc = new BreadCrumbs($modx,$scriptProperties);
return $bc->run();',
          'locked' => false,
          'properties' => NULL,
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * BreadCrumbs
 *
 * Copyright 2009-2011 by Shaun McCormick <shaun+bc@modx.com>
 *
 * BreadCrumbs is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * BreadCrumbs is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * BreadCrumbs; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package breadcrumbs
 */
/**
 * @name BreadCrumbs
 * @version 0.9f
 * @created 2006-06-12
 * @since 2009-05-11
 * @author Jared <jaredc@honeydewdesign.com>
 * @editor Bill Wilson
 * @editor wendy@djamoer.net
 * @editor grad
 * @editor Shaun McCormick <shaun@collabpad.com>
 * @editor Shawn Wilkerson <shawn@shawnwilkerson.com>
 * @editor Wieger Sloot, Sterc.nl <wieger@sterc.nl>
 * @tester Bob Ray
 * @package breadcrumbs
 *
 * This snippet was designed to show the path through the various levels of site
 * structure back to the root. It is NOT necessarily the path the user took to
 * arrive at a given page.
 *
 * @see breadcrumbs.class.php for config settings
 *
 * Included classes
 * .B_crumbBox Span that surrounds all crumb output
 * .B_hideCrumb Span surrounding the "..." if there are more crumbs than will be shown
 * .B_currentCrumb Span or A tag surrounding the current crumb
 * .B_firstCrumb Span that always surrounds the first crumb, whether it is "home" or not
 * .B_lastCrumb Span surrounding last crumb, whether it is the current page or not .
 * .B_crumb Class given to each A tag surrounding the intermediate crumbs (not home, or
 * hide)
 * .B_homeCrumb Class given to the home crumb
 */
require_once $modx->getOption(\'breadcrumbs.core_path\',null,$modx->getOption(\'core_path\').\'components/breadcrumbs/\').\'model/breadcrumbs/breadcrumbs.class.php\';
$bc = new BreadCrumbs($modx,$scriptProperties);
return $bc->run();',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'pdoMenu' => 
      array (
        'fields' => 
        array (
          'id' => 38,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoMenu',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */

// Convert parameters from Wayfinder if exists
if (isset($startId)) {
    $scriptProperties[\'parents\'] = $startId;
}
if (!empty($includeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $includeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',\' . $v;
        } else {
            $scriptProperties[\'resources\'] = $v;
        }
    }
}
if (!empty($excludeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $excludeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',-\' . $v;
        } else {
            $scriptProperties[\'resources\'] = \'-\' . $v;
        }
    }
}

if (!empty($previewUnpublished) && $modx->hasPermission(\'view_unpublished\')) {
    $scriptProperties[\'showUnpublished\'] = 1;
}

$scriptProperties[\'depth\'] = empty($level) ? 100 : abs($level) - 1;
if (!empty($contexts)) {
    $scriptProperties[\'context\'] = $contexts;
}
if (empty($scriptProperties[\'context\'])) {
    $scriptProperties[\'context\'] = $modx->resource->context_key;
}

// Save original parents specified by user
$specified_parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));

if ($scriptProperties[\'parents\'] === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
} elseif ($scriptProperties[\'parents\'] === 0 || $scriptProperties[\'parents\'] === \'0\') {
    if ($scriptProperties[\'depth\'] !== \'\' && $scriptProperties[\'depth\'] !== 100) {
        $contexts = array_map(\'trim\', explode(\',\', $scriptProperties[\'context\']));
        $parents = array();
        if (!empty($scriptProperties[\'showDeleted\'])) {
            $pdoFetch = $modx->getService(\'pdoFetch\');
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $pdoFetch->getChildIds(\'modResource\', 0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        } else {
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $modx->getChildIds(0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        }
        $scriptProperties[\'parents\'] = !empty($parents) ? implode(\',\', $parents) : \'+0\';
        $scriptProperties[\'depth\'] = 0;
    }
    $scriptProperties[\'includeParents\'] = 1;
    $scriptProperties[\'displayStart\'] = 0;
} else {
    $parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));
    $parents_in = $parents_out = array();
    foreach ($parents as $v) {
        if (!is_numeric($v)) {
            continue;
        }
        if ($v[0] == \'-\') {
            $parents_out[] = abs($v);
        } else {
            $parents_in[] = abs($v);
        }
    }

    if (empty($parents_in)) {
        $scriptProperties[\'includeParents\'] = 1;
        $scriptProperties[\'displayStart\'] = 0;
    }
}

if (!empty($displayStart)) {
    $scriptProperties[\'includeParents\'] = 1;
}
if (!empty($ph)) {
    $toPlaceholder = $ph;
}
if (!empty($sortOrder)) {
    $scriptProperties[\'sortdir\'] = $sortOrder;
}
if (!empty($sortBy)) {
    $scriptProperties[\'sortby\'] = $sortBy;
}
if (!empty($permissions)) {
    $scriptProperties[\'checkPermissions\'] = $permissions;
}
if (!empty($cacheResults)) {
    $scriptProperties[\'cache\'] = $cacheResults;
}
if (!empty($ignoreHidden)) {
    $scriptProperties[\'showHidden\'] = $ignoreHidden;
}

$wfTemplates = array(
    \'outerTpl\' => \'tplOuter\',
    \'rowTpl\' => \'tpl\',
    \'parentRowTpl\' => \'tplParentRow\',
    \'parentRowHereTpl\' => \'tplParentRowHere\',
    \'hereTpl\' => \'tplHere\',
    \'innerTpl\' => \'tplInner\',
    \'innerRowTpl\' => \'tplInnerRow\',
    \'innerHereTpl\' => \'tplInnerHere\',
    \'activeParentRowTpl\' => \'tplParentRowActive\',
    \'categoryFoldersTpl\' => \'tplCategoryFolder\',
    \'startItemTpl\' => \'tplStart\',
);
foreach ($wfTemplates as $k => $v) {
    if (isset(${$k})) {
        $scriptProperties[$v] = ${$k};
    }
}
//---

/** @var pdoMenu $pdoMenu */
$fqn = $modx->getOption(\'pdoMenu.class\', null, \'pdotools.pdomenu\', true);
$path = $modx->getOption(\'pdomenu_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoMenu = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoMenu->pdoTools->addTime(\'pdoTools loaded\');

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
if (empty($scriptProperties[\'cache_key\'])) {
    $scriptProperties[\'cache_key\'] = \'pdomenu/\' . sha1(serialize($scriptProperties));
}

$output = \'\';
$tree = array();
if ($cache) {
    $tree = $pdoMenu->pdoTools->getCache($scriptProperties);
}
if (empty($tree)) {
    $data = $pdoMenu->pdoTools->run();
    $data = $pdoMenu->pdoTools->buildTree($data, \'id\', \'parent\', $specified_parents);
    $tree = array();
    foreach ($data as $k => $v) {
        if (empty($v[\'id\'])) {
            if (!in_array($k, $specified_parents) && !$pdoMenu->checkResource($k)) {
                continue;
            } else {
                $tree = array_merge($tree, $v[\'children\']);
            }
        } else {
            $tree[$k] = $v;
        }
    }
    if ($cache) {
        $pdoMenu->pdoTools->setCache($tree, $scriptProperties);
    }
}
if (!empty($tree)) {
    $output = $pdoMenu->templateTree($tree);
}

if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $output .= \'<pre class="pdoMenuLog">\' . print_r($pdoMenu->pdoTools->getTime(), 1) . \'</pre>\';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
          'locked' => false,
          'properties' => 
          array (
            'showLog' => 
            array (
              'name' => 'showLog',
              'desc' => 'pdotools_prop_showLog',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать дополнительную информацию о работе сниппета. Только для авторизованных в контекте "mgr".',
              'area_trans' => '',
            ),
            'fastMode' => 
            array (
              'name' => 'fastMode',
              'desc' => 'pdotools_prop_fastMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Быстрый режим обработки чанков. Все необработанные теги (условия, сниппеты и т.п.) будут вырезаны.',
              'area_trans' => '',
            ),
            'level' => 
            array (
              'name' => 'level',
              'desc' => 'pdotools_prop_level',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Уровень генерируемого меню.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'pdotools_prop_parents',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список родителей, через запятую, для поиска результатов. По умолчанию выборка ограничена текущим родителем. Если поставить 0 - выборка не ограничивается. Если id родителя начинается с дефиса, он и его потомки исключается из выборки.',
              'area_trans' => '',
            ),
            'displayStart' => 
            array (
              'name' => 'displayStart',
              'desc' => 'pdotools_prop_displayStart',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить показ начальных узлов меню. Полезно при указании более одного "parents".',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'pdotools_prop_resources',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ресурсов, через запятую, для вывода в результатах. Если id ресурса начинается с дефиса, этот ресурс исключается из выборки.',
              'area_trans' => '',
            ),
            'templates' => 
            array (
              'name' => 'templates',
              'desc' => 'pdotools_prop_templates',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список шаблонов, через запятую, для фильтрации результатов. Если id шаблона начинается с дефиса, ресурсы с ним исключается из выборки.',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'pdotools_prop_context',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение выборки по контексту ресурсов.',
              'area_trans' => '',
            ),
            'cache' => 
            array (
              'name' => 'cache',
              'desc' => 'pdotools_prop_cache',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Кэширование результатов работы сниппета.',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'pdotools_prop_cacheTime',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 3600,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Время актуальности кэша в секундах.',
              'area_trans' => '',
            ),
            'cacheAnonymous' => 
            array (
              'name' => 'cacheAnonymous',
              'desc' => 'pdotools_prop_cacheAnonymous',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить кэширование только для неавторизованных посетителей.',
              'area_trans' => '',
            ),
            'plPrefix' => 
            array (
              'name' => 'plPrefix',
              'desc' => 'pdotools_prop_plPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'wf.',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для выставляемых плейсхолдеров, по умолчанию "wf.".',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'pdotools_prop_showHidden',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать ресурсы, скрытые в меню.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'pdotools_prop_showUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать неопубликованные ресурсы.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'pdotools_prop_showDeleted',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать удалённые ресурсы.',
              'area_trans' => '',
            ),
            'previewUnpublished' => 
            array (
              'name' => 'previewUnpublished',
              'desc' => 'pdotools_prop_previewUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить показ неопубликованных документов, если у пользователя есть на это разрешение.',
              'area_trans' => '',
            ),
            'hideSubMenus' => 
            array (
              'name' => 'hideSubMenus',
              'desc' => 'pdotools_prop_hideSubMenus',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Спрятать неактивные ветки меню.',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'pdotools_prop_useWeblinkUrl',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Генерировать ссылку с учетом класса ресурса.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'pdotools_prop_sortdir',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки: по убыванию или возрастанию.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'pdotools_prop_sortby',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'menuindex',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Любое поле ресурса для сортировки, включая ТВ параметр, если он указан в параметре "includeTVs". Можно указывать JSON строку с массивом нескольких полей. Для случайно сортировки укажите "RAND()"',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'rowIdPrefix' => 
            array (
              'name' => 'rowIdPrefix',
              'desc' => 'pdotools_prop_rowIdPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс id="" для выставления идентификатора в чанк.',
              'area_trans' => '',
            ),
            'firstClass' => 
            array (
              'name' => 'firstClass',
              'desc' => 'pdotools_prop_firstClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'first',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс для первого пункта меню.',
              'area_trans' => '',
            ),
            'lastClass' => 
            array (
              'name' => 'lastClass',
              'desc' => 'pdotools_prop_lastClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'last',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс последнего пункта меню.',
              'area_trans' => '',
            ),
            'hereClass' => 
            array (
              'name' => 'hereClass',
              'desc' => 'pdotools_prop_hereClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'active',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс для активного пункта меню.',
              'area_trans' => '',
            ),
            'parentClass' => 
            array (
              'name' => 'parentClass',
              'desc' => 'pdotools_prop_parentClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс категории меню.',
              'area_trans' => '',
            ),
            'rowClass' => 
            array (
              'name' => 'rowClass',
              'desc' => 'pdotools_prop_rowClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс одной строки меню.',
              'area_trans' => '',
            ),
            'outerClass' => 
            array (
              'name' => 'outerClass',
              'desc' => 'pdotools_prop_outerClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс обертки меню.',
              'area_trans' => '',
            ),
            'innerClass' => 
            array (
              'name' => 'innerClass',
              'desc' => 'pdotools_prop_innerClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс внутренних ссылок меню.',
              'area_trans' => '',
            ),
            'levelClass' => 
            array (
              'name' => 'levelClass',
              'desc' => 'pdotools_prop_levelClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс уровня меню. Например, если укажите "level", то будет "level1", "level2" и т.д.',
              'area_trans' => '',
            ),
            'selfClass' => 
            array (
              'name' => 'selfClass',
              'desc' => 'pdotools_prop_selfClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс текущего документа в меню.',
              'area_trans' => '',
            ),
            'webLinkClass' => 
            array (
              'name' => 'webLinkClass',
              'desc' => 'pdotools_prop_webLinkClass',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Класс документа-ссылки.',
              'area_trans' => '',
            ),
            'tplOuter' => 
            array (
              'name' => 'tplOuter',
              'desc' => 'pdotools_prop_tplOuter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <ul[[+classes]]>[[+wrapper]]</ul>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка всего блока меню.',
              'area_trans' => '',
            ),
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'pdotools_prop_tpl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li[[+classes]]><a href="[[+link]]" [[+attributes]]>[[+menutitle]]</a>[[+wrapper]]</li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для оформления ресурса. Если не указан, то содержимое полей ресурса будет распечатано на экран.',
              'area_trans' => '',
            ),
            'tplParentRow' => 
            array (
              'name' => 'tplParentRow',
              'desc' => 'pdotools_prop_tplParentRow',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplParentRowHere' => 
            array (
              'name' => 'tplParentRowHere',
              'desc' => 'pdotools_prop_tplParentRowHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления текущего контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplHere' => 
            array (
              'name' => 'tplHere',
              'desc' => 'pdotools_prop_tplHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк текущего документа',
              'area_trans' => '',
            ),
            'tplInner' => 
            array (
              'name' => 'tplInner',
              'desc' => 'pdotools_prop_tplInner',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка внутренних пунктов меню. Если пуст - будет использовать "tplInner".',
              'area_trans' => '',
            ),
            'tplInnerRow' => 
            array (
              'name' => 'tplInnerRow',
              'desc' => 'pdotools_prop_tplInnerRow',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка активного пункта меню.',
              'area_trans' => '',
            ),
            'tplInnerHere' => 
            array (
              'name' => 'tplInnerHere',
              'desc' => 'pdotools_prop_tplInnerHere',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка активного пункта меню.',
              'area_trans' => '',
            ),
            'tplParentRowActive' => 
            array (
              'name' => 'tplParentRowActive',
              'desc' => 'pdotools_prop_tplParentRowActive',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления активного контейнера с потомками.',
              'area_trans' => '',
            ),
            'tplCategoryFolder' => 
            array (
              'name' => 'tplCategoryFolder',
              'desc' => 'pdotools_prop_tplCategoryFolder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Специальный чанк оформления категории. Категория - это документ с потомками и или нулевым шаблоном, или с атрибутом "rel=\\"category\\"".',
              'area_trans' => '',
            ),
            'tplStart' => 
            array (
              'name' => 'tplStart',
              'desc' => 'pdotools_prop_tplStart',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <h2[[+classes]]>[[+menutitle]]</h2>[[+wrapper]]',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления корневого пункта, при условии, что включен "displayStart".',
              'area_trans' => '',
            ),
            'checkPermissions' => 
            array (
              'name' => 'checkPermissions',
              'desc' => 'pdotools_prop_checkPermissions',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Укажите, какие разрешения нужно проверять у пользователя при выводе документов.',
              'area_trans' => '',
            ),
            'hereId' => 
            array (
              'name' => 'hereId',
              'desc' => 'pdotools_prop_hereId',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Id документа, текущего для генерируемого меню. Нужно указывать только если скрипт сам его неверно определяет, например при выводе меню из чанка другого сниппета.',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'pdotools_prop_where',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Массив дополнительных параметров выборки, закодированный в JSON.',
              'area_trans' => '',
            ),
            'select' => 
            array (
              'name' => 'select',
              'desc' => 'pdotools_prop_select',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список полей для выборки, через запятую. Можно указывать JSON строку с массивом, например {"modResource":"id,pagetitle,content"}.',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'pdotools_prop_scheme',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => '',
                  'text' => 'System default',
                  'name' => 'System default',
                ),
                1 => 
                array (
                  'value' => -1,
                  'text' => '-1 (relative to site_url)',
                  'name' => '-1 (relative to site_url)',
                ),
                2 => 
                array (
                  'value' => 'full',
                  'text' => 'full (absolute, prepended with site_url)',
                  'name' => 'full (absolute, prepended with site_url)',
                ),
                3 => 
                array (
                  'value' => 'abs',
                  'text' => 'abs (absolute, prepended with base_url)',
                  'name' => 'abs (absolute, prepended with base_url)',
                ),
                4 => 
                array (
                  'value' => 'http',
                  'text' => 'http (absolute, forced to http scheme)',
                  'name' => 'http (absolute, forced to http scheme)',
                ),
                5 => 
                array (
                  'value' => 'https',
                  'text' => 'https (absolute, forced to https scheme)',
                  'name' => 'https (absolute, forced to https scheme)',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема формирования ссылок: "uri" для подстановки поля uri документа (очень быстро) или параметр для modX::makeUrl().',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'countChildren' => 
            array (
              'name' => 'countChildren',
              'desc' => 'pdotools_prop_countChildren',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Вывести точное количество активных потомков документа в плейсхолдер [[+children]].',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdomenu.php',
          'content' => '/** @var array $scriptProperties */

// Convert parameters from Wayfinder if exists
if (isset($startId)) {
    $scriptProperties[\'parents\'] = $startId;
}
if (!empty($includeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $includeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',\' . $v;
        } else {
            $scriptProperties[\'resources\'] = $v;
        }
    }
}
if (!empty($excludeDocs)) {
    $tmp = array_map(\'trim\', explode(\',\', $excludeDocs));
    foreach ($tmp as $v) {
        if (!empty($scriptProperties[\'resources\'])) {
            $scriptProperties[\'resources\'] .= \',-\' . $v;
        } else {
            $scriptProperties[\'resources\'] = \'-\' . $v;
        }
    }
}

if (!empty($previewUnpublished) && $modx->hasPermission(\'view_unpublished\')) {
    $scriptProperties[\'showUnpublished\'] = 1;
}

$scriptProperties[\'depth\'] = empty($level) ? 100 : abs($level) - 1;
if (!empty($contexts)) {
    $scriptProperties[\'context\'] = $contexts;
}
if (empty($scriptProperties[\'context\'])) {
    $scriptProperties[\'context\'] = $modx->resource->context_key;
}

// Save original parents specified by user
$specified_parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));

if ($scriptProperties[\'parents\'] === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
} elseif ($scriptProperties[\'parents\'] === 0 || $scriptProperties[\'parents\'] === \'0\') {
    if ($scriptProperties[\'depth\'] !== \'\' && $scriptProperties[\'depth\'] !== 100) {
        $contexts = array_map(\'trim\', explode(\',\', $scriptProperties[\'context\']));
        $parents = array();
        if (!empty($scriptProperties[\'showDeleted\'])) {
            $pdoFetch = $modx->getService(\'pdoFetch\');
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $pdoFetch->getChildIds(\'modResource\', 0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        } else {
            foreach ($contexts as $ctx) {
                $parents = array_merge($parents,
                    $modx->getChildIds(0, $scriptProperties[\'depth\'], array(\'context\' => $ctx)));
            }
        }
        $scriptProperties[\'parents\'] = !empty($parents) ? implode(\',\', $parents) : \'+0\';
        $scriptProperties[\'depth\'] = 0;
    }
    $scriptProperties[\'includeParents\'] = 1;
    $scriptProperties[\'displayStart\'] = 0;
} else {
    $parents = array_map(\'trim\', explode(\',\', $scriptProperties[\'parents\']));
    $parents_in = $parents_out = array();
    foreach ($parents as $v) {
        if (!is_numeric($v)) {
            continue;
        }
        if ($v[0] == \'-\') {
            $parents_out[] = abs($v);
        } else {
            $parents_in[] = abs($v);
        }
    }

    if (empty($parents_in)) {
        $scriptProperties[\'includeParents\'] = 1;
        $scriptProperties[\'displayStart\'] = 0;
    }
}

if (!empty($displayStart)) {
    $scriptProperties[\'includeParents\'] = 1;
}
if (!empty($ph)) {
    $toPlaceholder = $ph;
}
if (!empty($sortOrder)) {
    $scriptProperties[\'sortdir\'] = $sortOrder;
}
if (!empty($sortBy)) {
    $scriptProperties[\'sortby\'] = $sortBy;
}
if (!empty($permissions)) {
    $scriptProperties[\'checkPermissions\'] = $permissions;
}
if (!empty($cacheResults)) {
    $scriptProperties[\'cache\'] = $cacheResults;
}
if (!empty($ignoreHidden)) {
    $scriptProperties[\'showHidden\'] = $ignoreHidden;
}

$wfTemplates = array(
    \'outerTpl\' => \'tplOuter\',
    \'rowTpl\' => \'tpl\',
    \'parentRowTpl\' => \'tplParentRow\',
    \'parentRowHereTpl\' => \'tplParentRowHere\',
    \'hereTpl\' => \'tplHere\',
    \'innerTpl\' => \'tplInner\',
    \'innerRowTpl\' => \'tplInnerRow\',
    \'innerHereTpl\' => \'tplInnerHere\',
    \'activeParentRowTpl\' => \'tplParentRowActive\',
    \'categoryFoldersTpl\' => \'tplCategoryFolder\',
    \'startItemTpl\' => \'tplStart\',
);
foreach ($wfTemplates as $k => $v) {
    if (isset(${$k})) {
        $scriptProperties[$v] = ${$k};
    }
}
//---

/** @var pdoMenu $pdoMenu */
$fqn = $modx->getOption(\'pdoMenu.class\', null, \'pdotools.pdomenu\', true);
$path = $modx->getOption(\'pdomenu_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoMenu = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoMenu->pdoTools->addTime(\'pdoTools loaded\');

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
if (empty($scriptProperties[\'cache_key\'])) {
    $scriptProperties[\'cache_key\'] = \'pdomenu/\' . sha1(serialize($scriptProperties));
}

$output = \'\';
$tree = array();
if ($cache) {
    $tree = $pdoMenu->pdoTools->getCache($scriptProperties);
}
if (empty($tree)) {
    $data = $pdoMenu->pdoTools->run();
    $data = $pdoMenu->pdoTools->buildTree($data, \'id\', \'parent\', $specified_parents);
    $tree = array();
    foreach ($data as $k => $v) {
        if (empty($v[\'id\'])) {
            if (!in_array($k, $specified_parents) && !$pdoMenu->checkResource($k)) {
                continue;
            } else {
                $tree = array_merge($tree, $v[\'children\']);
            }
        } else {
            $tree[$k] = $v;
        }
    }
    if ($cache) {
        $pdoMenu->pdoTools->setCache($tree, $scriptProperties);
    }
}
if (!empty($tree)) {
    $output = $pdoMenu->templateTree($tree);
}

if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $output .= \'<pre class="pdoMenuLog">\' . print_r($pdoMenu->pdoTools->getTime(), 1) . \'</pre>\';
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $output);
} else {
    return $output;
}',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'getImageList' => 
      array (
        'fields' => 
        array (
          'id' => 10,
          'source' => 0,
          'property_preprocess' => false,
          'name' => 'getImageList',
          'description' => '',
          'editor_type' => 0,
          'category' => 9,
          'cache_type' => 0,
          'snippet' => '/**
 * getImageList
 *
 * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>
 *
 * getImageList is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package migx
 */
/**
 * getImageList
 *
 * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution 
 *
 * @version 1.4
 * @author Bruno Perner <b.perner@gmx.de>
 * @copyright Copyright &copy; 2009-2014
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License
 * version 2 or (at your option) any later version.
 * @package migx
 */

/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/
/* get default properties */


$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');
$inherit_children_tvname = $modx->getOption(\'inherit_children_tvname\', $scriptProperties, \'\');
$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');
$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');
$emptyTpl = $modx->getOption(\'emptyTpl\', $scriptProperties, \'\'); 
$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');
$offset = $modx->getOption(\'offset\', $scriptProperties, 0);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);
$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images
$where = $modx->getOption(\'where\', $scriptProperties, \'\');
$where = !empty($where) ? $modx->fromJSON($where) : array();
$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');
$sort = !empty($sort) ? $modx->fromJSON($sort) : array();
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');
$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);
$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');
$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');
$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');
$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));
$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;
$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');
$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');
$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');
$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');
$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');
$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;
//split json into parts
$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));
$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from
$inheritFrom = !empty($inheritFrom) ? explode(\',\', $inheritFrom) : \'\';

$modx->setPlaceholder(\'docid\', $docid);

$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);
$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);

$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);
if (!($migx instanceof Migx))
    return \'\';
$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';

if (!empty($tvname)) {
    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {

        /*
        *   get inputProperties
        */


        $properties = $tv->get(\'input_properties\');
        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();

        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');
        if (!empty($migx->config[\'configs\'])) {
            $migx->loadConfigs();
            // get tabs from file or migx-config-table
            $formtabs = $migx->getTabs();
        }
        if (empty($formtabs) && isset($properties[\'formtabs\'])) {
            //try to get formtabs and its fields from properties
            $formtabs = $modx->fromJSON($properties[\'formtabs\']);
        }

        if (!empty($properties[\'basePath\'])) {
            if ($properties[\'autoResourceFolders\'] == \'true\') {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';
            } else {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];
            }
        }
        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {
            $jsonVarKey = $properties[\'jsonvarkey\'];
            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
        }

        if (empty($outputvalue)) {
            $outputvalue = $tv->renderOutput($docid);
            if (empty($outputvalue) && !empty($inheritFrom)) {
                foreach ($inheritFrom as $from) {
                    if ($from == \'parents\') {
                        if (!empty($inherit_children_tvname)){
                            //try to get items from optional MIGX-TV for children
                            if ($inh_tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inherit_children_tvname))) {
                                $outputvalue = $inh_tv->processInheritBinding(\'\', $docid);    
                            }
                        }
                        $outputvalue = empty($outputvalue) ? $tv->processInheritBinding(\'\', $docid) : $outputvalue;
                    } else {
                        $outputvalue = $tv->renderOutput($from);
                    }
                    if (!empty($outputvalue)) {
                        break;
                    }
                }
            }
        }


        /*
        *   get inputTvs 
        */
        $inputTvs = array();
        if (is_array($formtabs)) {

            //multiple different Forms
            // Note: use same field-names and inputTVs in all forms
            $inputTvs = $migx->extractInputTvs($formtabs);
        }
        if ($migx->source = $tv->getSource($migx->working_context, false)) {
            $migx->source->initialize();
        }

    }


}

if (empty($outputvalue)) {
    $modx->setPlaceholder($totalVar, 0);
    return \'\';
}

//echo $outputvalue.\'<br/><br/>\';

$items = $modx->fromJSON($outputvalue);

// where filter
if (is_array($where) && count($where) > 0) {
    $items = $migx->filterItems($where, $items);
}
$modx->setPlaceholder($totalVar, count($items));

if (!empty($reverse)) {
    $items = array_reverse($items);
}

// sort items
if (is_array($sort) && count($sort) > 0) {
    $items = $migx->sortDbResult($items, $sort);
}

$summaries = array();
$output = \'\';
$items = $offset > 0 ? array_slice($items, $offset) : $items;
$count = count($items);

if ($count > 0) {
    $limit = $limit == 0 || $limit > $count ? $count : $limit;
    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;
    //preselect important items
    $preitems = array();
    if ($randomize && $preselectLimit > 0) {
        for ($i = 0; $i < $preselectLimit; $i++) {
            $preitems[] = $items[$i];
            unset($items[$i]);
        }
        $limit = $limit - count($preitems);
    }

    //shuffle items
    if ($randomize) {
        shuffle($items);
    }

    //limit items
    $count = count($items);
    $tempitems = array();

    for ($i = 0; $i < $limit; $i++) {
        if ($i >= $count) {
            break;
        }
        $tempitems[] = $items[$i];
    }
    $items = $tempitems;

    //add preselected items and schuffle again
    if ($randomize && $preselectLimit > 0) {
        $items = array_merge($preitems, $items);
        shuffle($items);
    }

    $properties = array();
    foreach ($scriptProperties as $property => $value) {
        $properties[\'property.\' . $property] = $value;
    }

    $idx = 0;
    $output = array();
    $template = array();
    $count = count($items);

    foreach ($items as $key => $item) {
        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';
        $fields = array();
        foreach ($item as $field => $value) {
            if (is_array($value)) {
                if (is_array($value[0])) {
                    //nested array - convert to json
                    $value = $modx->toJson($value);
                } else {
                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)
                }
            }


            $inputTVkey = $formname . $field;

            if ($processTVs && isset($inputTvs[$inputTVkey])) {
                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {

                } else {
                    $tv = $modx->newObject(\'modTemplateVar\');
                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);
                }
                $inputTV = $inputTvs[$inputTVkey];

                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');
                //don\'t manipulate any urls here
                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');
                $tv->set(\'default_text\', $value);
                $value = $tv->renderOutput($docid);
                //set option back
                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);
                //now manipulate urls
                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {
                    $mTypes = explode(\',\', $mTypes);
                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {
                        //$value = $mediasource->prepareOutputUrl($value);
                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));
                    }
                }

            }
            $fields[$field] = $value;

        }

        if (!empty($addfields)) {
            foreach ($addfields as $addfield) {
                $addfield = explode(\':\', $addfield);
                $addname = $addfield[0];
                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';
                $fields[$addname] = $adddefault;
            }
        }

        if (!empty($sumFields)) {
            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);
            foreach ($sumFields as $sumField) {
                if (isset($fields[$sumField])) {
                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];
                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];
                }
            }
        }


        if ($toJsonPlaceholder) {
            $output[] = $fields;
        } else {
            $fields[\'_alt\'] = $idx % 2;
            $idx++;
            $fields[\'_first\'] = $idx == 1 ? true : \'\';
            $fields[\'_last\'] = $idx == $limit ? true : \'\';
            $fields[\'idx\'] = $idx;
            $rowtpl = \'\';
            //get changing tpls from field
            if (substr($tpl, 0, 7) == "@FIELD:") {
                $tplField = substr($tpl, 7);
                $rowtpl = $fields[$tplField];
            }

            if ($fields[\'_first\'] && !empty($tplFirst)) {
                $rowtpl = $tplFirst;
            }
            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {
                $rowtpl = $tplLast;
            }
            $tplidx = \'tpl_\' . $idx;
            if (empty($rowtpl) && !empty($$tplidx)) {
                $rowtpl = $$tplidx;
            }
            if ($idx > 1 && empty($rowtpl)) {
                $divisors = $migx->getDivisors($idx);
                if (!empty($divisors)) {
                    foreach ($divisors as $divisor) {
                        $tplnth = \'tpl_n\' . $divisor;
                        if (!empty($$tplnth)) {
                            $rowtpl = $$tplnth;
                            if (!empty($rowtpl)) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($count == 1 && isset($tpl_oneresult)) {
                $rowtpl = $tpl_oneresult;
            }

            $fields = array_merge($fields, $properties);

            if (!empty($rowtpl)) {
                $template = $migx->getTemplate($tpl, $template);
                $fields[\'_tpl\'] = $template[$tpl];
            } else {
                $rowtpl = $tpl;

            }
            $template = $migx->getTemplate($rowtpl, $template);


            if ($template[$rowtpl]) {
                $chunk = $modx->newObject(\'modChunk\');
                $chunk->setCacheable(false);
                $chunk->setContent($template[$rowtpl]);


                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {
                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);
                } else {
                    $output[] = $chunk->process($fields);
                }
            } else {
                if (!empty($placeholdersKeyField)) {
                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                } else {
                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                }
            }
        }


    }
}

if (count($summaries) > 0) {
    $modx->toPlaceholders($summaries);
}


if ($toJsonPlaceholder) {
    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));
    return \'\';
}

if (!empty($toSeparatePlaceholders)) {
    $modx->toPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}
/*
if (!empty($outerTpl))
$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));
else 
*/

if ($count > 0 && $splits > 0) {
    $size = ceil($count / $splits);
    $chunks = array_chunk($output, $size);
    $output = array();
    foreach ($chunks as $chunk) {
        $o = implode($outputSeparator, $chunk);
        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));
    }
    $outputSeparator = $splitSeparator;
}

if (is_array($output)) {
    $o = implode($outputSeparator, $output);
} else {
    $o = $output;
}

if (!empty($o) && !empty($wrapperTpl)) {
    $template = $migx->getTemplate($wrapperTpl);
    if ($template[$wrapperTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$wrapperTpl]);
        $properties[\'output\'] = $o;
        $o = $chunk->process($properties);
    }
}

if (empty($o) && !empty($emptyTpl)) {
    $template = $migx->getTemplate($emptyTpl);
    if ($template[$emptyTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$emptyTpl]);
        $o = $chunk->process($properties);
    }
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $o);
    return \'\';
}

return $o;',
          'locked' => false,
          'properties' => 
          array (
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * getImageList
 *
 * Copyright 2009-2014 by Bruno Perner <b.perner@gmx.de>
 *
 * getImageList is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * getImageList is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * getImageList; if not, write to the Free Software Foundation, Inc., 59 Temple Place,
 * Suite 330, Boston, MA 02111-1307 USA
 *
 * @package migx
 */
/**
 * getImageList
 *
 * display Items from outputvalue of TV with custom-TV-input-type MIGX or from other JSON-string for MODx Revolution 
 *
 * @version 1.4
 * @author Bruno Perner <b.perner@gmx.de>
 * @copyright Copyright &copy; 2009-2014
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU General Public License
 * version 2 or (at your option) any later version.
 * @package migx
 */

/*example: <ul>[[!getImageList? &tvname=`myTV`&tpl=`@CODE:<li>[[+idx]]<img src="[[+imageURL]]"/><p>[[+imageAlt]]</p></li>`]]</ul>*/
/* get default properties */


$tvname = $modx->getOption(\'tvname\', $scriptProperties, \'\');
$inherit_children_tvname = $modx->getOption(\'inherit_children_tvname\', $scriptProperties, \'\');
$tpl = $modx->getOption(\'tpl\', $scriptProperties, \'\');
$wrapperTpl = $modx->getOption(\'wrapperTpl\', $scriptProperties, \'\');
$emptyTpl = $modx->getOption(\'emptyTpl\', $scriptProperties, \'\'); 
$limit = $modx->getOption(\'limit\', $scriptProperties, \'0\');
$offset = $modx->getOption(\'offset\', $scriptProperties, 0);
$totalVar = $modx->getOption(\'totalVar\', $scriptProperties, \'total\');
$randomize = $modx->getOption(\'randomize\', $scriptProperties, false);
$preselectLimit = $modx->getOption(\'preselectLimit\', $scriptProperties, 0); // when random preselect important images
$where = $modx->getOption(\'where\', $scriptProperties, \'\');
$where = !empty($where) ? $modx->fromJSON($where) : array();
$sort = $modx->getOption(\'sort\', $scriptProperties, \'\');
$sort = !empty($sort) ? $modx->fromJSON($sort) : array();
$toSeparatePlaceholders = $modx->getOption(\'toSeparatePlaceholders\', $scriptProperties, false);
$toPlaceholder = $modx->getOption(\'toPlaceholder\', $scriptProperties, false);
$outputSeparator = $modx->getOption(\'outputSeparator\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$placeholdersKeyField = $modx->getOption(\'placeholdersKeyField\', $scriptProperties, \'MIGX_id\');
$toJsonPlaceholder = $modx->getOption(\'toJsonPlaceholder\', $scriptProperties, false);
$jsonVarKey = $modx->getOption(\'jsonVarKey\', $scriptProperties, \'migx_outputvalue\');
$outputvalue = $modx->getOption(\'value\', $scriptProperties, \'\');
$outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
$docidVarKey = $modx->getOption(\'docidVarKey\', $scriptProperties, \'migx_docid\');
$docid = $modx->getOption(\'docid\', $scriptProperties, (isset($modx->resource) ? $modx->resource->get(\'id\') : 1));
$docid = isset($_REQUEST[$docidVarKey]) ? $_REQUEST[$docidVarKey] : $docid;
$processTVs = $modx->getOption(\'processTVs\', $scriptProperties, \'1\');
$reverse = $modx->getOption(\'reverse\', $scriptProperties, \'0\');
$sumFields = $modx->getOption(\'sumFields\', $scriptProperties, \'\');
$sumPrefix = $modx->getOption(\'sumPrefix\', $scriptProperties, \'summary_\');
$addfields = $modx->getOption(\'addfields\', $scriptProperties, \'\');
$addfields = !empty($addfields) ? explode(\',\', $addfields) : null;
//split json into parts
$splits = $modx->fromJson($modx->getOption(\'splits\', $scriptProperties, 0));
$splitTpl = $modx->getOption(\'splitTpl\', $scriptProperties, \'\');
$splitSeparator = $modx->getOption(\'splitSeparator\', $scriptProperties, \'\');
$inheritFrom = $modx->getOption(\'inheritFrom\', $scriptProperties, \'\'); //commaseparated list of resource-ids or/and the keyword \'parents\' where to inherit from
$inheritFrom = !empty($inheritFrom) ? explode(\',\', $inheritFrom) : \'\';

$modx->setPlaceholder(\'docid\', $docid);

$base_path = $modx->getOption(\'base_path\', null, MODX_BASE_PATH);
$base_url = $modx->getOption(\'base_url\', null, MODX_BASE_URL);

$migx = $modx->getService(\'migx\', \'Migx\', $modx->getOption(\'migx.core_path\', null, $modx->getOption(\'core_path\') . \'components/migx/\') . \'model/migx/\', $scriptProperties);
if (!($migx instanceof Migx))
    return \'\';
$migx->working_context = isset($modx->resource) ? $modx->resource->get(\'context_key\') : \'web\';

if (!empty($tvname)) {
    if ($tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $tvname))) {

        /*
        *   get inputProperties
        */


        $properties = $tv->get(\'input_properties\');
        $properties = isset($properties[\'formtabs\']) ? $properties : $tv->getProperties();

        $migx->config[\'configs\'] = $modx->getOption(\'configs\', $properties, \'\');
        if (!empty($migx->config[\'configs\'])) {
            $migx->loadConfigs();
            // get tabs from file or migx-config-table
            $formtabs = $migx->getTabs();
        }
        if (empty($formtabs) && isset($properties[\'formtabs\'])) {
            //try to get formtabs and its fields from properties
            $formtabs = $modx->fromJSON($properties[\'formtabs\']);
        }

        if (!empty($properties[\'basePath\'])) {
            if ($properties[\'autoResourceFolders\'] == \'true\') {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'basePath\'] . $docid . \'/\';
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'] . $docid . \'/\';
            } else {
                $scriptProperties[\'base_path\'] = $base_path . $properties[\'base_path\'];
                $scriptProperties[\'base_url\'] = $base_url . $properties[\'basePath\'];
            }
        }
        if ($jsonVarKey == \'migx_outputvalue\' && !empty($properties[\'jsonvarkey\'])) {
            $jsonVarKey = $properties[\'jsonvarkey\'];
            $outputvalue = isset($_REQUEST[$jsonVarKey]) ? $_REQUEST[$jsonVarKey] : $outputvalue;
        }

        if (empty($outputvalue)) {
            $outputvalue = $tv->renderOutput($docid);
            if (empty($outputvalue) && !empty($inheritFrom)) {
                foreach ($inheritFrom as $from) {
                    if ($from == \'parents\') {
                        if (!empty($inherit_children_tvname)){
                            //try to get items from optional MIGX-TV for children
                            if ($inh_tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inherit_children_tvname))) {
                                $outputvalue = $inh_tv->processInheritBinding(\'\', $docid);    
                            }
                        }
                        $outputvalue = empty($outputvalue) ? $tv->processInheritBinding(\'\', $docid) : $outputvalue;
                    } else {
                        $outputvalue = $tv->renderOutput($from);
                    }
                    if (!empty($outputvalue)) {
                        break;
                    }
                }
            }
        }


        /*
        *   get inputTvs 
        */
        $inputTvs = array();
        if (is_array($formtabs)) {

            //multiple different Forms
            // Note: use same field-names and inputTVs in all forms
            $inputTvs = $migx->extractInputTvs($formtabs);
        }
        if ($migx->source = $tv->getSource($migx->working_context, false)) {
            $migx->source->initialize();
        }

    }


}

if (empty($outputvalue)) {
    $modx->setPlaceholder($totalVar, 0);
    return \'\';
}

//echo $outputvalue.\'<br/><br/>\';

$items = $modx->fromJSON($outputvalue);

// where filter
if (is_array($where) && count($where) > 0) {
    $items = $migx->filterItems($where, $items);
}
$modx->setPlaceholder($totalVar, count($items));

if (!empty($reverse)) {
    $items = array_reverse($items);
}

// sort items
if (is_array($sort) && count($sort) > 0) {
    $items = $migx->sortDbResult($items, $sort);
}

$summaries = array();
$output = \'\';
$items = $offset > 0 ? array_slice($items, $offset) : $items;
$count = count($items);

if ($count > 0) {
    $limit = $limit == 0 || $limit > $count ? $count : $limit;
    $preselectLimit = $preselectLimit > $count ? $count : $preselectLimit;
    //preselect important items
    $preitems = array();
    if ($randomize && $preselectLimit > 0) {
        for ($i = 0; $i < $preselectLimit; $i++) {
            $preitems[] = $items[$i];
            unset($items[$i]);
        }
        $limit = $limit - count($preitems);
    }

    //shuffle items
    if ($randomize) {
        shuffle($items);
    }

    //limit items
    $count = count($items);
    $tempitems = array();

    for ($i = 0; $i < $limit; $i++) {
        if ($i >= $count) {
            break;
        }
        $tempitems[] = $items[$i];
    }
    $items = $tempitems;

    //add preselected items and schuffle again
    if ($randomize && $preselectLimit > 0) {
        $items = array_merge($preitems, $items);
        shuffle($items);
    }

    $properties = array();
    foreach ($scriptProperties as $property => $value) {
        $properties[\'property.\' . $property] = $value;
    }

    $idx = 0;
    $output = array();
    $template = array();
    $count = count($items);

    foreach ($items as $key => $item) {
        $formname = isset($item[\'MIGX_formname\']) ? $item[\'MIGX_formname\'] . \'_\' : \'\';
        $fields = array();
        foreach ($item as $field => $value) {
            if (is_array($value)) {
                if (is_array($value[0])) {
                    //nested array - convert to json
                    $value = $modx->toJson($value);
                } else {
                    $value = implode(\'||\', $value); //handle arrays (checkboxes, multiselects)
                }
            }


            $inputTVkey = $formname . $field;

            if ($processTVs && isset($inputTvs[$inputTVkey])) {
                if (isset($inputTvs[$inputTVkey][\'inputTV\']) && $tv = $modx->getObject(\'modTemplateVar\', array(\'name\' => $inputTvs[$inputTVkey][\'inputTV\']))) {

                } else {
                    $tv = $modx->newObject(\'modTemplateVar\');
                    $tv->set(\'type\', $inputTvs[$inputTVkey][\'inputTVtype\']);
                }
                $inputTV = $inputTvs[$inputTVkey];

                $mTypes = $modx->getOption(\'manipulatable_url_tv_output_types\', null, \'image,file\');
                //don\'t manipulate any urls here
                $modx->setOption(\'manipulatable_url_tv_output_types\', \'\');
                $tv->set(\'default_text\', $value);
                $value = $tv->renderOutput($docid);
                //set option back
                $modx->setOption(\'manipulatable_url_tv_output_types\', $mTypes);
                //now manipulate urls
                if ($mediasource = $migx->getFieldSource($inputTV, $tv)) {
                    $mTypes = explode(\',\', $mTypes);
                    if (!empty($value) && in_array($tv->get(\'type\'), $mTypes)) {
                        //$value = $mediasource->prepareOutputUrl($value);
                        $value = str_replace(\'/./\', \'/\', $mediasource->prepareOutputUrl($value));
                    }
                }

            }
            $fields[$field] = $value;

        }

        if (!empty($addfields)) {
            foreach ($addfields as $addfield) {
                $addfield = explode(\':\', $addfield);
                $addname = $addfield[0];
                $adddefault = isset($addfield[1]) ? $addfield[1] : \'\';
                $fields[$addname] = $adddefault;
            }
        }

        if (!empty($sumFields)) {
            $sumFields = is_array($sumFields) ? $sumFields : explode(\',\', $sumFields);
            foreach ($sumFields as $sumField) {
                if (isset($fields[$sumField])) {
                    $summaries[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField] + $fields[$sumField];
                    $fields[$sumPrefix . $sumField] = $summaries[$sumPrefix . $sumField];
                }
            }
        }


        if ($toJsonPlaceholder) {
            $output[] = $fields;
        } else {
            $fields[\'_alt\'] = $idx % 2;
            $idx++;
            $fields[\'_first\'] = $idx == 1 ? true : \'\';
            $fields[\'_last\'] = $idx == $limit ? true : \'\';
            $fields[\'idx\'] = $idx;
            $rowtpl = \'\';
            //get changing tpls from field
            if (substr($tpl, 0, 7) == "@FIELD:") {
                $tplField = substr($tpl, 7);
                $rowtpl = $fields[$tplField];
            }

            if ($fields[\'_first\'] && !empty($tplFirst)) {
                $rowtpl = $tplFirst;
            }
            if ($fields[\'_last\'] && empty($rowtpl) && !empty($tplLast)) {
                $rowtpl = $tplLast;
            }
            $tplidx = \'tpl_\' . $idx;
            if (empty($rowtpl) && !empty($$tplidx)) {
                $rowtpl = $$tplidx;
            }
            if ($idx > 1 && empty($rowtpl)) {
                $divisors = $migx->getDivisors($idx);
                if (!empty($divisors)) {
                    foreach ($divisors as $divisor) {
                        $tplnth = \'tpl_n\' . $divisor;
                        if (!empty($$tplnth)) {
                            $rowtpl = $$tplnth;
                            if (!empty($rowtpl)) {
                                break;
                            }
                        }
                    }
                }
            }

            if ($count == 1 && isset($tpl_oneresult)) {
                $rowtpl = $tpl_oneresult;
            }

            $fields = array_merge($fields, $properties);

            if (!empty($rowtpl)) {
                $template = $migx->getTemplate($tpl, $template);
                $fields[\'_tpl\'] = $template[$tpl];
            } else {
                $rowtpl = $tpl;

            }
            $template = $migx->getTemplate($rowtpl, $template);


            if ($template[$rowtpl]) {
                $chunk = $modx->newObject(\'modChunk\');
                $chunk->setCacheable(false);
                $chunk->setContent($template[$rowtpl]);


                if (!empty($placeholdersKeyField) && isset($fields[$placeholdersKeyField])) {
                    $output[$fields[$placeholdersKeyField]] = $chunk->process($fields);
                } else {
                    $output[] = $chunk->process($fields);
                }
            } else {
                if (!empty($placeholdersKeyField)) {
                    $output[$fields[$placeholdersKeyField]] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                } else {
                    $output[] = \'<pre>\' . print_r($fields, 1) . \'</pre>\';
                }
            }
        }


    }
}

if (count($summaries) > 0) {
    $modx->toPlaceholders($summaries);
}


if ($toJsonPlaceholder) {
    $modx->setPlaceholder($toJsonPlaceholder, $modx->toJson($output));
    return \'\';
}

if (!empty($toSeparatePlaceholders)) {
    $modx->toPlaceholders($output, $toSeparatePlaceholders);
    return \'\';
}
/*
if (!empty($outerTpl))
$o = parseTpl($outerTpl, array(\'output\'=>implode($outputSeparator, $output)));
else 
*/

if ($count > 0 && $splits > 0) {
    $size = ceil($count / $splits);
    $chunks = array_chunk($output, $size);
    $output = array();
    foreach ($chunks as $chunk) {
        $o = implode($outputSeparator, $chunk);
        $output[] = $modx->getChunk($splitTpl, array(\'output\' => $o));
    }
    $outputSeparator = $splitSeparator;
}

if (is_array($output)) {
    $o = implode($outputSeparator, $output);
} else {
    $o = $output;
}

if (!empty($o) && !empty($wrapperTpl)) {
    $template = $migx->getTemplate($wrapperTpl);
    if ($template[$wrapperTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$wrapperTpl]);
        $properties[\'output\'] = $o;
        $o = $chunk->process($properties);
    }
}

if (empty($o) && !empty($emptyTpl)) {
    $template = $migx->getTemplate($emptyTpl);
    if ($template[$emptyTpl]) {
        $chunk = $modx->newObject(\'modChunk\');
        $chunk->setCacheable(false);
        $chunk->setContent($template[$emptyTpl]);
        $o = $chunk->process($properties);
    }
}

if (!empty($toPlaceholder)) {
    $modx->setPlaceholder($toPlaceholder, $o);
    return \'\';
}

return $o;',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
        ),
      ),
      'pdoPage' => 
      array (
        'fields' => 
        array (
          'id' => 37,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoPage',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */
// Default variables
if (empty($pageVarKey)) {
    $pageVarKey = \'page\';
}
if (empty($pageNavVar)) {
    $pageNavVar = \'page.nav\';
}
if (empty($pageCountVar)) {
    $pageCountVar = \'pageCount\';
}
if (empty($totalVar)) {
    $totalVar = \'total\';
}
if (empty($page)) {
    $page = 1;
}
if (empty($pageLimit)) {
    $pageLimit = 5;
} else {
    $pageLimit = (integer)$pageLimit;
}
if (!isset($plPrefix)) {
    $plPrefix = \'\';
}
if (!empty($scriptProperties[\'ajaxMode\'])) {
    $scriptProperties[\'ajax\'] = 1;
}

// Convert parameters from getPage if exists
if (!empty($namespace)) {
    $plPrefix = $namespace;
}
if (!empty($pageNavTpl)) {
    $scriptProperties[\'tplPage\'] = $pageNavTpl;
}
if (!empty($pageNavOuterTpl)) {
    $scriptProperties[\'tplPageWrapper\'] = $pageNavOuterTpl;
}
if (!empty($pageActiveTpl)) {
    $scriptProperties[\'tplPageActive\'] = $pageActiveTpl;
}
if (!empty($pageFirstTpl)) {
    $scriptProperties[\'tplPageFirst\'] = $pageFirstTpl;
}
if (!empty($pagePrevTpl)) {
    $scriptProperties[\'tplPagePrev\'] = $pagePrevTpl;
}
if (!empty($pageNextTpl)) {
    $scriptProperties[\'tplPageNext\'] = $pageNextTpl;
}
if (!empty($pageLastTpl)) {
    $scriptProperties[\'tplPageLast\'] = $pageLastTpl;
}
if (!empty($pageSkipTpl)) {
    $scriptProperties[\'tplPageSkip\'] = $pageSkipTpl;
}
if (!empty($pageNavScheme)) {
    $scriptProperties[\'scheme\'] = $pageNavScheme;
}
if (!empty($cache_expires)) {
    $scriptProperties[\'cacheTime\'] = $cache_expires;
}
//---
$strictMode = !empty($strictMode);

$isAjax = !empty($scriptProperties[\'ajax\']) && !empty($_SERVER[\'HTTP_X_REQUESTED_WITH\']) && $_SERVER[\'HTTP_X_REQUESTED_WITH\'] == \'XMLHttpRequest\';
if ($isAjax && !isset($_REQUEST[$pageVarKey])) {
    return;
}

/** @var pdoPage $pdoPage */
$fqn = $modx->getOption(\'pdoPage.class\', null, \'pdotools.pdopage\', true);
$path = $modx->getOption(\'pdopage_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoPage = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoPage->pdoTools->addTime(\'pdoTools loaded\');

// Script and styles
if (!$isAjax && !empty($scriptProperties[\'ajaxMode\'])) {
    $pdoPage->loadJsCss();
}
// Removing of default scripts and styles so they do not overwrote nested snippet parameters
if ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoPage\'))) {
    $properties = $snippet->get(\'properties\');
    if ($scriptProperties[\'frontend_js\'] == $properties[\'frontend_js\'][\'value\']) {
        unset($scriptProperties[\'frontend_js\']);
    }
    if ($scriptProperties[\'frontend_css\'] == $properties[\'frontend_css\'][\'value\']) {
        unset($scriptProperties[\'frontend_css\']);
    }
}

// Page
if (isset($_REQUEST[$pageVarKey]) && $strictMode && (!is_numeric($_REQUEST[$pageVarKey]) || ($_REQUEST[$pageVarKey] <= 1 && !$isAjax))) {
    return $pdoPage->redirectToFirst($isAjax);
} elseif (!empty($_REQUEST[$pageVarKey])) {
    $page = (integer)$_REQUEST[$pageVarKey];
}
$scriptProperties[\'page\'] = $page;
$scriptProperties[\'request\'] = $_REQUEST;

// Limit
if (isset($_REQUEST[\'limit\'])) {
    if (is_numeric($_REQUEST[\'limit\']) && abs($_REQUEST[\'limit\']) > 0) {
        $scriptProperties[\'limit\'] = abs($_REQUEST[\'limit\']);
    } elseif ($strictMode) {
        unset($_GET[\'limit\']);

        return $pdoPage->redirectToFirst($isAjax);
    }
}
if (!empty($maxLimit) && !empty($scriptProperties[\'limit\']) && $scriptProperties[\'limit\'] > $maxLimit) {
    $scriptProperties[\'limit\'] = $maxLimit;
}

// Offset
$offset = !empty($scriptProperties[\'offset\']) && $scriptProperties[\'offset\'] > 0
    ? (int)$scriptProperties[\'offset\']
    : 0;
$scriptProperties[\'offset\'] = $page > 1
    ? $scriptProperties[\'limit\'] * ($page - 1) + $offset
    : $offset;
if (!empty($scriptProperties[\'offset\']) && empty($scriptProperties[\'limit\'])) {
    $scriptProperties[\'limit\'] = 10000000;
}

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
$url = $pdoPage->getBaseUrl();
$output = $pagination = $total = $pageCount = \'\';

$data = $cache
    ? $pdoPage->pdoTools->getCache($scriptProperties)
    : array();

if (empty($data)) {
    $output = $pdoPage->pdoTools->runSnippet($scriptProperties[\'element\'], $scriptProperties);
    if ($output === false) {
        return \'\';
    } elseif (!empty($toPlaceholder)) {
        $output = $modx->getPlaceholder($toPlaceholder);
    }

    /** Pagination */
    $total = (int)$modx->getPlaceholder($totalVar);
    $pageCount = !empty($scriptProperties[\'limit\']) && $total > $offset
        ? ceil(($total - $offset) / $scriptProperties[\'limit\'])
        : 0;

    // Redirect to start if somebody specified incorrect page
    if ($page > 1 && $page > $pageCount && $strictMode) {
        return $pdoPage->redirectToFirst($isAjax);
    }
    if (!empty($pageCount) && $pageCount > 1) {
        $pagination = array(
            \'first\' => $page > 1 && !empty($tplPageFirst)
                ? $pdoPage->makePageLink($url, 1, $tplPageFirst)
                : \'\',
            \'prev\' => $page > 1 && !empty($tplPagePrev)
                ? $pdoPage->makePageLink($url, $page - 1, $tplPagePrev)
                : \'\',
            \'pages\' => $pageLimit >= 7 && empty($disableModernPagination)
                ? $pdoPage->buildModernPagination($page, $pageCount, $url)
                : $pdoPage->buildClassicPagination($page, $pageCount, $url),
            \'next\' => $page < $pageCount && !empty($tplPageNext)
                ? $pdoPage->makePageLink($url, $page + 1, $tplPageNext)
                : \'\',
            \'last\' => $page < $pageCount && !empty($tplPageLast)
                ? $pdoPage->makePageLink($url, $pageCount, $tplPageLast)
                : \'\',
        );

        if (!empty($pageCount)) {
            foreach (array(\'first\', \'prev\', \'next\', \'last\') as $v) {
                $tpl = \'tplPage\' . ucfirst($v) . \'Empty\';
                if (!empty(${$tpl}) && empty($pagination[$v])) {
                    $pagination[$v] = $pdoPage->pdoTools->getChunk(${$tpl});
                }
            }
        }

        if (!empty($setMeta) && !$isAjax) {
            $modx->regClientStartupHTMLBlock(\'<link rel="canonical" href="\' . $url . \'"/>\');
            if ($page > 1) {
                $modx->regClientStartupHTMLBlock(
                    \'<link rel="prev" href="\' . $pdoPage->makePageLink($url, $page - 1) . \'"/>\'
                );
            }
            if ($page < $pageCount) {
                $modx->regClientStartupHTMLBlock(
                    \'<link rel="next" href="\' . $pdoPage->makePageLink($url, $page + 1) . \'"/>\'
                );
            }
        }
    } else {
        $pagination = array(
            \'first\' => \'\',
            \'prev\' => \'\',
            \'pages\' => \'\',
            \'next\' => \'\',
            \'last\' => \'\'
        );
    }

    $data = array(
        \'output\' => $output,
        $pageVarKey => $page,
        $pageCountVar => $pageCount,
        $pageNavVar => !empty($tplPageWrapper)
            ? $pdoPage->pdoTools->getChunk($tplPageWrapper, $pagination)
            : $pdoPage->pdoTools->parseChunk(\'\', $pagination),
        $totalVar => $total,
    );
    if ($cache) {
        $pdoPage->pdoTools->setCache($data, $scriptProperties);
    }
}

if ($isAjax) {
    if ($pageNavVar != \'pagination\') {
        $data[\'pagination\'] = $data[$pageNavVar];
        unset($data[$pageNavVar]);
    }
    if ($pageCountVar != \'pages\') {
        $data[\'pages\'] = (int)$data[$pageCountVar];
        unset($data[$pageCountVar]);
    }
    if ($pageVarKey != \'page\') {
        $data[\'page\'] = (int)$data[$pageVarKey];
        unset($data[$pageVarKey]);
    }
    if ($totalVar != \'total\') {
        $data[\'total\'] = (int)$data[$totalVar];
        unset($data[$totalVar]);
    }

    $maxIterations = (integer)$modx->getOption(\'parser_max_iterations\', null, 10);
    $modx->getParser()->processElementTags(\'\', $data[\'output\'], false, false, \'[[\', \']]\', array(), $maxIterations);
    $modx->getParser()->processElementTags(\'\', $data[\'output\'], true, true, \'[[\', \']]\', array(), $maxIterations);

    @session_write_close();
    exit(json_encode($data));
} else {
    $modx->setPlaceholders($data, $plPrefix);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $data[\'output\']);
    } else {
        return $data[\'output\'];
    }
}',
          'locked' => false,
          'properties' => 
          array (
            'plPrefix' => 
            array (
              'name' => 'plPrefix',
              'desc' => 'pdotools_prop_plPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для выставляемых плейсхолдеров, по умолчанию "wf.".',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'maxLimit' => 
            array (
              'name' => 'maxLimit',
              'desc' => 'pdotools_prop_maxLimit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 100,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Максимально возможный лимит выборки. Перекрывает лимит, указанный пользователем через url.',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'page' => 
            array (
              'name' => 'page',
              'desc' => 'pdotools_prop_page',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер страницы для вывода. Перекрывается номером, указанным пользователем через url.',
              'area_trans' => '',
            ),
            'pageVarKey' => 
            array (
              'name' => 'pageVarKey',
              'desc' => 'pdotools_prop_pageVarKey',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'page',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя переменной для поиска номера страницы в url.',
              'area_trans' => '',
            ),
            'totalVar' => 
            array (
              'name' => 'totalVar',
              'desc' => 'pdotools_prop_totalVar',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'page.total',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя плейсхолдера для сохранения общего количества результатов.',
              'area_trans' => '',
            ),
            'pageLimit' => 
            array (
              'name' => 'pageLimit',
              'desc' => 'pdotools_prop_pageLimit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 5,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Количество ссылок на страницы. Если больше или равно 7 - включается продвинутый режим отображения.',
              'area_trans' => '',
            ),
            'element' => 
            array (
              'name' => 'element',
              'desc' => 'pdotools_prop_element',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'pdoResources',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя сниппета для запуска.',
              'area_trans' => '',
            ),
            'pageNavVar' => 
            array (
              'name' => 'pageNavVar',
              'desc' => 'pdotools_prop_pageNavVar',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'page.nav',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя плейсхолдера для вывода пагинации.',
              'area_trans' => '',
            ),
            'pageCountVar' => 
            array (
              'name' => 'pageCountVar',
              'desc' => 'pdotools_prop_pageCountVar',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'pageCount',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя плейсхолдера для вывода количества страниц.',
              'area_trans' => '',
            ),
            'pageLinkScheme' => 
            array (
              'name' => 'pageLinkScheme',
              'desc' => 'pdotools_prop_pageLinkScheme',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема генерации ссылки на страницу. Можно использовать плейсхолдеры [[+pageVarKey]] и [[+page]]',
              'area_trans' => '',
            ),
            'tplPage' => 
            array (
              'name' => 'tplPage',
              'desc' => 'pdotools_prop_tplPage',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li><a href="[[+href]]">[[+pageNo]]</a></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления обычной ссылки на страницу.',
              'area_trans' => '',
            ),
            'tplPageWrapper' => 
            array (
              'name' => 'tplPageWrapper',
              'desc' => 'pdotools_prop_tplPageWrapper',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <div class="pagination"><ul class="pagination">[[+first]][[+prev]][[+pages]][[+next]][[+last]]</ul></div>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления всего блока пагинации, содержит плейсхолдеры страниц.',
              'area_trans' => '',
            ),
            'tplPageActive' => 
            array (
              'name' => 'tplPageActive',
              'desc' => 'pdotools_prop_tplPageActive',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="active"><a href="[[+href]]">[[+pageNo]]</a></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления ссылки на текущую страницу.',
              'area_trans' => '',
            ),
            'tplPageFirst' => 
            array (
              'name' => 'tplPageFirst',
              'desc' => 'pdotools_prop_tplPageFirst',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="control"><a href="[[+href]]">[[%pdopage_first]]</a></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления ссылки на первую страницу.',
              'area_trans' => '',
            ),
            'tplPageLast' => 
            array (
              'name' => 'tplPageLast',
              'desc' => 'pdotools_prop_tplPageLast',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="control"><a href="[[+href]]">[[%pdopage_last]]</a></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления ссылки на последнюю страницу.',
              'area_trans' => '',
            ),
            'tplPagePrev' => 
            array (
              'name' => 'tplPagePrev',
              'desc' => 'pdotools_prop_tplPagePrev',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="control"><a href="[[+href]]">&laquo;</a></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления ссылки на предыдущую страницу.',
              'area_trans' => '',
            ),
            'tplPageNext' => 
            array (
              'name' => 'tplPageNext',
              'desc' => 'pdotools_prop_tplPageNext',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="control"><a href="[[+href]]">&raquo;</a></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления ссылки на следующую страницу.',
              'area_trans' => '',
            ),
            'tplPageSkip' => 
            array (
              'name' => 'tplPageSkip',
              'desc' => 'pdotools_prop_tplPageSkip',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="disabled"><span>...</span></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк оформления пропущенных страниц при продвинутом режиме отображения (&pageLimit >= 7).',
              'area_trans' => '',
            ),
            'tplPageFirstEmpty' => 
            array (
              'name' => 'tplPageFirstEmpty',
              'desc' => 'pdotools_prop_tplPageFirstEmpty',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="disabled"><span>[[%pdopage_first]]</span></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк, выводящийся при отсутствии ссылки на первую страницу.',
              'area_trans' => '',
            ),
            'tplPageLastEmpty' => 
            array (
              'name' => 'tplPageLastEmpty',
              'desc' => 'pdotools_prop_tplPageLastEmpty',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="disabled"><span>[[%pdopage_last]]</span></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк, выводящийся при отсутствии ссылки на последнюю страницу.',
              'area_trans' => '',
            ),
            'tplPagePrevEmpty' => 
            array (
              'name' => 'tplPagePrevEmpty',
              'desc' => 'pdotools_prop_tplPagePrevEmpty',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="disabled"><span>&laquo;</span></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк, выводящийся при отсутствии ссылки на предыдущую страницу.',
              'area_trans' => '',
            ),
            'tplPageNextEmpty' => 
            array (
              'name' => 'tplPageNextEmpty',
              'desc' => 'pdotools_prop_tplPageNextEmpty',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <li class="disabled"><span>&raquo;</span></li>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк, выводящийся при отсутствии ссылки на следующую страницу.',
              'area_trans' => '',
            ),
            'cache' => 
            array (
              'name' => 'cache',
              'desc' => 'pdotools_prop_cache',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Кэширование результатов работы сниппета.',
              'area_trans' => '',
            ),
            'cacheTime' => 
            array (
              'name' => 'cacheTime',
              'desc' => 'pdotools_prop_cacheTime',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 3600,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Время актуальности кэша в секундах.',
              'area_trans' => '',
            ),
            'cacheAnonymous' => 
            array (
              'name' => 'cacheAnonymous',
              'desc' => 'pdotools_prop_cacheAnonymous',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить кэширование только для неавторизованных посетителей.',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'ajax' => 
            array (
              'name' => 'ajax',
              'desc' => 'pdotools_prop_ajax',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включить поддержку ajax запросов.',
              'area_trans' => '',
            ),
            'ajaxMode' => 
            array (
              'name' => 'ajaxMode',
              'desc' => 'pdotools_prop_ajaxMode',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'None',
                  'value' => '',
                  'name' => 'None',
                ),
                1 => 
                array (
                  'text' => 'Default',
                  'value' => 'default',
                  'name' => 'Default',
                ),
                2 => 
                array (
                  'text' => 'Scroll',
                  'value' => 'scroll',
                  'name' => 'Scroll',
                ),
                3 => 
                array (
                  'text' => 'Button',
                  'value' => 'button',
                  'name' => 'Button',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ajax пагинация "из коробки". Доступны 3 режима: "default", "button" и "scroll".',
              'area_trans' => '',
            ),
            'ajaxElemWrapper' => 
            array (
              'name' => 'ajaxElemWrapper',
              'desc' => 'pdotools_prop_ajaxElemWrapper',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '#pdopage',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'jQuery селектор элемента-обёртки с результатами и пагинацией.',
              'area_trans' => '',
            ),
            'ajaxElemRows' => 
            array (
              'name' => 'ajaxElemRows',
              'desc' => 'pdotools_prop_ajaxElemRows',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '#pdopage .rows',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'jQuery селектор элемента с результатами.',
              'area_trans' => '',
            ),
            'ajaxElemPagination' => 
            array (
              'name' => 'ajaxElemPagination',
              'desc' => 'pdotools_prop_ajaxElemPagination',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '#pdopage .pagination',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'jQuery селектор элемента с пагинацией.',
              'area_trans' => '',
            ),
            'ajaxElemLink' => 
            array (
              'name' => 'ajaxElemLink',
              'desc' => 'pdotools_prop_ajaxElemLink',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '#pdopage .pagination a',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'jQuery селектор ссылки на страницу.',
              'area_trans' => '',
            ),
            'ajaxElemMore' => 
            array (
              'name' => 'ajaxElemMore',
              'desc' => 'pdotools_prop_ajaxElemMore',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '#pdopage .btn-more',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'jQuery селектор кнопки загрузки результатов при ajaxMode = button.',
              'area_trans' => '',
            ),
            'ajaxTplMore' => 
            array (
              'name' => 'ajaxTplMore',
              'desc' => 'pdotools_prop_ajaxTplMore',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '@INLINE <button class="btn btn-default btn-more">[[%pdopage_more]]</button>',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Шаблон кнопки для загрузки новых результатов при ajaxMode = button. Должен включать селектор, указанный в "ajaxElemMore".',
              'area_trans' => '',
            ),
            'ajaxHistory' => 
            array (
              'name' => 'ajaxHistory',
              'desc' => 'pdotools_prop_ajaxHistory',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'Auto',
                  'value' => '',
                  'name' => 'Auto',
                ),
                1 => 
                array (
                  'text' => 'Enabled',
                  'value' => 1,
                  'name' => 'Enabled',
                ),
                2 => 
                array (
                  'text' => 'Disabled',
                  'value' => 0,
                  'name' => 'Disabled',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Сохранять номер страницы в url при работе в режиме ajax.',
              'area_trans' => '',
            ),
            'frontend_js' => 
            array (
              'name' => 'frontend_js',
              'desc' => 'pdotools_prop_frontend_js',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '[[+assetsUrl]]js/pdopage.min.js',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ссылка на javascript для подключения сниппетом.',
              'area_trans' => '',
            ),
            'frontend_css' => 
            array (
              'name' => 'frontend_css',
              'desc' => 'pdotools_prop_frontend_css',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '[[+assetsUrl]]css/pdopage.min.css',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ссылка на css стили оформления для подключения сниппетом.',
              'area_trans' => '',
            ),
            'setMeta' => 
            array (
              'name' => 'setMeta',
              'desc' => 'pdotools_prop_setMeta',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Регистрация мета-тегов со ссылками на предыдущую и следующую страницу.',
              'area_trans' => '',
            ),
            'strictMode' => 
            array (
              'name' => 'strictMode',
              'desc' => 'pdotools_prop_strictMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Строгий режим работы. pdoPage делает редиректы при загрузке несуществующих страниц.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdopage.php',
          'content' => '/** @var array $scriptProperties */
// Default variables
if (empty($pageVarKey)) {
    $pageVarKey = \'page\';
}
if (empty($pageNavVar)) {
    $pageNavVar = \'page.nav\';
}
if (empty($pageCountVar)) {
    $pageCountVar = \'pageCount\';
}
if (empty($totalVar)) {
    $totalVar = \'total\';
}
if (empty($page)) {
    $page = 1;
}
if (empty($pageLimit)) {
    $pageLimit = 5;
} else {
    $pageLimit = (integer)$pageLimit;
}
if (!isset($plPrefix)) {
    $plPrefix = \'\';
}
if (!empty($scriptProperties[\'ajaxMode\'])) {
    $scriptProperties[\'ajax\'] = 1;
}

// Convert parameters from getPage if exists
if (!empty($namespace)) {
    $plPrefix = $namespace;
}
if (!empty($pageNavTpl)) {
    $scriptProperties[\'tplPage\'] = $pageNavTpl;
}
if (!empty($pageNavOuterTpl)) {
    $scriptProperties[\'tplPageWrapper\'] = $pageNavOuterTpl;
}
if (!empty($pageActiveTpl)) {
    $scriptProperties[\'tplPageActive\'] = $pageActiveTpl;
}
if (!empty($pageFirstTpl)) {
    $scriptProperties[\'tplPageFirst\'] = $pageFirstTpl;
}
if (!empty($pagePrevTpl)) {
    $scriptProperties[\'tplPagePrev\'] = $pagePrevTpl;
}
if (!empty($pageNextTpl)) {
    $scriptProperties[\'tplPageNext\'] = $pageNextTpl;
}
if (!empty($pageLastTpl)) {
    $scriptProperties[\'tplPageLast\'] = $pageLastTpl;
}
if (!empty($pageSkipTpl)) {
    $scriptProperties[\'tplPageSkip\'] = $pageSkipTpl;
}
if (!empty($pageNavScheme)) {
    $scriptProperties[\'scheme\'] = $pageNavScheme;
}
if (!empty($cache_expires)) {
    $scriptProperties[\'cacheTime\'] = $cache_expires;
}
//---
$strictMode = !empty($strictMode);

$isAjax = !empty($scriptProperties[\'ajax\']) && !empty($_SERVER[\'HTTP_X_REQUESTED_WITH\']) && $_SERVER[\'HTTP_X_REQUESTED_WITH\'] == \'XMLHttpRequest\';
if ($isAjax && !isset($_REQUEST[$pageVarKey])) {
    return;
}

/** @var pdoPage $pdoPage */
$fqn = $modx->getOption(\'pdoPage.class\', null, \'pdotools.pdopage\', true);
$path = $modx->getOption(\'pdopage_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoPage = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoPage->pdoTools->addTime(\'pdoTools loaded\');

// Script and styles
if (!$isAjax && !empty($scriptProperties[\'ajaxMode\'])) {
    $pdoPage->loadJsCss();
}
// Removing of default scripts and styles so they do not overwrote nested snippet parameters
if ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoPage\'))) {
    $properties = $snippet->get(\'properties\');
    if ($scriptProperties[\'frontend_js\'] == $properties[\'frontend_js\'][\'value\']) {
        unset($scriptProperties[\'frontend_js\']);
    }
    if ($scriptProperties[\'frontend_css\'] == $properties[\'frontend_css\'][\'value\']) {
        unset($scriptProperties[\'frontend_css\']);
    }
}

// Page
if (isset($_REQUEST[$pageVarKey]) && $strictMode && (!is_numeric($_REQUEST[$pageVarKey]) || ($_REQUEST[$pageVarKey] <= 1 && !$isAjax))) {
    return $pdoPage->redirectToFirst($isAjax);
} elseif (!empty($_REQUEST[$pageVarKey])) {
    $page = (integer)$_REQUEST[$pageVarKey];
}
$scriptProperties[\'page\'] = $page;
$scriptProperties[\'request\'] = $_REQUEST;

// Limit
if (isset($_REQUEST[\'limit\'])) {
    if (is_numeric($_REQUEST[\'limit\']) && abs($_REQUEST[\'limit\']) > 0) {
        $scriptProperties[\'limit\'] = abs($_REQUEST[\'limit\']);
    } elseif ($strictMode) {
        unset($_GET[\'limit\']);

        return $pdoPage->redirectToFirst($isAjax);
    }
}
if (!empty($maxLimit) && !empty($scriptProperties[\'limit\']) && $scriptProperties[\'limit\'] > $maxLimit) {
    $scriptProperties[\'limit\'] = $maxLimit;
}

// Offset
$offset = !empty($scriptProperties[\'offset\']) && $scriptProperties[\'offset\'] > 0
    ? (int)$scriptProperties[\'offset\']
    : 0;
$scriptProperties[\'offset\'] = $page > 1
    ? $scriptProperties[\'limit\'] * ($page - 1) + $offset
    : $offset;
if (!empty($scriptProperties[\'offset\']) && empty($scriptProperties[\'limit\'])) {
    $scriptProperties[\'limit\'] = 10000000;
}

$cache = !empty($cache) || (!$modx->user->id && !empty($cacheAnonymous));
$url = $pdoPage->getBaseUrl();
$output = $pagination = $total = $pageCount = \'\';

$data = $cache
    ? $pdoPage->pdoTools->getCache($scriptProperties)
    : array();

if (empty($data)) {
    $output = $pdoPage->pdoTools->runSnippet($scriptProperties[\'element\'], $scriptProperties);
    if ($output === false) {
        return \'\';
    } elseif (!empty($toPlaceholder)) {
        $output = $modx->getPlaceholder($toPlaceholder);
    }

    /** Pagination */
    $total = (int)$modx->getPlaceholder($totalVar);
    $pageCount = !empty($scriptProperties[\'limit\']) && $total > $offset
        ? ceil(($total - $offset) / $scriptProperties[\'limit\'])
        : 0;

    // Redirect to start if somebody specified incorrect page
    if ($page > 1 && $page > $pageCount && $strictMode) {
        return $pdoPage->redirectToFirst($isAjax);
    }
    if (!empty($pageCount) && $pageCount > 1) {
        $pagination = array(
            \'first\' => $page > 1 && !empty($tplPageFirst)
                ? $pdoPage->makePageLink($url, 1, $tplPageFirst)
                : \'\',
            \'prev\' => $page > 1 && !empty($tplPagePrev)
                ? $pdoPage->makePageLink($url, $page - 1, $tplPagePrev)
                : \'\',
            \'pages\' => $pageLimit >= 7 && empty($disableModernPagination)
                ? $pdoPage->buildModernPagination($page, $pageCount, $url)
                : $pdoPage->buildClassicPagination($page, $pageCount, $url),
            \'next\' => $page < $pageCount && !empty($tplPageNext)
                ? $pdoPage->makePageLink($url, $page + 1, $tplPageNext)
                : \'\',
            \'last\' => $page < $pageCount && !empty($tplPageLast)
                ? $pdoPage->makePageLink($url, $pageCount, $tplPageLast)
                : \'\',
        );

        if (!empty($pageCount)) {
            foreach (array(\'first\', \'prev\', \'next\', \'last\') as $v) {
                $tpl = \'tplPage\' . ucfirst($v) . \'Empty\';
                if (!empty(${$tpl}) && empty($pagination[$v])) {
                    $pagination[$v] = $pdoPage->pdoTools->getChunk(${$tpl});
                }
            }
        }

        if (!empty($setMeta) && !$isAjax) {
            $modx->regClientStartupHTMLBlock(\'<link rel="canonical" href="\' . $url . \'"/>\');
            if ($page > 1) {
                $modx->regClientStartupHTMLBlock(
                    \'<link rel="prev" href="\' . $pdoPage->makePageLink($url, $page - 1) . \'"/>\'
                );
            }
            if ($page < $pageCount) {
                $modx->regClientStartupHTMLBlock(
                    \'<link rel="next" href="\' . $pdoPage->makePageLink($url, $page + 1) . \'"/>\'
                );
            }
        }
    } else {
        $pagination = array(
            \'first\' => \'\',
            \'prev\' => \'\',
            \'pages\' => \'\',
            \'next\' => \'\',
            \'last\' => \'\'
        );
    }

    $data = array(
        \'output\' => $output,
        $pageVarKey => $page,
        $pageCountVar => $pageCount,
        $pageNavVar => !empty($tplPageWrapper)
            ? $pdoPage->pdoTools->getChunk($tplPageWrapper, $pagination)
            : $pdoPage->pdoTools->parseChunk(\'\', $pagination),
        $totalVar => $total,
    );
    if ($cache) {
        $pdoPage->pdoTools->setCache($data, $scriptProperties);
    }
}

if ($isAjax) {
    if ($pageNavVar != \'pagination\') {
        $data[\'pagination\'] = $data[$pageNavVar];
        unset($data[$pageNavVar]);
    }
    if ($pageCountVar != \'pages\') {
        $data[\'pages\'] = (int)$data[$pageCountVar];
        unset($data[$pageCountVar]);
    }
    if ($pageVarKey != \'page\') {
        $data[\'page\'] = (int)$data[$pageVarKey];
        unset($data[$pageVarKey]);
    }
    if ($totalVar != \'total\') {
        $data[\'total\'] = (int)$data[$totalVar];
        unset($data[$totalVar]);
    }

    $maxIterations = (integer)$modx->getOption(\'parser_max_iterations\', null, 10);
    $modx->getParser()->processElementTags(\'\', $data[\'output\'], false, false, \'[[\', \']]\', array(), $maxIterations);
    $modx->getParser()->processElementTags(\'\', $data[\'output\'], true, true, \'[[\', \']]\', array(), $maxIterations);

    @session_write_close();
    exit(json_encode($data));
} else {
    $modx->setPlaceholders($data, $plPrefix);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $data[\'output\']);
    } else {
        return $data[\'output\'];
    }
}',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'quasiDate' => 
      array (
        'fields' => 
        array (
          'id' => 53,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'quasiDate',
          'description' => '',
          'editor_type' => 0,
          'category' => 0,
          'cache_type' => 0,
          'snippet' => '/**
 * https://quasi-art.ru/
 * 2015-2017
 */
// Сначала язык берётся из системных настроек
$cultureKey = $modx->getOption(\'cultureKey\');
// Переопределение из параметров сниппета
$cultureKey = $modx->getOption(\'cultureKey\', $scriptProperties, $cultureKey);
$input = (int)$input;

// Language data
switch ($cultureKey) {
	case \'en\':
		$monthes = \'january, february, march, april, may, june, july, august, september, october, november, december\';
		$weekdays = \'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday\';
		break;
	case \'ua\':
		$monthes = \'січня, лютого, березня, квітня, травня, червня, липня, серпня, вересня, жовтня, листопада, грудня\';
		$weekdays = \'Понеділок, Вівторок, Середа, Четвер, П\\\'ятниця, Субота, Неділя\';
		break;
	case \'ru\':
	default:
		$monthes = \'января, февраля, марта, апреля, мая, июня, июля, августа, сентября, октября, ноября, декабря\';
		$weekdays = \'Понедельник, Вторник, Среда, Четверг, Пятница, Суббота, Воскресенье\';
		break;
}

$monthes = explode(\',\', $monthes);
array_unshift($monthes, \'\');
$weekdays = explode(\',\', $weekdays);

$monthName = trim($modx->getOption(date(\'n\', $input), $monthes, \'\'));
$weekdayName = trim($modx->getOption((date(\'w\', $input)+6)%7, $weekdays, \'\'));

// Будущий результат
$output = $options;

// Название месяца
$output = str_replace(\'%month\', $monthName, $output);
$output = str_replace(\'%weekday\', $weekdayName,	$output);
// Замена стандартных значений
$chars = \'HisdjmwY\';
$charsLength = strlen($chars);
for ($i = 0; $i < $charsLength; $i++) {
	$output = str_replace(\'%\'.$chars[$i], date($chars[$i], $input), $output);
}

return $output;',
          'locked' => false,
          'properties' => 
          array (
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => '',
          'content' => '/**
 * https://quasi-art.ru/
 * 2015-2017
 */
// Сначала язык берётся из системных настроек
$cultureKey = $modx->getOption(\'cultureKey\');
// Переопределение из параметров сниппета
$cultureKey = $modx->getOption(\'cultureKey\', $scriptProperties, $cultureKey);
$input = (int)$input;

// Language data
switch ($cultureKey) {
	case \'en\':
		$monthes = \'january, february, march, april, may, june, july, august, september, october, november, december\';
		$weekdays = \'Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday\';
		break;
	case \'ua\':
		$monthes = \'січня, лютого, березня, квітня, травня, червня, липня, серпня, вересня, жовтня, листопада, грудня\';
		$weekdays = \'Понеділок, Вівторок, Середа, Четвер, П\\\'ятниця, Субота, Неділя\';
		break;
	case \'ru\':
	default:
		$monthes = \'января, февраля, марта, апреля, мая, июня, июля, августа, сентября, октября, ноября, декабря\';
		$weekdays = \'Понедельник, Вторник, Среда, Четверг, Пятница, Суббота, Воскресенье\';
		break;
}

$monthes = explode(\',\', $monthes);
array_unshift($monthes, \'\');
$weekdays = explode(\',\', $weekdays);

$monthName = trim($modx->getOption(date(\'n\', $input), $monthes, \'\'));
$weekdayName = trim($modx->getOption((date(\'w\', $input)+6)%7, $weekdays, \'\'));

// Будущий результат
$output = $options;

// Название месяца
$output = str_replace(\'%month\', $monthName, $output);
$output = str_replace(\'%weekday\', $weekdayName,	$output);
// Замена стандартных значений
$chars = \'HisdjmwY\';
$charsLength = strlen($chars);
for ($i = 0; $i < $charsLength; $i++) {
	$output = str_replace(\'%\'.$chars[$i], date($chars[$i], $input), $output);
}

return $output;',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'pdoResources' => 
      array (
        'fields' => 
        array (
          'id' => 31,
          'source' => 1,
          'property_preprocess' => false,
          'name' => 'pdoResources',
          'description' => '',
          'editor_type' => 0,
          'category' => 10,
          'cache_type' => 0,
          'snippet' => '/** @var array $scriptProperties */
if (isset($parents) && $parents === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
}
if (!empty($returnIds)) {
    $scriptProperties[\'return\'] = \'ids\';
}

// Adding extra parameters into special place so we can put them in a results
/** @var modSnippet $snippet */
$additionalPlaceholders = $properties = array();
if (isset($this) && $this instanceof modSnippet) {
    $properties = $this->get(\'properties\');
}
elseif ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoResources\'))) {
    $properties = $snippet->get(\'properties\');
}
if (!empty($properties)) {
    foreach ($scriptProperties as $k => $v) {
        if (!isset($properties[$k])) {
            $additionalPlaceholders[$k] = $v;
        }
    }
}
$scriptProperties[\'additionalPlaceholders\'] = $additionalPlaceholders;

/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption(\'pdoFetch.class\', null, \'pdotools.pdofetch\', true);
$path = $modx->getOption(\'pdofetch_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoFetch = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoFetch->addTime(\'pdoTools loaded\');
$output = $pdoFetch->run();

$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="pdoResourcesLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

// Return output
if (!empty($returnIds)) {
    $modx->setPlaceholder(\'pdoResources.log\', $log);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
} elseif (!empty($toSeparatePlaceholders)) {
    $output[\'log\'] = $log;
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    $output .= $log;

    if (!empty($tplWrapper) && (!empty($wrapIfEmpty) || !empty($output))) {
        $output = $pdoFetch->getChunk($tplWrapper, array_merge($additionalPlaceholders, array(\'output\' => $output)),
            $pdoFetch->config[\'fastMode\']);
    }

    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
}',
          'locked' => false,
          'properties' => 
          array (
            'tpl' => 
            array (
              'name' => 'tpl',
              'desc' => 'pdotools_prop_tpl',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для оформления ресурса. Если не указан, то содержимое полей ресурса будет распечатано на экран.',
              'area_trans' => '',
            ),
            'returnIds' => 
            array (
              'name' => 'returnIds',
              'desc' => 'pdotools_prop_returnIds',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Возвращать строку со списком id ресурсов, вместо оформленных результатов.',
              'area_trans' => '',
            ),
            'showLog' => 
            array (
              'name' => 'showLog',
              'desc' => 'pdotools_prop_showLog',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать дополнительную информацию о работе сниппета. Только для авторизованных в контекте "mgr".',
              'area_trans' => '',
            ),
            'fastMode' => 
            array (
              'name' => 'fastMode',
              'desc' => 'pdotools_prop_fastMode',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Быстрый режим обработки чанков. Все необработанные теги (условия, сниппеты и т.п.) будут вырезаны.',
              'area_trans' => '',
            ),
            'sortby' => 
            array (
              'name' => 'sortby',
              'desc' => 'pdotools_prop_sortby',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'publishedon',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Любое поле ресурса для сортировки, включая ТВ параметр, если он указан в параметре "includeTVs". Можно указывать JSON строку с массивом нескольких полей. Для случайно сортировки укажите "RAND()"',
              'area_trans' => '',
            ),
            'sortbyTV' => 
            array (
              'name' => 'sortbyTV',
              'desc' => 'pdotools_prop_sortbyTV',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Сортировка по ТВ параметру. Если он не указан в &includeTVs, то будет подключен автоматически.',
              'area_trans' => '',
            ),
            'sortbyTVType' => 
            array (
              'name' => 'sortbyTVType',
              'desc' => 'pdotools_prop_sortbyTVType',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Тип сортировки по ТВ параметру. Возможные варианты: string, integer, decimal и datetime. Если пусто, то ТВ будет отсортирован в зависимости от его типа: как текст, число или дата.',
              'area_trans' => '',
            ),
            'sortdir' => 
            array (
              'name' => 'sortdir',
              'desc' => 'pdotools_prop_sortdir',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'DESC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки: по убыванию или возрастанию.',
              'area_trans' => '',
            ),
            'sortdirTV' => 
            array (
              'name' => 'sortdirTV',
              'desc' => 'pdotools_prop_sortdirTV',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'ASC',
                  'value' => 'ASC',
                  'name' => 'ASC',
                ),
                1 => 
                array (
                  'text' => 'DESC',
                  'value' => 'DESC',
                  'name' => 'DESC',
                ),
              ),
              'value' => 'ASC',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Направление сортировки ТВ: по убыванию или возрастанию. Если не указан, то будет равен параметру &sortdir.',
              'area_trans' => '',
            ),
            'limit' => 
            array (
              'name' => 'limit',
              'desc' => 'pdotools_prop_limit',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение количества результатов выборки. Можно использовать "0".',
              'area_trans' => '',
            ),
            'offset' => 
            array (
              'name' => 'offset',
              'desc' => 'pdotools_prop_offset',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 0,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Пропуск результатов от начала.',
              'area_trans' => '',
            ),
            'depth' => 
            array (
              'name' => 'depth',
              'desc' => 'pdotools_prop_depth',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => 10,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Глубина поиска дочерних ресурсов от родителя.',
              'area_trans' => '',
            ),
            'outputSeparator' => 
            array (
              'name' => 'outputSeparator',
              'desc' => 'pdotools_prop_outputSeparator',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '
',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Необязательная строка для разделения результатов работы.',
              'area_trans' => '',
            ),
            'toPlaceholder' => 
            array (
              'name' => 'toPlaceholder',
              'desc' => 'pdotools_prop_toPlaceholder',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если не пусто, сниппет сохранит все данные в плейсхолдер с этим именем, вместо вывода не экран.',
              'area_trans' => '',
            ),
            'parents' => 
            array (
              'name' => 'parents',
              'desc' => 'pdotools_prop_parents',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список родителей, через запятую, для поиска результатов. По умолчанию выборка ограничена текущим родителем. Если поставить 0 - выборка не ограничивается. Если id родителя начинается с дефиса, он и его потомки исключается из выборки.',
              'area_trans' => '',
            ),
            'includeContent' => 
            array (
              'name' => 'includeContent',
              'desc' => 'pdotools_prop_includeContent',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включаем поле "content" в выборку.',
              'area_trans' => '',
            ),
            'includeTVs' => 
            array (
              'name' => 'includeTVs',
              'desc' => 'pdotools_prop_includeTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров для выборки, через запятую. Например: "action,time" дадут плейсхолдеры [[+action]] и [[+time]].',
              'area_trans' => '',
            ),
            'prepareTVs' => 
            array (
              'name' => 'prepareTVs',
              'desc' => 'pdotools_prop_prepareTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '1',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров, которые нужно подготовить перед выводом. По умолчанию, установлено в "1", что означает подготовку всех ТВ, указанных в "&includeTVs=``"',
              'area_trans' => '',
            ),
            'processTVs' => 
            array (
              'name' => 'processTVs',
              'desc' => 'pdotools_prop_processTVs',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ТВ параметров, которые нужно обработать перед выводом. Если установить в "1" - будут обработаны все ТВ, указанные в "&includeTVs=``". По умолчанию параметр пуст.',
              'area_trans' => '',
            ),
            'tvPrefix' => 
            array (
              'name' => 'tvPrefix',
              'desc' => 'pdotools_prop_tvPrefix',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'tv.',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Префикс для ТВ параметров.',
              'area_trans' => '',
            ),
            'tvFilters' => 
            array (
              'name' => 'tvFilters',
              'desc' => 'pdotools_prop_tvFilters',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список фильтров по ТВ, с разделителями AND и OR. Разделитель, указанный в параметре "&tvFiltersOrDelimiter" представляет логическое условие OR и по нему условия группируются в первую очередь.  Внутри каждой группы вы можете задать список значений, разделив их "&tvFiltersAndDelimiter". Поиск значений может проводиться в каком-то конкретном ТВ, если он указан ("myTV==value"), или в любом ("value"). Пример вызова: "&tvFilters=`filter2==one,filter1==bar%||filter1==foo`". <br />Обратите внимание: фильтрация использует оператор LIKE и знак "%" является метасимволом. <br />И еще: Поиск идёт по значениям, которые физически находятся в БД, то есть, сюда не подставляются значения по умолчанию из настроек ТВ.',
              'area_trans' => '',
            ),
            'tvFiltersAndDelimiter' => 
            array (
              'name' => 'tvFiltersAndDelimiter',
              'desc' => 'pdotools_prop_tvFiltersAndDelimiter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => ',',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Разделитель для условий AND в параметре "&tvFilters". По умолчанию: ",".',
              'area_trans' => '',
            ),
            'tvFiltersOrDelimiter' => 
            array (
              'name' => 'tvFiltersOrDelimiter',
              'desc' => 'pdotools_prop_tvFiltersOrDelimiter',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '||',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Разделитель для условий OR в параметре "&tvFilters". По умолчанию: "||".',
              'area_trans' => '',
            ),
            'where' => 
            array (
              'name' => 'where',
              'desc' => 'pdotools_prop_where',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Массив дополнительных параметров выборки, закодированный в JSON.',
              'area_trans' => '',
            ),
            'showUnpublished' => 
            array (
              'name' => 'showUnpublished',
              'desc' => 'pdotools_prop_showUnpublished',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать неопубликованные ресурсы.',
              'area_trans' => '',
            ),
            'showDeleted' => 
            array (
              'name' => 'showDeleted',
              'desc' => 'pdotools_prop_showDeleted',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать удалённые ресурсы.',
              'area_trans' => '',
            ),
            'showHidden' => 
            array (
              'name' => 'showHidden',
              'desc' => 'pdotools_prop_showHidden',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => true,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Показывать ресурсы, скрытые в меню.',
              'area_trans' => '',
            ),
            'hideContainers' => 
            array (
              'name' => 'hideContainers',
              'desc' => 'pdotools_prop_hideContainers',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Отключает вывод контейнеров, то есть, ресурсов с isfolder = 1.',
              'area_trans' => '',
            ),
            'context' => 
            array (
              'name' => 'context',
              'desc' => 'pdotools_prop_context',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Ограничение выборки по контексту ресурсов.',
              'area_trans' => '',
            ),
            'idx' => 
            array (
              'name' => 'idx',
              'desc' => 'pdotools_prop_idx',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Вы можете указать стартовый номер итерации вывода результатов.',
              'area_trans' => '',
            ),
            'first' => 
            array (
              'name' => 'first',
              'desc' => 'pdotools_prop_first',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер первой итерации вывода результатов.',
              'area_trans' => '',
            ),
            'last' => 
            array (
              'name' => 'last',
              'desc' => 'pdotools_prop_last',
              'type' => 'numberfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Номер последней итерации вывода результатов. По умолчанию он рассчитается автоматически, по формуле (total + first - 1).',
              'area_trans' => '',
            ),
            'tplFirst' => 
            array (
              'name' => 'tplFirst',
              'desc' => 'pdotools_prop_tplFirst',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для первого ресурса в результатах.',
              'area_trans' => '',
            ),
            'tplLast' => 
            array (
              'name' => 'tplLast',
              'desc' => 'pdotools_prop_tplLast',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для последнего ресурса в результатах.',
              'area_trans' => '',
            ),
            'tplOdd' => 
            array (
              'name' => 'tplOdd',
              'desc' => 'pdotools_prop_tplOdd',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя чанка для каждого второго ресурса.',
              'area_trans' => '',
            ),
            'tplWrapper' => 
            array (
              'name' => 'tplWrapper',
              'desc' => 'pdotools_prop_tplWrapper',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Чанк-обёртка, для заворачивания всех результатов. Понимает один плейсхолдер: [[+output]]. Не работает вместе с параметром "toSeparatePlaceholders".',
              'area_trans' => '',
            ),
            'wrapIfEmpty' => 
            array (
              'name' => 'wrapIfEmpty',
              'desc' => 'pdotools_prop_wrapIfEmpty',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Включает вывод чанка-обертки (tplWrapper) даже если результатов нет.',
              'area_trans' => '',
            ),
            'totalVar' => 
            array (
              'name' => 'totalVar',
              'desc' => 'pdotools_prop_totalVar',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => 'total',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Имя плейсхолдера для сохранения общего количества результатов.',
              'area_trans' => '',
            ),
            'resources' => 
            array (
              'name' => 'resources',
              'desc' => 'pdotools_prop_resources',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список ресурсов, через запятую, для вывода в результатах. Если id ресурса начинается с дефиса, этот ресурс исключается из выборки.',
              'area_trans' => '',
            ),
            'tplCondition' => 
            array (
              'name' => 'tplCondition',
              'desc' => 'pdotools_prop_tplCondition',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Поле ресурса, из которого будет получено значение для выбора чанка по условию в "conditionalTpls".',
              'area_trans' => '',
            ),
            'tplOperator' => 
            array (
              'name' => 'tplOperator',
              'desc' => 'pdotools_prop_tplOperator',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'text' => 'is equal to',
                  'value' => '==',
                  'name' => 'is equal to',
                ),
                1 => 
                array (
                  'text' => 'is not equal to',
                  'value' => '!=',
                  'name' => 'is not equal to',
                ),
                2 => 
                array (
                  'text' => 'less than',
                  'value' => '<',
                  'name' => 'less than',
                ),
                3 => 
                array (
                  'text' => 'less than or equal to',
                  'value' => '<=',
                  'name' => 'less than or equal to',
                ),
                4 => 
                array (
                  'text' => 'greater than or equal to',
                  'value' => '>=',
                  'name' => 'greater than or equal to',
                ),
                5 => 
                array (
                  'text' => 'is empty',
                  'value' => 'empty',
                  'name' => 'is empty',
                ),
                6 => 
                array (
                  'text' => 'is not empty',
                  'value' => '!empty',
                  'name' => 'is not empty',
                ),
                7 => 
                array (
                  'text' => 'is null',
                  'value' => 'null',
                  'name' => 'is null',
                ),
                8 => 
                array (
                  'text' => 'is in array',
                  'value' => 'inarray',
                  'name' => 'is in array',
                ),
                9 => 
                array (
                  'text' => 'is between',
                  'value' => 'between',
                  'name' => 'is between',
                ),
              ),
              'value' => '==',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Необязательный оператор для проведения сравнения поля ресурса в "tplCondition" с массивом значений и чанков в "conditionalTpls".',
              'area_trans' => '',
            ),
            'conditionalTpls' => 
            array (
              'name' => 'conditionalTpls',
              'desc' => 'pdotools_prop_conditionalTpls',
              'type' => 'textarea',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'JSON строка с массивом, у которого в ключах указано то, с чем будет сравниваться "tplCondition", а в значениях - чанки, которые будут использованы для вывода, если сравнение будет успешно. Оператор сравнения указывается в "tplOperator". Для операторов типа "isempty" можно использовать массив без ключей.',
              'area_trans' => '',
            ),
            'select' => 
            array (
              'name' => 'select',
              'desc' => 'pdotools_prop_select',
              'type' => 'textarea',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список полей для выборки, через запятую. Можно указывать JSON строку с массивом, например {"modResource":"id,pagetitle,content"}.',
              'area_trans' => '',
            ),
            'toSeparatePlaceholders' => 
            array (
              'name' => 'toSeparatePlaceholders',
              'desc' => 'pdotools_prop_toSeparatePlaceholders',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Если вы укажете слово в этом параметре, то ВСЕ результаты будут выставлены в разные плейсхолдеры, начинающиеся с этого слова и заканчивающиеся порядковым номером строки, от нуля. Например, указав в параметре "myPl", вы получите плейсхолдеры [[+myPl0]], [[+myPl1]] и т.д.',
              'area_trans' => '',
            ),
            'loadModels' => 
            array (
              'name' => 'loadModels',
              'desc' => 'pdotools_prop_loadModels',
              'type' => 'textfield',
              'options' => 
              array (
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Список компонентов, через запятую, чьи модели нужно загрузить для построения запроса. Например: "&loadModels=`ms2gallery,msearch2`".',
              'area_trans' => '',
            ),
            'scheme' => 
            array (
              'name' => 'scheme',
              'desc' => 'pdotools_prop_scheme',
              'type' => 'list',
              'options' => 
              array (
                0 => 
                array (
                  'value' => '',
                  'text' => 'System default',
                  'name' => 'System default',
                ),
                1 => 
                array (
                  'value' => -1,
                  'text' => '-1 (relative to site_url)',
                  'name' => '-1 (relative to site_url)',
                ),
                2 => 
                array (
                  'value' => 'full',
                  'text' => 'full (absolute, prepended with site_url)',
                  'name' => 'full (absolute, prepended with site_url)',
                ),
                3 => 
                array (
                  'value' => 'abs',
                  'text' => 'abs (absolute, prepended with base_url)',
                  'name' => 'abs (absolute, prepended with base_url)',
                ),
                4 => 
                array (
                  'value' => 'http',
                  'text' => 'http (absolute, forced to http scheme)',
                  'name' => 'http (absolute, forced to http scheme)',
                ),
                5 => 
                array (
                  'value' => 'https',
                  'text' => 'https (absolute, forced to https scheme)',
                  'name' => 'https (absolute, forced to https scheme)',
                ),
              ),
              'value' => '',
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Схема формирования ссылок: "uri" для подстановки поля uri документа (очень быстро) или параметр для modX::makeUrl().',
              'area_trans' => '',
            ),
            'useWeblinkUrl' => 
            array (
              'name' => 'useWeblinkUrl',
              'desc' => 'pdotools_prop_useWeblinkUrl',
              'type' => 'combo-boolean',
              'options' => 
              array (
              ),
              'value' => false,
              'lexicon' => 'pdotools:properties',
              'area' => '',
              'desc_trans' => 'Генерировать ссылку с учетом класса ресурса.',
              'area_trans' => '',
            ),
          ),
          'moduleguid' => '',
          'static' => false,
          'static_file' => 'core/components/pdotools/elements/snippets/snippet.pdoresources.php',
          'content' => '/** @var array $scriptProperties */
if (isset($parents) && $parents === \'\') {
    $scriptProperties[\'parents\'] = $modx->resource->id;
}
if (!empty($returnIds)) {
    $scriptProperties[\'return\'] = \'ids\';
}

// Adding extra parameters into special place so we can put them in a results
/** @var modSnippet $snippet */
$additionalPlaceholders = $properties = array();
if (isset($this) && $this instanceof modSnippet) {
    $properties = $this->get(\'properties\');
}
elseif ($snippet = $modx->getObject(\'modSnippet\', array(\'name\' => \'pdoResources\'))) {
    $properties = $snippet->get(\'properties\');
}
if (!empty($properties)) {
    foreach ($scriptProperties as $k => $v) {
        if (!isset($properties[$k])) {
            $additionalPlaceholders[$k] = $v;
        }
    }
}
$scriptProperties[\'additionalPlaceholders\'] = $additionalPlaceholders;

/** @var pdoFetch $pdoFetch */
$fqn = $modx->getOption(\'pdoFetch.class\', null, \'pdotools.pdofetch\', true);
$path = $modx->getOption(\'pdofetch_class_path\', null, MODX_CORE_PATH . \'components/pdotools/model/\', true);
if ($pdoClass = $modx->loadClass($fqn, $path, false, true)) {
    $pdoFetch = new $pdoClass($modx, $scriptProperties);
} else {
    return false;
}
$pdoFetch->addTime(\'pdoTools loaded\');
$output = $pdoFetch->run();

$log = \'\';
if ($modx->user->hasSessionContext(\'mgr\') && !empty($showLog)) {
    $log .= \'<pre class="pdoResourcesLog">\' . print_r($pdoFetch->getTime(), 1) . \'</pre>\';
}

// Return output
if (!empty($returnIds)) {
    $modx->setPlaceholder(\'pdoResources.log\', $log);
    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
} elseif (!empty($toSeparatePlaceholders)) {
    $output[\'log\'] = $log;
    $modx->setPlaceholders($output, $toSeparatePlaceholders);
} else {
    $output .= $log;

    if (!empty($tplWrapper) && (!empty($wrapIfEmpty) || !empty($output))) {
        $output = $pdoFetch->getChunk($tplWrapper, array_merge($additionalPlaceholders, array(\'output\' => $output)),
            $pdoFetch->config[\'fastMode\']);
    }

    if (!empty($toPlaceholder)) {
        $modx->setPlaceholder($toPlaceholder, $output);
    } else {
        return $output;
    }
}',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
    'modTemplateVar' => 
    array (
      'titl' => 
      array (
        'fields' => 
        array (
          'id' => 22,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'titl',
          'caption' => 'Мета тайтл',
          'description' => '',
          'editor_type' => 0,
          'category' => 20,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '[[*pagetitle]] - [[++site_name]]',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '[[*pagetitle]] - [[++site_name]]',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'logo' => 
      array (
        'fields' => 
        array (
          'id' => 2,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'logo',
          'caption' => 'Логотип',
          'description' => '',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell' => 
      array (
        'fields' => 
        array (
          'id' => 4,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell',
          'caption' => 'Начало номера',
          'description' => 'Первые 5 цифр номера',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 3,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'tell2' => 
      array (
        'fields' => 
        array (
          'id' => 5,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'tell2',
          'caption' => 'Остальные цыфры',
          'description' => 'остальные цифры',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 4,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'addres' => 
      array (
        'fields' => 
        array (
          'id' => 3,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'addres',
          'caption' => 'Адрес',
          'description' => '',
          'editor_type' => 0,
          'category' => 13,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'image' => 
      array (
        'fields' => 
        array (
          'id' => 25,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'image',
          'name' => 'image',
          'caption' => 'Изображение',
          'description' => '',
          'editor_type' => 0,
          'category' => 22,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'special' => 
      array (
        'fields' => 
        array (
          'id' => 30,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'special',
          'caption' => 'Специальность',
          'description' => '',
          'editor_type' => 0,
          'category' => 26,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'obrazovanie' => 
      array (
        'fields' => 
        array (
          'id' => 31,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'text',
          'name' => 'obrazovanie',
          'caption' => 'Образование',
          'description' => '',
          'editor_type' => 0,
          'category' => 26,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'minLength' => '',
            'maxLength' => '',
            'regex' => '',
            'regexText' => '',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'staj_diplomi' => 
      array (
        'fields' => 
        array (
          'id' => 32,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'migx',
          'name' => 'staj_diplomi',
          'caption' => 'Стаж  работы + Дипломы',
          'description' => '',
          'editor_type' => 0,
          'category' => 26,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'configs' => '',
            'formtabs' => '[{"caption":"Стаж + Диплом", "fields": [
{"field":"image","caption":"Иконка","inputTVtype":"image"},
{"field":"title","caption":"Заголовок","inputTVtype":"text"}
]
}]',
            'columns' => '[{"header": "Иконка", "sortable": "false", "dataIndex":"image","renderer": "this.renderImage"
},{"header": "Заголовок", "sortable": "false", "dataIndex":"title"}]',
            'btntext' => 'Добавить стаж(диплом)',
            'previewurl' => '',
            'jsonvarkey' => '',
            'autoResourceFolders' => 'false',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'okaz_uslugi' => 
      array (
        'fields' => 
        array (
          'id' => 33,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'migx',
          'name' => 'okaz_uslugi',
          'caption' => 'Оказываемые услуги',
          'description' => '',
          'editor_type' => 0,
          'category' => 26,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'configs' => '',
            'formtabs' => '[{"caption":"Оказываемые услуги", "fields": [
{"field":"image","caption":"Иконка","inputTVtype":"image"},
{"field":"title","caption":"Заголовок","inputTVtype":"text"},
{"field":"url","caption":"Ссылка","inputTVtype":"text"}
]
}]',
            'columns' => '[{"header": "Иконка", "sortable": "false", "dataIndex":"image","renderer": "this.renderImage"
},{"header": "Заголовок", "sortable": "false", "dataIndex":"title"},
{"header": "Ссылка", "sortable": "false", "dataIndex":"url"
}]',
            'btntext' => 'добавить услугу',
            'previewurl' => '',
            'jsonvarkey' => '',
            'autoResourceFolders' => 'false',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'activnost' => 
      array (
        'fields' => 
        array (
          'id' => 35,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'migx',
          'name' => 'activnost',
          'caption' => 'Профессиональная активность',
          'description' => '',
          'editor_type' => 0,
          'category' => 26,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'configs' => '',
            'formtabs' => '[{"caption":"Профессиональная активность", "fields": [
{"field":"year","caption":"Год","inputTVtype":"text"},
{"field":"desc","caption":"Описание","inputTVtype":"textarea"},
{"field":"city","caption":"Место(город)","inputTVtype":"text"},
{"field":"data","caption":"Время(дата) проведения","inputTVtype":"text"}
]
}]',
            'columns' => '[{"header": "Год", "sortable": "false", "dataIndex":"year"},
{"header": "Описание", "sortable": "false", "dataIndex":"desc"},
{"header": "Место(город)", "sortable": "false", "dataIndex":"city"},
{"header": "Время(дата) проведения", "sortable": "false", "dataIndex":"data"}]',
            'btntext' => 'добавить активность',
            'previewurl' => '',
            'jsonvarkey' => '',
            'autoResourceFolders' => 'false',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'dimlom_sertivifikat' => 
      array (
        'fields' => 
        array (
          'id' => 38,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'migx',
          'name' => 'dimlom_sertivifikat',
          'caption' => 'Дипломы и Сертификаты',
          'description' => '',
          'editor_type' => 0,
          'category' => 26,
          'locked' => false,
          'elements' => '',
          'rank' => 0,
          'display' => 'default',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'configs' => '',
            'formtabs' => '[{"caption":"Дипломы и сертификаты", "fields": [
{"field":"image","caption":"Изображение","inputTVtype":"image"}
]
}]',
            'columns' => '[{"header": "Изображение", "sortable": "false", "dataIndex":"image","renderer": "this.renderImage"
}]',
            'btntext' => 'добавить диплом(сертификат)',
            'previewurl' => '',
            'jsonvarkey' => '',
            'autoResourceFolders' => 'false',
          ),
          'output_properties' => 
          array (
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
      'reviews_more' => 
      array (
        'fields' => 
        array (
          'id' => 115,
          'source' => 1,
          'property_preprocess' => false,
          'type' => 'listbox-multiple',
          'name' => 'reviews_more',
          'caption' => 'Отзывы',
          'description' => '',
          'editor_type' => 0,
          'category' => 26,
          'locked' => false,
          'elements' => '@SELECT pagetitle, id FROM modx_site_content WHERE parent=22',
          'rank' => 0,
          'display' => 'delim',
          'default_text' => '',
          'properties' => 
          array (
          ),
          'input_properties' => 
          array (
            'allowBlank' => 'true',
            'listWidth' => '',
            'title' => '',
            'typeAhead' => 'false',
            'typeAheadDelay' => '250',
            'listEmptyText' => '',
            'stackItems' => 'false',
          ),
          'output_properties' => 
          array (
            'delimiter' => ',',
          ),
          'static' => false,
          'static_file' => '',
          'content' => '',
        ),
        'policies' => 
        array (
          'web' => 
          array (
          ),
        ),
        'source' => 
        array (
          'id' => 1,
          'name' => 'Filesystem',
          'description' => '',
          'class_key' => 'sources.modFileMediaSource',
          'properties' => 
          array (
          ),
          'is_stream' => true,
        ),
      ),
    ),
  ),
);